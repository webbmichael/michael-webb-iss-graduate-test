// import data from './dev_test.json' assert {type: 'json'}



const barForm = document.getElementById("barForm");
const pieForm = document.getElementById("pieForm");
let chartForm = document.chartSelect.chartSelect;
var prev = null;

for (var i = 0; i < chartForm.length; i++) {
    chartForm[i].addEventListener('change', function() {
        (prev) ? console.log(prev.value): null;
        if (this !== prev) {
            prev = this;
        }
        console.log(this.value)

        if(this.value == 'pie'){
            barForm.style.display="none";
            pieForm.style.display="flex";
        }
        if(this.value == 'bar'){
            pieForm.style.display="none";
            barForm.style.display="flex";
        }
    });
}
barForm.addEventListener('submit', e =>{
    e.preventDefault();
    let barData =  data.filter(event =>{
        return (
            e.target.uSBar.checked && event.fund == e.target.uSBar.value || 
            e.target.uKEUBar.checked && event.fund == e.target.uKEUBar.value ||
            e.target.restOfWorldBar.checked && event.fund == e.target.restOfWorldBar.value

        )
    })
    barData = barData.filter(event =>{
        return (
            e.target.apple.checked && event.ifa == e.target.apple.value || 
            e.target.pear.checked && event.ifa == e.target.pear.value ||
            e.target.orange.checked && event.ifa == e.target.orange.value
        )
    })

    // create strings for title
    let countryString = ''
    let countryArray =[]
    if(e.target.uSBar.checked){
        countryArray.push("US")
    };
    if(e.target.uKEUBar.checked){
        countryArray.push("UK & Europe")
    };
    if(e.target.restOfWorldBar.checked){
        countryArray.push("Rest of world")
    };
    countryArray.forEach((country,i) =>{
        if(i === (countryArray.length-1) && countryArray.length!==1){
            countryString += ` and ${country}`
        }else if(countryArray.length !== 1 && i!==0){
            countryString += `, ${country}`
        }else{
            countryString += `${country}`
        }
    })
    

    let ifaString = ''
    let ifaArray =[]
    if(e.target.apple.checked){
        ifaArray.push("Apple Financial")
    };
    if(e.target.pear.checked){
        ifaArray.push("Pear Wealth")
    };
    if(e.target.orange.checked){
        ifaArray.push("Orange Investments")
    };
    ifaArray.forEach((ifa,i) =>{
        if(i === (ifaArray.length-1) && ifaArray.length!==1){
            ifaString += ` and ${ifa}`
        }else if(ifaArray.length !== 1 && i!==0){
            ifaString += `, ${ifa}`
        }else{
            ifaString += `${ifa}`
        }
    })


    createBarData(barData,countryString,ifaString)
})

pieForm.addEventListener('submit',e=>{
    e.preventDefault()
    let pieData = data.filter(event =>{
        return (
            e.target.uS.checked && event.fund == e.target.uS.value || 
            e.target.uKEU.checked && event.fund == e.target.uKEU.value ||
            e.target.restOfWorld.checked && event.fund == e.target.restOfWorld.value
        )
    })
    pieData = pieData.filter( event => event.year == e.target.year.value)
    let dataToSubmit = [["ifa","Sales"],["Apple Financial",0],["Pear Wealth",0],["Orange Investments",0]]
    pieData.forEach(data =>{
        if(data.ifa === 'Apple Financial'){
            dataToSubmit[1][1] += Number(data.sales)
        }
        if(data.ifa === 'Pear Wealth'){
            dataToSubmit[2][1] += Number(data.sales)
        }
        if(data.ifa === 'Orange Investments'){
            dataToSubmit[3][1] += Number(data.sales)
        }
    })

    // create strings for title
    let countryString = ''
    let countryArray =[]
    if(e.target.uS.checked){
        countryArray.push("US")
    };
    if(e.target.uKEU.checked){
        countryArray.push("UK & Europe")
    };
    if(e.target.restOfWorld.checked){
        countryArray.push("Rest of world")
    };
    countryArray.forEach((country,i) =>{
        if(i === (countryArray.length-1) && countryArray.length!==1){
            countryString += ` and ${country}`
        }else if(countryArray.length !== 1 && i!==0){
            countryString += `, ${country}`
        }else{
            countryString += `${country}`
        }
    })

    if(e.target.year.value ==="Select a year"){
        window.alert("Select a year")
    }else if(!e.target.uS.checked && !e.target.uKEU.checked && !e.target.restOfWorld.checked ){
        window.alert("Select funds to compare")
    }else{
        createPieChart(dataToSubmit,countryString,e.target.year.value)

    }


})
const pieAllFunds = document.getElementById("all");
pieAllFunds.addEventListener('change',e => {
    const selected = e.target.checked;
    const boxes = document.querySelectorAll('.form-check-input')
    if(selected){
        boxes.forEach(box => box.checked = true)
    }
    if(!selected){
        boxes.forEach(box => box.checked = false)
    }
})
const barAllFunds = document.getElementById("allBar");
barAllFunds.addEventListener('change',e => {
    const selected = e.target.checked;
    const boxes = document.querySelectorAll('.fund')
    if(selected){
        boxes.forEach(box => box.checked = true)
    }
    if(!selected){
        boxes.forEach(box => box.checked = false)
    }
})
const ifaAllFunds = document.getElementById("all-Barf");
ifaAllFunds.addEventListener('change',e => {
    const selected = e.target.checked;
    const boxes = document.querySelectorAll('.ifa')
    if(selected){
        boxes.forEach(box => box.checked = true)
    }
    if(!selected){
        boxes.forEach(box => box.checked = false)
    }
})

// variables to change are ifa and fund
function createBarData(barData,fundString,ifaString){
    let array = [["Year","Sales"]];
   
  
   let dataToSubmit = [["Year","Sales"],["2017",0],["2018",0],["2019",0],["2020",0]]
   console.log(barData)
    barData.forEach(dataBar =>{
        if(dataBar.year === '2017'){
            dataToSubmit[1][1] += Number(dataBar.sales)
        }
        if(dataBar.year === '2018'){
            dataToSubmit[2][1] += Number(dataBar.sales)
        }
        if(dataBar.year === '2019'){
            dataToSubmit[3][1] += Number(dataBar.sales)
        }
        if(dataBar.year === '2020'){
            dataToSubmit[4][1] += Number(dataBar.sales)
        }
    })
    

createBarChart(dataToSubmit,fundString,ifaString)
   
}


