import data from './dev_test.json' assert {type: 'json'}



const barForm = document.getElementById("barForm");
const pieForm = document.getElementById("pieForm");
let chartForm = document.chartSelect.chartSelect;
var prev = null;

for (var i = 0; i < chartForm.length; i++) {
    chartForm[i].addEventListener('change', function() {
        (prev) ? console.log(prev.value): null;
        if (this !== prev) {
            prev = this;
        }
        console.log(this.value)

        if(this.value == 'pie'){
            barForm.style.display="none";
            pieForm.style.display="block";
        }
        if(this.value == 'bar'){
            pieForm.style.display="none";
            barForm.style.display="block";
        }
    });
}
barForm.addEventListener('submit', e =>{
    e.preventDefault();
    const selectedIfa = e.target.company.value
    const selectedFund = e.target.funds.value
    createBarData(selectedIfa, selectedFund)
})

pieForm.addEventListener('submit',e=>{
    e.preventDefault()
    console.log(e.target.uS.checked)
    let pieData = data.filter(event =>{

        return (
            e.target.uS.checked && event.fund == e.target.uS.value || 
            e.target.uKEU.checked && event.fund == e.target.uKEU.value ||
            e.target.restOfWorld.checked && event.fund == e.target.restOfWorld.value
        )
    })
    pieData = pieData.filter( event => event.year == e.target.year.value)
    let dataToSubmit = [["ifa","Sales"],["Apple Financial",0],["Pear Wealth",0],["Orange Investments",0]]
    pieData.forEach(data =>{
        if(data.ifa === 'Apple Financial'){
            dataToSubmit[1][1] += Number(data.sales)
        }
        if(data.ifa === 'Pear Wealth'){
            dataToSubmit[2][1] += Number(data.sales)
        }
        if(data.ifa === 'Orange Investments'){
            dataToSubmit[3][1] += Number(data.sales)
        }
    })

    console.log(dataToSubmit)
    
    
    
    console.log(pieData)

    createPieChart(dataToSubmit)

})
const pieAllFunds = document.getElementById("all");
pieAllFunds.addEventListener('change',e => {
    const selected = e.target.checked;
    const boxes = document.querySelectorAll('.form-check-input')
    if(selected){
        boxes.forEach(box => box.checked = true)
    }
    if(!selected){
        boxes.forEach(box => box.checked = false)
    }
})

// variables to change are ifa and fund
function createBarData(ifa, funds){
    let array = [["Year","Sales"]];
    let ifaData = data.filter(e => e.fund === funds && e.ifa === ifa)
    console.log(ifaData)
    console.log()

    ifaData.forEach(e => {
       let x = [e.year,Number(e.sales)]
       array.push(x)
   })
   createBarChart(array)
}


