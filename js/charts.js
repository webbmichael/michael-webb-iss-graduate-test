function hi(){
    console.log("hello world")
}
function chartData(data) {
    return{
        type:"default",
        key:"csv",
        data: data
    }
}

var barSettings = {
    scales: {
      x: {
        data: {
          extract: {
            key: 'csv',
            field: 'Year'
          }
        },
        padding: 0.2
      },
      color: {
        data: {
          extract: {
            key: 'csv',
            field: 'Year'
          }
        },
        type: 'color',
        padding: 0.2
      },
      y: {
        data: {
          key: 'csv',
          fields: ['Sales']
        },
        include: [0],
        invert: true
      },
    },
    components: [{
      type: 'box-marker',
      data: {
        extract: {
          key: 'csv',
          field: 'Year',
          props: {
            start: 0,
            end: {
              field: 'Sales'
            }
          }
        }
      },
      settings: {
        major: {
          scale: 'x'
        },
        minor: {
          scale: 'y'
        },
        box: {
          fill: { scale: 'color' },
          strokeWidth: 0
        }
      }
    }, {
      type: 'axis',
      dock: 'left',
      scale: 'y'
    }, {
      type: 'axis',
      dock: 'bottom',
      scale: 'x'
    }, {
      type: 'legend-cat',
      scale: 'color',
      dock: 'top'
    }]
  };

function createBarChart(data){
    picasso.chart({
        element: document.getElementById('container'),
        settings: barSettings,
        data: chartData(data)
    });
}


function chartPieData(data){
    return{
        type:"matrix",
        data: data
    }
}

var pieSettings = {
    scales: {
      c: {
        data: { extract: { field: 'ifa' } }, type: 'color'
      }
    },
    components: [{
      type: 'legend-cat',
      scale: 'c'
    },{
      key: 'p',
      type: 'pie',
      data: {
        extract: {
          field: 'ifa',
          props: {
            num: { field: 'Sales' }
          }
        }
      },
      settings: {
        slice: {
          arc: { ref: 'num' },
          fill: { scale: 'c' },
          outerRadius: () => 0.9,
          strokeWidth: 1,
          stroke: 'rgba(255, 255, 255, 0.5)'
        }
      }
    }]
  }

  function createPieChart(data){
    picasso.chart({
        element: document.querySelector('#container'),
        settings: pieSettings,
        data: chartPieData(data)
    });
  }