/**
 * Minified by jsDelivr using Terser v5.10.0.
 * Original file: /npm/picasso.js@1.6.0/dist/picasso.js
 *
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
!(function (t, e) {
  "object" == typeof exports && "undefined" != typeof module
    ? (module.exports = e())
    : "function" == typeof define && define.amd
    ? define(e)
    : ((t = "undefined" != typeof globalThis ? globalThis : t || self).picasso =
        e());
})(this, function () {
  "use strict";
  var t = { a: 7, c: 6, h: 1, l: 2, m: 2, q: 4, s: 4, t: 2, v: 1, z: 0 },
    e = /([astvzqmhlc])([^astvzqmhlc]*)/gi,
    n = /-?[0-9]*\.?[0-9]+(?:e[-+]?\d+)?/gi;
  var i = function (i) {
    var r = [],
      a = String(i).trim();
    return (
      ("M" !== a[0] && "m" !== a[0]) ||
        a.replace(e, function (e, i, a) {
          var o = i.toLowerCase(),
            s = (function (t) {
              var e = t.match(n);
              return e ? e.map(Number) : [];
            })(a),
            c = i;
          if (
            ("m" === o &&
              s.length > 2 &&
              (r.push([c].concat(s.splice(0, 2))),
              (o = "l"),
              (c = "m" === c ? "l" : "L")),
            s.length < t[o])
          )
            return "";
          for (
            r.push([c].concat(s.splice(0, t[o])));
            s.length >= t[o] && s.length && t[o];

          )
            r.push([c].concat(s.splice(0, t[o])));
          return "";
        }),
      r
    );
  };
  function r(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];
      (i.enumerable = i.enumerable || !1),
        (i.configurable = !0),
        "value" in i && (i.writable = !0),
        Object.defineProperty(t, i.key, i);
    }
  }
  function a(t) {
    return (
      (function (t) {
        if (Array.isArray(t)) return o(t);
      })(t) ||
      (function (t) {
        if (
          ("undefined" != typeof Symbol && null != t[Symbol.iterator]) ||
          null != t["@@iterator"]
        )
          return Array.from(t);
      })(t) ||
      (function (t, e) {
        if (!t) return;
        if ("string" == typeof t) return o(t, e);
        var n = Object.prototype.toString.call(t).slice(8, -1);
        "Object" === n && t.constructor && (n = t.constructor.name);
        if ("Map" === n || "Set" === n) return Array.from(t);
        if (
          "Arguments" === n ||
          /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
        )
          return o(t, e);
      })(t) ||
      (function () {
        throw new TypeError(
          "Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      })()
    );
  }
  function o(t, e) {
    (null == e || e > t.length) && (e = t.length);
    for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
    return i;
  }
  var s = i;
  function c(t, e) {
    var n = t.x * Math.cos(e) - t.y * Math.sin(e),
      i = t.y * Math.cos(e) + t.x * Math.sin(e);
    (t.x = n), (t.y = i);
  }
  function u(t, e, n) {
    (t.x += e), (t.y += n);
  }
  function l(t, e) {
    (t.x *= e), (t.y *= e);
  }
  var h = function (t) {
      if (
        void 0 !== t &&
        t.CanvasRenderingContext2D &&
        (!t.Path2D ||
          !(function (t) {
            var e = t.document.createElement("canvas").getContext("2d"),
              n = new t.Path2D("M0 0 L1 1");
            return (
              (e.strokeStyle = "red"),
              (e.lineWidth = 1),
              e.stroke(n),
              255 === e.getImageData(0, 0, 1, 1).data[0]
            );
          })(t))
      ) {
        var e = (function () {
            function t(e) {
              var n;
              ((function (t, e) {
                if (!(t instanceof e))
                  throw new TypeError("Cannot call a class as a function");
              })(this, t),
              (this.segments = []),
              e && e instanceof t)
                ? (n = this.segments).push.apply(n, a(e.segments))
                : e && (this.segments = s(e));
            }
            var e, n, i;
            return (
              (e = t),
              (n = [
                {
                  key: "addPath",
                  value: function (e) {
                    var n;
                    e &&
                      e instanceof t &&
                      (n = this.segments).push.apply(n, a(e.segments));
                  },
                },
                {
                  key: "moveTo",
                  value: function (t, e) {
                    this.segments.push(["M", t, e]);
                  },
                },
                {
                  key: "lineTo",
                  value: function (t, e) {
                    this.segments.push(["L", t, e]);
                  },
                },
                {
                  key: "arc",
                  value: function (t, e, n, i, r, a) {
                    this.segments.push(["AC", t, e, n, i, r, !!a]);
                  },
                },
                {
                  key: "arcTo",
                  value: function (t, e, n, i, r) {
                    this.segments.push(["AT", t, e, n, i, r]);
                  },
                },
                {
                  key: "ellipse",
                  value: function (t, e, n, i, r, a, o, s) {
                    this.segments.push(["E", t, e, n, i, r, a, o, !!s]);
                  },
                },
                {
                  key: "closePath",
                  value: function () {
                    this.segments.push(["Z"]);
                  },
                },
                {
                  key: "bezierCurveTo",
                  value: function (t, e, n, i, r, a) {
                    this.segments.push(["C", t, e, n, i, r, a]);
                  },
                },
                {
                  key: "quadraticCurveTo",
                  value: function (t, e, n, i) {
                    this.segments.push(["Q", t, e, n, i]);
                  },
                },
                {
                  key: "rect",
                  value: function (t, e, n, i) {
                    this.segments.push(["R", t, e, n, i]);
                  },
                },
              ]),
              n && r(e.prototype, n),
              i && r(e, i),
              t
            );
          })(),
          n = t.CanvasRenderingContext2D.prototype.fill,
          i = t.CanvasRenderingContext2D.prototype.stroke;
        (t.CanvasRenderingContext2D.prototype.fill = function () {
          for (var t = arguments.length, e = new Array(t), i = 0; i < t; i++)
            e[i] = arguments[i];
          var r = "nonzero";
          if (0 === e.length || (1 === e.length && "string" == typeof e[0]))
            n.apply(this, e);
          else {
            2 === arguments.length && (r = e[1]);
            var a = e[0];
            h(this, a.segments), n.call(this, r);
          }
        }),
          (t.CanvasRenderingContext2D.prototype.stroke = function (t) {
            t ? (h(this, t.segments), i.call(this)) : i.call(this);
          });
        var o = t.CanvasRenderingContext2D.prototype.isPointInPath;
        (t.CanvasRenderingContext2D.prototype.isPointInPath = function () {
          for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
            e[n] = arguments[n];
          if ("Path2D" === e[0].constructor.name) {
            var i = e[1],
              r = e[2],
              a = e[3] || "nonzero",
              s = e[0];
            return h(this, s.segments), o.apply(this, [i, r, a]);
          }
          return o.apply(this, e);
        }),
          (t.Path2D = e);
      }
      function h(t, e) {
        var n,
          i,
          r,
          a,
          o,
          s,
          h,
          f,
          d,
          g,
          p,
          y,
          v,
          m,
          x,
          b,
          _,
          w,
          k,
          M,
          R,
          S,
          N,
          z,
          C,
          A,
          E = { x: 0, y: 0 },
          O = { x: 0, y: 0 };
        t.beginPath();
        for (var T = 0; T < e.length; ++T) {
          var j = e[T];
          switch (
            ("S" !== (M = j[0]) &&
              "s" !== M &&
              "C" !== M &&
              "c" !== M &&
              ((S = null), (N = null)),
            "T" !== M &&
              "t" !== M &&
              "Q" !== M &&
              "q" !== M &&
              ((z = null), (C = null)),
            M)
          ) {
            case "m":
            case "M":
              "m" === M ? ((p += j[1]), (v += j[2])) : ((p = j[1]), (v = j[2])),
                ("M" !== M && E) || (E = { x: p, y: v }),
                t.moveTo(p, v);
              break;
            case "l":
              (p += j[1]), (v += j[2]), t.lineTo(p, v);
              break;
            case "L":
              (p = j[1]), (v = j[2]), t.lineTo(p, v);
              break;
            case "H":
              (p = j[1]), t.lineTo(p, v);
              break;
            case "h":
              (p += j[1]), t.lineTo(p, v);
              break;
            case "V":
              (v = j[1]), t.lineTo(p, v);
              break;
            case "v":
              (v += j[1]), t.lineTo(p, v);
              break;
            case "a":
            case "A":
              "a" === M ? ((p += j[6]), (v += j[7])) : ((p = j[6]), (v = j[7])),
                (b = j[1]),
                (_ = j[2]),
                (h = (j[3] * Math.PI) / 180),
                (r = !!j[4]),
                (a = !!j[5]),
                (o = { x: p, y: v }),
                c((s = { x: (O.x - o.x) / 2, y: (O.y - o.y) / 2 }), -h),
                (f = (s.x * s.x) / (b * b) + (s.y * s.y) / (_ * _)) > 1 &&
                  ((b *= f = Math.sqrt(f)), (_ *= f)),
                (d = b * b * _ * _),
                (g = b * b * s.y * s.y + _ * _ * s.x * s.x),
                l(
                  (R = { x: (b * s.y) / _, y: (-_ * s.x) / b }),
                  a !== r
                    ? Math.sqrt((d - g) / g) || 0
                    : -Math.sqrt((d - g) / g) || 0
                ),
                (i = Math.atan2((s.y - R.y) / _, (s.x - R.x) / b)),
                (n = Math.atan2(-(s.y + R.y) / _, -(s.x + R.x) / b)),
                c(R, h),
                u(R, (o.x + O.x) / 2, (o.y + O.y) / 2),
                t.save(),
                t.translate(R.x, R.y),
                t.rotate(h),
                t.scale(b, _),
                t.arc(0, 0, 1, i, n, !a),
                t.restore();
              break;
            case "C":
              (S = j[3]),
                (N = j[4]),
                (p = j[5]),
                (v = j[6]),
                t.bezierCurveTo(j[1], j[2], S, N, p, v);
              break;
            case "c":
              t.bezierCurveTo(
                j[1] + p,
                j[2] + v,
                j[3] + p,
                j[4] + v,
                j[5] + p,
                j[6] + v
              ),
                (S = j[3] + p),
                (N = j[4] + v),
                (p += j[5]),
                (v += j[6]);
              break;
            case "S":
              (null !== S && null !== N) || ((S = p), (N = v)),
                t.bezierCurveTo(2 * p - S, 2 * v - N, j[1], j[2], j[3], j[4]),
                (S = j[1]),
                (N = j[2]),
                (p = j[3]),
                (v = j[4]);
              break;
            case "s":
              (null !== S && null !== N) || ((S = p), (N = v)),
                t.bezierCurveTo(
                  2 * p - S,
                  2 * v - N,
                  j[1] + p,
                  j[2] + v,
                  j[3] + p,
                  j[4] + v
                ),
                (S = j[1] + p),
                (N = j[2] + v),
                (p += j[3]),
                (v += j[4]);
              break;
            case "Q":
              (z = j[1]),
                (C = j[2]),
                (p = j[3]),
                (v = j[4]),
                t.quadraticCurveTo(z, C, p, v);
              break;
            case "q":
              (z = j[1] + p),
                (C = j[2] + v),
                (p += j[3]),
                (v += j[4]),
                t.quadraticCurveTo(z, C, p, v);
              break;
            case "T":
              (null !== z && null !== C) || ((z = p), (C = v)),
                (z = 2 * p - z),
                (C = 2 * v - C),
                (p = j[1]),
                (v = j[2]),
                t.quadraticCurveTo(z, C, p, v);
              break;
            case "t":
              (null !== z && null !== C) || ((z = p), (C = v)),
                (z = 2 * p - z),
                (C = 2 * v - C),
                (p += j[1]),
                (v += j[2]),
                t.quadraticCurveTo(z, C, p, v);
              break;
            case "z":
            case "Z":
              (p = E.x), (v = E.y), (E = void 0), t.closePath();
              break;
            case "AC":
              (p = j[1]),
                (v = j[2]),
                (x = j[3]),
                (i = j[4]),
                (n = j[5]),
                (A = j[6]),
                t.arc(p, v, x, i, n, A);
              break;
            case "AT":
              (y = j[1]),
                (m = j[2]),
                (p = j[3]),
                (v = j[4]),
                (x = j[5]),
                t.arcTo(y, m, p, v, x);
              break;
            case "E":
              (p = j[1]),
                (v = j[2]),
                (b = j[3]),
                (_ = j[4]),
                (h = j[5]),
                (i = j[6]),
                (n = j[7]),
                (A = j[8]),
                t.save(),
                t.translate(p, v),
                t.rotate(h),
                t.scale(b, _),
                t.arc(0, 0, 1, i, n, A),
                t.restore();
              break;
            case "R":
              (p = j[1]),
                (v = j[2]),
                (w = j[3]),
                (k = j[4]),
                (E = { x: p, y: v }),
                t.rect(p, v, w, k);
          }
          (O.x = p), (O.y = v);
        }
      }
    },
    f = i,
    d = h;
  "undefined" != typeof window && d(window);
  var g = { path2dPolyfill: d, parsePath: f },
    p = Object.prototype.hasOwnProperty,
    y = Object.prototype.toString,
    v = Object.defineProperty,
    m = Object.getOwnPropertyDescriptor,
    x = function (t) {
      return "function" == typeof Array.isArray
        ? Array.isArray(t)
        : "[object Array]" === y.call(t);
    },
    b = function (t) {
      if (!t || "[object Object]" !== y.call(t)) return !1;
      var e,
        n = p.call(t, "constructor"),
        i =
          t.constructor &&
          t.constructor.prototype &&
          p.call(t.constructor.prototype, "isPrototypeOf");
      if (t.constructor && !n && !i) return !1;
      for (e in t);
      return void 0 === e || p.call(t, e);
    },
    _ = function (t, e) {
      v && "__proto__" === e.name
        ? v(t, e.name, {
            enumerable: !0,
            configurable: !0,
            value: e.newValue,
            writable: !0,
          })
        : (t[e.name] = e.newValue);
    },
    w = function (t, e) {
      if ("__proto__" === e) {
        if (!p.call(t, e)) return;
        if (m) return m(t, e).value;
      }
      return t[e];
    },
    k = function t() {
      var e,
        n,
        i,
        r,
        a,
        o,
        s = arguments[0],
        c = 1,
        u = arguments.length,
        l = !1;
      for (
        "boolean" == typeof s && ((l = s), (s = arguments[1] || {}), (c = 2)),
          (null == s || ("object" != typeof s && "function" != typeof s)) &&
            (s = {});
        c < u;
        ++c
      )
        if (null != (e = arguments[c]))
          for (n in e)
            (i = w(s, n)),
              s !== (r = w(e, n)) &&
                (l && r && (b(r) || (a = x(r)))
                  ? (a
                      ? ((a = !1), (o = i && x(i) ? i : []))
                      : (o = i && b(i) ? i : {}),
                    _(s, { name: n, newValue: t(l, o, r) }))
                  : void 0 !== r && _(s, { name: n, newValue: r }));
      return s;
    },
    M = "1.6.0";
  function R(t, e) {
    var n = Object.keys(t);
    if (Object.getOwnPropertySymbols) {
      var i = Object.getOwnPropertySymbols(t);
      e &&
        (i = i.filter(function (e) {
          return Object.getOwnPropertyDescriptor(t, e).enumerable;
        })),
        n.push.apply(n, i);
    }
    return n;
  }
  function S(t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = null != arguments[e] ? arguments[e] : {};
      e % 2
        ? R(Object(n), !0).forEach(function (e) {
            E(t, e, n[e]);
          })
        : Object.getOwnPropertyDescriptors
        ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n))
        : R(Object(n)).forEach(function (e) {
            Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
          });
    }
    return t;
  }
  function N(t) {
    return (
      (N =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            }),
      N(t)
    );
  }
  function z(t, e) {
    if (!(t instanceof e))
      throw new TypeError("Cannot call a class as a function");
  }
  function C(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];
      (i.enumerable = i.enumerable || !1),
        (i.configurable = !0),
        "value" in i && (i.writable = !0),
        Object.defineProperty(t, i.key, i);
    }
  }
  function A(t, e, n) {
    return (
      e && C(t.prototype, e),
      n && C(t, n),
      Object.defineProperty(t, "prototype", { writable: !1 }),
      t
    );
  }
  function E(t, e, n) {
    return (
      e in t
        ? Object.defineProperty(t, e, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0,
          })
        : (t[e] = n),
      t
    );
  }
  function O(t, e) {
    if ("function" != typeof e && null !== e)
      throw new TypeError("Super expression must either be null or a function");
    (t.prototype = Object.create(e && e.prototype, {
      constructor: { value: t, writable: !0, configurable: !0 },
    })),
      Object.defineProperty(t, "prototype", { writable: !1 }),
      e && j(t, e);
  }
  function T(t) {
    return (
      (T = Object.setPrototypeOf
        ? Object.getPrototypeOf
        : function (t) {
            return t.__proto__ || Object.getPrototypeOf(t);
          }),
      T(t)
    );
  }
  function j(t, e) {
    return (
      (j =
        Object.setPrototypeOf ||
        function (t, e) {
          return (t.__proto__ = e), t;
        }),
      j(t, e)
    );
  }
  function P() {
    if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
    if (Reflect.construct.sham) return !1;
    if ("function" == typeof Proxy) return !0;
    try {
      return (
        Boolean.prototype.valueOf.call(
          Reflect.construct(Boolean, [], function () {})
        ),
        !0
      );
    } catch (t) {
      return !1;
    }
  }
  function B(t, e, n) {
    return (
      (B = P()
        ? Reflect.construct
        : function (t, e, n) {
            var i = [null];
            i.push.apply(i, e);
            var r = new (Function.bind.apply(t, i))();
            return n && j(r, n.prototype), r;
          }),
      B.apply(null, arguments)
    );
  }
  function D(t) {
    if (void 0 === t)
      throw new ReferenceError(
        "this hasn't been initialised - super() hasn't been called"
      );
    return t;
  }
  function F(t, e) {
    if (e && ("object" == typeof e || "function" == typeof e)) return e;
    if (void 0 !== e)
      throw new TypeError(
        "Derived constructors may only return object or undefined"
      );
    return D(t);
  }
  function L(t) {
    var e = P();
    return function () {
      var n,
        i = T(t);
      if (e) {
        var r = T(this).constructor;
        n = Reflect.construct(i, arguments, r);
      } else n = i.apply(this, arguments);
      return F(this, n);
    };
  }
  function I(t, e) {
    for (
      ;
      !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = T(t));

    );
    return t;
  }
  function W() {
    return (
      (W =
        "undefined" != typeof Reflect && Reflect.get
          ? Reflect.get
          : function (t, e, n) {
              var i = I(t, e);
              if (i) {
                var r = Object.getOwnPropertyDescriptor(i, e);
                return r.get
                  ? r.get.call(arguments.length < 3 ? t : n)
                  : r.value;
              }
            }),
      W.apply(this, arguments)
    );
  }
  function V(t, e) {
    return (
      (function (t) {
        if (Array.isArray(t)) return t;
      })(t) ||
      (function (t, e) {
        var n =
          null == t
            ? null
            : ("undefined" != typeof Symbol && t[Symbol.iterator]) ||
              t["@@iterator"];
        if (null == n) return;
        var i,
          r,
          a = [],
          o = !0,
          s = !1;
        try {
          for (
            n = n.call(t);
            !(o = (i = n.next()).done) &&
            (a.push(i.value), !e || a.length !== e);
            o = !0
          );
        } catch (t) {
          (s = !0), (r = t);
        } finally {
          try {
            o || null == n.return || n.return();
          } finally {
            if (s) throw r;
          }
        }
        return a;
      })(t, e) ||
      H(t, e) ||
      (function () {
        throw new TypeError(
          "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      })()
    );
  }
  function $(t) {
    return (
      (function (t) {
        if (Array.isArray(t)) return q(t);
      })(t) ||
      (function (t) {
        if (
          ("undefined" != typeof Symbol && null != t[Symbol.iterator]) ||
          null != t["@@iterator"]
        )
          return Array.from(t);
      })(t) ||
      H(t) ||
      (function () {
        throw new TypeError(
          "Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      })()
    );
  }
  function H(t, e) {
    if (t) {
      if ("string" == typeof t) return q(t, e);
      var n = Object.prototype.toString.call(t).slice(8, -1);
      return (
        "Object" === n && t.constructor && (n = t.constructor.name),
        "Map" === n || "Set" === n
          ? Array.from(t)
          : "Arguments" === n ||
            /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
          ? q(t, e)
          : void 0
      );
    }
  }
  function q(t, e) {
    (null == e || e > t.length) && (e = t.length);
    for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
    return i;
  }
  function U(t) {
    return !!t.changedTouches;
  }
  function Y(t) {
    return "number" == typeof t && !isNaN(t);
  }
  function X(t) {
    return "number" != typeof t || isNaN(t);
  }
  function G(t) {
    for (
      var e = t.length, n = NaN, i = NaN, r = NaN, a = NaN, o = 0;
      o < e;
      o++
    )
      (n = isNaN(n) ? t[o].x : Math.min(n, t[o].x)),
        (i = isNaN(i) ? t[o].x : Math.max(i, t[o].x)),
        (r = isNaN(r) ? t[o].y : Math.min(r, t[o].y)),
        (a = isNaN(a) ? t[o].y : Math.max(a, t[o].y));
    return [n, r, i, a];
  }
  function Z(t) {
    return [
      { x: t.x1 || 0, y: t.y1 || 0 },
      { x: t.x2 || 0, y: t.y2 || 0 },
    ];
  }
  function J(t) {
    return [
      { x: t.x, y: t.y },
      { x: t.x + t.width, y: t.y },
      { x: t.x + t.width, y: t.y + t.height },
      { x: t.x, y: t.y + t.height },
    ];
  }
  function K(t) {
    var e = V(G(t), 4),
      n = e[0],
      i = e[1];
    return { x: n, y: i, width: e[2] - n, height: e[3] - i };
  }
  function Q(t) {
    return { x1: t[0].x, y1: t[0].y, x2: t[1].x, y2: t[1].y };
  }
  function tt(t) {
    var e = t || {},
      n = e.x,
      i = e.y,
      r = e.width,
      a = e.height,
      o = e.x1,
      s = e.x2,
      c = e.y1,
      u = e.y2,
      l = e.cx,
      h = e.cy,
      f = e.r,
      d = e.vertices;
    return Y(l) && Y(h) && Y(f)
      ? "circle"
      : Y(o) && Y(s) && Y(c) && Y(u)
      ? "line"
      : Y(n) && Y(i) && Y(r) && Y(a)
      ? "rect"
      : Y(n) && Y(i)
      ? "point"
      : Array.isArray(d)
      ? d.every(function (t) {
          return Array.isArray(t);
        })
        ? "geopolygon"
        : "polygon"
      : null;
  }
  function et(t, e) {
    return { x: e.x - t, y: e.y - t, width: e.width + t, height: e.height + t };
  }
  function nt(t, e) {
    var n = e.types,
      i = e.logger,
      r = {},
      a = [];
    Array.isArray(t)
      ? a.push.apply(a, $(t))
      : (i.warn('Deprecated: "data-source" configuration"'), a.push(t)),
      a.forEach(function (t, e) {
        var a = n(t.type);
        if (a) {
          var o = t.key;
          void 0 === t.key &&
            (i.warn(
              "Missing key for dataset. Using index '".concat(e, "' as key.")
            ),
            (o = e));
          var s = a({ key: o, data: t.data, config: t.config });
          r[o] = s;
        }
      });
    return function (t) {
      return t ? r[t] : r[Object.keys(r)[0]];
    };
  }
  var it = Math.PI,
    rt = 2 * it,
    at = 1e-6,
    ot = rt - at;
  function st() {
    (this._x0 = this._y0 = this._x1 = this._y1 = null), (this._ = "");
  }
  function ct() {
    return new st();
  }
  function ut(t) {
    return function () {
      return t;
    };
  }
  st.prototype = ct.prototype = {
    constructor: st,
    moveTo: function (t, e) {
      this._ +=
        "M" + (this._x0 = this._x1 = +t) + "," + (this._y0 = this._y1 = +e);
    },
    closePath: function () {
      null !== this._x1 &&
        ((this._x1 = this._x0), (this._y1 = this._y0), (this._ += "Z"));
    },
    lineTo: function (t, e) {
      this._ += "L" + (this._x1 = +t) + "," + (this._y1 = +e);
    },
    quadraticCurveTo: function (t, e, n, i) {
      this._ +=
        "Q" + +t + "," + +e + "," + (this._x1 = +n) + "," + (this._y1 = +i);
    },
    bezierCurveTo: function (t, e, n, i, r, a) {
      this._ +=
        "C" +
        +t +
        "," +
        +e +
        "," +
        +n +
        "," +
        +i +
        "," +
        (this._x1 = +r) +
        "," +
        (this._y1 = +a);
    },
    arcTo: function (t, e, n, i, r) {
      (t = +t), (e = +e), (n = +n), (i = +i), (r = +r);
      var a = this._x1,
        o = this._y1,
        s = n - t,
        c = i - e,
        u = a - t,
        l = o - e,
        h = u * u + l * l;
      if (r < 0) throw new Error("negative radius: " + r);
      if (null === this._x1)
        this._ += "M" + (this._x1 = t) + "," + (this._y1 = e);
      else if (h > at)
        if (Math.abs(l * s - c * u) > at && r) {
          var f = n - a,
            d = i - o,
            g = s * s + c * c,
            p = f * f + d * d,
            y = Math.sqrt(g),
            v = Math.sqrt(h),
            m = r * Math.tan((it - Math.acos((g + h - p) / (2 * y * v))) / 2),
            x = m / v,
            b = m / y;
          Math.abs(x - 1) > at &&
            (this._ += "L" + (t + x * u) + "," + (e + x * l)),
            (this._ +=
              "A" +
              r +
              "," +
              r +
              ",0,0," +
              +(l * f > u * d) +
              "," +
              (this._x1 = t + b * s) +
              "," +
              (this._y1 = e + b * c));
        } else this._ += "L" + (this._x1 = t) + "," + (this._y1 = e);
      else;
    },
    arc: function (t, e, n, i, r, a) {
      (t = +t), (e = +e), (a = !!a);
      var o = (n = +n) * Math.cos(i),
        s = n * Math.sin(i),
        c = t + o,
        u = e + s,
        l = 1 ^ a,
        h = a ? i - r : r - i;
      if (n < 0) throw new Error("negative radius: " + n);
      null === this._x1
        ? (this._ += "M" + c + "," + u)
        : (Math.abs(this._x1 - c) > at || Math.abs(this._y1 - u) > at) &&
          (this._ += "L" + c + "," + u),
        n &&
          (h < 0 && (h = (h % rt) + rt),
          h > ot
            ? (this._ +=
                "A" +
                n +
                "," +
                n +
                ",0,1," +
                l +
                "," +
                (t - o) +
                "," +
                (e - s) +
                "A" +
                n +
                "," +
                n +
                ",0,1," +
                l +
                "," +
                (this._x1 = c) +
                "," +
                (this._y1 = u))
            : h > at &&
              (this._ +=
                "A" +
                n +
                "," +
                n +
                ",0," +
                +(h >= it) +
                "," +
                l +
                "," +
                (this._x1 = t + n * Math.cos(r)) +
                "," +
                (this._y1 = e + n * Math.sin(r))));
    },
    rect: function (t, e, n, i) {
      this._ +=
        "M" +
        (this._x0 = this._x1 = +t) +
        "," +
        (this._y0 = this._y1 = +e) +
        "h" +
        +n +
        "v" +
        +i +
        "h" +
        -n +
        "Z";
    },
    toString: function () {
      return this._;
    },
  };
  var lt = Math.abs,
    ht = Math.atan2,
    ft = Math.cos,
    dt = Math.max,
    gt = Math.min,
    pt = Math.sin,
    yt = Math.sqrt,
    vt = 1e-12,
    mt = Math.PI,
    xt = mt / 2,
    bt = 2 * mt;
  function _t(t) {
    return t > 1 ? 0 : t < -1 ? mt : Math.acos(t);
  }
  function wt(t) {
    return t >= 1 ? xt : t <= -1 ? -xt : Math.asin(t);
  }
  function kt(t) {
    return t.innerRadius;
  }
  function Mt(t) {
    return t.outerRadius;
  }
  function Rt(t) {
    return t.startAngle;
  }
  function St(t) {
    return t.endAngle;
  }
  function Nt(t) {
    return t && t.padAngle;
  }
  function zt(t, e, n, i, r, a, o, s) {
    var c = n - t,
      u = i - e,
      l = o - r,
      h = s - a,
      f = h * c - l * u;
    if (!(f * f < vt))
      return [t + (f = (l * (e - a) - h * (t - r)) / f) * c, e + f * u];
  }
  function Ct(t, e, n, i, r, a, o) {
    var s = t - n,
      c = e - i,
      u = (o ? a : -a) / yt(s * s + c * c),
      l = u * c,
      h = -u * s,
      f = t + l,
      d = e + h,
      g = n + l,
      p = i + h,
      y = (f + g) / 2,
      v = (d + p) / 2,
      m = g - f,
      x = p - d,
      b = m * m + x * x,
      _ = r - a,
      w = f * p - g * d,
      k = (x < 0 ? -1 : 1) * yt(dt(0, _ * _ * b - w * w)),
      M = (w * x - m * k) / b,
      R = (-w * m - x * k) / b,
      S = (w * x + m * k) / b,
      N = (-w * m + x * k) / b,
      z = M - y,
      C = R - v,
      A = S - y,
      E = N - v;
    return (
      z * z + C * C > A * A + E * E && ((M = S), (R = N)),
      {
        cx: M,
        cy: R,
        x01: -l,
        y01: -h,
        x11: M * (r / _ - 1),
        y11: R * (r / _ - 1),
      }
    );
  }
  function At(t) {
    this._context = t;
  }
  function Et(t) {
    return new At(t);
  }
  function Ot(t) {
    return t[0];
  }
  function Tt(t) {
    return t[1];
  }
  function jt() {
    var t = Ot,
      e = null,
      n = ut(0),
      i = Tt,
      r = ut(!0),
      a = null,
      o = Et,
      s = null;
    function c(c) {
      var u,
        l,
        h,
        f,
        d,
        g = c.length,
        p = !1,
        y = new Array(g),
        v = new Array(g);
      for (null == a && (s = o((d = ct()))), u = 0; u <= g; ++u) {
        if (!(u < g && r((f = c[u]), u, c)) === p)
          if ((p = !p)) (l = u), s.areaStart(), s.lineStart();
          else {
            for (s.lineEnd(), s.lineStart(), h = u - 1; h >= l; --h)
              s.point(y[h], v[h]);
            s.lineEnd(), s.areaEnd();
          }
        p &&
          ((y[u] = +t(f, u, c)),
          (v[u] = +n(f, u, c)),
          s.point(e ? +e(f, u, c) : y[u], i ? +i(f, u, c) : v[u]));
      }
      if (d) return (s = null), d + "" || null;
    }
    function u() {
      return (function () {
        var t = Ot,
          e = Tt,
          n = ut(!0),
          i = null,
          r = Et,
          a = null;
        function o(o) {
          var s,
            c,
            u,
            l = o.length,
            h = !1;
          for (null == i && (a = r((u = ct()))), s = 0; s <= l; ++s)
            !(s < l && n((c = o[s]), s, o)) === h &&
              ((h = !h) ? a.lineStart() : a.lineEnd()),
              h && a.point(+t(c, s, o), +e(c, s, o));
          if (u) return (a = null), u + "" || null;
        }
        return (
          (o.x = function (e) {
            return arguments.length
              ? ((t = "function" == typeof e ? e : ut(+e)), o)
              : t;
          }),
          (o.y = function (t) {
            return arguments.length
              ? ((e = "function" == typeof t ? t : ut(+t)), o)
              : e;
          }),
          (o.defined = function (t) {
            return arguments.length
              ? ((n = "function" == typeof t ? t : ut(!!t)), o)
              : n;
          }),
          (o.curve = function (t) {
            return arguments.length ? ((r = t), null != i && (a = r(i)), o) : r;
          }),
          (o.context = function (t) {
            return arguments.length
              ? (null == t ? (i = a = null) : (a = r((i = t))), o)
              : i;
          }),
          o
        );
      })()
        .defined(r)
        .curve(o)
        .context(a);
    }
    return (
      (c.x = function (n) {
        return arguments.length
          ? ((t = "function" == typeof n ? n : ut(+n)), (e = null), c)
          : t;
      }),
      (c.x0 = function (e) {
        return arguments.length
          ? ((t = "function" == typeof e ? e : ut(+e)), c)
          : t;
      }),
      (c.x1 = function (t) {
        return arguments.length
          ? ((e = null == t ? null : "function" == typeof t ? t : ut(+t)), c)
          : e;
      }),
      (c.y = function (t) {
        return arguments.length
          ? ((n = "function" == typeof t ? t : ut(+t)), (i = null), c)
          : n;
      }),
      (c.y0 = function (t) {
        return arguments.length
          ? ((n = "function" == typeof t ? t : ut(+t)), c)
          : n;
      }),
      (c.y1 = function (t) {
        return arguments.length
          ? ((i = null == t ? null : "function" == typeof t ? t : ut(+t)), c)
          : i;
      }),
      (c.lineX0 = c.lineY0 =
        function () {
          return u().x(t).y(n);
        }),
      (c.lineY1 = function () {
        return u().x(t).y(i);
      }),
      (c.lineX1 = function () {
        return u().x(e).y(n);
      }),
      (c.defined = function (t) {
        return arguments.length
          ? ((r = "function" == typeof t ? t : ut(!!t)), c)
          : r;
      }),
      (c.curve = function (t) {
        return arguments.length ? ((o = t), null != a && (s = o(a)), c) : o;
      }),
      (c.context = function (t) {
        return arguments.length
          ? (null == t ? (a = s = null) : (s = o((a = t))), c)
          : a;
      }),
      c
    );
  }
  function Pt(t, e) {
    return e < t ? -1 : e > t ? 1 : e >= t ? 0 : NaN;
  }
  function Bt(t) {
    return t;
  }
  At.prototype = {
    areaStart: function () {
      this._line = 0;
    },
    areaEnd: function () {
      this._line = NaN;
    },
    lineStart: function () {
      this._point = 0;
    },
    lineEnd: function () {
      (this._line || (0 !== this._line && 1 === this._point)) &&
        this._context.closePath(),
        (this._line = 1 - this._line);
    },
    point: function (t, e) {
      switch (((t = +t), (e = +e), this._point)) {
        case 0:
          (this._point = 1),
            this._line
              ? this._context.lineTo(t, e)
              : this._context.moveTo(t, e);
          break;
        case 1:
          this._point = 2;
        default:
          this._context.lineTo(t, e);
      }
    },
  };
  var Dt = Array.prototype.slice;
  function Ft(t, e, n) {
    t._context.bezierCurveTo(
      (2 * t._x0 + t._x1) / 3,
      (2 * t._y0 + t._y1) / 3,
      (t._x0 + 2 * t._x1) / 3,
      (t._y0 + 2 * t._y1) / 3,
      (t._x0 + 4 * t._x1 + e) / 6,
      (t._y0 + 4 * t._y1 + n) / 6
    );
  }
  function Lt(t) {
    this._context = t;
  }
  function It(t, e, n) {
    t._context.bezierCurveTo(
      t._x1 + t._k * (t._x2 - t._x0),
      t._y1 + t._k * (t._y2 - t._y0),
      t._x2 + t._k * (t._x1 - e),
      t._y2 + t._k * (t._y1 - n),
      t._x2,
      t._y2
    );
  }
  function Wt(t, e) {
    (this._context = t), (this._k = (1 - e) / 6);
  }
  (Lt.prototype = {
    areaStart: function () {
      this._line = 0;
    },
    areaEnd: function () {
      this._line = NaN;
    },
    lineStart: function () {
      (this._x0 = this._x1 = this._y0 = this._y1 = NaN), (this._point = 0);
    },
    lineEnd: function () {
      switch (this._point) {
        case 3:
          Ft(this, this._x1, this._y1);
        case 2:
          this._context.lineTo(this._x1, this._y1);
      }
      (this._line || (0 !== this._line && 1 === this._point)) &&
        this._context.closePath(),
        (this._line = 1 - this._line);
    },
    point: function (t, e) {
      switch (((t = +t), (e = +e), this._point)) {
        case 0:
          (this._point = 1),
            this._line
              ? this._context.lineTo(t, e)
              : this._context.moveTo(t, e);
          break;
        case 1:
          this._point = 2;
          break;
        case 2:
          (this._point = 3),
            this._context.lineTo(
              (5 * this._x0 + this._x1) / 6,
              (5 * this._y0 + this._y1) / 6
            );
        default:
          Ft(this, t, e);
      }
      (this._x0 = this._x1),
        (this._x1 = t),
        (this._y0 = this._y1),
        (this._y1 = e);
    },
  }),
    (Wt.prototype = {
      areaStart: function () {
        this._line = 0;
      },
      areaEnd: function () {
        this._line = NaN;
      },
      lineStart: function () {
        (this._x0 = this._x1 = this._x2 = this._y0 = this._y1 = this._y2 = NaN),
          (this._point = 0);
      },
      lineEnd: function () {
        switch (this._point) {
          case 2:
            this._context.lineTo(this._x2, this._y2);
            break;
          case 3:
            It(this, this._x1, this._y1);
        }
        (this._line || (0 !== this._line && 1 === this._point)) &&
          this._context.closePath(),
          (this._line = 1 - this._line);
      },
      point: function (t, e) {
        switch (((t = +t), (e = +e), this._point)) {
          case 0:
            (this._point = 1),
              this._line
                ? this._context.lineTo(t, e)
                : this._context.moveTo(t, e);
            break;
          case 1:
            (this._point = 2), (this._x1 = t), (this._y1 = e);
            break;
          case 2:
            this._point = 3;
          default:
            It(this, t, e);
        }
        (this._x0 = this._x1),
          (this._x1 = this._x2),
          (this._x2 = t),
          (this._y0 = this._y1),
          (this._y1 = this._y2),
          (this._y2 = e);
      },
    });
  var Vt = (function t(e) {
    function n(t) {
      return new Wt(t, e);
    }
    return (
      (n.tension = function (e) {
        return t(+e);
      }),
      n
    );
  })(0);
  function $t(t, e) {
    (this._context = t), (this._alpha = e);
  }
  $t.prototype = {
    areaStart: function () {
      this._line = 0;
    },
    areaEnd: function () {
      this._line = NaN;
    },
    lineStart: function () {
      (this._x0 = this._x1 = this._x2 = this._y0 = this._y1 = this._y2 = NaN),
        (this._l01_a =
          this._l12_a =
          this._l23_a =
          this._l01_2a =
          this._l12_2a =
          this._l23_2a =
          this._point =
            0);
    },
    lineEnd: function () {
      switch (this._point) {
        case 2:
          this._context.lineTo(this._x2, this._y2);
          break;
        case 3:
          this.point(this._x2, this._y2);
      }
      (this._line || (0 !== this._line && 1 === this._point)) &&
        this._context.closePath(),
        (this._line = 1 - this._line);
    },
    point: function (t, e) {
      if (((t = +t), (e = +e), this._point)) {
        var n = this._x2 - t,
          i = this._y2 - e;
        this._l23_a = Math.sqrt(
          (this._l23_2a = Math.pow(n * n + i * i, this._alpha))
        );
      }
      switch (this._point) {
        case 0:
          (this._point = 1),
            this._line
              ? this._context.lineTo(t, e)
              : this._context.moveTo(t, e);
          break;
        case 1:
          this._point = 2;
          break;
        case 2:
          this._point = 3;
        default:
          !(function (t, e, n) {
            var i = t._x1,
              r = t._y1,
              a = t._x2,
              o = t._y2;
            if (t._l01_a > vt) {
              var s = 2 * t._l01_2a + 3 * t._l01_a * t._l12_a + t._l12_2a,
                c = 3 * t._l01_a * (t._l01_a + t._l12_a);
              (i = (i * s - t._x0 * t._l12_2a + t._x2 * t._l01_2a) / c),
                (r = (r * s - t._y0 * t._l12_2a + t._y2 * t._l01_2a) / c);
            }
            if (t._l23_a > vt) {
              var u = 2 * t._l23_2a + 3 * t._l23_a * t._l12_a + t._l12_2a,
                l = 3 * t._l23_a * (t._l23_a + t._l12_a);
              (a = (a * u + t._x1 * t._l23_2a - e * t._l12_2a) / l),
                (o = (o * u + t._y1 * t._l23_2a - n * t._l12_2a) / l);
            }
            t._context.bezierCurveTo(i, r, a, o, t._x2, t._y2);
          })(this, t, e);
      }
      (this._l01_a = this._l12_a),
        (this._l12_a = this._l23_a),
        (this._l01_2a = this._l12_2a),
        (this._l12_2a = this._l23_2a),
        (this._x0 = this._x1),
        (this._x1 = this._x2),
        (this._x2 = t),
        (this._y0 = this._y1),
        (this._y1 = this._y2),
        (this._y2 = e);
    },
  };
  var Ht = (function t(e) {
    function n(t) {
      return e ? new $t(t, e) : new Wt(t, 0);
    }
    return (
      (n.alpha = function (e) {
        return t(+e);
      }),
      n
    );
  })(0.5);
  function qt(t) {
    return t < 0 ? -1 : 1;
  }
  function Ut(t, e, n) {
    var i = t._x1 - t._x0,
      r = e - t._x1,
      a = (t._y1 - t._y0) / (i || (r < 0 && -0)),
      o = (n - t._y1) / (r || (i < 0 && -0)),
      s = (a * r + o * i) / (i + r);
    return (
      (qt(a) + qt(o)) * Math.min(Math.abs(a), Math.abs(o), 0.5 * Math.abs(s)) ||
      0
    );
  }
  function Yt(t, e) {
    var n = t._x1 - t._x0;
    return n ? ((3 * (t._y1 - t._y0)) / n - e) / 2 : e;
  }
  function Xt(t, e, n) {
    var i = t._x0,
      r = t._y0,
      a = t._x1,
      o = t._y1,
      s = (a - i) / 3;
    t._context.bezierCurveTo(i + s, r + s * e, a - s, o - s * n, a, o);
  }
  function Gt(t) {
    this._context = t;
  }
  function Zt(t) {
    this._context = new Jt(t);
  }
  function Jt(t) {
    this._context = t;
  }
  function Kt(t) {
    this._context = t;
  }
  function Qt(t) {
    var e,
      n,
      i = t.length - 1,
      r = new Array(i),
      a = new Array(i),
      o = new Array(i);
    for (r[0] = 0, a[0] = 2, o[0] = t[0] + 2 * t[1], e = 1; e < i - 1; ++e)
      (r[e] = 1), (a[e] = 4), (o[e] = 4 * t[e] + 2 * t[e + 1]);
    for (
      r[i - 1] = 2, a[i - 1] = 7, o[i - 1] = 8 * t[i - 1] + t[i], e = 1;
      e < i;
      ++e
    )
      (n = r[e] / a[e - 1]), (a[e] -= n), (o[e] -= n * o[e - 1]);
    for (r[i - 1] = o[i - 1] / a[i - 1], e = i - 2; e >= 0; --e)
      r[e] = (o[e] - r[e + 1]) / a[e];
    for (a[i - 1] = (t[i] + r[i - 1]) / 2, e = 0; e < i - 1; ++e)
      a[e] = 2 * t[e + 1] - r[e + 1];
    return [r, a];
  }
  function te(t, e) {
    (this._context = t), (this._t = e);
  }
  function ee(t, e) {
    if ((r = t.length) > 1)
      for (var n, i, r, a = 1, o = t[e[0]], s = o.length; a < r; ++a)
        for (i = o, o = t[e[a]], n = 0; n < s; ++n)
          o[n][1] += o[n][0] = isNaN(i[n][1]) ? i[n][0] : i[n][1];
  }
  function ne(t) {
    for (var e = t.length, n = new Array(e); --e >= 0; ) n[e] = e;
    return n;
  }
  function ie(t, e) {
    return t[e];
  }
  function re(t) {
    for (var e, n = -1, i = 0, r = t.length, a = -1 / 0; ++n < r; )
      (e = +t[n][1]) > a && ((a = e), (i = n));
    return i;
  }
  function ae(t) {
    for (var e, n = 0, i = -1, r = t.length; ++i < r; )
      (e = +t[i][1]) && (n += e);
    return n;
  }
  function oe(t) {
    var e,
      n =
        arguments.length > 1 && void 0 !== arguments[1]
          ? arguments[1]
          : "unspecified",
      i = arguments.length > 2 ? arguments[2] : void 0,
      r = {},
      a = t || {
        get: function () {},
        has: function () {
          return !1;
        },
        default: function () {},
      };
    function o(t, e) {
      if (!t || "string" != typeof t)
        throw new TypeError("Invalid argument: key must be a non-empty string");
      return !(t in r) && ((r[t] = e), !0);
    }
    function s(t) {
      return r[t] || a.get(t);
    }
    function c(t) {
      return !!r[t] || a.has(t);
    }
    function u(t) {
      var e = r[t];
      return delete r[t], e;
    }
    function l() {
      return Object.keys(r);
    }
    function h() {
      return Object.keys(r).map(function (t) {
        return r[t];
      });
    }
    function f(t) {
      return void 0 !== t && (e = t), e;
    }
    function d(t, r) {
      if (void 0 !== r) return o(t, r);
      var a = s(t);
      return (
        i &&
          void 0 === a &&
          i.warn("".concat(t, " does not exist in ").concat(n, " registry")),
        a || s(e)
      );
    }
    return (
      (e = a.default()),
      (d.add = o),
      (d.get = s),
      (d.has = c),
      (d.remove = u),
      (d.getKeys = l),
      (d.getValues = h),
      (d.default = f),
      (d.register = o),
      d
    );
  }
  function se(t, e) {
    if (
      (n = (t = e ? t.toExponential(e - 1) : t.toExponential()).indexOf("e")) <
      0
    )
      return null;
    var n,
      i = t.slice(0, n);
    return [i.length > 1 ? i[0] + i.slice(2) : i, +t.slice(n + 1)];
  }
  function ce(t) {
    return (t = se(Math.abs(t))) ? t[1] : NaN;
  }
  (Gt.prototype = {
    areaStart: function () {
      this._line = 0;
    },
    areaEnd: function () {
      this._line = NaN;
    },
    lineStart: function () {
      (this._x0 = this._x1 = this._y0 = this._y1 = this._t0 = NaN),
        (this._point = 0);
    },
    lineEnd: function () {
      switch (this._point) {
        case 2:
          this._context.lineTo(this._x1, this._y1);
          break;
        case 3:
          Xt(this, this._t0, Yt(this, this._t0));
      }
      (this._line || (0 !== this._line && 1 === this._point)) &&
        this._context.closePath(),
        (this._line = 1 - this._line);
    },
    point: function (t, e) {
      var n = NaN;
      if (((e = +e), (t = +t) !== this._x1 || e !== this._y1)) {
        switch (this._point) {
          case 0:
            (this._point = 1),
              this._line
                ? this._context.lineTo(t, e)
                : this._context.moveTo(t, e);
            break;
          case 1:
            this._point = 2;
            break;
          case 2:
            (this._point = 3), Xt(this, Yt(this, (n = Ut(this, t, e))), n);
            break;
          default:
            Xt(this, this._t0, (n = Ut(this, t, e)));
        }
        (this._x0 = this._x1),
          (this._x1 = t),
          (this._y0 = this._y1),
          (this._y1 = e),
          (this._t0 = n);
      }
    },
  }),
    ((Zt.prototype = Object.create(Gt.prototype)).point = function (t, e) {
      Gt.prototype.point.call(this, e, t);
    }),
    (Jt.prototype = {
      moveTo: function (t, e) {
        this._context.moveTo(e, t);
      },
      closePath: function () {
        this._context.closePath();
      },
      lineTo: function (t, e) {
        this._context.lineTo(e, t);
      },
      bezierCurveTo: function (t, e, n, i, r, a) {
        this._context.bezierCurveTo(e, t, i, n, a, r);
      },
    }),
    (Kt.prototype = {
      areaStart: function () {
        this._line = 0;
      },
      areaEnd: function () {
        this._line = NaN;
      },
      lineStart: function () {
        (this._x = []), (this._y = []);
      },
      lineEnd: function () {
        var t = this._x,
          e = this._y,
          n = t.length;
        if (n)
          if (
            (this._line
              ? this._context.lineTo(t[0], e[0])
              : this._context.moveTo(t[0], e[0]),
            2 === n)
          )
            this._context.lineTo(t[1], e[1]);
          else
            for (var i = Qt(t), r = Qt(e), a = 0, o = 1; o < n; ++a, ++o)
              this._context.bezierCurveTo(
                i[0][a],
                r[0][a],
                i[1][a],
                r[1][a],
                t[o],
                e[o]
              );
        (this._line || (0 !== this._line && 1 === n)) &&
          this._context.closePath(),
          (this._line = 1 - this._line),
          (this._x = this._y = null);
      },
      point: function (t, e) {
        this._x.push(+t), this._y.push(+e);
      },
    }),
    (te.prototype = {
      areaStart: function () {
        this._line = 0;
      },
      areaEnd: function () {
        this._line = NaN;
      },
      lineStart: function () {
        (this._x = this._y = NaN), (this._point = 0);
      },
      lineEnd: function () {
        0 < this._t &&
          this._t < 1 &&
          2 === this._point &&
          this._context.lineTo(this._x, this._y),
          (this._line || (0 !== this._line && 1 === this._point)) &&
            this._context.closePath(),
          this._line >= 0 &&
            ((this._t = 1 - this._t), (this._line = 1 - this._line));
      },
      point: function (t, e) {
        switch (((t = +t), (e = +e), this._point)) {
          case 0:
            (this._point = 1),
              this._line
                ? this._context.lineTo(t, e)
                : this._context.moveTo(t, e);
            break;
          case 1:
            this._point = 2;
          default:
            if (this._t <= 0)
              this._context.lineTo(this._x, e), this._context.lineTo(t, e);
            else {
              var n = this._x * (1 - this._t) + t * this._t;
              this._context.lineTo(n, this._y), this._context.lineTo(n, e);
            }
        }
        (this._x = t), (this._y = e);
      },
    });
  var ue,
    le =
      /^(?:(.)?([<>=^]))?([+\-( ])?([$#])?(0)?(\d+)?(,)?(\.\d+)?(~)?([a-z%])?$/i;
  function he(t) {
    if (!(e = le.exec(t))) throw new Error("invalid format: " + t);
    var e;
    return new fe({
      fill: e[1],
      align: e[2],
      sign: e[3],
      symbol: e[4],
      zero: e[5],
      width: e[6],
      comma: e[7],
      precision: e[8] && e[8].slice(1),
      trim: e[9],
      type: e[10],
    });
  }
  function fe(t) {
    (this.fill = void 0 === t.fill ? " " : t.fill + ""),
      (this.align = void 0 === t.align ? ">" : t.align + ""),
      (this.sign = void 0 === t.sign ? "-" : t.sign + ""),
      (this.symbol = void 0 === t.symbol ? "" : t.symbol + ""),
      (this.zero = !!t.zero),
      (this.width = void 0 === t.width ? void 0 : +t.width),
      (this.comma = !!t.comma),
      (this.precision = void 0 === t.precision ? void 0 : +t.precision),
      (this.trim = !!t.trim),
      (this.type = void 0 === t.type ? "" : t.type + "");
  }
  function de(t, e) {
    var n = se(t, e);
    if (!n) return t + "";
    var i = n[0],
      r = n[1];
    return r < 0
      ? "0." + new Array(-r).join("0") + i
      : i.length > r + 1
      ? i.slice(0, r + 1) + "." + i.slice(r + 1)
      : i + new Array(r - i.length + 2).join("0");
  }
  (he.prototype = fe.prototype),
    (fe.prototype.toString = function () {
      return (
        this.fill +
        this.align +
        this.sign +
        this.symbol +
        (this.zero ? "0" : "") +
        (void 0 === this.width ? "" : Math.max(1, 0 | this.width)) +
        (this.comma ? "," : "") +
        (void 0 === this.precision
          ? ""
          : "." + Math.max(0, 0 | this.precision)) +
        (this.trim ? "~" : "") +
        this.type
      );
    });
  var ge = {
    "%": function (t, e) {
      return (100 * t).toFixed(e);
    },
    b: function (t) {
      return Math.round(t).toString(2);
    },
    c: function (t) {
      return t + "";
    },
    d: function (t) {
      return Math.abs((t = Math.round(t))) >= 1e21
        ? t.toLocaleString("en").replace(/,/g, "")
        : t.toString(10);
    },
    e: function (t, e) {
      return t.toExponential(e);
    },
    f: function (t, e) {
      return t.toFixed(e);
    },
    g: function (t, e) {
      return t.toPrecision(e);
    },
    o: function (t) {
      return Math.round(t).toString(8);
    },
    p: function (t, e) {
      return de(100 * t, e);
    },
    r: de,
    s: function (t, e) {
      var n = se(t, e);
      if (!n) return t + "";
      var i = n[0],
        r = n[1],
        a = r - (ue = 3 * Math.max(-8, Math.min(8, Math.floor(r / 3)))) + 1,
        o = i.length;
      return a === o
        ? i
        : a > o
        ? i + new Array(a - o + 1).join("0")
        : a > 0
        ? i.slice(0, a) + "." + i.slice(a)
        : "0." + new Array(1 - a).join("0") + se(t, Math.max(0, e + a - 1))[0];
    },
    X: function (t) {
      return Math.round(t).toString(16).toUpperCase();
    },
    x: function (t) {
      return Math.round(t).toString(16);
    },
  };
  function pe(t) {
    return t;
  }
  var ye,
    ve,
    me,
    xe = Array.prototype.map,
    be = [
      "y",
      "z",
      "a",
      "f",
      "p",
      "n",
      "µ",
      "m",
      "",
      "k",
      "M",
      "G",
      "T",
      "P",
      "E",
      "Z",
      "Y",
    ];
  function _e(t) {
    var e,
      n,
      i =
        void 0 === t.grouping || void 0 === t.thousands
          ? pe
          : ((e = xe.call(t.grouping, Number)),
            (n = t.thousands + ""),
            function (t, i) {
              for (
                var r = t.length, a = [], o = 0, s = e[0], c = 0;
                r > 0 &&
                s > 0 &&
                (c + s + 1 > i && (s = Math.max(1, i - c)),
                a.push(t.substring((r -= s), r + s)),
                !((c += s + 1) > i));

              )
                s = e[(o = (o + 1) % e.length)];
              return a.reverse().join(n);
            }),
      r = void 0 === t.currency ? "" : t.currency[0] + "",
      a = void 0 === t.currency ? "" : t.currency[1] + "",
      o = void 0 === t.decimal ? "." : t.decimal + "",
      s =
        void 0 === t.numerals
          ? pe
          : (function (t) {
              return function (e) {
                return e.replace(/[0-9]/g, function (e) {
                  return t[+e];
                });
              };
            })(xe.call(t.numerals, String)),
      c = void 0 === t.percent ? "%" : t.percent + "",
      u = void 0 === t.minus ? "-" : t.minus + "",
      l = void 0 === t.nan ? "NaN" : t.nan + "";
    function h(t) {
      var e = (t = he(t)).fill,
        n = t.align,
        h = t.sign,
        f = t.symbol,
        d = t.zero,
        g = t.width,
        p = t.comma,
        y = t.precision,
        v = t.trim,
        m = t.type;
      "n" === m
        ? ((p = !0), (m = "g"))
        : ge[m] || (void 0 === y && (y = 12), (v = !0), (m = "g")),
        (d || ("0" === e && "=" === n)) && ((d = !0), (e = "0"), (n = "="));
      var x =
          "$" === f
            ? r
            : "#" === f && /[boxX]/.test(m)
            ? "0" + m.toLowerCase()
            : "",
        b = "$" === f ? a : /[%p]/.test(m) ? c : "",
        _ = ge[m],
        w = /[defgprs%]/.test(m);
      function k(t) {
        var r,
          a,
          c,
          f = x,
          k = b;
        if ("c" === m) (k = _(t) + k), (t = "");
        else {
          var M = (t = +t) < 0 || 1 / t < 0;
          if (
            ((t = isNaN(t) ? l : _(Math.abs(t), y)),
            v &&
              (t = (function (t) {
                t: for (var e, n = t.length, i = 1, r = -1; i < n; ++i)
                  switch (t[i]) {
                    case ".":
                      r = e = i;
                      break;
                    case "0":
                      0 === r && (r = i), (e = i);
                      break;
                    default:
                      if (!+t[i]) break t;
                      r > 0 && (r = 0);
                  }
                return r > 0 ? t.slice(0, r) + t.slice(e + 1) : t;
              })(t)),
            M && 0 == +t && "+" !== h && (M = !1),
            (f =
              (M ? ("(" === h ? h : u) : "-" === h || "(" === h ? "" : h) + f),
            (k =
              ("s" === m ? be[8 + ue / 3] : "") +
              k +
              (M && "(" === h ? ")" : "")),
            w)
          )
            for (r = -1, a = t.length; ++r < a; )
              if (48 > (c = t.charCodeAt(r)) || c > 57) {
                (k = (46 === c ? o + t.slice(r + 1) : t.slice(r)) + k),
                  (t = t.slice(0, r));
                break;
              }
        }
        p && !d && (t = i(t, 1 / 0));
        var R = f.length + t.length + k.length,
          S = R < g ? new Array(g - R + 1).join(e) : "";
        switch (
          (p &&
            d &&
            ((t = i(S + t, S.length ? g - k.length : 1 / 0)), (S = "")),
          n)
        ) {
          case "<":
            t = f + t + k + S;
            break;
          case "=":
            t = f + S + t + k;
            break;
          case "^":
            t = S.slice(0, (R = S.length >> 1)) + f + t + k + S.slice(R);
            break;
          default:
            t = S + f + t + k;
        }
        return s(t);
      }
      return (
        (y =
          void 0 === y
            ? 6
            : /[gprs]/.test(m)
            ? Math.max(1, Math.min(21, y))
            : Math.max(0, Math.min(20, y))),
        (k.toString = function () {
          return t + "";
        }),
        k
      );
    }
    return {
      format: h,
      formatPrefix: function (t, e) {
        var n = h((((t = he(t)).type = "f"), t)),
          i = 3 * Math.max(-8, Math.min(8, Math.floor(ce(e) / 3))),
          r = Math.pow(10, -i),
          a = be[8 + i / 3];
        return function (t) {
          return n(r * t) + a;
        };
      },
    };
  }
  !(function (t) {
    (ye = _e(t)), (ve = ye.format), (me = ye.formatPrefix);
  })({
    decimal: ".",
    thousands: ",",
    grouping: [3],
    currency: ["$", ""],
    minus: "-",
  });
  var we = new Date(),
    ke = new Date();
  function Me(t, e, n, i) {
    function r(e) {
      return t((e = 0 === arguments.length ? new Date() : new Date(+e))), e;
    }
    return (
      (r.floor = function (e) {
        return t((e = new Date(+e))), e;
      }),
      (r.ceil = function (n) {
        return t((n = new Date(n - 1))), e(n, 1), t(n), n;
      }),
      (r.round = function (t) {
        var e = r(t),
          n = r.ceil(t);
        return t - e < n - t ? e : n;
      }),
      (r.offset = function (t, n) {
        return e((t = new Date(+t)), null == n ? 1 : Math.floor(n)), t;
      }),
      (r.range = function (n, i, a) {
        var o,
          s = [];
        if (
          ((n = r.ceil(n)),
          (a = null == a ? 1 : Math.floor(a)),
          !(n < i && a > 0))
        )
          return s;
        do {
          s.push((o = new Date(+n))), e(n, a), t(n);
        } while (o < n && n < i);
        return s;
      }),
      (r.filter = function (n) {
        return Me(
          function (e) {
            if (e >= e) for (; t(e), !n(e); ) e.setTime(e - 1);
          },
          function (t, i) {
            if (t >= t)
              if (i < 0) for (; ++i <= 0; ) for (; e(t, -1), !n(t); );
              else for (; --i >= 0; ) for (; e(t, 1), !n(t); );
          }
        );
      }),
      n &&
        ((r.count = function (e, i) {
          return (
            we.setTime(+e), ke.setTime(+i), t(we), t(ke), Math.floor(n(we, ke))
          );
        }),
        (r.every = function (t) {
          return (
            (t = Math.floor(t)),
            isFinite(t) && t > 0
              ? t > 1
                ? r.filter(
                    i
                      ? function (e) {
                          return i(e) % t == 0;
                        }
                      : function (e) {
                          return r.count(0, e) % t == 0;
                        }
                  )
                : r
              : null
          );
        })),
      r
    );
  }
  var Re = 864e5,
    Se = 6048e5,
    Ne = Me(
      function (t) {
        t.setHours(0, 0, 0, 0);
      },
      function (t, e) {
        t.setDate(t.getDate() + e);
      },
      function (t, e) {
        return (
          (e - t - 6e4 * (e.getTimezoneOffset() - t.getTimezoneOffset())) / Re
        );
      },
      function (t) {
        return t.getDate() - 1;
      }
    ),
    ze = Ne;
  function Ce(t) {
    return Me(
      function (e) {
        e.setDate(e.getDate() - ((e.getDay() + 7 - t) % 7)),
          e.setHours(0, 0, 0, 0);
      },
      function (t, e) {
        t.setDate(t.getDate() + 7 * e);
      },
      function (t, e) {
        return (
          (e - t - 6e4 * (e.getTimezoneOffset() - t.getTimezoneOffset())) / Se
        );
      }
    );
  }
  var Ae = Ce(0),
    Ee = Ce(1);
  Ce(2), Ce(3);
  var Oe = Ce(4);
  Ce(5), Ce(6);
  var Te = Me(
    function (t) {
      t.setMonth(0, 1), t.setHours(0, 0, 0, 0);
    },
    function (t, e) {
      t.setFullYear(t.getFullYear() + e);
    },
    function (t, e) {
      return e.getFullYear() - t.getFullYear();
    },
    function (t) {
      return t.getFullYear();
    }
  );
  Te.every = function (t) {
    return isFinite((t = Math.floor(t))) && t > 0
      ? Me(
          function (e) {
            e.setFullYear(Math.floor(e.getFullYear() / t) * t),
              e.setMonth(0, 1),
              e.setHours(0, 0, 0, 0);
          },
          function (e, n) {
            e.setFullYear(e.getFullYear() + n * t);
          }
        )
      : null;
  };
  var je = Te,
    Pe = Me(
      function (t) {
        t.setUTCHours(0, 0, 0, 0);
      },
      function (t, e) {
        t.setUTCDate(t.getUTCDate() + e);
      },
      function (t, e) {
        return (e - t) / Re;
      },
      function (t) {
        return t.getUTCDate() - 1;
      }
    ),
    Be = Pe;
  function De(t) {
    return Me(
      function (e) {
        e.setUTCDate(e.getUTCDate() - ((e.getUTCDay() + 7 - t) % 7)),
          e.setUTCHours(0, 0, 0, 0);
      },
      function (t, e) {
        t.setUTCDate(t.getUTCDate() + 7 * e);
      },
      function (t, e) {
        return (e - t) / Se;
      }
    );
  }
  var Fe = De(0),
    Le = De(1);
  De(2), De(3);
  var Ie = De(4);
  De(5), De(6);
  var We = Me(
    function (t) {
      t.setUTCMonth(0, 1), t.setUTCHours(0, 0, 0, 0);
    },
    function (t, e) {
      t.setUTCFullYear(t.getUTCFullYear() + e);
    },
    function (t, e) {
      return e.getUTCFullYear() - t.getUTCFullYear();
    },
    function (t) {
      return t.getUTCFullYear();
    }
  );
  We.every = function (t) {
    return isFinite((t = Math.floor(t))) && t > 0
      ? Me(
          function (e) {
            e.setUTCFullYear(Math.floor(e.getUTCFullYear() / t) * t),
              e.setUTCMonth(0, 1),
              e.setUTCHours(0, 0, 0, 0);
          },
          function (e, n) {
            e.setUTCFullYear(e.getUTCFullYear() + n * t);
          }
        )
      : null;
  };
  var Ve = We;
  function $e(t) {
    if (0 <= t.y && t.y < 100) {
      var e = new Date(-1, t.m, t.d, t.H, t.M, t.S, t.L);
      return e.setFullYear(t.y), e;
    }
    return new Date(t.y, t.m, t.d, t.H, t.M, t.S, t.L);
  }
  function He(t) {
    if (0 <= t.y && t.y < 100) {
      var e = new Date(Date.UTC(-1, t.m, t.d, t.H, t.M, t.S, t.L));
      return e.setUTCFullYear(t.y), e;
    }
    return new Date(Date.UTC(t.y, t.m, t.d, t.H, t.M, t.S, t.L));
  }
  function qe(t, e, n) {
    return { y: t, m: e, d: n, H: 0, M: 0, S: 0, L: 0 };
  }
  function Ue(t) {
    var e = t.dateTime,
      n = t.date,
      i = t.time,
      r = t.periods,
      a = t.days,
      o = t.shortDays,
      s = t.months,
      c = t.shortMonths,
      u = Qe(r),
      l = tn(r),
      h = Qe(a),
      f = tn(a),
      d = Qe(o),
      g = tn(o),
      p = Qe(s),
      y = tn(s),
      v = Qe(c),
      m = tn(c),
      x = {
        a: function (t) {
          return o[t.getDay()];
        },
        A: function (t) {
          return a[t.getDay()];
        },
        b: function (t) {
          return c[t.getMonth()];
        },
        B: function (t) {
          return s[t.getMonth()];
        },
        c: null,
        d: wn,
        e: wn,
        f: Nn,
        g: Fn,
        G: In,
        H: kn,
        I: Mn,
        j: Rn,
        L: Sn,
        m: zn,
        M: Cn,
        p: function (t) {
          return r[+(t.getHours() >= 12)];
        },
        q: function (t) {
          return 1 + ~~(t.getMonth() / 3);
        },
        Q: ui,
        s: li,
        S: An,
        u: En,
        U: On,
        V: jn,
        w: Pn,
        W: Bn,
        x: null,
        X: null,
        y: Dn,
        Y: Ln,
        Z: Wn,
        "%": ci,
      },
      b = {
        a: function (t) {
          return o[t.getUTCDay()];
        },
        A: function (t) {
          return a[t.getUTCDay()];
        },
        b: function (t) {
          return c[t.getUTCMonth()];
        },
        B: function (t) {
          return s[t.getUTCMonth()];
        },
        c: null,
        d: Vn,
        e: Vn,
        f: Yn,
        g: ri,
        G: oi,
        H: $n,
        I: Hn,
        j: qn,
        L: Un,
        m: Xn,
        M: Gn,
        p: function (t) {
          return r[+(t.getUTCHours() >= 12)];
        },
        q: function (t) {
          return 1 + ~~(t.getUTCMonth() / 3);
        },
        Q: ui,
        s: li,
        S: Zn,
        u: Jn,
        U: Kn,
        V: ti,
        w: ei,
        W: ni,
        x: null,
        X: null,
        y: ii,
        Y: ai,
        Z: si,
        "%": ci,
      },
      _ = {
        a: function (t, e, n) {
          var i = d.exec(e.slice(n));
          return i ? ((t.w = g[i[0].toLowerCase()]), n + i[0].length) : -1;
        },
        A: function (t, e, n) {
          var i = h.exec(e.slice(n));
          return i ? ((t.w = f[i[0].toLowerCase()]), n + i[0].length) : -1;
        },
        b: function (t, e, n) {
          var i = v.exec(e.slice(n));
          return i ? ((t.m = m[i[0].toLowerCase()]), n + i[0].length) : -1;
        },
        B: function (t, e, n) {
          var i = p.exec(e.slice(n));
          return i ? ((t.m = y[i[0].toLowerCase()]), n + i[0].length) : -1;
        },
        c: function (t, n, i) {
          return M(t, e, n, i);
        },
        d: fn,
        e: fn,
        f: mn,
        g: cn,
        G: sn,
        H: gn,
        I: gn,
        j: dn,
        L: vn,
        m: hn,
        M: pn,
        p: function (t, e, n) {
          var i = u.exec(e.slice(n));
          return i ? ((t.p = l[i[0].toLowerCase()]), n + i[0].length) : -1;
        },
        q: ln,
        Q: bn,
        s: _n,
        S: yn,
        u: nn,
        U: rn,
        V: an,
        w: en,
        W: on,
        x: function (t, e, i) {
          return M(t, n, e, i);
        },
        X: function (t, e, n) {
          return M(t, i, e, n);
        },
        y: cn,
        Y: sn,
        Z: un,
        "%": xn,
      };
    function w(t, e) {
      return function (n) {
        var i,
          r,
          a,
          o = [],
          s = -1,
          c = 0,
          u = t.length;
        for (n instanceof Date || (n = new Date(+n)); ++s < u; )
          37 === t.charCodeAt(s) &&
            (o.push(t.slice(c, s)),
            null != (r = Ye[(i = t.charAt(++s))])
              ? (i = t.charAt(++s))
              : (r = "e" === i ? " " : "0"),
            (a = e[i]) && (i = a(n, r)),
            o.push(i),
            (c = s + 1));
        return o.push(t.slice(c, s)), o.join("");
      };
    }
    function k(t, e) {
      return function (n) {
        var i,
          r,
          a = qe(1900, void 0, 1);
        if (M(a, t, (n += ""), 0) != n.length) return null;
        if ("Q" in a) return new Date(a.Q);
        if ("s" in a) return new Date(1e3 * a.s + ("L" in a ? a.L : 0));
        if (
          (e && !("Z" in a) && (a.Z = 0),
          "p" in a && (a.H = (a.H % 12) + 12 * a.p),
          void 0 === a.m && (a.m = "q" in a ? a.q : 0),
          "V" in a)
        ) {
          if (a.V < 1 || a.V > 53) return null;
          "w" in a || (a.w = 1),
            "Z" in a
              ? ((r = (i = He(qe(a.y, 0, 1))).getUTCDay()),
                (i = r > 4 || 0 === r ? Le.ceil(i) : Le(i)),
                (i = Be.offset(i, 7 * (a.V - 1))),
                (a.y = i.getUTCFullYear()),
                (a.m = i.getUTCMonth()),
                (a.d = i.getUTCDate() + ((a.w + 6) % 7)))
              : ((r = (i = $e(qe(a.y, 0, 1))).getDay()),
                (i = r > 4 || 0 === r ? Ee.ceil(i) : Ee(i)),
                (i = ze.offset(i, 7 * (a.V - 1))),
                (a.y = i.getFullYear()),
                (a.m = i.getMonth()),
                (a.d = i.getDate() + ((a.w + 6) % 7)));
        } else ("W" in a || "U" in a) && ("w" in a || (a.w = "u" in a ? a.u % 7 : "W" in a ? 1 : 0), (r = "Z" in a ? He(qe(a.y, 0, 1)).getUTCDay() : $e(qe(a.y, 0, 1)).getDay()), (a.m = 0), (a.d = "W" in a ? ((a.w + 6) % 7) + 7 * a.W - ((r + 5) % 7) : a.w + 7 * a.U - ((r + 6) % 7)));
        return "Z" in a
          ? ((a.H += (a.Z / 100) | 0), (a.M += a.Z % 100), He(a))
          : $e(a);
      };
    }
    function M(t, e, n, i) {
      for (var r, a, o = 0, s = e.length, c = n.length; o < s; ) {
        if (i >= c) return -1;
        if (37 === (r = e.charCodeAt(o++))) {
          if (
            ((r = e.charAt(o++)),
            !(a = _[r in Ye ? e.charAt(o++) : r]) || (i = a(t, n, i)) < 0)
          )
            return -1;
        } else if (r != n.charCodeAt(i++)) return -1;
      }
      return i;
    }
    return (
      (x.x = w(n, x)),
      (x.X = w(i, x)),
      (x.c = w(e, x)),
      (b.x = w(n, b)),
      (b.X = w(i, b)),
      (b.c = w(e, b)),
      {
        format: function (t) {
          var e = w((t += ""), x);
          return (
            (e.toString = function () {
              return t;
            }),
            e
          );
        },
        parse: function (t) {
          var e = k((t += ""), !1);
          return (
            (e.toString = function () {
              return t;
            }),
            e
          );
        },
        utcFormat: function (t) {
          var e = w((t += ""), b);
          return (
            (e.toString = function () {
              return t;
            }),
            e
          );
        },
        utcParse: function (t) {
          var e = k((t += ""), !0);
          return (
            (e.toString = function () {
              return t;
            }),
            e
          );
        },
      }
    );
  }
  var Ye = { "-": "", _: " ", 0: "0" },
    Xe = /^\s*\d+/,
    Ge = /^%/,
    Ze = /[\\^$*+?|[\]().{}]/g;
  function Je(t, e, n) {
    var i = t < 0 ? "-" : "",
      r = (i ? -t : t) + "",
      a = r.length;
    return i + (a < n ? new Array(n - a + 1).join(e) + r : r);
  }
  function Ke(t) {
    return t.replace(Ze, "\\$&");
  }
  function Qe(t) {
    return new RegExp("^(?:" + t.map(Ke).join("|") + ")", "i");
  }
  function tn(t) {
    for (var e = {}, n = -1, i = t.length; ++n < i; ) e[t[n].toLowerCase()] = n;
    return e;
  }
  function en(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 1));
    return i ? ((t.w = +i[0]), n + i[0].length) : -1;
  }
  function nn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 1));
    return i ? ((t.u = +i[0]), n + i[0].length) : -1;
  }
  function rn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.U = +i[0]), n + i[0].length) : -1;
  }
  function an(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.V = +i[0]), n + i[0].length) : -1;
  }
  function on(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.W = +i[0]), n + i[0].length) : -1;
  }
  function sn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 4));
    return i ? ((t.y = +i[0]), n + i[0].length) : -1;
  }
  function cn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i
      ? ((t.y = +i[0] + (+i[0] > 68 ? 1900 : 2e3)), n + i[0].length)
      : -1;
  }
  function un(t, e, n) {
    var i = /^(Z)|([+-]\d\d)(?::?(\d\d))?/.exec(e.slice(n, n + 6));
    return i
      ? ((t.Z = i[1] ? 0 : -(i[2] + (i[3] || "00"))), n + i[0].length)
      : -1;
  }
  function ln(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 1));
    return i ? ((t.q = 3 * i[0] - 3), n + i[0].length) : -1;
  }
  function hn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.m = i[0] - 1), n + i[0].length) : -1;
  }
  function fn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.d = +i[0]), n + i[0].length) : -1;
  }
  function dn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 3));
    return i ? ((t.m = 0), (t.d = +i[0]), n + i[0].length) : -1;
  }
  function gn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.H = +i[0]), n + i[0].length) : -1;
  }
  function pn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.M = +i[0]), n + i[0].length) : -1;
  }
  function yn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 2));
    return i ? ((t.S = +i[0]), n + i[0].length) : -1;
  }
  function vn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 3));
    return i ? ((t.L = +i[0]), n + i[0].length) : -1;
  }
  function mn(t, e, n) {
    var i = Xe.exec(e.slice(n, n + 6));
    return i ? ((t.L = Math.floor(i[0] / 1e3)), n + i[0].length) : -1;
  }
  function xn(t, e, n) {
    var i = Ge.exec(e.slice(n, n + 1));
    return i ? n + i[0].length : -1;
  }
  function bn(t, e, n) {
    var i = Xe.exec(e.slice(n));
    return i ? ((t.Q = +i[0]), n + i[0].length) : -1;
  }
  function _n(t, e, n) {
    var i = Xe.exec(e.slice(n));
    return i ? ((t.s = +i[0]), n + i[0].length) : -1;
  }
  function wn(t, e) {
    return Je(t.getDate(), e, 2);
  }
  function kn(t, e) {
    return Je(t.getHours(), e, 2);
  }
  function Mn(t, e) {
    return Je(t.getHours() % 12 || 12, e, 2);
  }
  function Rn(t, e) {
    return Je(1 + ze.count(je(t), t), e, 3);
  }
  function Sn(t, e) {
    return Je(t.getMilliseconds(), e, 3);
  }
  function Nn(t, e) {
    return Sn(t, e) + "000";
  }
  function zn(t, e) {
    return Je(t.getMonth() + 1, e, 2);
  }
  function Cn(t, e) {
    return Je(t.getMinutes(), e, 2);
  }
  function An(t, e) {
    return Je(t.getSeconds(), e, 2);
  }
  function En(t) {
    var e = t.getDay();
    return 0 === e ? 7 : e;
  }
  function On(t, e) {
    return Je(Ae.count(je(t) - 1, t), e, 2);
  }
  function Tn(t) {
    var e = t.getDay();
    return e >= 4 || 0 === e ? Oe(t) : Oe.ceil(t);
  }
  function jn(t, e) {
    return (t = Tn(t)), Je(Oe.count(je(t), t) + (4 === je(t).getDay()), e, 2);
  }
  function Pn(t) {
    return t.getDay();
  }
  function Bn(t, e) {
    return Je(Ee.count(je(t) - 1, t), e, 2);
  }
  function Dn(t, e) {
    return Je(t.getFullYear() % 100, e, 2);
  }
  function Fn(t, e) {
    return Je((t = Tn(t)).getFullYear() % 100, e, 2);
  }
  function Ln(t, e) {
    return Je(t.getFullYear() % 1e4, e, 4);
  }
  function In(t, e) {
    var n = t.getDay();
    return Je(
      (t = n >= 4 || 0 === n ? Oe(t) : Oe.ceil(t)).getFullYear() % 1e4,
      e,
      4
    );
  }
  function Wn(t) {
    var e = t.getTimezoneOffset();
    return (
      (e > 0 ? "-" : ((e *= -1), "+")) +
      Je((e / 60) | 0, "0", 2) +
      Je(e % 60, "0", 2)
    );
  }
  function Vn(t, e) {
    return Je(t.getUTCDate(), e, 2);
  }
  function $n(t, e) {
    return Je(t.getUTCHours(), e, 2);
  }
  function Hn(t, e) {
    return Je(t.getUTCHours() % 12 || 12, e, 2);
  }
  function qn(t, e) {
    return Je(1 + Be.count(Ve(t), t), e, 3);
  }
  function Un(t, e) {
    return Je(t.getUTCMilliseconds(), e, 3);
  }
  function Yn(t, e) {
    return Un(t, e) + "000";
  }
  function Xn(t, e) {
    return Je(t.getUTCMonth() + 1, e, 2);
  }
  function Gn(t, e) {
    return Je(t.getUTCMinutes(), e, 2);
  }
  function Zn(t, e) {
    return Je(t.getUTCSeconds(), e, 2);
  }
  function Jn(t) {
    var e = t.getUTCDay();
    return 0 === e ? 7 : e;
  }
  function Kn(t, e) {
    return Je(Fe.count(Ve(t) - 1, t), e, 2);
  }
  function Qn(t) {
    var e = t.getUTCDay();
    return e >= 4 || 0 === e ? Ie(t) : Ie.ceil(t);
  }
  function ti(t, e) {
    return (
      (t = Qn(t)), Je(Ie.count(Ve(t), t) + (4 === Ve(t).getUTCDay()), e, 2)
    );
  }
  function ei(t) {
    return t.getUTCDay();
  }
  function ni(t, e) {
    return Je(Le.count(Ve(t) - 1, t), e, 2);
  }
  function ii(t, e) {
    return Je(t.getUTCFullYear() % 100, e, 2);
  }
  function ri(t, e) {
    return Je((t = Qn(t)).getUTCFullYear() % 100, e, 2);
  }
  function ai(t, e) {
    return Je(t.getUTCFullYear() % 1e4, e, 4);
  }
  function oi(t, e) {
    var n = t.getUTCDay();
    return Je(
      (t = n >= 4 || 0 === n ? Ie(t) : Ie.ceil(t)).getUTCFullYear() % 1e4,
      e,
      4
    );
  }
  function si() {
    return "+0000";
  }
  function ci() {
    return "%";
  }
  function ui(t) {
    return +t;
  }
  function li(t) {
    return Math.floor(+t / 1e3);
  }
  var hi = oe();
  hi("d3-number", function (t, e, n) {
    var i, r;
    function a(t) {
      return r(t);
    }
    function o() {
      a.locale({
        decimal: n || ".",
        thousands: e || ",",
        grouping: [3],
        currency: ["$", ""],
      });
    }
    return (
      (a.locale = function (e) {
        return (i = _e(e)), (r = i.format(t)), this;
      }),
      o(),
      (a.format = function (t, r, a, s) {
        return (a || s) && ((e = a), (n = s), o()), i.format(t)(r);
      }),
      (a.pattern = function (e) {
        return e && ((t = e), (r = i.format(e))), t;
      }),
      a
    );
  }),
    hi("d3-time", function (t) {
      var e = Ue({
          dateTime: "%x, %X",
          date: "%-m/%-d/%Y",
          time: "%-I:%M:%S %p",
          periods: ["AM", "PM"],
          days: [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
          ],
          shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          months: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
          ],
          shortMonths: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ],
        }),
        n = e.format(t);
      function i(t) {
        return n(t);
      }
      return (
        (i.format = function (t, n) {
          return e.format(t)(n);
        }),
        (i.locale = function () {
          return (e = Ue.apply(void 0, arguments)), (n = e.format(t)), this;
        }),
        (i.parse = function (t, n) {
          return e.parse(t)(n);
        }),
        (i.parsePattern = function (t) {
          return e.parse(t);
        }),
        i
      );
    });
  var fi = {
    id: function (t) {
      return "".concat(t.source, "/").concat(t.key || t.title);
    },
    key: function (t) {
      return String(t.key || t.title);
    },
    tags: function (t) {
      return t.tags;
    },
    min: function (t) {
      return t.min;
    },
    max: function (t) {
      return t.max;
    },
    type: function (t) {
      return t.type;
    },
    title: function (t) {
      return String(t.title);
    },
    values: function (t) {
      return t.values;
    },
    value: function (t) {
      return t;
    },
    label: function (t) {
      return t;
    },
    formatter: function (t) {
      return (function (t) {
        if ("function" == typeof t.formatter) return t.formatter();
        var e = t.formatter || {};
        return hi(e.type || "d3-number")(e.format || "");
      })(t);
    },
  };
  function di(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      n = e.id,
      i = void 0 === n ? fi.id : n,
      r = e.key,
      a = void 0 === r ? fi.key : r,
      o = e.min,
      s = void 0 === o ? fi.min : o,
      c = e.max,
      u = void 0 === c ? fi.max : c,
      l = e.type,
      h = void 0 === l ? fi.type : l,
      f = e.tags,
      d = void 0 === f ? fi.tags : f,
      g = e.title,
      p = void 0 === g ? fi.title : g,
      y = e.values,
      v = void 0 === y ? fi.values : y,
      m = e.value,
      x = void 0 === m ? fi.value : m,
      b = e.label,
      _ = void 0 === b ? fi.label : b,
      w = e.formatter,
      k = void 0 === w ? fi.formatter : w,
      M = {
        id: function () {
          return i(t);
        },
        key: function () {
          return a(t);
        },
        raw: function () {
          return t;
        },
        tags: function () {
          return d(t);
        },
        type: function () {
          return h(t);
        },
        min: function () {
          return s(t);
        },
        max: function () {
          return u(t);
        },
        title: function () {
          return p(t);
        },
        items: function () {
          return v(t);
        },
        formatter: function () {
          return k(t);
        },
        value: x,
        label: _,
      };
    return M;
  }
  var gi = {
      diverging: function (t, e) {
        if ((s = t.length) > 0)
          for (var n, i, r, a, o, s, c = 0, u = t[e[0]].length; c < u; ++c)
            for (a = o = 0, n = 0; n < s; ++n)
              (r = (i = t[e[n]][c])[1] - i[0]) >= 0
                ? ((i[0] = a), (i[1] = a += r))
                : r < 0
                ? ((i[1] = o), (i[0] = o += r))
                : (i[0] = a);
      },
      none: ee,
      silhouette: function (t, e) {
        if ((n = t.length) > 0) {
          for (var n, i = 0, r = t[e[0]], a = r.length; i < a; ++i) {
            for (var o = 0, s = 0; o < n; ++o) s += t[o][i][1] || 0;
            r[i][1] += r[i][0] = -s / 2;
          }
          ee(t, e);
        }
      },
      expand: function (t, e) {
        if ((i = t.length) > 0) {
          for (var n, i, r, a = 0, o = t[0].length; a < o; ++a) {
            for (r = n = 0; n < i; ++n) r += t[n][a][1] || 0;
            if (r) for (n = 0; n < i; ++n) t[n][a][1] /= r;
          }
          ee(t, e);
        }
      },
      wiggle: function (t, e) {
        if ((r = t.length) > 0 && (i = (n = t[e[0]]).length) > 0) {
          for (var n, i, r, a = 0, o = 1; o < i; ++o) {
            for (var s = 0, c = 0, u = 0; s < r; ++s) {
              for (
                var l = t[e[s]],
                  h = l[o][1] || 0,
                  f = (h - (l[o - 1][1] || 0)) / 2,
                  d = 0;
                d < s;
                ++d
              ) {
                var g = t[e[d]];
                f += (g[o][1] || 0) - (g[o - 1][1] || 0);
              }
              (c += h), (u += f * h);
            }
            (n[o - 1][1] += n[o - 1][0] = a), c && (a -= u / c);
          }
          (n[o - 1][1] += n[o - 1][0] = a), ee(t, e);
        }
      },
    },
    pi = {
      ascending: function (t) {
        var e = t.map(ae);
        return ne(t).sort(function (t, n) {
          return e[t] - e[n];
        });
      },
      insideout: function (t) {
        var e,
          n,
          i = t.length,
          r = t.map(ae),
          a = (function (t) {
            var e = t.map(re);
            return ne(t).sort(function (t, n) {
              return e[t] - e[n];
            });
          })(t),
          o = 0,
          s = 0,
          c = [],
          u = [];
        for (e = 0; e < i; ++e)
          (n = a[e]),
            o < s ? ((o += r[n]), c.push(n)) : ((s += r[n]), u.push(n));
        return u.reverse().concat(c);
      },
      none: ne,
      reverse: function (t) {
        return ne(t).reverse();
      },
    };
  function yi(t, e, n) {
    for (
      var i = {},
        r = e.stackKey,
        a = e.value,
        o = e.startProp || "start",
        s = e.endProp || "end",
        c = e.offset || "none",
        u = e.order || "none",
        l = e.valueRef || "",
        h = 0,
        f = {},
        d = 0;
      d < t.items.length;
      d++
    ) {
      var g = t.items[d],
        p = l ? g[l] : null;
      if (p && p.source) {
        var y = "".concat(p.source.key || "", "/").concat(p.source.field);
        f[y] || (f[y] = p.source);
      }
      var v = r(g);
      (i[v] = i[v] || { items: [] }),
        i[v].items.push(g),
        (h = Math.max(h, i[v].items.length));
    }
    for (
      var m = Array.apply(null, { length: h }).map(Number.call, Number),
        x = Object.keys(i).map(function (t) {
          return i[t].items;
        }),
        b = (function () {
          var t = ut([]),
            e = ne,
            n = ee,
            i = ie;
          function r(r) {
            var a,
              o,
              s = t.apply(this, arguments),
              c = r.length,
              u = s.length,
              l = new Array(u);
            for (a = 0; a < u; ++a) {
              for (
                var h, f = s[a], d = (l[a] = new Array(c)), g = 0;
                g < c;
                ++g
              )
                (d[g] = h = [0, +i(r[g], f, g, r)]), (h.data = r[g]);
              d.key = f;
            }
            for (a = 0, o = e(l); a < u; ++a) l[o[a]].index = a;
            return n(l, o), l;
          }
          return (
            (r.keys = function (e) {
              return arguments.length
                ? ((t = "function" == typeof e ? e : ut(Dt.call(e))), r)
                : t;
            }),
            (r.value = function (t) {
              return arguments.length
                ? ((i = "function" == typeof t ? t : ut(+t)), r)
                : i;
            }),
            (r.order = function (t) {
              return arguments.length
                ? ((e =
                    null == t
                      ? ne
                      : "function" == typeof t
                      ? t
                      : ut(Dt.call(t))),
                  r)
                : e;
            }),
            (r.offset = function (t) {
              return arguments.length ? ((n = null == t ? ee : t), r) : n;
            }),
            r
          );
        })()
          .keys(m)
          .value(function (t, e) {
            return t[e] ? a(t[e]) : 0;
          })
          .order(pi[u])
          .offset(gi[c]),
        _ = b(x),
        w = [],
        k = 0;
      k < _.length;
      k++
    )
      for (var M = _[k], R = 0; R < M.length; R++) {
        var S = M[R],
          N = M[R].data[M.key];
        N &&
          ((N[o] = { value: S[0] }),
          (N[s] = { value: S[1] }),
          w.push(S[0], S[1]));
      }
    var z = Object.keys(f)
        .map(function (t) {
          var e = n(f[t].key);
          return e ? e.field(f[t].field) : null;
        })
        .filter(function (t) {
          return !!t;
        }),
      C = di({
        title: z
          .map(function (t) {
            return t.title();
          })
          .join(", "),
        min: Math.min.apply(Math, w),
        max: Math.max.apply(Math, w),
        type: "measure",
        formatter: z[0] ? z[0].formatter : void 0,
      });
    t.fields.push(C);
  }
  function vi(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
      i = {},
      r = n.logger;
    if (Array.isArray(t))
      i.items = t.map(function (t) {
        return { value: t, label: String(t) };
      });
    else if (t) {
      if ("collection" in t) i = S({}, e.collection(t.collection));
      else {
        var a = e.dataset ? e.dataset(t.source) : null,
          o =
            t.value ||
            function (t) {
              return t;
            },
          s =
            t.label ||
            function (t) {
              return t;
            };
        if (t.groupBy || t.mapTo)
          r.warn('Deprecated "data" configuration', t), (i.items = []);
        else if (t.hierarchy)
          (i.root = a.hierarchy ? a.hierarchy(t.hierarchy) : null),
            (i.fields = a.fields());
        else if (t.items)
          i.items = t.items.map(function (t) {
            return { value: o(t), label: String(s(t)) };
          });
        else if (t.extract) {
          var c = Array.isArray(t.extract) ? t.extract : [t.extract];
          i.items = [];
          var u,
            l = [];
          if (
            (c.forEach(function (t) {
              var n,
                r = t.source ? e.dataset(t.source) : a;
              r &&
                ((n = i.items).push.apply(n, $(r.extract(t))),
                void 0 !== t.field && l.push(r.field(t.field)));
            }),
            l.length && (i.fields = l),
            t.amend && Array.isArray(t.amend))
          )
            (u = i.items).push.apply(u, $(t.amend));
        } else if (void 0 !== t.field && a) {
          var h = a.field(t.field);
          h &&
            (i.fields || (i.fields = []),
            i.fields.push(h),
            "value" in t ||
              ((o =
                h.value ||
                function (t) {
                  return t;
                }),
              (s =
                h.label ||
                function (t) {
                  return t;
                }),
              (i.value = o)),
            (i.items = h.items().map(function (e) {
              return {
                value: o(e),
                label: String(s(e)),
                source: { field: t.field },
              };
            })));
        } else
          t.fields &&
            t.fields.forEach(function (t) {
              var n,
                r = "object" === N(t) && t.source ? e.dataset(t.source) : a;
              r &&
                (n =
                  "object" === N(t) && void 0 !== t.field
                    ? r.field(t.field)
                    : r.field(t)) &&
                (i.fields || (i.fields = []), i.fields.push(n));
            });
        i.items && t.map && (i.items = i.items.map(t.map));
      }
      t && t.stack && yi(i, t.stack, e.dataset);
    }
    return (
      t &&
        !Array.isArray(t) &&
        "function" == typeof t.filter &&
        i.items &&
        (i.items = i.items.filter(t.filter)),
      t &&
        !Array.isArray(t) &&
        "function" == typeof t.sort &&
        i.items &&
        (i.items = i.items.sort(t.sort)),
      i
    );
  }
  function mi(t, e, n) {
    var i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : vi,
      r = {};
    (t || []).forEach(function (t) {
      if (!t.key) throw new Error('Data collection is missing "key" property');
      if ("object" === N(t.data) && "collection" in t.data)
        throw new Error(
          "Data config for collections may not reference other collections"
        );
      r[t.key] = function () {
        return i(t.data, e, n);
      };
    });
    var a = function (t) {
      var e, n;
      if (
        ("string" == typeof t
          ? (e = t)
          : "object" === N(t) && ((e = t.key), (n = t)),
        !(e in r))
      )
        throw new Error("Unknown data collection: ".concat(e));
      "function" == typeof r[e] && (r[e] = r[e]());
      var i = r[e];
      if (n && n.fields && n.fields.filter) {
        var a = i.fields.filter(n.fields.filter);
        i.fields.length !== a.length && (i = k(i, { fields: a }));
      }
      return i;
    };
    return a;
  }
  function xi(t, e, n) {
    var i,
      r = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : vi;
    if (t.data) {
      var a = r(t.data, e, n);
      if (a && a.fields && a.fields[0]) return a.fields[0].formatter();
    }
    if (
      ((i = t.formatter
        ? "".concat(t.formatter, "-").concat(t.type || "number")
        : t.type || "d3-number"),
      n.formatter.has(i))
    ) {
      var o = n.formatter.get(i)(t.format || "");
      return o;
    }
    throw new Error("Formatter of type '".concat(i, "' was not found"));
  }
  function bi(t, e, n) {
    var i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : xi,
      r = {};
    return {
      get: function (a) {
        var o;
        return (
          "string" == typeof a && t[a]
            ? (o = a)
            : "object" === N(a) && "formatter" in a && t[a.formatter]
            ? (o = a.formatter)
            : "object" === N(a) && "type" in a && t[a.type] && (o = a.type),
          o ? ((r[o] = r[o] || i(t[o], e, n)), r[o]) : i(a || {}, e, n)
        );
      },
      all: function () {
        return Object.keys(t).forEach(this.get), r;
      },
    };
  }
  function _i(t, e) {
    return t < e ? -1 : t > e ? 1 : t >= e ? 0 : NaN;
  }
  var wi,
    ki,
    Mi =
      (1 === (wi = _i).length &&
        ((ki = wi),
        (wi = function (t, e) {
          return _i(ki(t), e);
        })),
      {
        left: function (t, e, n, i) {
          for (null == n && (n = 0), null == i && (i = t.length); n < i; ) {
            var r = (n + i) >>> 1;
            wi(t[r], e) < 0 ? (n = r + 1) : (i = r);
          }
          return n;
        },
        right: function (t, e, n, i) {
          for (null == n && (n = 0), null == i && (i = t.length); n < i; ) {
            var r = (n + i) >>> 1;
            wi(t[r], e) > 0 ? (i = r) : (n = r + 1);
          }
          return n;
        },
      }),
    Ri = Mi.right;
  function Si(t, e, n) {
    (t = +t),
      (e = +e),
      (n = (r = arguments.length) < 2 ? ((e = t), (t = 0), 1) : r < 3 ? 1 : +n);
    for (
      var i = -1, r = 0 | Math.max(0, Math.ceil((e - t) / n)), a = new Array(r);
      ++i < r;

    )
      a[i] = t + i * n;
    return a;
  }
  var Ni = Math.sqrt(50),
    zi = Math.sqrt(10),
    Ci = Math.sqrt(2);
  function Ai(t, e, n) {
    var i = (e - t) / Math.max(0, n),
      r = Math.floor(Math.log(i) / Math.LN10),
      a = i / Math.pow(10, r);
    return r >= 0
      ? (a >= Ni ? 10 : a >= zi ? 5 : a >= Ci ? 2 : 1) * Math.pow(10, r)
      : -Math.pow(10, -r) / (a >= Ni ? 10 : a >= zi ? 5 : a >= Ci ? 2 : 1);
  }
  function Ei(t, e) {
    switch (arguments.length) {
      case 0:
        break;
      case 1:
        this.range(t);
        break;
      default:
        this.range(e).domain(t);
    }
    return this;
  }
  var Oi = "$";
  function Ti() {}
  function ji(t, e) {
    var n = new Ti();
    if (t instanceof Ti)
      t.each(function (t, e) {
        n.set(e, t);
      });
    else if (Array.isArray(t)) {
      var i,
        r = -1,
        a = t.length;
      if (null == e) for (; ++r < a; ) n.set(r, t[r]);
      else for (; ++r < a; ) n.set(e((i = t[r]), r, t), i);
    } else if (t) for (var o in t) n.set(o, t[o]);
    return n;
  }
  function Pi() {}
  Ti.prototype = ji.prototype = {
    constructor: Ti,
    has: function (t) {
      return Oi + t in this;
    },
    get: function (t) {
      return this[Oi + t];
    },
    set: function (t, e) {
      return (this[Oi + t] = e), this;
    },
    remove: function (t) {
      var e = Oi + t;
      return e in this && delete this[e];
    },
    clear: function () {
      for (var t in this) t[0] === Oi && delete this[t];
    },
    keys: function () {
      var t = [];
      for (var e in this) e[0] === Oi && t.push(e.slice(1));
      return t;
    },
    values: function () {
      var t = [];
      for (var e in this) e[0] === Oi && t.push(this[e]);
      return t;
    },
    entries: function () {
      var t = [];
      for (var e in this)
        e[0] === Oi && t.push({ key: e.slice(1), value: this[e] });
      return t;
    },
    size: function () {
      var t = 0;
      for (var e in this) e[0] === Oi && ++t;
      return t;
    },
    empty: function () {
      for (var t in this) if (t[0] === Oi) return !1;
      return !0;
    },
    each: function (t) {
      for (var e in this) e[0] === Oi && t(this[e], e.slice(1), this);
    },
  };
  var Bi = ji.prototype;
  Pi.prototype = {
    constructor: Pi,
    has: Bi.has,
    add: function (t) {
      return (this[Oi + (t += "")] = t), this;
    },
    remove: Bi.remove,
    clear: Bi.clear,
    values: Bi.keys,
    size: Bi.size,
    empty: Bi.empty,
    each: Bi.each,
  };
  var Di = Array.prototype,
    Fi = Di.map,
    Li = Di.slice,
    Ii = { name: "implicit" };
  function Wi() {
    var t = ji(),
      e = [],
      n = [],
      i = Ii;
    function r(r) {
      var a = r + "",
        o = t.get(a);
      if (!o) {
        if (i !== Ii) return i;
        t.set(a, (o = e.push(r)));
      }
      return n[(o - 1) % n.length];
    }
    return (
      (r.domain = function (n) {
        if (!arguments.length) return e.slice();
        (e = []), (t = ji());
        for (var i, a, o = -1, s = n.length; ++o < s; )
          t.has((a = (i = n[o]) + "")) || t.set(a, e.push(i));
        return r;
      }),
      (r.range = function (t) {
        return arguments.length ? ((n = Li.call(t)), r) : n.slice();
      }),
      (r.unknown = function (t) {
        return arguments.length ? ((i = t), r) : i;
      }),
      (r.copy = function () {
        return Wi(e, n).unknown(i);
      }),
      Ei.apply(r, arguments),
      r
    );
  }
  function Vi() {
    var t,
      e,
      n = Wi().unknown(void 0),
      i = n.domain,
      r = n.range,
      a = [0, 1],
      o = !1,
      s = 0,
      c = 0,
      u = 0.5;
    function l() {
      var n = i().length,
        l = a[1] < a[0],
        h = a[l - 0],
        f = a[1 - l];
      (t = (f - h) / Math.max(1, n - s + 2 * c)),
        o && (t = Math.floor(t)),
        (h += (f - h - t * (n - s)) * u),
        (e = t * (1 - s)),
        o && ((h = Math.round(h)), (e = Math.round(e)));
      var d = Si(n).map(function (e) {
        return h + t * e;
      });
      return r(l ? d.reverse() : d);
    }
    return (
      delete n.unknown,
      (n.domain = function (t) {
        return arguments.length ? (i(t), l()) : i();
      }),
      (n.range = function (t) {
        return arguments.length ? ((a = [+t[0], +t[1]]), l()) : a.slice();
      }),
      (n.rangeRound = function (t) {
        return (a = [+t[0], +t[1]]), (o = !0), l();
      }),
      (n.bandwidth = function () {
        return e;
      }),
      (n.step = function () {
        return t;
      }),
      (n.round = function (t) {
        return arguments.length ? ((o = !!t), l()) : o;
      }),
      (n.padding = function (t) {
        return arguments.length ? ((s = Math.min(1, (c = +t))), l()) : s;
      }),
      (n.paddingInner = function (t) {
        return arguments.length ? ((s = Math.min(1, t)), l()) : s;
      }),
      (n.paddingOuter = function (t) {
        return arguments.length ? ((c = +t), l()) : c;
      }),
      (n.align = function (t) {
        return arguments.length ? ((u = Math.max(0, Math.min(1, t))), l()) : u;
      }),
      (n.copy = function () {
        return Vi(i(), a).round(o).paddingInner(s).paddingOuter(c).align(u);
      }),
      Ei.apply(l(), arguments)
    );
  }
  function $i(t, e, n) {
    (t.prototype = e.prototype = n), (n.constructor = t);
  }
  function Hi(t, e) {
    var n = Object.create(t.prototype);
    for (var i in e) n[i] = e[i];
    return n;
  }
  function qi() {}
  var Ui = 0.7,
    Yi = 1 / Ui,
    Xi = "\\s*([+-]?\\d+)\\s*",
    Gi = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)\\s*",
    Zi = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)%\\s*",
    Ji = /^#([0-9a-f]{3,8})$/,
    Ki = new RegExp("^rgb\\(" + [Xi, Xi, Xi] + "\\)$"),
    Qi = new RegExp("^rgb\\(" + [Zi, Zi, Zi] + "\\)$"),
    tr = new RegExp("^rgba\\(" + [Xi, Xi, Xi, Gi] + "\\)$"),
    er = new RegExp("^rgba\\(" + [Zi, Zi, Zi, Gi] + "\\)$"),
    nr = new RegExp("^hsl\\(" + [Gi, Zi, Zi] + "\\)$"),
    ir = new RegExp("^hsla\\(" + [Gi, Zi, Zi, Gi] + "\\)$"),
    rr = {
      aliceblue: 15792383,
      antiquewhite: 16444375,
      aqua: 65535,
      aquamarine: 8388564,
      azure: 15794175,
      beige: 16119260,
      bisque: 16770244,
      black: 0,
      blanchedalmond: 16772045,
      blue: 255,
      blueviolet: 9055202,
      brown: 10824234,
      burlywood: 14596231,
      cadetblue: 6266528,
      chartreuse: 8388352,
      chocolate: 13789470,
      coral: 16744272,
      cornflowerblue: 6591981,
      cornsilk: 16775388,
      crimson: 14423100,
      cyan: 65535,
      darkblue: 139,
      darkcyan: 35723,
      darkgoldenrod: 12092939,
      darkgray: 11119017,
      darkgreen: 25600,
      darkgrey: 11119017,
      darkkhaki: 12433259,
      darkmagenta: 9109643,
      darkolivegreen: 5597999,
      darkorange: 16747520,
      darkorchid: 10040012,
      darkred: 9109504,
      darksalmon: 15308410,
      darkseagreen: 9419919,
      darkslateblue: 4734347,
      darkslategray: 3100495,
      darkslategrey: 3100495,
      darkturquoise: 52945,
      darkviolet: 9699539,
      deeppink: 16716947,
      deepskyblue: 49151,
      dimgray: 6908265,
      dimgrey: 6908265,
      dodgerblue: 2003199,
      firebrick: 11674146,
      floralwhite: 16775920,
      forestgreen: 2263842,
      fuchsia: 16711935,
      gainsboro: 14474460,
      ghostwhite: 16316671,
      gold: 16766720,
      goldenrod: 14329120,
      gray: 8421504,
      green: 32768,
      greenyellow: 11403055,
      grey: 8421504,
      honeydew: 15794160,
      hotpink: 16738740,
      indianred: 13458524,
      indigo: 4915330,
      ivory: 16777200,
      khaki: 15787660,
      lavender: 15132410,
      lavenderblush: 16773365,
      lawngreen: 8190976,
      lemonchiffon: 16775885,
      lightblue: 11393254,
      lightcoral: 15761536,
      lightcyan: 14745599,
      lightgoldenrodyellow: 16448210,
      lightgray: 13882323,
      lightgreen: 9498256,
      lightgrey: 13882323,
      lightpink: 16758465,
      lightsalmon: 16752762,
      lightseagreen: 2142890,
      lightskyblue: 8900346,
      lightslategray: 7833753,
      lightslategrey: 7833753,
      lightsteelblue: 11584734,
      lightyellow: 16777184,
      lime: 65280,
      limegreen: 3329330,
      linen: 16445670,
      magenta: 16711935,
      maroon: 8388608,
      mediumaquamarine: 6737322,
      mediumblue: 205,
      mediumorchid: 12211667,
      mediumpurple: 9662683,
      mediumseagreen: 3978097,
      mediumslateblue: 8087790,
      mediumspringgreen: 64154,
      mediumturquoise: 4772300,
      mediumvioletred: 13047173,
      midnightblue: 1644912,
      mintcream: 16121850,
      mistyrose: 16770273,
      moccasin: 16770229,
      navajowhite: 16768685,
      navy: 128,
      oldlace: 16643558,
      olive: 8421376,
      olivedrab: 7048739,
      orange: 16753920,
      orangered: 16729344,
      orchid: 14315734,
      palegoldenrod: 15657130,
      palegreen: 10025880,
      paleturquoise: 11529966,
      palevioletred: 14381203,
      papayawhip: 16773077,
      peachpuff: 16767673,
      peru: 13468991,
      pink: 16761035,
      plum: 14524637,
      powderblue: 11591910,
      purple: 8388736,
      rebeccapurple: 6697881,
      red: 16711680,
      rosybrown: 12357519,
      royalblue: 4286945,
      saddlebrown: 9127187,
      salmon: 16416882,
      sandybrown: 16032864,
      seagreen: 3050327,
      seashell: 16774638,
      sienna: 10506797,
      silver: 12632256,
      skyblue: 8900331,
      slateblue: 6970061,
      slategray: 7372944,
      slategrey: 7372944,
      snow: 16775930,
      springgreen: 65407,
      steelblue: 4620980,
      tan: 13808780,
      teal: 32896,
      thistle: 14204888,
      tomato: 16737095,
      turquoise: 4251856,
      violet: 15631086,
      wheat: 16113331,
      white: 16777215,
      whitesmoke: 16119285,
      yellow: 16776960,
      yellowgreen: 10145074,
    };
  function ar() {
    return this.rgb().formatHex();
  }
  function or() {
    return this.rgb().formatRgb();
  }
  function sr(t) {
    var e, n;
    return (
      (t = (t + "").trim().toLowerCase()),
      (e = Ji.exec(t))
        ? ((n = e[1].length),
          (e = parseInt(e[1], 16)),
          6 === n
            ? cr(e)
            : 3 === n
            ? new fr(
                ((e >> 8) & 15) | ((e >> 4) & 240),
                ((e >> 4) & 15) | (240 & e),
                ((15 & e) << 4) | (15 & e),
                1
              )
            : 8 === n
            ? ur(
                (e >> 24) & 255,
                (e >> 16) & 255,
                (e >> 8) & 255,
                (255 & e) / 255
              )
            : 4 === n
            ? ur(
                ((e >> 12) & 15) | ((e >> 8) & 240),
                ((e >> 8) & 15) | ((e >> 4) & 240),
                ((e >> 4) & 15) | (240 & e),
                (((15 & e) << 4) | (15 & e)) / 255
              )
            : null)
        : (e = Ki.exec(t))
        ? new fr(e[1], e[2], e[3], 1)
        : (e = Qi.exec(t))
        ? new fr((255 * e[1]) / 100, (255 * e[2]) / 100, (255 * e[3]) / 100, 1)
        : (e = tr.exec(t))
        ? ur(e[1], e[2], e[3], e[4])
        : (e = er.exec(t))
        ? ur((255 * e[1]) / 100, (255 * e[2]) / 100, (255 * e[3]) / 100, e[4])
        : (e = nr.exec(t))
        ? yr(e[1], e[2] / 100, e[3] / 100, 1)
        : (e = ir.exec(t))
        ? yr(e[1], e[2] / 100, e[3] / 100, e[4])
        : rr.hasOwnProperty(t)
        ? cr(rr[t])
        : "transparent" === t
        ? new fr(NaN, NaN, NaN, 0)
        : null
    );
  }
  function cr(t) {
    return new fr((t >> 16) & 255, (t >> 8) & 255, 255 & t, 1);
  }
  function ur(t, e, n, i) {
    return i <= 0 && (t = e = n = NaN), new fr(t, e, n, i);
  }
  function lr(t) {
    return (
      t instanceof qi || (t = sr(t)),
      t ? new fr((t = t.rgb()).r, t.g, t.b, t.opacity) : new fr()
    );
  }
  function hr(t, e, n, i) {
    return 1 === arguments.length ? lr(t) : new fr(t, e, n, null == i ? 1 : i);
  }
  function fr(t, e, n, i) {
    (this.r = +t), (this.g = +e), (this.b = +n), (this.opacity = +i);
  }
  function dr() {
    return "#" + pr(this.r) + pr(this.g) + pr(this.b);
  }
  function gr() {
    var t = this.opacity;
    return (
      (1 === (t = isNaN(t) ? 1 : Math.max(0, Math.min(1, t)))
        ? "rgb("
        : "rgba(") +
      Math.max(0, Math.min(255, Math.round(this.r) || 0)) +
      ", " +
      Math.max(0, Math.min(255, Math.round(this.g) || 0)) +
      ", " +
      Math.max(0, Math.min(255, Math.round(this.b) || 0)) +
      (1 === t ? ")" : ", " + t + ")")
    );
  }
  function pr(t) {
    return (
      ((t = Math.max(0, Math.min(255, Math.round(t) || 0))) < 16 ? "0" : "") +
      t.toString(16)
    );
  }
  function yr(t, e, n, i) {
    return (
      i <= 0
        ? (t = e = n = NaN)
        : n <= 0 || n >= 1
        ? (t = e = NaN)
        : e <= 0 && (t = NaN),
      new mr(t, e, n, i)
    );
  }
  function vr(t) {
    if (t instanceof mr) return new mr(t.h, t.s, t.l, t.opacity);
    if ((t instanceof qi || (t = sr(t)), !t)) return new mr();
    if (t instanceof mr) return t;
    var e = (t = t.rgb()).r / 255,
      n = t.g / 255,
      i = t.b / 255,
      r = Math.min(e, n, i),
      a = Math.max(e, n, i),
      o = NaN,
      s = a - r,
      c = (a + r) / 2;
    return (
      s
        ? ((o =
            e === a
              ? (n - i) / s + 6 * (n < i)
              : n === a
              ? (i - e) / s + 2
              : (e - n) / s + 4),
          (s /= c < 0.5 ? a + r : 2 - a - r),
          (o *= 60))
        : (s = c > 0 && c < 1 ? 0 : o),
      new mr(o, s, c, t.opacity)
    );
  }
  function mr(t, e, n, i) {
    (this.h = +t), (this.s = +e), (this.l = +n), (this.opacity = +i);
  }
  function xr(t, e, n) {
    return (
      255 *
      (t < 60
        ? e + ((n - e) * t) / 60
        : t < 180
        ? n
        : t < 240
        ? e + ((n - e) * (240 - t)) / 60
        : e)
    );
  }
  function br(t) {
    return function () {
      return t;
    };
  }
  function _r(t) {
    return 1 == (t = +t)
      ? wr
      : function (e, n) {
          return n - e
            ? (function (t, e, n) {
                return (
                  (t = Math.pow(t, n)),
                  (e = Math.pow(e, n) - t),
                  (n = 1 / n),
                  function (i) {
                    return Math.pow(t + i * e, n);
                  }
                );
              })(e, n, t)
            : br(isNaN(e) ? n : e);
        };
  }
  function wr(t, e) {
    var n = e - t;
    return n
      ? (function (t, e) {
          return function (n) {
            return t + n * e;
          };
        })(t, n)
      : br(isNaN(t) ? e : t);
  }
  $i(qi, sr, {
    copy: function (t) {
      return Object.assign(new this.constructor(), this, t);
    },
    displayable: function () {
      return this.rgb().displayable();
    },
    hex: ar,
    formatHex: ar,
    formatHsl: function () {
      return vr(this).formatHsl();
    },
    formatRgb: or,
    toString: or,
  }),
    $i(
      fr,
      hr,
      Hi(qi, {
        brighter: function (t) {
          return (
            (t = null == t ? Yi : Math.pow(Yi, t)),
            new fr(this.r * t, this.g * t, this.b * t, this.opacity)
          );
        },
        darker: function (t) {
          return (
            (t = null == t ? Ui : Math.pow(Ui, t)),
            new fr(this.r * t, this.g * t, this.b * t, this.opacity)
          );
        },
        rgb: function () {
          return this;
        },
        displayable: function () {
          return (
            -0.5 <= this.r &&
            this.r < 255.5 &&
            -0.5 <= this.g &&
            this.g < 255.5 &&
            -0.5 <= this.b &&
            this.b < 255.5 &&
            0 <= this.opacity &&
            this.opacity <= 1
          );
        },
        hex: dr,
        formatHex: dr,
        formatRgb: gr,
        toString: gr,
      })
    ),
    $i(
      mr,
      function (t, e, n, i) {
        return 1 === arguments.length
          ? vr(t)
          : new mr(t, e, n, null == i ? 1 : i);
      },
      Hi(qi, {
        brighter: function (t) {
          return (
            (t = null == t ? Yi : Math.pow(Yi, t)),
            new mr(this.h, this.s, this.l * t, this.opacity)
          );
        },
        darker: function (t) {
          return (
            (t = null == t ? Ui : Math.pow(Ui, t)),
            new mr(this.h, this.s, this.l * t, this.opacity)
          );
        },
        rgb: function () {
          var t = (this.h % 360) + 360 * (this.h < 0),
            e = isNaN(t) || isNaN(this.s) ? 0 : this.s,
            n = this.l,
            i = n + (n < 0.5 ? n : 1 - n) * e,
            r = 2 * n - i;
          return new fr(
            xr(t >= 240 ? t - 240 : t + 120, r, i),
            xr(t, r, i),
            xr(t < 120 ? t + 240 : t - 120, r, i),
            this.opacity
          );
        },
        displayable: function () {
          return (
            ((0 <= this.s && this.s <= 1) || isNaN(this.s)) &&
            0 <= this.l &&
            this.l <= 1 &&
            0 <= this.opacity &&
            this.opacity <= 1
          );
        },
        formatHsl: function () {
          var t = this.opacity;
          return (
            (1 === (t = isNaN(t) ? 1 : Math.max(0, Math.min(1, t)))
              ? "hsl("
              : "hsla(") +
            (this.h || 0) +
            ", " +
            100 * (this.s || 0) +
            "%, " +
            100 * (this.l || 0) +
            "%" +
            (1 === t ? ")" : ", " + t + ")")
          );
        },
      })
    );
  var kr = (function t(e) {
    var n = _r(e);
    function i(t, e) {
      var i = n((t = hr(t)).r, (e = hr(e)).r),
        r = n(t.g, e.g),
        a = n(t.b, e.b),
        o = wr(t.opacity, e.opacity);
      return function (e) {
        return (
          (t.r = i(e)), (t.g = r(e)), (t.b = a(e)), (t.opacity = o(e)), t + ""
        );
      };
    }
    return (i.gamma = t), i;
  })(1);
  function Mr(t, e) {
    e || (e = []);
    var n,
      i = t ? Math.min(e.length, t.length) : 0,
      r = e.slice();
    return function (a) {
      for (n = 0; n < i; ++n) r[n] = t[n] * (1 - a) + e[n] * a;
      return r;
    };
  }
  function Rr(t, e) {
    var n,
      i = e ? e.length : 0,
      r = t ? Math.min(i, t.length) : 0,
      a = new Array(r),
      o = new Array(i);
    for (n = 0; n < r; ++n) a[n] = Or(t[n], e[n]);
    for (; n < i; ++n) o[n] = e[n];
    return function (t) {
      for (n = 0; n < r; ++n) o[n] = a[n](t);
      return o;
    };
  }
  function Sr(t, e) {
    var n = new Date();
    return (
      (t = +t),
      (e = +e),
      function (i) {
        return n.setTime(t * (1 - i) + e * i), n;
      }
    );
  }
  function Nr(t, e) {
    return (
      (t = +t),
      (e = +e),
      function (n) {
        return t * (1 - n) + e * n;
      }
    );
  }
  function zr(t, e) {
    var n,
      i = {},
      r = {};
    for (n in ((null !== t && "object" == typeof t) || (t = {}),
    (null !== e && "object" == typeof e) || (e = {}),
    e))
      n in t ? (i[n] = Or(t[n], e[n])) : (r[n] = e[n]);
    return function (t) {
      for (n in i) r[n] = i[n](t);
      return r;
    };
  }
  var Cr = /[-+]?(?:\d+\.?\d*|\.?\d+)(?:[eE][-+]?\d+)?/g,
    Ar = new RegExp(Cr.source, "g");
  function Er(t, e) {
    var n,
      i,
      r,
      a = (Cr.lastIndex = Ar.lastIndex = 0),
      o = -1,
      s = [],
      c = [];
    for (t += "", e += ""; (n = Cr.exec(t)) && (i = Ar.exec(e)); )
      (r = i.index) > a &&
        ((r = e.slice(a, r)), s[o] ? (s[o] += r) : (s[++o] = r)),
        (n = n[0]) === (i = i[0])
          ? s[o]
            ? (s[o] += i)
            : (s[++o] = i)
          : ((s[++o] = null), c.push({ i: o, x: Nr(n, i) })),
        (a = Ar.lastIndex);
    return (
      a < e.length && ((r = e.slice(a)), s[o] ? (s[o] += r) : (s[++o] = r)),
      s.length < 2
        ? c[0]
          ? (function (t) {
              return function (e) {
                return t(e) + "";
              };
            })(c[0].x)
          : (function (t) {
              return function () {
                return t;
              };
            })(e)
        : ((e = c.length),
          function (t) {
            for (var n, i = 0; i < e; ++i) s[(n = c[i]).i] = n.x(t);
            return s.join("");
          })
    );
  }
  function Or(t, e) {
    var n,
      i = typeof e;
    return null == e || "boolean" === i
      ? br(e)
      : ("number" === i
          ? Nr
          : "string" === i
          ? (n = sr(e))
            ? ((e = n), kr)
            : Er
          : e instanceof sr
          ? kr
          : e instanceof Date
          ? Sr
          : (function (t) {
              return ArrayBuffer.isView(t) && !(t instanceof DataView);
            })(e)
          ? Mr
          : Array.isArray(e)
          ? Rr
          : ("function" != typeof e.valueOf &&
              "function" != typeof e.toString) ||
            isNaN(e)
          ? zr
          : Nr)(t, e);
  }
  function Tr(t, e) {
    return (
      (t = +t),
      (e = +e),
      function (n) {
        return Math.round(t * (1 - n) + e * n);
      }
    );
  }
  function jr(t) {
    return +t;
  }
  var Pr = [0, 1];
  function Br(t) {
    return t;
  }
  function Dr(t, e) {
    return (e -= t = +t)
      ? function (n) {
          return (n - t) / e;
        }
      : (function (t) {
          return function () {
            return t;
          };
        })(isNaN(e) ? NaN : 0.5);
  }
  function Fr(t) {
    var e,
      n = t[0],
      i = t[t.length - 1];
    return (
      n > i && ((e = n), (n = i), (i = e)),
      function (t) {
        return Math.max(n, Math.min(i, t));
      }
    );
  }
  function Lr(t, e, n) {
    var i = t[0],
      r = t[1],
      a = e[0],
      o = e[1];
    return (
      r < i ? ((i = Dr(r, i)), (a = n(o, a))) : ((i = Dr(i, r)), (a = n(a, o))),
      function (t) {
        return a(i(t));
      }
    );
  }
  function Ir(t, e, n) {
    var i = Math.min(t.length, e.length) - 1,
      r = new Array(i),
      a = new Array(i),
      o = -1;
    for (
      t[i] < t[0] && ((t = t.slice().reverse()), (e = e.slice().reverse()));
      ++o < i;

    )
      (r[o] = Dr(t[o], t[o + 1])), (a[o] = n(e[o], e[o + 1]));
    return function (e) {
      var n = Ri(t, e, 1, i) - 1;
      return a[n](r[n](e));
    };
  }
  function Wr(t, e) {
    return e
      .domain(t.domain())
      .range(t.range())
      .interpolate(t.interpolate())
      .clamp(t.clamp())
      .unknown(t.unknown());
  }
  function Vr(t, e) {
    return (function () {
      var t,
        e,
        n,
        i,
        r,
        a,
        o = Pr,
        s = Pr,
        c = Or,
        u = Br;
      function l() {
        return (
          (i = Math.min(o.length, s.length) > 2 ? Ir : Lr), (r = a = null), h
        );
      }
      function h(e) {
        return isNaN((e = +e)) ? n : (r || (r = i(o.map(t), s, c)))(t(u(e)));
      }
      return (
        (h.invert = function (n) {
          return u(e((a || (a = i(s, o.map(t), Nr)))(n)));
        }),
        (h.domain = function (t) {
          return arguments.length
            ? ((o = Fi.call(t, jr)), u === Br || (u = Fr(o)), l())
            : o.slice();
        }),
        (h.range = function (t) {
          return arguments.length ? ((s = Li.call(t)), l()) : s.slice();
        }),
        (h.rangeRound = function (t) {
          return (s = Li.call(t)), (c = Tr), l();
        }),
        (h.clamp = function (t) {
          return arguments.length ? ((u = t ? Fr(o) : Br), h) : u !== Br;
        }),
        (h.interpolate = function (t) {
          return arguments.length ? ((c = t), l()) : c;
        }),
        (h.unknown = function (t) {
          return arguments.length ? ((n = t), h) : n;
        }),
        function (n, i) {
          return (t = n), (e = i), l();
        }
      );
    })()(t, e);
  }
  function $r(t, e, n, i) {
    var r,
      a = (function (t, e, n) {
        var i = Math.abs(e - t) / Math.max(0, n),
          r = Math.pow(10, Math.floor(Math.log(i) / Math.LN10)),
          a = i / r;
        return (
          a >= Ni ? (r *= 10) : a >= zi ? (r *= 5) : a >= Ci && (r *= 2),
          e < t ? -r : r
        );
      })(t, e, n);
    switch ((i = he(null == i ? ",f" : i)).type) {
      case "s":
        var o = Math.max(Math.abs(t), Math.abs(e));
        return (
          null != i.precision ||
            isNaN(
              (r = (function (t, e) {
                return Math.max(
                  0,
                  3 * Math.max(-8, Math.min(8, Math.floor(ce(e) / 3))) -
                    ce(Math.abs(t))
                );
              })(a, o))
            ) ||
            (i.precision = r),
          me(i, o)
        );
      case "":
      case "e":
      case "g":
      case "p":
      case "r":
        null != i.precision ||
          isNaN(
            (r = (function (t, e) {
              return (
                (t = Math.abs(t)),
                (e = Math.abs(e) - t),
                Math.max(0, ce(e) - ce(t)) + 1
              );
            })(a, Math.max(Math.abs(t), Math.abs(e))))
          ) ||
          (i.precision = r - ("e" === i.type));
        break;
      case "f":
      case "%":
        null != i.precision ||
          isNaN(
            (r = (function (t) {
              return Math.max(0, -ce(Math.abs(t)));
            })(a))
          ) ||
          (i.precision = r - 2 * ("%" === i.type));
    }
    return ve(i);
  }
  function Hr(t) {
    var e = t.domain;
    return (
      (t.ticks = function (t) {
        var n = e();
        return (function (t, e, n) {
          var i,
            r,
            a,
            o,
            s = -1;
          if (((n = +n), (t = +t) == (e = +e) && n > 0)) return [t];
          if (
            ((i = e < t) && ((r = t), (t = e), (e = r)),
            0 === (o = Ai(t, e, n)) || !isFinite(o))
          )
            return [];
          if (o > 0)
            for (
              t = Math.ceil(t / o),
                e = Math.floor(e / o),
                a = new Array((r = Math.ceil(e - t + 1)));
              ++s < r;

            )
              a[s] = (t + s) * o;
          else
            for (
              t = Math.floor(t * o),
                e = Math.ceil(e * o),
                a = new Array((r = Math.ceil(t - e + 1)));
              ++s < r;

            )
              a[s] = (t - s) / o;
          return i && a.reverse(), a;
        })(n[0], n[n.length - 1], null == t ? 10 : t);
      }),
      (t.tickFormat = function (t, n) {
        var i = e();
        return $r(i[0], i[i.length - 1], null == t ? 10 : t, n);
      }),
      (t.nice = function (n) {
        null == n && (n = 10);
        var i,
          r = e(),
          a = 0,
          o = r.length - 1,
          s = r[a],
          c = r[o];
        return (
          c < s && ((i = s), (s = c), (c = i), (i = a), (a = o), (o = i)),
          (i = Ai(s, c, n)) > 0
            ? (i = Ai(
                (s = Math.floor(s / i) * i),
                (c = Math.ceil(c / i) * i),
                n
              ))
            : i < 0 &&
              (i = Ai(
                (s = Math.ceil(s * i) / i),
                (c = Math.floor(c * i) / i),
                n
              )),
          i > 0
            ? ((r[a] = Math.floor(s / i) * i),
              (r[o] = Math.ceil(c / i) * i),
              e(r))
            : i < 0 &&
              ((r[a] = Math.ceil(s * i) / i),
              (r[o] = Math.floor(c * i) / i),
              e(r)),
          t
        );
      }),
      t
    );
  }
  function qr() {
    var t = Vr(Br, Br);
    return (
      (t.copy = function () {
        return Wr(t, qr());
      }),
      Ei.apply(t, arguments),
      Hr(t)
    );
  }
  function Ur() {
    var t,
      e = [0.5],
      n = [0, 1],
      i = 1;
    function r(r) {
      return r <= r ? n[Ri(e, r, 0, i)] : t;
    }
    return (
      (r.domain = function (t) {
        return arguments.length
          ? ((e = Li.call(t)), (i = Math.min(e.length, n.length - 1)), r)
          : e.slice();
      }),
      (r.range = function (t) {
        return arguments.length
          ? ((n = Li.call(t)), (i = Math.min(e.length, n.length - 1)), r)
          : n.slice();
      }),
      (r.invertExtent = function (t) {
        var i = n.indexOf(t);
        return [e[i - 1], e[i]];
      }),
      (r.unknown = function (e) {
        return arguments.length ? ((t = e), r) : t;
      }),
      (r.copy = function () {
        return Ur().domain(e).range(n).unknown(t);
      }),
      Ei.apply(r, arguments)
    );
  }
  function Yr(t) {
    return void 0 === t
      ? function (t) {
          return t;
        }
      : function (e) {
          return t(e);
        };
  }
  function Xr(t) {
    return Math.max(0, Math.min(1, t));
  }
  function Gr(t) {
    return "object" === N(t);
  }
  function Zr(t, e, n) {
    for (var i = Math.abs(e - n) / (t + 1), r = [], a = 1; a <= t; a++) {
      var o = a * i;
      r.push(e < n ? e + o : e - o);
    }
    return r;
  }
  function Jr(t, e, n) {
    if (1 === t.length) return t;
    for (var i = t.concat([]), r = 0; r < t.length; r++) {
      var a = t[r],
        o = t[r + 1];
      0 === r && a !== n.start()
        ? (i.push.apply(i, $(Zr(e, a, o))),
          (a -= o - a),
          (o = t[r]),
          i.push.apply(i, $(Zr(e, a, o))))
        : r === t.length - 1 && o !== n.end()
        ? ((o = a + (a - t[r - 1])), i.push.apply(i, $(Zr(e, a, o))))
        : i.push.apply(i, $(Zr(e, a, o)));
    }
    return i.filter(function (t) {
      return t >= n.min() && t <= n.max();
    });
  }
  function Kr(t) {
    var e = t.distance,
      n = t.scale,
      i = t.minorCount,
      r = void 0 === i ? 0 : i,
      a = t.unitDivider,
      o = void 0 === a ? 100 : a,
      s = t.formatter,
      c = void 0 === s ? void 0 : s,
      u = X(o) || X(e) ? 2 : Math.max(e / o, 2),
      l = Math.min(1e3, Math.round(u)),
      h = n.ticks(l);
    h.length <= 1 && (h = n.ticks(l + 1));
    var f = r > 0 ? Jr(h, r, n) : h;
    f.sort(function (t, e) {
      return t - e;
    });
    var d = f.map(Yr(c));
    return f.map(function (t, e) {
      var i = n(t);
      return {
        position: i,
        start: i,
        end: i,
        label: d[e],
        value: t,
        isMinor: -1 === h.indexOf(t),
      };
    });
  }
  function Qr(t) {
    var e = t.distance,
      n = t.scale,
      i = t.minorCount,
      r = void 0 === i ? 0 : i,
      a = t.unitDivider,
      o = void 0 === a ? 100 : a,
      s = t.formatter,
      c = void 0 === s ? void 0 : s,
      u = X(o) || X(e) ? 2 : Math.max(e / o, 2),
      l = Math.min(1e3, Math.round(u)),
      h = l > 10 ? 10 : l;
    n.nice(h);
    var f = n.ticks(l),
      d = r > 0 ? Jr(f, r, n) : f;
    d.sort(function (t, e) {
      return t - e;
    });
    var g = d.map(Yr(c));
    return d.map(function (t, e) {
      var i = n(t);
      return {
        position: i,
        start: i,
        end: i,
        label: g[e],
        value: t,
        isMinor: -1 === f.indexOf(t),
      };
    });
  }
  function ta(t) {
    var e,
      n = t.settings,
      i = t.scale,
      r = t.distance,
      a = t.formatter,
      o =
        void 0 === a
          ? function (t) {
              return t;
            }
          : a,
      s =
        n.minorTicks && !X(n.minorTicks.count)
          ? Math.min(100, n.minorTicks.count)
          : 0;
    if (Array.isArray(n.ticks.values)) {
      var c = n.ticks.values.filter(function (t) {
        return "object" === N(t) ? !X(t.value) : !X(t);
      });
      e = (function (t) {
        var e = t.values,
          n = t.scale,
          i = t.formatter,
          r =
            void 0 === i
              ? function (t) {
                  return t;
                }
              : i;
        return e
          .sort(function (t, e) {
            return (Gr(t) ? t.value : t) - (Gr(e) ? e.value : e);
          })
          .filter(function (t, e, i) {
            var r = Gr(t) ? t.value : t;
            return r <= n.max() && r >= n.min() && i.indexOf(t) === e;
          })
          .map(function (t) {
            var e = Gr(t),
              i = e ? t.value : t,
              a = n(i);
            return {
              position: a,
              value: i,
              label: e && void 0 !== t.label ? t.label : r(i),
              isMinor: !!e && !!t.isMinor,
              start: e && !isNaN(t.start) ? Xr(n(t.start)) : a,
              end: e && !isNaN(t.end) ? Xr(n(t.end)) : a,
            };
          });
      })({ values: c, scale: i.copy(), formatter: o });
    } else if (X(n.ticks.count)) {
      (e = (n.ticks.tight ? Qr : Kr)({
        distance: r,
        minorCount: s,
        unitDivider: n.ticks.distance,
        scale: i,
        formatter: o,
      })),
        n.ticks.forceBounds &&
          (function (t, e, n) {
            var i = t.map(function (t) {
                return t.position;
              }),
              r = e.range();
            -1 === i.indexOf(r[0])
              ? t.splice(0, 0, {
                  position: r[0],
                  start: r[0],
                  end: r[0],
                  label: n(e.start()),
                  isMinor: !1,
                  value: e.start(),
                })
              : t[0] && t[0].isMinor && (t[0].isMinor = !1);
            var a = t[t.length - 1];
            -1 === i.indexOf(r[1])
              ? t.push({
                  position: r[1],
                  start: r[1],
                  end: r[1],
                  label: n(e.end()),
                  isMinor: !1,
                  value: e.end(),
                })
              : a && a.isMinor && (a.isMinor = !1);
          })(e, i, o);
    } else {
      e = (function (t) {
        var e = t.count,
          n = t.minorCount,
          i = t.scale,
          r = t.formatter;
        return i.ticks((e - 1) * n + e).map(function (t, e) {
          var a = i(t);
          return {
            position: a,
            start: a,
            end: a,
            label: r(t),
            isMinor: e % (n + 1) != 0,
            value: t,
          };
        });
      })({
        count: Math.min(1e3, n.ticks.count),
        minorCount: s,
        scale: i.copy(),
        formatter: o,
      });
    }
    return e;
  }
  function ea(t) {
    var e = t.scale,
      n = e.domain(),
      i = e.data().items,
      r = e.labels ? e.labels() : n,
      a = e.bandwidth();
    return n.map(function (t, n) {
      var o = e(t);
      return {
        position: o + a / 2,
        label: "".concat(r[n]),
        data: i ? i[n] : void 0,
        start: o,
        end: o + a,
      };
    });
  }
  function na() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
      i = {};
    return (
      Object.keys(e).forEach(function (r) {
        var a = N(t[r]);
        i[r] = "function" === a ? t[r](n) : "undefined" === a ? e[r] : t[r];
      }),
      i
    );
  }
  var ia = { min: NaN, max: NaN, expand: NaN, include: [], invert: !1 },
    ra = {
      tight: !1,
      forceBounds: !1,
      values: void 0,
      count: NaN,
      distance: 100,
    },
    aa = { count: NaN };
  function oa(t, e) {
    var n = +t.min,
      i = +t.max,
      r = 0,
      a = 1;
    if (e && e[0]) {
      var o = e
          .map(function (t) {
            return t.min();
          })
          .filter(function (t) {
            return !isNaN(t);
          }),
        s = e
          .map(function (t) {
            return t.max();
          })
          .filter(function (t) {
            return !isNaN(t);
          });
      if (
        ((r = o.length ? Math.min.apply(Math, $(o)) : Number.NaN),
        (a = s.length ? Math.max.apply(Math, $(s)) : Number.NaN),
        isNaN(r) || isNaN(a))
      )
        (r = -1), (a = 1);
      else if (r === a && 0 === r) (r = -1), (a = 1);
      else if (r === a && r) (r -= Math.abs(0.1 * r)), (a += Math.abs(0.1 * a));
      else if (!isNaN(t.expand)) {
        var c = a - r;
        (r -= c * t.expand), (a += c * t.expand);
      }
      if (Array.isArray(t.include)) {
        var u = t.include.filter(function (t) {
          return !isNaN(t);
        });
        (r = Math.min.apply(Math, $(u).concat([r]))),
          (a = Math.max.apply(Math, $(u).concat([a])));
      }
    }
    return { mini: isNaN(n) ? r : n, maxi: isNaN(i) ? a : i };
  }
  function sa(t, e) {
    t.instance ||
      ((t.instance = e.copy()),
      t.instance.domain([e.start(), e.end()]),
      t.instance.clamp(!0),
      t.instance.range(t.invert ? [1, 0] : [0, 1]));
  }
  function ca() {
    var t,
      e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      i = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
      r = qr(),
      a = { instance: null, invert: !1 },
      o = { data: n, resources: i },
      s = na(e, ia, o);
    function c(t) {
      return X(t) ? NaN : r(t);
    }
    (s.ticks = na(e.ticks, ra, o)),
      (s.minorTicks = na(e.minorTicks, aa, o)),
      (c.data = function () {
        return n;
      }),
      (c.invert = function (t) {
        return r.invert(t);
      }),
      (c.rangeRound = function (t) {
        return r.rangeRound(t), c;
      }),
      (c.clamp = function () {
        var t =
          !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
        return r.clamp(t), c;
      }),
      (c.cachedTicks = function () {
        return t;
      }),
      (c.clearTicksCache = function () {
        return (t = void 0), this;
      }),
      (c.ticks = function (e) {
        return null !== e && "object" === N(e)
          ? ((e.settings = e.settings || {}),
            (e.settings = k(!0, {}, s, e.settings)),
            (e.scale = c),
            (t = ta(e)))
          : (t = r.ticks(e));
      }),
      (c.nice = function (t) {
        return r.nice(t), c;
      }),
      (c.tickFormat = function (t, e) {
        return r.tickFormat(t, e);
      }),
      (c.interpolate = function (t) {
        return r.interpolate(t), c;
      }),
      (c.domain = function (t) {
        return arguments.length
          ? (r.domain(t),
            a.instance && a.instance.domain([c.start(), c.end()]),
            c)
          : r.domain();
      }),
      (c.range = function (t) {
        return arguments.length ? (r.range(t), c) : r.range();
      }),
      (c.start = function () {
        return c.domain()[0];
      }),
      (c.end = function () {
        return c.domain()[this.domain().length - 1];
      }),
      (c.min = function () {
        return Math.min(this.start(), this.end());
      }),
      (c.max = function () {
        return Math.max(this.start(), this.end());
      }),
      (c.classify = function (t) {
        for (
          var e = (c.start() - c.end()) / t,
            n = [c.end()],
            i = [],
            r = e / 2,
            a = 0;
          a < t;
          a++
        ) {
          var o = n[n.length - 1] || 0,
            s = o + e,
            u = c(o + r);
          n.push.apply(n, [s, s]), i.push.apply(i, [u, u]);
        }
        return n.pop(), c.domain(n), c.range(i), c;
      }),
      (c.copy = function () {
        var t = ca(e, n, i);
        return t.domain(c.domain()), t.range(c.range()), t.clamp(r.clamp()), t;
      }),
      (c.norm = function (t) {
        return sa(a, c), a.instance(t);
      }),
      (c.normInvert = function (t) {
        return sa(a, c), a.instance.invert(t);
      });
    var u = oa(s, n ? n.fields : []),
      l = u.mini,
      h = u.maxi;
    return (
      c.domain([l, h]),
      c.range(s.invert ? [1, 0] : [0, 1]),
      (a.invert = s.invert),
      c
    );
  }
  var ua = {
    padding: 0,
    paddingInner: NaN,
    paddingOuter: NaN,
    align: 0.5,
    invert: !1,
    maxPxStep: NaN,
    range: [0, 1],
  };
  function la() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
      i = { data: e, resources: n },
      r = na(t, ua, i),
      a = e.items || [],
      o = {},
      s = [],
      c = [];
    function u(t, n) {
      (t.data = function () {
        return e;
      }),
        (t.datum = function (t) {
          return a[o[t]];
        }),
        (t.start = function () {
          return t.domain()[0];
        }),
        (t.end = function () {
          return t.domain()[t.domain().length - 1];
        }),
        (t.labels = function () {
          return c;
        }),
        (t.ticks = function () {
          var e =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
          return (e.scale = t), ea(e, n.trackBy);
        });
    }
    var l = Vi();
    u(l, t),
      (l.pxScale = function (e) {
        var n = r.maxPxStep;
        if (isNaN(n)) return l;
        var i = l.domain().length,
          a = Math.max(1, i - l.paddingInner() + 2 * l.paddingOuter());
        if (a * n >= e) return l;
        var o = l.copy();
        (o.type = l.type), u(o, t);
        var s = (a * n) / e,
          c = (1 - s) * l.align();
        return o.range(r.invert ? [s + c, c] : [c, s + c]), o;
      });
    for (
      var h =
          "function" == typeof t.value
            ? t.value
            : function (t) {
                return t.datum.value;
              },
        f =
          "function" == typeof t.label
            ? t.label
            : function (t) {
                return t.datum.label;
              },
        d = 0;
      d < a.length;
      d++
    ) {
      var g = k({ datum: a[d] }, i),
        p = h(g, d);
      -1 === s.indexOf(p) && (s.push(p), c.push(f(g, d)), (o[p] = d));
    }
    return (
      l.domain(s),
      l.range(r.invert ? r.range.slice().reverse() : r.range),
      l.padding(isNaN(r.padding) ? 0 : r.padding),
      isNaN(r.paddingInner) || l.paddingInner(r.paddingInner),
      isNaN(r.paddingOuter) || l.paddingOuter(r.paddingOuter),
      l.align(isNaN(r.align) ? 0.5 : r.align),
      l
    );
  }
  var ha = { depth: 0 };
  function fa(t, e, n) {
    return t
      .ancestors()
      .map(function (t) {
        return e(k({ datum: t.data }, n));
      })
      .reverse()
      .slice(1)
      .toString();
  }
  function da(t, e, n) {
    var i = e.ticks.depth,
      r = e.value,
      a = e.label,
      o = [],
      s = [],
      c = {},
      u = [],
      l = 0;
    if (!t) return { values: o, labels: s, items: c, ticks: u };
    t.eachAfter(function (t) {
      if (t.depth > 0) {
        var e = fa(t, r, n),
          h = t.leaves() || [t],
          f = r(k({ datum: t.data }, n)),
          d = a(k({ datum: t.data }, n)),
          g = Array.isArray(t.children),
          p = {
            key: e,
            count: h.length,
            value: f,
            label: d,
            leftEdge: fa(h[0], r, n),
            rightEdge: fa(h[Math.max(h.length - 1, 0)], r, n),
            node: t,
          };
        g
          ? (o.push("SPACER_".concat(l, "_SPACER")), l++)
          : (o.push(e), s.push(d)),
          ((i <= 0 && !g) || t.depth === i) && u.push(p),
          (c[e] = p);
      }
    });
    var h = t.height - 1;
    return h > 0 && o.splice(-h), { values: o, labels: s, items: c, ticks: u };
  }
  var ga = function (t) {
      return t.min();
    },
    pa = function (t) {
      return t.max();
    };
  function ya(t, e) {
    var n = t && !isNaN(t.min),
      i = t && !isNaN(t.max),
      r = n ? +t.min : 0,
      a = i ? +t.max : 1;
    if (e && e.length) {
      if (!n) {
        var o = e.map(ga).filter(Y);
        r = o.length ? Math.min.apply(Math, $(o)) : r;
      }
      if (!i) {
        var s = e.map(pa).filter(Y);
        a = s.length ? Math.max.apply(Math, $(s)) : a;
      }
    }
    return [r, a];
  }
  var va = { domain: [], range: [], invert: !1, min: NaN, max: NaN };
  function ma(t, e, n) {
    var i = t.length;
    if (2 === i) return [e, n];
    var r = [],
      a = (n - e) / (i - 1);
    r.push(e);
    for (var o = 1; o < i - 1; o++) r.push(e + a * o);
    return r.push(n), r;
  }
  function xa() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
      i = ca(t, e, n).clamp(!0).interpolate(kr),
      r = na(t, va, { data: e, resources: n }),
      a = Array.isArray(r.domain) && r.domain.length,
      o = Array.isArray(r.range) && r.range.length,
      s = i;
    k(!0, s, i);
    var c = ya(r, e ? e.fields : []),
      u = V(c, 2),
      l = u[0],
      h = u[1],
      f = a ? r.domain.length : -1,
      d = n.theme ? n.theme.palette("sequential", f > 0 ? f : 2) : [],
      g = o ? r.range : d;
    return (
      s.range(r.invert ? g.slice().reverse() : g.slice()),
      s.domain(a ? r.domain : ma(s.range(), l, h)),
      s
    );
  }
  var ba = { domain: [], range: [], invert: !1, min: NaN, max: NaN, nice: !1 };
  function _a(t, e, n) {
    var i = t.length;
    if (2 === i) return [e + (n - e) / 2];
    for (var r = [], a = (n - e) / i, o = 1; o < i; o++) r.push(e + a * o);
    return r;
  }
  function wa(t, e, n, i) {
    (n = t[0]), (i = t && t.length >= 2 ? t[t.length - 1] : i);
    var r = xa().domain([n, i]).range(e),
      a = [n].concat(
        $(
          (function (t) {
            for (var e = [], n = 0; n < t.length - 1; n++)
              e.push((t[n] + t[n + 1]) / 2);
            return e;
          })(t)
        ),
        [i]
      );
    return a.map(function (t) {
      return r(t);
    });
  }
  function ka(t, e, n) {
    var i = 2 === t.length ? 10 : Math.max(1, t.length),
      r = qr().domain([e, n]).nice(i).ticks(i);
    if (!t || !t.length) return r;
    for (var a = Math.max(0, t.length - 1); r.length > a; )
      r[0] - e <= n - r[r.length - 1] ? r.shift() : r.pop();
    return r;
  }
  var Ma = { domain: [], range: [] };
  function Ra() {
    for (
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
        i = Wi(),
        r = { data: e, resources: n },
        a = na(t, Ma, r),
        o =
          "function" == typeof t.value
            ? t.value
            : function (t) {
                return t.datum.value;
              },
        s =
          "function" == typeof t.label
            ? t.label
            : function (t) {
                return t.datum.label;
              },
        c = e.items || [],
        u = {},
        l = [],
        h = [],
        f = 0;
      f < c.length;
      f++
    ) {
      var d = k({ datum: c[f] }, r),
        g = o(d, f);
      -1 === l.indexOf(g) && (l.push(g), h.push(s(d, f)), (u[g] = f));
    }
    return (
      (i.data = function () {
        return e;
      }),
      (i.labels = function () {
        return h;
      }),
      (i.label = function (t) {
        return h[l.indexOf(t)];
      }),
      (i.datum = function (t) {
        return c[u[t]];
      }),
      i.range(a.range),
      Array.isArray(a.domain) && a.domain.length
        ? i.domain(a.domain)
        : i.domain(l),
      i
    );
  }
  var Sa = { domain: [], range: [], unknown: void 0 },
    Na = { domain: [], range: [], override: !1 };
  var za = oe();
  function Ca(t, e, n) {
    var i = t.data;
    t.source &&
      (n.logger.warn("Deprecated: Scale data source configuration"),
      (i = { extract: [] }),
      (Array.isArray(t.source) ? t.source : [t.source]).forEach(function (t) {
        i.extract.push({ field: t });
      }));
    var r,
      a = vi(i, e, n),
      o =
        t.type ||
        (function (t) {
          return t.root
            ? "h-band"
            : t.fields && t.fields[0]
            ? ((e = t.fields),
              (n = e.map(function (t) {
                return "dimension" === t.type() ? "band" : "linear";
              })),
              -1 !== n.indexOf("linear") ? "linear" : "band")
            : "linear";
          var e, n;
        })(a);
    return (
      "color" === o &&
        (o =
          a.fields && a.fields[0] && "dimension" === a.fields[0].type()
            ? "categorical-color"
            : "sequential-color"),
      n.scale.has(o) &&
        ((r = (r = n.scale.get(o))(t, a, {
          theme: n.theme,
          logger: n.logger,
        })).type = o),
      r
    );
  }
  function Aa(t, e, n) {
    var i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : Ca,
      r = {};
    return {
      get: function (a) {
        var o;
        return (
          "string" == typeof a && t[a]
            ? (o = a)
            : "object" === N(a) && "scale" in a && t[a.scale] && (o = a.scale),
          o ? ((r[o] = r[o] || i(t[o], e, n)), r[o]) : i(a, e, n)
        );
      },
      all: function () {
        return Object.keys(t).forEach(this.get), r;
      },
    };
  }
  za("linear", ca),
    za("band", la),
    za("h-band", function t() {
      var e =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        i = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
        r = { data: n, resources: i },
        a = na(e, ua, r);
      (a.ticks = na(e.ticks, ha, r)),
        (a.value =
          "function" == typeof e.value
            ? e.value
            : function (t) {
                return t.datum.value;
              }),
        (a.label =
          "function" == typeof e.label
            ? e.label
            : function (t) {
                return t.datum.value;
              });
      var o = la(a),
        s = da(n.root, a, r),
        c = s.values,
        u = s.labels,
        l = s.items,
        h = s.ticks,
        f = function (t) {
          var e = String(t),
            n = l[e];
          return o(n ? (a.invert ? n.rightEdge : n.leftEdge) : e);
        };
      k(!0, f, o),
        (f.bandwidth = function (t) {
          var e = l[String(t)],
            n = o.bandwidth();
          if (e && !e.isLeaf) {
            var i = f(e.leftEdge),
              r = f(e.rightEdge);
            return Math.abs(i - r) + n;
          }
          return n;
        }),
        (f.step = function (t) {
          var e = l[String(t)],
            n = e ? e.count : 1,
            i = o.step();
          return (i *= n);
        }),
        (f.data = function () {
          return n;
        }),
        (f.datum = function (t) {
          var e = l[String(t)];
          return e ? e.node.data : null;
        }),
        (f.copy = function () {
          return t(e, n, i);
        }),
        (f.labels = function () {
          return u;
        }),
        (f.ticks = function () {
          return h.map(function (t) {
            var e = f(t.key),
              n = f.bandwidth(t.key);
            return {
              position: e + n / 2,
              label: t.label,
              data: t.node.data,
              start: e,
              end: e + n,
            };
          });
        });
      var d = o.pxScale;
      return (
        (f.pxScale = function (t) {
          return (o = d(t)), f;
        }),
        f.domain(c),
        f
      );
    }),
    za("sequential-color", xa),
    za("threshold-color", function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
        i = Ur(),
        r = na(t, ba, { data: e, resources: n }),
        a = Array.isArray(r.domain) && r.domain.length,
        o = Array.isArray(r.range) && r.range.length;
      function s(t) {
        return X(t) ? NaN : i(t);
      }
      Object.keys(i).forEach(function (t) {
        return (s[t] = i[t]);
      });
      var c = e.fields,
        u = ya(r, c),
        l = V(u, 2),
        h = l[0],
        f = l[1],
        d = a ? r.domain.length : -1,
        g = n.theme ? n.theme.palette("sequential", d > 0 ? d : 2) : [],
        p = o ? r.range : g,
        y = [];
      return (
        (y = a ? r.domain : r.nice ? ka(p, h, f) : [h + (f - h) / 2]),
        p.length > y.length + 1
          ? (y = _a(p, h, f))
          : p.length < y.length + 1 && (p = wa(y, p, h, f)),
        (s.data = function () {
          return e;
        }),
        s.range(r.invert ? p.slice().reverse() : p),
        s.domain(y),
        s
      );
    }),
    za("categorical-color", function () {
      var t,
        e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        i = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
        r = Ra(e, n, i),
        a = i.theme,
        o = na(e, Sa, { data: n, resources: i });
      if (
        ((o.explicit = na(e.explicit, Na, { data: n, resources: i })),
        (t =
          Array.isArray(o.range) && 0 !== o.range.length
            ? o.range.slice()
            : a
            ? a.palette("categorical", r.domain().length).slice()
            : []),
        o.unknown)
      )
        r.unknown(o.unknown);
      else if (a && a.palette("unknown")) {
        var s = a.palette("unknown");
        r.unknown(s[0]);
      }
      if (Array.isArray(o.explicit.domain) && o.explicit.domain.length) {
        for (
          var c = r.domain().slice(),
            u = o.explicit.domain,
            l = Array.isArray(o.explicit.range) ? o.explicit.range : [],
            h = Math.floor(c.length / t.length),
            f = 1;
          f < h + 1;
          f *= 2
        )
          t = t.concat(t);
        if (o.explicit.override)
          for (var d = 0; d < u.length; d++) {
            var g = c.indexOf(u[d]);
            g > -1 && (t[g] = l[d]);
          }
        else {
          var p = u
            .map(function (t, e) {
              return [c.indexOf(t), t, l[e]];
            })
            .sort(function (t, e) {
              return t[0] - e[0];
            });
          p.forEach(function (e) {
            var n = c.indexOf(e[1]);
            -1 !== n && t.splice(n, 0, e[2]);
          });
        }
        t.length = c.length;
      }
      return r.range(t), r;
    });
  var Ea = {};
  function Oa() {
    Oa.init.call(this);
  }
  (Ea.isObject = function (t) {
    return "object" == typeof t && null !== t;
  }),
    (Ea.isNumber = function (t) {
      return "number" == typeof t;
    }),
    (Ea.isUndefined = function (t) {
      return void 0 === t;
    }),
    (Ea.isFunction = function (t) {
      return "function" == typeof t;
    });
  var Ta = Oa;
  (Oa.EventEmitter = Oa),
    (Oa.prototype._events = void 0),
    (Oa.prototype._maxListeners = void 0),
    (Oa.defaultMaxListeners = 10),
    (Oa.init = function () {
      (this._events = this._events || {}),
        (this._maxListeners = this._maxListeners || void 0);
    }),
    (Oa.prototype.setMaxListeners = function (t) {
      if (!Ea.isNumber(t) || t < 0 || isNaN(t))
        throw TypeError("n must be a positive number");
      return (this._maxListeners = t), this;
    }),
    (Oa.prototype.emit = function (t) {
      var e, n, i, r, a, o;
      if (
        (this._events || (this._events = {}),
        "error" === t && !this._events.error)
      )
        throw (e = arguments[1]) instanceof Error
          ? e
          : Error('Uncaught, unspecified "error" event.');
      if (((n = this._events[t]), Ea.isUndefined(n))) return !1;
      if (Ea.isFunction(n))
        switch (arguments.length) {
          case 1:
            n.call(this);
            break;
          case 2:
            n.call(this, arguments[1]);
            break;
          case 3:
            n.call(this, arguments[1], arguments[2]);
            break;
          default:
            for (i = arguments.length, r = new Array(i - 1), a = 1; a < i; a++)
              r[a - 1] = arguments[a];
            n.apply(this, r);
        }
      else if (Ea.isObject(n)) {
        for (i = arguments.length, r = new Array(i - 1), a = 1; a < i; a++)
          r[a - 1] = arguments[a];
        for (i = (o = n.slice()).length, a = 0; a < i; a++) o[a].apply(this, r);
      }
      return !0;
    }),
    (Oa.prototype.addListener = function (t, e) {
      var n;
      if (!Ea.isFunction(e)) throw TypeError("listener must be a function");
      (this._events || (this._events = {}),
      this._events.newListener &&
        this.emit("newListener", t, Ea.isFunction(e.listener) ? e.listener : e),
      this._events[t]
        ? Ea.isObject(this._events[t])
          ? this._events[t].push(e)
          : (this._events[t] = [this._events[t], e])
        : (this._events[t] = e),
      Ea.isObject(this._events[t]) && !this._events[t].warned) &&
        (n = Ea.isUndefined(this._maxListeners)
          ? Oa.defaultMaxListeners
          : this._maxListeners) &&
        n > 0 &&
        this._events[t].length > n &&
        ((this._events[t].warned = !0),
        Ea.isFunction(console.error) &&
          console.error(
            "(node) warning: possible EventEmitter memory leak detected. %d listeners added. Use emitter.setMaxListeners() to increase limit.",
            this._events[t].length
          ),
        Ea.isFunction(console.trace) && console.trace());
      return this;
    }),
    (Oa.prototype.on = Oa.prototype.addListener),
    (Oa.prototype.once = function (t, e) {
      if (!Ea.isFunction(e)) throw TypeError("listener must be a function");
      var n = !1;
      function i() {
        this.removeListener(t, i), n || ((n = !0), e.apply(this, arguments));
      }
      return (i.listener = e), this.on(t, i), this;
    }),
    (Oa.prototype.removeListener = function (t, e) {
      var n, i, r, a;
      if (!Ea.isFunction(e)) throw TypeError("listener must be a function");
      if (!this._events || !this._events[t]) return this;
      if (
        ((r = (n = this._events[t]).length),
        (i = -1),
        n === e || (Ea.isFunction(n.listener) && n.listener === e))
      )
        delete this._events[t],
          this._events.removeListener && this.emit("removeListener", t, e);
      else if (Ea.isObject(n)) {
        for (a = r; a-- > 0; )
          if (n[a] === e || (n[a].listener && n[a].listener === e)) {
            i = a;
            break;
          }
        if (i < 0) return this;
        1 === n.length
          ? ((n.length = 0), delete this._events[t])
          : n.splice(i, 1),
          this._events.removeListener && this.emit("removeListener", t, e);
      }
      return this;
    }),
    (Oa.prototype.removeAllListeners = function (t) {
      var e, n;
      if (!this._events) return this;
      if (!this._events.removeListener)
        return (
          0 === arguments.length
            ? (this._events = {})
            : this._events[t] && delete this._events[t],
          this
        );
      if (0 === arguments.length) {
        for (e in this._events)
          "removeListener" !== e && this.removeAllListeners(e);
        return (
          this.removeAllListeners("removeListener"), (this._events = {}), this
        );
      }
      if (((n = this._events[t]), Ea.isFunction(n))) this.removeListener(t, n);
      else if (Array.isArray(n))
        for (; n.length; ) this.removeListener(t, n[n.length - 1]);
      return delete this._events[t], this;
    }),
    (Oa.prototype.listeners = function (t) {
      return this._events && this._events[t]
        ? Ea.isFunction(this._events[t])
          ? [this._events[t]]
          : this._events[t].slice()
        : [];
    }),
    (Oa.listenerCount = function (t, e) {
      return t._events && t._events[e]
        ? Ea.isFunction(t._events[e])
          ? 1
          : t._events[e].length
        : 0;
    });
  var ja = function (t) {
    return (
      Object.keys(Ta.prototype).forEach(function (e) {
        t[e] = Ta.prototype[e];
      }),
      Ta.init(t),
      t
    );
  };
  function Pa() {
    var t = 0,
      e = 0,
      n = 0,
      i = 0;
    n = n || t;
    var r = {
      move: function (t) {
        this.moveTo(n + t);
      },
      moveTo: function (a) {
        var o = Math.max(t, Math.min(e - i, a));
        n !== o && ((n = o), r.emit("update"));
      },
      update: function (a) {
        var o = !1,
          s = a.min;
        t = void 0 === s ? t : s;
        var c = a.max;
        (e = void 0 === c ? e : c),
          void 0 !== a.viewSize &&
            a.viewSize !== i &&
            ((i = a.viewSize), (o = !0));
        var u = Math.max(t, Math.min(e - i, n));
        n !== u && ((n = u), (o = !0)), o && r.emit("update");
      },
      getState: function () {
        return { min: t, max: e, start: n, viewSize: i };
      },
    };
    return ja(r), r;
  }
  function Ba(t, e) {
    var n = t.min || 0,
      i = t.max || 0,
      r = t.viewSize || 0,
      a = e || Pa();
    return a.update({ min: n, max: i, viewSize: r }), a;
  }
  function Da(t, e) {
    var n = {};
    for (var i in t)
      Object.prototype.hasOwnProperty.call(t, i) &&
        (n[i] = Ba(t[i], e ? e[i] : null));
    return n;
  }
  function Fa(t, e) {
    return e[t] || (e[t] = Pa()), e[t];
  }
  function La(t, e) {
    return t <= e;
  }
  function Ia(t, e) {
    return t < e;
  }
  function Wa(t, e, n) {
    for (var i = 0; i < t.length && e > t[i]; ) ++i;
    return t[i] === e && n && ++i, i;
  }
  function Va(t, e, n, i) {
    for (var r = t.length, a = 1; a < r; a += 2)
      if (n(t[a - 1], e) && i(e, t[a])) return !0;
    return !1;
  }
  function $a() {
    var t,
      e,
      n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      i = [];
    function r() {}
    return (
      (r.configure = function () {
        var n =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          i = n.includeMax,
          r = void 0 === i || i,
          a = n.includeMin,
          o = void 0 === a || a;
        (t = r ? La : Ia), (e = o ? La : Ia);
      }),
      (r.add = function (t) {
        var e,
          n = t.min,
          r = t.max,
          a = Wa(i, n),
          o = Wa(i, r, !0),
          s = [a, o - a];
        a % 2 == 0 && s.push(n), o % 2 == 0 && s.push(r);
        var c = i.join(",");
        return (e = i).splice.apply(e, s), c !== i.join(",");
      }),
      (r.remove = function (t) {
        var e,
          n = t.min,
          r = t.max,
          a = Wa(i, n),
          o = Wa(i, r, !0),
          s = [a, o - a];
        a % 2 == 1 && s.push(n), o % 2 == 1 && s.push(r);
        var c = i.join(",");
        return (e = i).splice.apply(e, s), c !== i.join(",");
      }),
      (r.set = function (t) {
        var e = i.join(",");
        return (
          (i = []),
          Array.isArray(t) ? t.forEach(r.add) : r.add(t),
          e !== i.join(",")
        );
      }),
      (r.clear = function () {
        var t = i.length > 0;
        return (i = []), t;
      }),
      (r.containsValue = function (n) {
        return Va(i, n, e, t);
      }),
      (r.containsRange = function (t) {
        var e = t.min,
          n = t.max,
          r = Wa(i, e, !0),
          a = Wa(i, n);
        return r === a && a % 2 == 1;
      }),
      (r.toggle = function (t) {
        return r.containsRange(t) ? r.remove(t) : r.add(t);
      }),
      (r.ranges = function () {
        for (var t = [], e = 1; e < i.length; e += 2)
          t.push({ min: i[e - 1], max: i[e] });
        return t;
      }),
      r.configure(n),
      r
    );
  }
  function Ha() {
    var t = [];
    function e() {}
    return (
      (e.add = function (e) {
        return -1 === t.indexOf(e) && (t.push(e), !0);
      }),
      (e.remove = function (e) {
        var n = t.indexOf(e);
        return -1 !== n && (t.splice(n, 1), !0);
      }),
      (e.contains = function (e) {
        return -1 !== t.indexOf(e);
      }),
      (e.values = function () {
        return t.slice();
      }),
      (e.clear = function () {
        return (t = []);
      }),
      (e.toString = function () {
        return t.join(";");
      }),
      e
    );
  }
  var qa = { key: void 0, includeMin: !0, includeMax: !0 };
  function Ua(t) {
    for (
      var e,
        n,
        i = t.items,
        r = t.collection,
        a = t.vc,
        o = {},
        s = [],
        c = 0,
        u = i.length;
      c < u;
      c++
    ) {
      r[(e = i[c].key)] || (r[e] = a()), (n = i[c].values || [i[c].value]);
      for (var l = 0; l < n.length; l++)
        r[e].add(n[l]) && ((o[e] = o[e] || []), o[e].push(n[l]));
    }
    for (var h = Object.keys(o), f = 0, d = h.length; f < d; f++)
      (e = h[f]), s.push({ id: e, values: o[e] });
    return s;
  }
  function Ya(t) {
    for (
      var e,
        n,
        i = t.items,
        r = t.collection,
        a = {},
        o = [],
        s = 0,
        c = i.length;
      s < c;
      s++
    )
      if (r[(e = i[s].key)]) {
        n = i[s].values || [i[s].value];
        for (var u = 0; u < n.length; u++)
          r[e].remove(n[u]) && ((a[e] = a[e] || []), a[e].push(n[u]));
      }
    for (var l = Object.keys(a), h = 0, f = l.length; h < f; h++)
      (e = l[h]), o.push({ id: e, values: a[e] });
    return o;
  }
  function Xa(t) {
    for (var e, n, i = {}, r = 0, a = t.length; r < a; r++) {
      (e = t[r].key), (n = t[r].values || [t[r].value]), i[e] || (i[e] = []);
      for (var o = 0; o < n.length; o++) {
        -1 === i[e].indexOf(n[o]) && i[e].push(n[o]);
      }
    }
    return i;
  }
  function Ga(t) {
    var e = t.key,
      n = t.collection,
      i = t.obj,
      r = t.fn,
      a = t.value;
    n[e] || (n[e] = r()), (i[e] = i[e] || []), i[e].push(a), n[e].add(a);
  }
  function Za(t) {
    for (
      var e,
        n,
        i,
        r = t.items,
        a = t.values,
        o = t.vc,
        s = {},
        c = {},
        u = [],
        l = [],
        h = Xa(r),
        f = Object.keys(h),
        d = 0,
        g = f.length;
      d < g;
      d++
    )
      for (var p = 0, y = (i = h[(e = f[d])]).length; p < y; p++)
        (n = i[p]),
          a[e] && a[e].contains(n)
            ? a[e] &&
              a[e].contains(n) &&
              ((c[e] = c[e] || []), c[e].push(n), a[e].remove(n))
            : Ga({ key: e, value: n, collection: a, obj: s, fn: o });
    for (var v = Object.keys(s), m = 0, x = v.length; m < x; m++)
      (e = v[m]), u.push({ id: e, values: s[e] });
    for (var b = Object.keys(c), _ = 0, w = b.length; _ < w; _++)
      (e = b[_]), l.push({ id: e, values: c[e] });
    return [u, l];
  }
  function Ja(t, e) {
    for (
      var n,
        i,
        r = [],
        a = Object.keys(t),
        o = function (t) {
          return -1 === e[n].indexOf(t);
        },
        s = 0,
        c = a.length;
      s < c;
      s++
    )
      (n = a[s]),
        e[n]
          ? (i = t[n].filter(o)).length && r.push({ id: n, values: i })
          : r.push({ id: n, values: t[n] });
    return r;
  }
  function Ka(t) {
    for (
      var e,
        n,
        i = t.items,
        r = t.vCollection,
        a = t.vc,
        o = {},
        s = Xa(i),
        c = {},
        u = Object.keys(r),
        l = 0,
        h = u.length;
      l < h;
      l++
    )
      (c[(n = u[l])] = r[n].values().slice()), delete r[n];
    for (
      var f = function (t) {
          (r[n] && r[n].contains(t)) ||
            Ga({ key: n, value: t, collection: r, obj: o, fn: a });
        },
        d = Object.keys(s),
        g = 0,
        p = d.length;
      g < p;
      g++
    )
      s[(n = d[g])].forEach(f);
    return (e = Ja(c, o)), [Ja(o, c), e];
  }
  function Qa(t, e, n) {
    var i = (function (t, e) {
      if (!Object.keys(e).length) return t;
      for (var n = t.length, i = Array(n), r = 0; r < n; r++)
        i[r] = t[r].key in e ? k({}, t[r], { key: e[t[r].key] }) : t[r];
      return i;
    })(e, n);
    return t && t.length
      ? t.reduce(function (t, e) {
          return e(t);
        }, i)
      : i;
  }
  function to(t) {
    return t.replace(/(-[a-z])/g, function (t) {
      return t.toUpperCase().replace("-", "");
    });
  }
  function eo(t) {
    return t.replace(/([A-Z])/g, function (t) {
      return "-".concat(t.toLowerCase());
    });
  }
  function no(t, e, n) {
    var i = n.ranges,
      r = n.interceptors,
      a = n.rc,
      o = n.aliases,
      s = n.rangeConfig,
      c = Qa(r["".concat(e, "Ranges")], t, o),
      u = !1;
    return (
      c.forEach(function (t) {
        var n = t.key;
        if ((i[n] || (i[n] = a(s.sources[n] || s.default)), "set" === e))
          u = i[n][e](t.ranges || t.range) || u;
        else
          for (var r = t.ranges || [t.range], o = 0; o < r.length; o++)
            u = i[n][e](r[o]) || u;
      }),
      u
    );
  }
  function io() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = t.vc,
      n = void 0 === e ? Ha : e,
      i = t.rc,
      r = void 0 === i ? $a : i,
      a = !1,
      o = {},
      s = {},
      c = {},
      u = { sources: {}, default: k({}, qa) },
      l = {
        addValues: [],
        removeValues: [],
        toggleValues: [],
        setValues: [],
        addRanges: [],
        setRanges: [],
        removeRanges: [],
        toggleRanges: [],
      },
      h = function () {
        var t = { values: {}, ranges: {} };
        return (
          Object.keys(s).forEach(function (e) {
            t.values[e] = s[e].values();
          }),
          Object.keys(o).forEach(function (e) {
            t.ranges[e] = o[e].ranges();
          }),
          t
        );
      },
      f = {
        ls: [],
        clear: function () {
          this.ls.forEach(function (t) {
            return t.clear();
          });
        },
        start: function () {
          this.ls.forEach(function (t) {
            return t.start();
          });
        },
        end: function () {
          this.ls.forEach(function (t) {
            return t.end();
          });
        },
        update: function () {
          var t = h();
          this.ls.forEach(function (e) {
            return e._state(t);
          });
        },
        updateValues: function () {
          var t = h();
          this.ls.forEach(function (e) {
            return e._state({ values: t.values });
          });
        },
        updateRanges: function () {
          var t = h();
          this.ls.forEach(function (e) {
            return e._state({ ranges: t.ranges });
          });
        },
      },
      d = {
        configure: function () {
          var t =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
          Array.isArray(t.ranges) &&
            t.ranges.length &&
            ((u = { sources: {}, default: k({}, qa) }),
            t.ranges.forEach(function (t) {
              var e = {};
              Object.keys(qa)
                .filter(function (t) {
                  return "key" !== t;
                })
                .forEach(function (n) {
                  e[n] = void 0 !== t[n] ? t[n] : qa[n];
                }),
                void 0 !== t.key ? (u.sources[t.key] = e) : (u.default = e);
            }),
            Object.keys(o).forEach(function (t) {
              return o[t].configure(u.sources[t] || u.default);
            }),
            d.emit("update", [], []));
        },
        link: function (t) {
          if (d === t) throw new Error("Can't link to self");
          f.ls.push(t), t._state(h());
        },
        _state: function (t) {
          if (!t) return h();
          if (t.values) {
            var e = [];
            Object.keys(t.values).forEach(function (n) {
              (s[n] && t.values[n].join(";") === s[n].toString()) ||
                e.push({ key: n, values: t.values[n] });
            }),
              Object.keys(s).forEach(function (n) {
                t.values[n] || e.push({ key: n, values: [] });
              }),
              e.length && d.setValues(e);
          }
          if (t.ranges) {
            var n = [];
            Object.keys(t.ranges).forEach(function (e) {
              (o[e] && t.ranges[e].join(";") === o[e].toString()) ||
                n.push({ key: e, ranges: t.ranges[e] });
            }),
              Object.keys(o).forEach(function (e) {
                t.ranges[e] || n.push({ key: e, ranges: [] });
              }),
              n.length && d.setRanges(n);
          }
        },
        start: function () {
          if (!a) {
            a = !0;
            for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
              e[n] = arguments[n];
            d.emit.apply(d, ["start"].concat(e)), f.start();
          }
        },
        end: function () {
          if (a) {
            (a = !1), (o = {}), (s = {});
            for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
              e[n] = arguments[n];
            d.emit.apply(d, ["end"].concat(e)), f.end();
          }
        },
        isActive: function () {
          return a;
        },
        clear: function () {
          var t = d
              .brushes()
              .filter(function (t) {
                return "value" === t.type && t.brush.values().length;
              })
              .map(function (t) {
                return { id: t.id, values: t.brush.values() };
              }),
            e = Object.keys(o).length > 0 || t.length;
          (o = {}), (s = {}), e && (d.emit("update", [], t), f.clear());
        },
        brushes: function () {
          var t = [];
          return (t = (t = t.concat(
            Object.keys(o).map(function (t) {
              return { type: "range", id: t, brush: o[t] };
            })
          )).concat(
            Object.keys(s).map(function (t) {
              return { type: "value", id: t, brush: s[t] };
            })
          ));
        },
        addValue: function (t, e) {
          d.addValues([{ key: t, value: e }]);
        },
        addValues: function (t) {
          var e = Qa(l.addValues, t, c),
            i = Ua({ vc: n, collection: s, items: e });
          d.emit("add-values", e),
            i.length &&
              (a || ((a = !0), d.emit("start")),
              d.emit("update", i, []),
              f.updateValues());
        },
        setValues: function (t) {
          var e = Qa(l.setValues, t, c),
            i = Ka({ items: e, vCollection: s, vc: n });
          d.emit("set-values", e),
            (i[0].length > 0 || i[1].length > 0) &&
              (a || ((a = !0), d.emit("start")),
              d.emit("update", i[0], i[1]),
              f.updateValues());
        },
        removeValue: function (t, e) {
          d.removeValues([{ key: t, value: e }]);
        },
        removeValues: function (t) {
          var e = Qa(l.removeValues, t, c),
            n = Ya({ collection: s, items: e });
          d.emit("remove-values", e),
            n.length && (d.emit("update", [], n), f.updateValues());
        },
        addAndRemoveValues: function (t, e) {
          var i = Qa(l.addValues, t, c),
            r = Qa(l.removeValues, e, c),
            o = Ua({ vc: n, collection: s, items: i }),
            u = Ya({ collection: s, items: r });
          d.emit("add-values", i),
            d.emit("remove-values", r),
            (o.length || u.length) &&
              (a || ((a = !0), d.emit("start")),
              d.emit("update", o, u),
              f.updateValues());
        },
        toggleValue: function (t, e) {
          d.toggleValues([{ key: t, value: e }]);
        },
        toggleValues: function (t) {
          var e = Qa(l.toggleValues, t, c),
            i = Za({ items: e, values: s, vc: n });
          d.emit("toggle-values", e),
            (i[0].length > 0 || i[1].length > 0) &&
              (a || ((a = !0), d.emit("start")),
              d.emit("update", i[0], i[1]),
              f.updateValues());
        },
        containsValue: function (t, e) {
          var n = c[t] || t;
          return !!s[n] && s[n].contains(e);
        },
        addRange: function (t, e) {
          d.addRanges([{ key: t, range: e }]);
        },
        addRanges: function (t) {
          no(t, "add", {
            ranges: o,
            rc: r,
            interceptors: l,
            aliases: c,
            rangeConfig: u,
          }) &&
            (a || ((a = !0), d.emit("start")),
            d.emit("update", [], []),
            f.updateRanges());
        },
        removeRange: function (t, e) {
          d.removeRanges([{ key: t, range: e }]);
        },
        removeRanges: function (t) {
          no(t, "remove", {
            ranges: o,
            rc: r,
            interceptors: l,
            aliases: c,
            rangeConfig: u,
          }) &&
            (a || ((a = !0), d.emit("start")),
            d.emit("update", [], []),
            f.updateRanges());
        },
        setRange: function (t, e) {
          d.setRanges([{ key: t, range: e }]);
        },
        setRanges: function (t) {
          no(t, "set", {
            ranges: o,
            rc: r,
            interceptors: l,
            aliases: c,
            rangeConfig: u,
          }) &&
            (a || ((a = !0), d.emit("start")),
            d.emit("update", [], []),
            f.updateRanges());
        },
        toggleRange: function (t, e) {
          d.toggleRanges([{ key: t, range: e }]);
        },
        toggleRanges: function (t) {
          no(t, "toggle", {
            ranges: o,
            rc: r,
            interceptors: l,
            aliases: c,
            rangeConfig: u,
          }) &&
            (a || ((a = !0), d.emit("start")),
            d.emit("update", [], []),
            f.updateRanges());
        },
        containsRangeValue: function (t, e) {
          var n = c[t] || t;
          return !!o[n] && o[n].containsValue(e);
        },
        containsRange: function (t, e) {
          var n = c[t] || t;
          return !!o[n] && o[n].containsRange(e);
        },
        containsMappedData: function (t, e, n) {
          for (
            var i, r, a, u, l = [], h = Object.keys(t), f = 0, d = h.length;
            f < d;
            f++
          ) {
            if ("value" === (i = h[f]))
              (r = t), (l[f] = { key: "", i: f, bool: !1 });
            else {
              if ("source" === i) continue;
              (r = t[i]), (l[f] = { key: i, i: f, bool: !1 });
            }
            void 0 !== (a = r.source && r.source.field) &&
              (void 0 !== r.source.key &&
                (a = "".concat(r.source.key, "/").concat(a)),
              a in c && (a = c[a]),
              (u = r.value),
              o[a]
                ? (l[f].bool = Array.isArray(u)
                    ? o[a].containsRange({ min: u[0], max: u[1] })
                    : o[a].containsValue(u))
                : s[a] && s[a].contains(u) && (l[f].bool = !0));
          }
          return e
            ? ((l = l.filter(function (t) {
                return -1 !== e.indexOf(t.key);
              })),
              "and" === n
                ? !!l.length &&
                  !l.some(function (t) {
                    return !1 === t.bool;
                  })
                : "xor" === n
                ? !!l.length &&
                  l.some(function (t) {
                    return t.bool;
                  }) &&
                  l.some(function (t) {
                    return !1 === t.bool;
                  })
                : l.some(function (t) {
                    return t.bool;
                  }))
            : l.some(function (t) {
                return t.bool;
              });
        },
        intercept: function (t, e) {
          var n = to(t);
          l[n] && l[n].push(e);
        },
        removeInterceptor: function (t, e) {
          var n = to(t);
          if (l[n]) {
            var i = l[n].indexOf(e);
            -1 !== i && l[n].splice(i, 1);
          }
        },
        removeAllInterceptors: function (t) {
          var e = [];
          if (t) {
            var n = to(t);
            l[n] && l[n].length && e.push({ name: t, handlers: l[n] });
          } else
            Object.keys(l).forEach(function (t) {
              l[t].length && e.push({ name: eo(t), handlers: l[t] });
            });
          e.forEach(function (t) {
            t.handlers.slice().forEach(function (e) {
              return d.removeInterceptor(t.name, e);
            });
          });
        },
        addKeyAlias: function (t, e) {
          c[t] = e;
        },
        removeKeyAlias: function (t) {
          delete c[t];
        },
      };
    return ja(d), d;
  }
  function ro(t) {
    return ((t *= 2) <= 1 ? t * t * t : (t -= 2) * t * t + 2) / 2;
  }
  function ao(t, e) {
    return t.data ? t.data.value : "text" === t.type ? t.text : e;
  }
  function oo(t, e, n) {
    var i,
      r = t.old,
      a = t.current,
      o = e.renderer,
      s = [],
      c = { nodes: [], ips: [] },
      u = { nodes: [], ips: [] },
      l = { nodes: [], ips: [] },
      h = [],
      f = null,
      d = n.trackBy || ao,
      g = {
        start: function () {
          var t = {};
          r.forEach(function (e, n) {
            var i = d(e, n);
            t[i] = e;
          }),
            a.forEach(function (e, n) {
              var i = d(e, n);
              t[i]
                ? (l.ips.push(zr(t[i], e)),
                  l.nodes.push(e),
                  s.push(t[i]),
                  (t[i] = !1))
                : (c.nodes.push(e),
                  c.ips.push(zr({ r: 0.001, opacity: 0 }, e)));
            }),
            Object.keys(t).forEach(function (e) {
              t[e] &&
                (u.nodes.push(t[e]),
                u.ips.push(zr(t[e], k({}, t[e], { r: 1e-4, opacity: 0 }))));
            }),
            h.push({
              easing: ro,
              duration: 200,
              tweens: u.ips,
              nodes: [].concat(s),
            }),
            h.push({ easing: ro, duration: 400, tweens: l.ips, nodes: [] }),
            h.push({
              easing: ro,
              duration: 200,
              tweens: c.ips,
              nodes: $(l.nodes),
            }),
            h.length &&
              ((f = o.getScene(a)),
              (h[0].started = Date.now()),
              "undefined" != typeof window &&
                (i = window.requestAnimationFrame(g.tick)));
        },
        tick: function () {
          var t = h[0];
          t || g.stop(), t.started || (t.started = Date.now());
          var e = (Date.now() - t.started) / t.duration,
            n = [],
            r = t.tweens.map(function (n) {
              return n(t.easing(Math.min(1, e)));
            });
          n.push.apply(n, $(r)),
            n.push.apply(n, $(t.nodes)),
            o.render(n),
            e >= 1 && (h.shift(), h.length || g.stop()),
            i && (i = window.requestAnimationFrame(g.tick));
        },
        stop: function () {
          i && (window.cancelAnimationFrame(i), (i = !1));
        },
        inProgress: function () {
          return !!i;
        },
        get targetScene() {
          return f;
        },
      };
    return g;
  }
  oe();
  var so = {
      fontFamily: "Arial",
      fontSize: "13px",
      color: "#595959",
      backgroundColor: "#ffffff",
      stroke: "#000000",
      strokeWidth: 0,
      $fill: "#333333",
    },
    co = /^\$/;
  function uo(t) {
    var e = N(t);
    return "object" !== e && "function" !== e && "undefined" !== e;
  }
  function lo(t, e, n) {
    var i = t[e];
    i && i.scale && i.scale.pxScale
      ? (i.scale = i.scale.pxScale(n))
      : i && i.pxScale && (t[e] = i.pxScale(n));
  }
  function ho(t, e) {
    return t.pxScale ? t.pxScale(e) : t;
  }
  var fo = {
    normalizeSettings: function (t, e, n) {
      var i = k({}, t),
        r = k({}, e);
      return (
        Object.keys(i).forEach(function (t) {
          r[t] = {};
          var a = i[t],
            o = N(a);
          if ("function" == typeof a) r[t].fn = a;
          else if (uo(a)) {
            var s = e[t];
            "string" == typeof s && co.test(s) && (s = so[s]);
            var c = N(s);
            r[t] = "undefined" === c || c === o ? a : s;
          } else
            a &&
              "object" === N(a) &&
              ("function" == typeof a.fn && (r[t].fn = a.fn),
              void 0 !== a.scale && (r[t].scale = n.scale(a.scale)),
              "string" == typeof a.ref && (r[t].ref = a.ref));
        }),
        Object.keys(e).forEach(function (t) {
          if (!(t in i)) {
            var n = e[t];
            "string" === N(n) && co.test(n) ? (r[t] = so[n]) : (r[t] = n);
          }
        }),
        r
      );
    },
    resolveForItem: function (t, e, n) {
      for (
        var i = {},
          r = Object.keys(e),
          a = r.length,
          o = t.datum,
          s = t.datum,
          c = 0;
        c < a;
        c++
      ) {
        var u = r[c],
          l = e[u],
          h = "object" === N(s) && void 0 !== l,
          f = h && "string" == typeof l.ref,
          d = h && u in s,
          g = f ? s[l.ref] : d ? s[u] : o;
        uo(l)
          ? (i[u] = l)
          : h && l.fn
          ? (l.scale && (t.scale = l.scale), (i[u] = l.fn.call(null, t, n)))
          : h && l.scale && g
          ? ((i[u] = l.scale(g.value)),
            l.scale.bandwidth && (i[u] += l.scale.bandwidth() / 2))
          : f && g
          ? (i[u] = g.value)
          : l.fn
          ? (i[u] = l.fn.call(null, t, n))
          : (i[u] = l);
      }
      return i;
    },
    updateScaleSize: lo,
  };
  function go(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : fo,
      n = {};
    function i(i) {
      var r = i.data,
        a = i.settings,
        o = i.defaults,
        s = void 0 === o ? {} : o,
        c = i.scaled,
        u = (n.norm = e.normalizeSettings(a, s, t.chart)),
        l = { scale: t.chart.scale, formatter: t.chart.formatter };
      c &&
        Object.keys(c).forEach(function (t) {
          u[t] && e.updateScaleSize(u, t, c[t]);
        });
      var h = [];
      if (!r || !Array.isArray(r.items)) {
        var f = { data: r, resources: l },
          d = e.resolveForItem(f, n.norm, -1);
        return { settings: n.norm, item: d };
      }
      for (var g, p = 0, y = r.items.length; p < y; p++) {
        g = { datum: r.items[p], data: r, resources: l };
        var v = e.resolveForItem(g, n.norm, p);
        (v.data = r.items[p]), h.push(v);
      }
      return { settings: n.norm, items: h };
    }
    return { resolve: i };
  }
  function po() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
    return t.reduce(function (t, e) {
      return Array.isArray(e.children)
        ? (t.push.apply(t, $(po(e.children))), t)
        : (t.push(e), t);
    }, []);
  }
  function yo(t, e) {
    var n = e.context,
      i = e.data,
      r = e.style,
      a = e.filter,
      o = e.mode,
      s = t.chart.brush(n),
      c = i,
      u = r.active || {},
      l = r.inactive || {},
      h = [];
    Object.keys(u).forEach(function (t) {
      h.push(t);
    }),
      Object.keys(l).forEach(function (t) {
        -1 === h.indexOf(t) && h.push(t);
      });
    var f = [],
      d = !1,
      g = function () {
        var e = po(t.nodes);
        return "function" == typeof a && (e = e.filter(a)), e;
      },
      p = function () {
        for (
          var t,
            e = g(),
            n = e.length,
            i = !1,
            r = "function" == typeof c ? c({ brush: s }) : c,
            a = function (n) {
              if (!(t = e[n].data)) return "continue";
              e[n].__style ||
                ((e[n].__style = {}),
                h.forEach(function (t) {
                  e[n].__style[t] = e[n][t];
                }));
              var a = s.containsMappedData(t, r, o),
                c = f.indexOf(e[n]),
                g = !1;
              if (
                (a && -1 === c
                  ? (f.push(e[n]), (g = !0))
                  : a || -1 === c || (f.splice(c, 1), (g = !0)),
                g || d)
              ) {
                var p = k({}, e[n], e[n].__style);
                h.forEach(function (t) {
                  e[n][t] =
                    a && (t in u)
                      ? "function" == typeof u[t]
                        ? u[t].call(null, p)
                        : u[t]
                      : !a && (t in l)
                      ? "function" == typeof l[t]
                        ? l[t].call(null, p)
                        : l[t]
                      : e[n].__style[t];
                }),
                  (i = !0);
              }
            },
            p = 0;
          p < n;
          p++
        )
          a(p);
        return (d = !1), i;
      },
      y = function () {
        for (
          var e =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : { suppressRender: !1 },
            n = e.suppressRender,
            i = g(),
            r = i.length,
            a = function (t) {
              if (!i[t].data) return "continue";
              (i[t].__style = i[t].__style || {}),
                h.forEach(function (e) {
                  (i[t].__style[e] = i[t][e]),
                    (e in l) &&
                      (i[t][e] =
                        "function" == typeof l[e]
                          ? l[e].call(null, i[t])
                          : l[e]);
                });
            },
            o = 0;
          o < r;
          o++
        )
          a(o);
        (d = !0), (f.length = 0), n || t.renderer.render(t.nodes);
      },
      v = function () {
        for (
          var e =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : { suppressRender: !1 },
            n = e.suppressRender,
            i = g(),
            r = i.length,
            a = function (t) {
              i[t].__style &&
                (Object.keys(i[t].__style).forEach(function (e) {
                  i[t][e] = i[t].__style[e];
                }),
                (i[t].__style = void 0));
            },
            o = 0;
          o < r;
          o++
        )
          a(o);
        (f.length = 0), n || t.renderer.render(t.nodes);
      },
      m = function () {
        p() &&
          ("function" == typeof t.config.sortNodes
            ? t.renderer.render(t.config.sortNodes(t))
            : t.renderer.render(t.nodes));
      };
    return (
      s.on("start", y),
      s.on("end", v),
      s.on("update", m),
      {
        isActive: function () {
          return s.isActive();
        },
        update: function () {
          (f.length = 0), (d = !0), p();
        },
        cleanUp: function () {
          s.removeListener("start", y),
            s.removeListener("end", v),
            s.removeListener("update", m);
        },
      }
    );
  }
  function vo(t) {
    for (
      var e = t.nodes, n = t.action, i = t.chart, r = t.trigger, a = [], o = 0;
      o < e.length;
      o++
    ) {
      var s = e[o].data;
      null !== s && a.push(s);
    }
    !(function (t) {
      var e = t.dataPoints,
        n = t.action,
        i = t.chart,
        r = t.trigger;
      if (r) {
        var a = r.data || [""],
          o = { items: [], actionFn: "toggleRanges" },
          s = { items: [], actionFn: "toggleValues" };
        -1 !== ["add", "remove", "set", "toggle"].indexOf(n) &&
          ((o.actionFn = "".concat(n, "Ranges")),
          (s.actionFn = "".concat(n, "Values")));
        for (
          var c = function (t) {
              var n = e[t];
              if (!n) return "continue";
              a.forEach(function (t) {
                var e = n && !t ? n : n[t];
                if (e) {
                  var i = { key: e.source.field };
                  void 0 !== e.source.key &&
                    (i.key = ""
                      .concat(e.source.key, "/")
                      .concat(e.source.field)),
                    Array.isArray(e.value)
                      ? ((i.range = { min: e.value[0], max: e.value[1] }),
                        o.items.push(i))
                      : ((i.value = e.value), s.items.push(i));
                }
              });
            },
            u = 0;
          u < e.length;
          u++
        )
          c(u);
        r.contexts.forEach(function (t) {
          o.items.length
            ? i.brush(t)[o.actionFn](o.items)
            : i.brush(t)[s.actionFn](s.items);
        });
      }
    })({ dataPoints: a, action: n, chart: i, trigger: r });
  }
  function mo(t) {
    var e = t.collisions,
      n = t.t,
      i = t.config,
      r = t.action,
      a = [],
      o = !1;
    e.length > 0 &&
      ((a = e), (o = !0), "stop" === n.propagation && (a = [e[e.length - 1]]));
    var s = a.map(function (t) {
      return t.node;
    });
    return (
      vo({ nodes: s, action: r, chart: i.chart, data: i.data, trigger: n }), o
    );
  }
  function xo(t, e, n) {
    var i = n.element().getBoundingClientRect(),
      r = U(t)
        ? (function (t, e) {
            return 1 !== t.changedTouches.length
              ? null
              : {
                  x: t.changedTouches[0].clientX - e.left,
                  y: t.changedTouches[0].clientY - e.top,
                };
          })(t, i)
        : (function (t, e) {
            return { x: t.clientX - e.left, y: t.clientY - e.top };
          })(t, i);
    return null === r || r.x < 0 || r.y < 0 || r.x > i.width || r.y > i.height
      ? []
      : (e.touchRadius > 0 && U(t)
          ? (r = { cx: r.x, cy: r.y, r: e.touchRadius })
          : e.mouseRadius > 0 &&
            !U(t) &&
            (r = { cx: r.x, cy: r.y, r: e.mouseRadius }),
        n.itemsAt(r));
  }
  function bo(t, e, n) {
    return t ? ("function" == typeof t ? t(e) : t) : n;
  }
  function _o(t) {
    var e = t.e,
      n = t.t,
      i = t.config;
    return mo({
      collisions: xo(e, n, i.renderer),
      t: n,
      config: i,
      action: bo(n.action, e, "toggle"),
    });
  }
  function wo(t) {
    var e = t.e,
      n = t.t,
      i = t.config;
    return mo({
      collisions: xo(e, n, i.renderer),
      t: n,
      config: i,
      action: bo(n.action, e, "set"),
    });
  }
  var ko = {
    fill: "fill",
    stroke: "stroke",
    opacity: "opacity",
    strokeWidth: "stroke-width",
    strokeLinejoin: "stroke-linejoin",
    fontFamily: "font-family",
    fontSize: "font-size",
    baseline: "dominant-baseline",
    dominantBaseline: "dominant-baseline",
    anchor: "text-anchor",
    textAnchor: "text-anchor",
    maxWidth: "maxWidth",
    transform: "transform",
    strokeDasharray: "stroke-dasharray",
    id: "id",
  };
  function Mo(t, e) {
    Object.keys(ko).forEach(function (n) {
      var i = e[n];
      void 0 !== i && (t[ko[n]] = i);
    });
  }
  function Ro(t) {
    for (
      var e =
          !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
        n = "",
        i = 0;
      i < t.length;
      i++
    ) {
      var r = t[i];
      (n +=
        0 === i
          ? "M".concat(r.x, " ").concat(r.y)
          : "L".concat(r.x, " ").concat(r.y)),
        (n += " ");
    }
    return e && (n += "Z"), n;
  }
  function So(t, e, n, i) {
    var r = n / 2,
      a = t - i / 2,
      o = e - i / 2,
      s = t - r,
      c = e - r;
    return [
      { x: a, y: o },
      { x: a, y: c },
      { x: a + i, y: c },
      { x: a + i, y: o },
      { x: s + n, y: o },
      { x: s + n, y: o + i },
      { x: a + i, y: o + i },
      { x: a + i, y: c + n },
      { x: a, y: c + n },
      { x: a, y: o + i },
      { x: s, y: o + i },
      { x: s, y: o },
    ];
  }
  function No(t) {
    return (-t / 180) * Math.PI;
  }
  function zo(t) {
    return (function (t) {
      var e = (0.5 * (Math.floor((t / Math.PI) * 2) + 2) + 0.25) * Math.PI,
        n = Math.cos(Math.abs(e - t)) * Math.sqrt(2),
        i = n * Math.cos(t),
        r = n * Math.sin(t);
      return {
        x1: i < 0 ? 1 : 0,
        y1: r < 0 ? 1 : 0,
        x2: i >= 0 ? i : i + 1,
        y2: r >= 0 ? r : r + 1,
      };
    })(No(t));
  }
  function Co(t, e) {
    return { x: t.x + e.x, y: t.y + e.y };
  }
  function Ao(t, e) {
    return { x: t.x - e.x, y: t.y - e.y };
  }
  function Eo(t, e) {
    return { x: t.x * e, y: t.y * e };
  }
  function Oo(t, e) {
    return t.x - e.x;
  }
  function To(t, e) {
    return t.y - e.y;
  }
  function jo(t, e) {
    return Math.pow(Oo(t, e), 2) + Math.pow(To(t, e), 2);
  }
  function Po(t, e) {
    return Math.sqrt(jo(t, e));
  }
  function Bo(t, e) {
    return t.x * e.x + t.y * e.y;
  }
  function Do(t, e) {
    var n =
        arguments.length > 2 && void 0 !== arguments[2]
          ? arguments[2]
          : { x: 0, y: 0 },
      i = Math.cos(e),
      r = Math.sin(e),
      a = Ao(t, n),
      o = { x: i * a.x - r * a.y, y: r * a.x + i * a.y };
    return Co(o, n);
  }
  var Fo = { up: 0, down: 180, left: 90, right: -90 };
  function Lo(t) {
    var e = t.x,
      n = t.y,
      i = t.size,
      r = i / 2;
    return { type: "rect", x: e - r, y: n - r, width: i, height: i };
  }
  var Io = oe();
  function Wo(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
    Object.keys(e).forEach(function (n) {
      void 0 !== ko[n] && "transform" !== n && (t[n] = e[n]);
    });
  }
  Io.add("circle", function (t) {
    return { type: "circle", fill: "black", cx: t.x, cy: t.y, r: t.size / 2 };
  }),
    Io.add("diamond", function (t) {
      var e = t.size,
        n = t.x - e / 2,
        i = t.y - e / 2;
      return {
        type: "path",
        fill: "black",
        d: Ro([
          { x: n, y: i + e / 2 },
          { x: n + e / 2, y: i },
          { x: n + e, y: i + e / 2 },
          { x: n + e / 2, y: i + e },
          { x: n, y: i + e / 2 },
        ]),
      };
    }),
    Io.add("saltire", function (t) {
      var e = No(45),
        n = t.size / 2,
        i = isNaN(t.width) ? n / 2 : t.width,
        r = Math.min(i, n),
        a = t.size,
        o = Math.sin(Math.asin(-e)) * (r / 2);
      (a += 2 * (n / Math.sin(-e) - n)), (a -= 2 * o);
      var s = { x: t.x, y: t.y },
        c = So(t.x, t.y, a, r).map(function (t) {
          return Do(t, e, s);
        });
      return { type: "path", fill: "black", d: Ro(c) };
    }),
    Io.add("square", function (t) {
      var e = t.size;
      return {
        type: "rect",
        fill: "black",
        x: t.x - e / 2,
        y: t.y - e / 2,
        width: e,
        height: e,
      };
    }),
    Io.add("triangle", function (t) {
      var e = t.size,
        n = { x: t.x, y: t.y },
        i = Fo[t.direction] || 0,
        r = e / 2,
        a = t.x - r,
        o = t.y - r,
        s = [
          { x: a, y: o + e },
          { x: a + r, y: o },
          { x: a + e, y: o + e },
          { x: a, y: o + e },
        ],
        c = No(i);
      return {
        type: "path",
        fill: "black",
        d: Ro(
          (s = s.map(function (t) {
            return Do(t, c, n);
          }))
        ),
      };
    }),
    Io.add("line", function (t) {
      var e = "vertical" === t.direction,
        n = t.size / 2,
        i = t.x,
        r = t.y;
      return {
        type: "line",
        stroke: "black",
        strokeWidth: 1,
        x1: i - (e ? 0 : n),
        y1: r - (e ? n : 0),
        x2: i + (e ? 0 : n),
        y2: r + (e ? n : 0),
        collider: Lo(t),
      };
    }),
    Io.add("star", function (t) {
      for (
        var e = t.size,
          n = [],
          i = e / 2,
          r = t.points || 5,
          a = Math.min(t.innerRadius || e / 2, e) / 2,
          o = isNaN(t.startAngle) ? 90 : t.startAngle,
          s = 360 / r,
          c = 1;
        c <= r;
        c++
      ) {
        var u = s * c + o,
          l = No(u),
          h = No(u + s / 2),
          f = Math.sin(l),
          d = Math.cos(l),
          g = Math.sin(h),
          p = Math.cos(h);
        n.push({ x: t.x + d * i, y: t.y + f * i }),
          n.push({ x: t.x + p * a, y: t.y + g * a });
      }
      return { type: "path", fill: "black", d: Ro(n) };
    }),
    Io.add("n-polygon", function (t) {
      for (
        var e = [],
          n = t.size / 2,
          i = Math.max(isNaN(t.sides) ? 6 : t.sides, 3),
          r = 360 / i,
          a = isNaN(t.startAngle) ? 0 : t.startAngle,
          o = 1;
        o <= i;
        o++
      ) {
        var s = No(r * o + a),
          c = Math.sin(s),
          u = Math.cos(s);
        e.push({ x: t.x + u * n, y: t.y + c * n });
      }
      return { type: "path", fill: "black", d: Ro(e) };
    }),
    Io.add("cross", function (t) {
      var e = t.x,
        n = t.y,
        i = t.size / 2,
        r = isNaN(t.width) ? i / 2 : t.width,
        a = Math.min(r, i);
      return { type: "path", fill: "black", d: Ro(So(e, n, t.size, a)) };
    }),
    Io.add("bar", function (t) {
      var e = { x: t.x, y: t.y },
        n = "vertical" === t.direction,
        i = t.size / 2,
        r = i / 2 / 2,
        a = [
          { x: e.x - i, y: e.y + r },
          { x: e.x - i, y: e.y - r },
          { x: e.x + i, y: e.y - r },
          { x: e.x + i, y: e.y + r },
        ];
      if (n) {
        var o = No(90);
        a = a.map(function (t) {
          return Do(t, o, e);
        });
      }
      var s = K(a);
      return (s.type = "rect"), (s.fill = "black"), s;
    }),
    Io.add("rect", function (t) {
      var e = t.x,
        n = t.y,
        i = t.size,
        r = void 0 === t.width ? i : t.width,
        a = void 0 === t.height ? i : t.height;
      return {
        type: "rect",
        fill: "black",
        x: e - r / 2,
        y: n - a / 2,
        width: r,
        height: a,
      };
    });
  var Vo = function () {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : Io;
    return function () {
      var e =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = t.get(e.type);
      if (n) {
        var i = n(e);
        return Wo(i, e), void 0 !== e.data && (i.data = e.data), i;
      }
      return n;
    };
  };
  function $o() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      n = t.dock,
      i = void 0 === n ? "center" : n,
      r = t.displayOrder,
      a = void 0 === r ? 0 : r,
      o = t.prioOrder,
      s = void 0 === o ? 0 : o,
      c = t.preferredSize,
      u = void 0 === c ? 0 : c,
      l = t.minimumLayoutMode,
      h = t.show,
      f = void 0 === h || h;
    i = i || "center";
    var d = {
      computePreferredSize: function (t) {
        var n = t.inner,
          i = t.outer,
          r = t.children;
        return "function" == typeof u
          ? u({ inner: n, outer: i, children: r, dock: this.dock() }, e)
          : u;
      },
      dock: function (t) {
        return void 0 !== t
          ? ((i = t), this)
          : "function" == typeof i
          ? i(e)
          : i;
      },
      displayOrder: function (t) {
        return void 0 !== t
          ? ((a = t), this)
          : "function" == typeof a
          ? a(e)
          : a;
      },
      prioOrder: function (t) {
        return void 0 !== t
          ? ((s = t), this)
          : "function" == typeof s
          ? s(e)
          : s;
      },
      minimumLayoutMode: function (t) {
        return void 0 !== t
          ? ((l = t), this)
          : "function" == typeof l
          ? l(e)
          : l;
      },
      show: function (t) {
        return void 0 !== t
          ? ((f = t), this)
          : "function" == typeof f
          ? f(e)
          : f;
      },
    };
    return d;
  }
  function Ho(t, e, n) {
    var i = e.require,
      r = void 0 === i ? [] : i,
      a = e.mediator || {},
      o = n.settings,
      s = n.formatter,
      c = n.scale,
      u = n.data,
      l = n.renderer,
      h = n.chart,
      f = n.dockConfig,
      d = n.mediator,
      g = n.instance,
      p = n.rect,
      y = n.style,
      v = n.registries,
      m = n.resolver,
      x = n.update,
      b = n._DO_NOT_USE_getInfo,
      _ = n.symbol,
      w = n.isVisible;
    (t.emit = function () {}),
      w && (t.isVisible = w),
      Object.defineProperty(t, "settings", { get: o }),
      Object.defineProperty(t, "data", { get: u }),
      Object.defineProperty(t, "formatter", { get: s }),
      Object.defineProperty(t, "scale", { get: c }),
      Object.defineProperty(t, "mediator", { get: d }),
      Object.defineProperty(t, "style", { get: y }),
      Object.defineProperty(t, "registries", { get: v }),
      p && Object.defineProperty(t, "rect", { get: p }),
      b && (t._DO_NOT_USE_getInfo = b),
      Object.keys(e).forEach(function (n) {
        var i;
        (i = n),
          [
            "on",
            "preferredSize",
            "created",
            "beforeMount",
            "mounted",
            "resize",
            "beforeUpdate",
            "updated",
            "beforeRender",
            "render",
            "beforeUnmount",
            "beforeDestroy",
            "destroyed",
            "defaultSettings",
            "data",
            "settings",
            "formatter",
            "scale",
            "chart",
            "dockConfig",
            "mediator",
            "style",
            "resolver",
            "registries",
            "_DO_NOT_USE_getInfo",
            "symbol",
            "isVisible",
          ].some(function (t) {
            return t === i;
          }) ||
            ("function" == typeof e[n] ? (t[n] = e[n].bind(t)) : (t[n] = e[n]));
      }),
      r.forEach(function (e) {
        "renderer" === e
          ? Object.defineProperty(t, "renderer", { get: l })
          : "chart" === e
          ? Object.defineProperty(t, "chart", { get: h })
          : "dockConfig" === e
          ? Object.defineProperty(t, "dockConfig", { get: f })
          : "instance" === e
          ? Object.defineProperty(t, "instance", { get: g })
          : "update" === e && x
          ? Object.defineProperty(t, "update", { get: x })
          : "resolver" === e
          ? Object.defineProperty(t, "resolver", { get: m })
          : "symbol" === e && Object.defineProperty(t, "symbol", { get: _ });
      }),
      Object.keys(a).forEach(function (e) {
        t.mediator.on(e, a[e].bind(t));
      });
  }
  function qo(t, e, n) {
    var i = function (e) {
        return t[e]
          ? (n.warn(
              "Deprecation Warning the ".concat(
                e,
                " property should be moved into layout: {} property"
              )
            ),
            t[e])
          : t.layout
          ? t.layout[e]
          : void 0;
      },
      r = {};
    return (
      (r.displayOrder = i("displayOrder")),
      (r.dock = i("dock")),
      (r.prioOrder = i("prioOrder")),
      (r.minimumLayoutMode = i("minimumLayoutMode")),
      (t.layout = t.layout || {}),
      (t.layout.displayOrder =
        void 0 !== r.displayOrder ? r.displayOrder : t.layout.displayOrder),
      (t.layout.prioOrder =
        void 0 !== r.prioOrder ? r.prioOrder : t.layout.prioOrder),
      (t.layout.dock = r.dock || t.layout.dock),
      (t.layout.minimumLayoutMode =
        r.minimumLayoutMode || t.layout.minimumLayoutMode),
      (r.show = t.show),
      (r.preferredSize = e),
      r
    );
  }
  function Uo(t, e, n) {
    Object.keys(n.on || {}).forEach(function (i) {
      t.eventListeners = t.eventListeners || [];
      var r = n.on[i].bind(t);
      t.eventListeners.push({ event: i, listener: r }), e.on(i, r);
    }),
      (t.emit = function (t) {
        for (
          var n = arguments.length, i = new Array(n > 1 ? n - 1 : 0), r = 1;
          r < n;
          r++
        )
          i[r - 1] = arguments[r];
        return e.emit.apply(e, [t].concat(i));
      });
  }
  function Yo(t, e) {
    t.eventListeners &&
      (t.eventListeners.forEach(function (t) {
        var n = t.event,
          i = t.listener;
        e.removeListener(n, i);
      }),
      (t.eventListeners.length = 0)),
      (t.emit = function () {});
  }
  function Xo(t, e, n) {
    var i = Co(
      (function (t, e) {
        var n = Bo(t, e) / Bo(e, e) || 1;
        return { x: e.x * n, y: e.y * n };
      })(Ao(n, t), Ao(e, t)),
      t
    );
    return i;
  }
  function Go(t, e, n) {
    return Po(t, n) + Po(e, n) - Po(t, e) < 1e-12;
  }
  function Zo(t, e) {
    return (
      t.x >= e.x &&
      t.x + t.width <= e.x + e.width &&
      t.y >= e.y &&
      t.y + t.height <= e.y + e.height
    );
  }
  function Jo(t) {
    return t.x1 === t.x2 && t.y1 === t.y2;
  }
  function Ko(t) {
    return t.width <= 0 || t.height <= 0;
  }
  function Qo(t) {
    return t.r <= 0;
  }
  function ts(t) {
    return t.edges.length <= 2;
  }
  function es(t, e) {
    for (var n = 0, i = e.edges.length; n < i; n++)
      if (!0 === cs(t, Q(e.edges[n]))) return !0;
    return !1;
  }
  function ns(t, e) {
    for (var n = e.numPolygons, i = e.polygons, r = 0; r < n; r++) {
      if (!0 === vs(t, i[r])) return !0;
    }
    return !1;
  }
  function is(t, e) {
    return !Qo(t) && jo({ x: t.cx, y: t.cy }, e) <= Math.pow(t.r, 2);
  }
  function rs(t, e) {
    if (Ko(e) || Qo(t)) return !1;
    var n = e.width / 2,
      i = e.height / 2,
      r = e.x + n,
      a = e.y + i,
      o = t.r,
      s = t.cx,
      c = t.cy,
      u = Math.abs(s - r),
      l = Math.abs(c - a);
    return (
      !(u > n + o || l > i + o) &&
      (u <= n ||
        l <= i ||
        Math.pow(u - n, 2) + Math.pow(l - i, 2) <= Math.pow(o, 2))
    );
  }
  function as(t, e) {
    if (Qo(t) || Jo(e)) return !1;
    var n = V(Z(e), 2),
      i = n[0],
      r = n[1];
    if (is(t, i) || is(t, r)) return !0;
    var a = { x: t.cx, y: t.cy },
      o = Xo(i, r, a);
    return jo(o, a) <= Math.pow(t.r, 2) && Go(i, r, o);
  }
  function os(t, e) {
    if (ts(e) || Qo(t)) return !1;
    if (ss(e, { x: t.cx, y: t.cy })) return !0;
    for (var n = e.edges.length, i = 0; i < n; i++) {
      if (as(t, Q(e.edges[i]))) return !0;
    }
    return !1;
  }
  function ss(t, e) {
    if (ts(t) || !fs(t.boundingRect(), e)) return !1;
    var n,
      i,
      r,
      a,
      o,
      s = e.x,
      c = e.y,
      u = t.vertices;
    for (n = !1, a = -1, r = (i = u.length) - 1; ++a < i; r = a) {
      if (u[a].x === s && u[a].y === c) return !0;
      if (u[a].y === u[r].y && u[a].y === c && (s - u[a].x) * (s - u[r].x) <= 0)
        return !0;
      if ((u[a].y < c && c <= u[r].y) || (u[r].y < c && c <= u[a].y)) {
        if (
          s ===
          (o = ((u[r].x - u[a].x) * (c - u[a].y)) / (u[r].y - u[a].y) + u[a].x)
        )
          return !0;
        s < o && (n = !n);
      }
    }
    return n;
  }
  function cs(t, e) {
    if (ts(t)) return !1;
    for (var n = 0, i = t.edges.length; n < i; n++) {
      if (gs(e, Q(t.edges[n]))) return !0;
    }
    var r = V(Z(e), 2),
      a = r[0],
      o = r[1];
    return ss(t, a) || ss(t, o);
  }
  function us(t, e) {
    if (ts(t)) return !1;
    for (var n = 0, i = t.edges.length; n < i; n++) {
      if (ds(e, Q(t.edges[n]))) return !0;
    }
    var r = V(J(e), 4),
      a = r[0],
      o = r[1],
      s = r[2],
      c = r[3];
    return ss(t, a) || ss(t, o) || ss(t, s) || ss(t, c);
  }
  function ls(t, e) {
    return (
      !Ko(t) &&
      !Ko(e) &&
      t.x <= e.x + e.width &&
      e.x <= t.x + t.width &&
      t.y <= e.y + e.height &&
      e.y <= t.y + t.height
    );
  }
  function hs(t, e) {
    return (
      !Ko(t) &&
      !Ko(e) &&
      [
        { x: e.x, y: e.y },
        { x: e.x + e.width, y: e.y },
        { x: e.x + e.width, y: e.y + e.height },
        { x: e.x, y: e.y + e.height },
      ].every(function (e) {
        return fs(t, e);
      })
    );
  }
  function fs(t, e) {
    return (
      !Ko(t) &&
      e.x >= t.x &&
      e.x <= t.x + t.width &&
      e.y >= t.y &&
      e.y <= t.y + t.height
    );
  }
  function ds(t, e) {
    if (Jo(e) || Ko(t)) return !1;
    var n = V(Z(e), 2),
      i = n[0],
      r = n[1];
    if (fs(t, i) || fs(t, r)) return !0;
    for (var a = J(t), o = a.length, s = 0; s < o; s++) {
      if (gs(Q([a[s], a[3 !== s ? s + 1 : 0]]), e)) return !0;
    }
    return !1;
  }
  function gs(t, e) {
    var n,
      i,
      r = V(Z(t), 2),
      a = r[0],
      o = r[1],
      s = V(Z(e), 2),
      c = s[0],
      u = s[1],
      l = Oo(o, a),
      h = To(o, a),
      f = Oo(u, c),
      d = To(u, c),
      g = Oo(a, c),
      p = To(a, c),
      y = d * l - f * h,
      v = f * p - d * g,
      m = l * p - h * g;
    if (0 === l && 0 === h) return !1;
    if (0 === f && 0 === d) return !1;
    if (0 === y) {
      if (0 === v && 0 === m) {
        if (0 === l) {
          if (0 === h) return a.x === o.x && a.y === o.y;
          (n = To(c, a) / h), (i = To(u, a) / h);
        } else (n = (c.x - a.x) / l), (i = (u.x - a.x) / l);
        return !((n < 0 && i < 0) || (n > 1 && i > 1));
      }
      return !1;
    }
    var x = v / y;
    return (y = m / y), x >= 0 && x <= 1 && y >= 0 && y <= 1;
  }
  function ps(t, e) {
    if (Jo(t)) return !1;
    var n = V(Z(t), 2);
    return Go(n[0], n[1], e);
  }
  function ys(t, e) {
    var n = t.boundingRect(),
      i = e.boundingRect();
    return !!ls(n, i) && (hs(n, i) ? es(t, e) : es(e, t));
  }
  function vs(t, e) {
    var n = t.boundingRect(),
      i = e.boundingRect();
    return (
      !!ls(n, i) &&
      (hs(i, n)
        ? (function (t, e) {
            for (var n = t.numPolygons, i = t.polygons, r = 0; r < n; r++)
              if (!0 === ys(e, i[r])) return !0;
            return !1;
          })(t, e)
        : (function (t, e) {
            for (var n = 0, i = e.edges.length; n < i; n++)
              if (!0 === bs(t, Q(e.edges[n]))) return !0;
            return !1;
          })(t, e))
    );
  }
  function ms(t, e) {
    if (Qo(t)) return !1;
    if (xs(e, { x: t.cx, y: t.cy })) return !0;
    for (var n = e.numPolygons, i = e.polygons, r = 0; r < n; r++)
      for (var a = i[r], o = a.edges.length, s = 0; s < o; s++) {
        if (as(t, Q(a.edges[s]))) return !0;
      }
    return !1;
  }
  function xs(t, e) {
    if (!fs(t.boundingRect(), e)) return !1;
    var n,
      i,
      r,
      a,
      o,
      s,
      c = e.x,
      u = e.y,
      l = t.numPolygons,
      h = !1;
    for (i = 0; i < l; i++)
      for (o = -1, a = (r = (n = t.vertices[i]).length) - 1; ++o < r; a = o) {
        if (n[o].x === c && n[o].y === u) return !0;
        if (
          n[o].y === n[a].y &&
          n[o].y === u &&
          (c - n[o].x) * (c - n[a].x) <= 0
        )
          return !0;
        if ((n[o].y < u && u <= n[a].y) || (n[a].y < u && u <= n[o].y)) {
          if (
            c ===
            (s =
              ((n[a].x - n[o].x) * (u - n[o].y)) / (n[a].y - n[o].y) + n[o].x)
          )
            return !0;
          c < s && (h = !h);
        }
      }
    return h;
  }
  function bs(t, e) {
    var n = V(Z(e), 2),
      i = n[0],
      r = n[1];
    if (xs(t, i) || xs(t, r)) return !0;
    for (var a = t.numPolygons, o = t.polygons, s = 0; s < a; s++)
      for (var c = o[s], u = 0, l = c.edges.length; u < l; u++) {
        if (gs(e, Q(c.edges[u]))) return !0;
      }
    return !1;
  }
  function _s(t, e) {
    var n = V(J(e), 4),
      i = n[0],
      r = n[1],
      a = n[2],
      o = n[3];
    if (xs(t, i) || xs(t, r) || xs(t, a) || xs(t, o)) return !0;
    for (var s = t.numPolygons, c = t.polygons, u = 0; u < s; u++)
      for (var l = c[u], h = 0, f = l.edges.length; h < f; h++) {
        if (ds(e, Q(l.edges[h]))) return !0;
      }
    return !1;
  }
  var ws = /^\$/,
    ks = "@extend";
  function Ms(t) {
    throw new Error('Cyclical reference for "'.concat(t, '"'));
  }
  function Rs(t, e, n) {
    if ("string" == typeof t) {
      var i = e[t];
      return (
        -1 !== n.indexOf(t) && Ms(t), ws.test(i) ? (n.push(t), Rs(i, e, n)) : i
      );
    }
    var r = t,
      a = k(!0, {}, e, t),
      o = {};
    if (t[ks]) {
      var s = t[ks];
      -1 !== n.indexOf(s) && Ms(s);
      var c = n.slice();
      c.push(s), (r = k(!0, {}, Rs(a[s], e, c), t));
    }
    return (
      Object.keys(r).forEach(function (t) {
        var e = n.slice();
        if (t !== ks && !ws.test(t)) {
          o[t] = r[t];
          var i = o[t];
          ws.test(i) && i in a
            ? (-1 !== n.indexOf(i) && Ms(i),
              e.push(i),
              "object" === N((i = a[i])) || (ws.test(i) && i in a)
                ? (o[t] = Rs(i, a, e))
                : (o[t] = i))
            : "object" === N(i) && (o[t] = Rs(i, a, e));
        }
      }),
      o
    );
  }
  function Ss(t, e) {
    return Rs(t, e, []);
  }
  function Ns(t) {
    (t.x = Math.floor(t.x)),
      (t.y = Math.floor(t.y)),
      (t.width = Math.floor(t.width)),
      (t.height = Math.floor(t.height));
  }
  function zs(t, e) {
    var n = { x: 0, y: 0, width: 0, height: 0 },
      i = { x: 0, y: 0, width: 0, height: 0 };
    return (
      (n.width = t.width || 0),
      (n.height = t.height || 0),
      void 0 !== e.size &&
        ((n.width = isNaN(e.size.width) ? n.width : e.size.width),
        (n.height = isNaN(e.size.height) ? n.height : e.size.height)),
      void 0 !== e.logicalSize
        ? ((i.width = isNaN(e.logicalSize.width)
            ? n.width
            : e.logicalSize.width),
          (i.height = isNaN(e.logicalSize.height)
            ? n.height
            : e.logicalSize.height),
          (i.align = isNaN(e.logicalSize.align)
            ? 0.5
            : Math.min(Math.max(e.logicalSize.align, 0), 1)),
          (i.preserveAspectRatio = e.logicalSize.preserveAspectRatio))
        : ((i.width = n.width),
          (i.height = n.height),
          (i.preserveAspectRatio = !1)),
      Ns(i),
      Ns(n),
      { logicalContainerRect: i, containerRect: n }
    );
  }
  function Cs(t) {
    var e = {
      center: {
        minWidthRatio: 0.5,
        minHeightRatio: 0.5,
        minWidth: 0,
        minHeight: 0,
      },
    };
    return (
      k(!0, e, t),
      (e.center.minWidthRatio = Math.min(
        Math.max(e.center.minWidthRatio, 0),
        1
      )),
      (e.center.minHeightRatio = Math.min(
        Math.max(e.center.minHeightRatio, 0),
        1
      )),
      (e.center.minWidth = Math.max(e.center.minWidth, 0)),
      (e.center.minHeight = Math.max(e.center.minHeight, 0)),
      e
    );
  }
  function As(t, e, n, i, r) {
    return {
      x: isNaN(t) ? 0 : t,
      y: isNaN(t) ? 0 : e,
      width: isNaN(t) ? 0 : n,
      height: isNaN(t) ? 0 : i,
      margin: isNaN(r) ? 0 : r,
    };
  }
  function Es(t, e, n) {
    if (void 0 === t.cachedSize) {
      var i,
        r = t.config.dock(),
        a = t.comp.preferredSize({ inner: e, outer: n, dock: r });
      isNaN(a)
        ? a && !isNaN(a.size) && ((a.width = a.size), (a.height = a.size))
        : (a = { width: a, height: a }),
        (i =
          "top" === r || "bottom" === r
            ? a.height
            : "right" === r || "left" === r
            ? a.width
            : Math.max(a.width, a.height)),
        (t.cachedSize = Math.ceil(i)),
        (t.edgeBleed = a.edgeBleed || 0);
    }
    return t.cachedSize;
  }
  function Os(t, e) {
    switch (e.config.dock()) {
      case "top":
        (t.y += e.cachedSize), (t.height -= e.cachedSize);
        break;
      case "bottom":
        t.height -= e.cachedSize;
        break;
      case "left":
        (t.x += e.cachedSize), (t.width -= e.cachedSize);
        break;
      case "right":
        t.width -= e.cachedSize;
    }
  }
  function Ts(t, e) {
    var n = e.edgeBleed;
    n &&
      ((t.left = Math.max(t.left, n.left || 0)),
      (t.right = Math.max(t.right, n.right || 0)),
      (t.top = Math.max(t.top, n.top || 0)),
      (t.bottom = Math.max(t.bottom, n.bottom || 0)));
  }
  function js(t, e, n) {
    e.x < n.left && ((e.width -= n.left - e.x), (e.x = n.left));
    var i = t.width - (e.x + e.width);
    i < n.right && (e.width -= n.right - i),
      e.y < n.top && ((e.height -= n.top - e.y), (e.y = n.top));
    var r = t.height - (e.y + e.height);
    r < n.bottom && (e.height -= n.bottom - r);
  }
  function Ps(t, e, n, i, r) {
    var a = k({}, e),
      o = k({}, n);
    Os(a, i), Ts(o, i), js(t, a, o);
    var s = (function (t, e, n) {
      var i =
          Math.min(n.center.minWidth, t.width) ||
          Math.max(t.width * n.center.minWidthRatio, 1),
        r =
          Math.min(n.center.minHeight, t.height) ||
          Math.max(t.height * n.center.minHeightRatio, 1);
      return e.width >= i && e.height >= r;
    })(t, a, r);
    return !!s && (Os(e, i), Ts(n, i), !0);
  }
  function Bs(t) {
    var e = t.layoutRect,
      n = t.visible,
      i = t.hidden,
      r = t.settings,
      a = As(e.x, e.y, e.width, e.height),
      o = { left: 0, right: 0, top: 0, bottom: 0 },
      s = n.slice();
    s.sort(function (t, e) {
      return t.config.prioOrder() - e.config.prioOrder();
    });
    for (var c = 0; c < s.length; ++c) {
      var u = s[c];
      Es(u, a, e), Ps(e, a, o, u, r) || (i.push(s.splice(c, 1)[0]), --c);
    }
    !(function (t, e) {
      if (0 !== e.length)
        for (var n = 0; n < t.length; ++n) {
          var i = t[n];
          if (i.referencedDocks.length) {
            var r = i.referencedDocks.every(function (t) {
              return e.some(function (e) {
                return e.key === t;
              });
            });
            r && e.push(t.splice(n, 1)[0]);
          }
        }
    })(n, i);
    var l = n.filter(function (t) {
      return -1 !== s.indexOf(t);
    });
    return (n.length = 0), n.push.apply(n, $(l)), js(e, a, o), a;
  }
  function Ds(t) {
    return {
      x: t.margin.left + t.x * t.scaleRatio.x,
      y: t.margin.top + t.y * t.scaleRatio.y,
      width: t.width * t.scaleRatio.x,
      height: t.height * t.scaleRatio.y,
    };
  }
  function Fs(t) {
    var e;
    return K((e = []).concat.apply(e, $(t.map(J))));
  }
  function Ls(t) {
    var e = t.visible,
      n = t.layoutRect,
      i = t.reducedRect,
      r = t.containerRect,
      a = t.translation,
      o = As(i.x, i.y, i.width, i.height),
      s = As(i.x, i.y, i.width, i.height),
      c = {},
      u = e.slice(),
      l = u.slice().sort(function (t, e) {
        return t.config.displayOrder() - e.config.displayOrder();
      });
    return (
      e
        .sort(function (t, e) {
          if (t.referencedDocks.length > 0 && e.referencedDocks.length > 0)
            return 0;
          if (e.referencedDocks.length > 0) return -1;
          if (t.referencedDocks.length > 0) return 1;
          var n = t.config.displayOrder() - e.config.displayOrder();
          return 0 === n ? u.indexOf(t) - u.indexOf(e) : n;
        })
        .forEach(function (t) {
          var e = {},
            u = {};
          switch (t.config.dock()) {
            case "top":
              (e.height = u.height = t.cachedSize),
                (e.width = n.width),
                (u.width = o.width),
                (e.x = n.x),
                (u.x = o.x),
                (e.y = u.y = o.y - t.cachedSize),
                (o.y -= t.cachedSize),
                (o.height += t.cachedSize);
              break;
            case "bottom":
              (e.x = n.x),
                (u.x = o.x),
                (e.y = u.y = o.y + o.height),
                (e.width = n.width),
                (u.width = o.width),
                (e.height = u.height = t.cachedSize),
                (o.height += t.cachedSize);
              break;
            case "left":
              (e.x = u.x = s.x - t.cachedSize),
                (e.y = n.y),
                (u.y = s.y),
                (e.width = u.width = t.cachedSize),
                (e.height = n.height),
                (u.height = s.height),
                (s.x -= t.cachedSize),
                (s.width += t.cachedSize);
              break;
            case "right":
              (e.x = u.x = s.x + s.width),
                (e.y = n.y),
                (u.y = s.y),
                (e.width = u.width = t.cachedSize),
                (e.height = n.height),
                (u.height = s.height),
                (s.width += t.cachedSize);
              break;
            case "center":
              (e.x = u.x = i.x),
                (e.y = u.y = i.y),
                (e.width = u.width = i.width),
                (e.height = u.height = i.height);
              break;
            default:
              if (t.referencedDocks.length > 0) {
                var l = t.referencedDocks
                  .map(function (t) {
                    return c[t];
                  })
                  .filter(function (t) {
                    return !!t;
                  });
                l.length > 0 &&
                  ((e = Fs(
                    l.map(function (t) {
                      return t.outerRect;
                    })
                  )),
                  (u = Fs(
                    l.map(function (t) {
                      return t.r;
                    })
                  )));
              }
          }
          t.key && (c[t.key] = { r: u, outerRect: e }),
            (function (t, e, n, i) {
              var r = { x: i.width / n.width, y: i.height / n.height },
                a = { left: 0, top: 0 };
              if (n.preserveAspectRatio) {
                var o = r.x < r.y,
                  s = Math.min(r.x, r.y);
                (r.x = s), (r.y = s);
                var c = o ? "height" : "width",
                  u = (i[c] - n[c] * r.x) * n.align;
                (a.left = o ? 0 : u), (a.top = o ? u : 0);
              }
              (t.scaleRatio = r),
                (t.margin = a),
                (e.scaleRatio = r),
                (e.margin = a),
                (n.scaleRatio = r),
                (n.margin = a);
            })(u, e, n, r),
            (u.edgeBleed = t.edgeBleed),
            (u.computed = Ds(u)),
            (e.edgeBleed = t.edgeBleed),
            (e.computed = Ds(e)),
            (u.x += a.x),
            (u.y += a.y),
            (e.x += a.x),
            (e.y += a.y),
            t.comp.resize(u, e),
            (t.cachedSize = void 0),
            (t.edgeBleed = void 0);
        }),
      l
    );
  }
  function Is(t) {
    if (!t.resize || "function" != typeof t.resize)
      throw new Error("Component is missing resize function");
    if (!t.dockConfig && !t.preferredSize)
      throw new Error("Component is missing preferredSize function");
  }
  function Ws(t, e, n) {
    for (var i, r, a, o, s, c = [], u = [], l = 0; l < t.length; ++l) {
      var h = t[l];
      Is(h);
      var f = h.dockConfig,
        d = h.key,
        g = f.dock(),
        p = /@/.test(g)
          ? g.split(",").map(function (t) {
              return t.replace(/^\s*@/, "");
            })
          : [];
      (i = f),
        (r = n),
        (a = void 0),
        (o = void 0),
        (s = void 0),
        (a = e.layoutModes || {}),
        (o = i.minimumLayoutMode()),
        (s = i.show()) && "object" === N(o)
          ? (s =
              a[o.width] &&
              a[o.height] &&
              r.width >= a[o.width].width &&
              r.height >= a[o.height].height)
          : s &&
            void 0 !== o &&
            (s = a[o] && r.width >= a[o].width && r.height >= a[o].height),
        s
          ? c.push({ comp: h, key: d, config: f, referencedDocks: p })
          : u.push({ comp: h, key: d, config: f, referencedDocks: p });
    }
    return [c, u];
  }
  var Vs = function (t, e) {
      for (var n = 0; n < t.length; n++) {
        var i = t[n];
        if (i.hasKey && i.key === e) return t[n];
      }
      return null;
    },
    $s = function t(e) {
      return null == e
        ? void 0
        : e.map(function (e) {
            var n = e.instance.dockConfig();
            return {
              preferredSize: function (i) {
                return n.computePreferredSize(
                  S(S({}, i), {}, { children: t(e.children) })
                );
              },
            };
          });
    },
    Hs = function (t, e) {
      return { visible: [], hidden: e, ordered: [] };
    },
    qs = function (t) {
      return function (e, n) {
        var i,
          r = n.map(function (t, e) {
            var n = t.instance.dockConfig();
            return {
              index: e,
              key: t.key,
              dockConfig: n,
              resize: t.instance.resize,
              preferredSize: function (e) {
                return n.computePreferredSize(
                  S(S({}, e), {}, { children: $s(t.children) })
                );
              },
            };
          }),
          a = function (t) {
            return n[t.index];
          },
          o = null !== (i = t(e, r)) && void 0 !== i ? i : {},
          s = o.visible,
          c = void 0 === s ? r : s,
          u = o.hidden,
          l = void 0 === u ? [] : u,
          h = o.ordered,
          f = void 0 === h ? c : h;
        return { visible: c.map(a), hidden: l.map(a), ordered: f.map(a) };
      };
    },
    Us = function (t) {
      var e,
        n,
        i =
          ((e = Cs(t)),
          (n = {
            layout: function (t) {
              var n =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : [];
              if (
                !t ||
                isNaN(t.x) ||
                isNaN(t.y) ||
                isNaN(t.width) ||
                isNaN(t.height)
              )
                throw new Error("Invalid rect");
              if (!n.length) return { visible: [], hidden: [], ordered: [] };
              var i = zs(t, e),
                r = i.logicalContainerRect,
                a = i.containerRect,
                o = V(Ws(n, e, r), 2),
                s = o[0],
                c = o[1],
                u = Ls({
                  visible: s,
                  layoutRect: r,
                  reducedRect: Bs({
                    layoutRect: r,
                    visible: s,
                    hidden: c,
                    settings: e,
                  }),
                  containerRect: a,
                  translation: { x: t.x, y: t.y },
                });
              return (
                c.forEach(function (t) {
                  t.comp.visible = !1;
                  var e = As();
                  t.comp.resize(e, e);
                }),
                {
                  visible: s.map(function (t) {
                    return t.comp;
                  }),
                  hidden: c.map(function (t) {
                    return t.comp;
                  }),
                  ordered: u.map(function (t) {
                    return t.comp;
                  }),
                }
              );
            },
            settings: function (t) {
              e = Cs(t);
            },
          }),
          n);
      return qs(function (t, e) {
        return i.layout(t, e);
      });
    },
    Ys = function (t) {
      return "function" == typeof t ? qs(t) : Us(t);
    };
  function Xs(t) {
    var e = t.createComponent,
      n = {},
      i = [],
      r = [],
      a = function t(n) {
        var r = e(n);
        return (
          r &&
            (i.push(r),
            n.components &&
              (r.children = n.components
                .map(function (e) {
                  return t(e);
                })
                .filter(function (t) {
                  return !!t;
                }))),
          r
        );
      },
      o = function t(e, n) {
        for (
          var r = function (r) {
              var a = e[r],
                o = n.find(function (t) {
                  return a.hasKey && a.key === t.key;
                });
              o
                ? a.children && o.components && t(a.children, o.components)
                : (e.splice(r, 1),
                  (function (t) {
                    var e = i.indexOf(t);
                    -1 !== e && i.splice(e, 1);
                  })(a),
                  a.children && t(a.children, []),
                  a.instance.destroy());
            },
            a = e.length - 1;
          a >= 0;
          a--
        )
          r(a);
      },
      s = function t(e) {
        var n = e.compList,
          i = e.data,
          r = e.excludeFromUpdate,
          o = e.formatters,
          s = e.scales;
        return e.settingsList
          .map(function (e) {
            var c = Vs(n, e.key);
            return r.indexOf(e.key) > -1
              ? c
              : c
              ? e.rendererSettings &&
                "function" == typeof e.rendererSettings.transform &&
                e.rendererSettings.transform()
                ? ((c.applyTransform = !0),
                  c.instance.renderer().settings(e.rendererSettings),
                  c)
                : ((c.updateWith = {
                    formatters: o,
                    scales: s,
                    data: i,
                    settings: e,
                  }),
                  e.components &&
                    (c.children = t({
                      compList: c.children || [],
                      data: i,
                      excludeFromUpdate: r,
                      formatters: o,
                      scales: s,
                      settingsList: e.components,
                    })),
                  c)
              : a(e);
          })
          .filter(function (t) {
            return !!t;
          });
      },
      c = function t(e) {
        var n = e.components,
          i = e.hidden,
          r = e.layoutFn,
          a = e.ordered,
          o = e.rect,
          s = e.visible,
          c = r(o, n),
          u = c.visible,
          l = c.hidden,
          h = c.ordered;
        s.push.apply(s, $(u)),
          i.push.apply(i, $(l)),
          a.push.apply(a, $(h)),
          u.forEach(function (e) {
            if (e.children) {
              var n,
                r,
                o = Ys(
                  null !==
                    (n =
                      null === (r = e.settings) || void 0 === r
                        ? void 0
                        : r.strategy) && void 0 !== n
                    ? n
                    : {}
                );
              t({
                components: e.children,
                hidden: i,
                layoutFn: o,
                ordered: a,
                rect: e.instance.getRect(),
                visible: s,
              });
            }
          }),
          l.forEach(function (e) {
            e.children &&
              t({
                components: e.children,
                hidden: i,
                layoutFn: Hs,
                ordered: a,
                rect: null,
                visible: s,
              });
          });
      };
    return (
      (n.destroy = function () {
        i.forEach(function (t) {
          return t.instance.destroy();
        }),
          (i = []),
          (r = []);
      }),
      (n.findComponentByKey = function (t) {
        return Vs(i, t);
      }),
      (n.forEach = function (t) {
        i.forEach(t);
      }),
      (n.layout = function (t) {
        var e = t.layoutSettings,
          n = t.rect,
          i = [],
          a = [],
          o = [],
          s = Ys(e);
        return (
          c({
            components: r,
            hidden: a,
            layoutFn: s,
            ordered: o,
            rect: n,
            visible: i,
          }),
          { visible: i, hidden: a, ordered: o }
        );
      }),
      (n.set = function (t) {
        var e = t.components;
        r = e
          .map(function (t) {
            return a(t);
          })
          .filter(function (t) {
            return !!t;
          });
      }),
      (n.update = function (t) {
        var e = t.components,
          n = t.data,
          i = t.excludeFromUpdate,
          a = t.formatters,
          c = t.scales;
        o(r, e),
          (r = s({
            compList: r,
            data: n,
            excludeFromUpdate: i,
            formatters: a,
            scales: c,
            settingsList: e,
          }));
      }),
      n
    );
  }
  function Gs(t, e, n) {
    var i = e.left - n.left,
      r = e.top - n.top,
      a = tt(t),
      o = k(!0, {}, t);
    switch (a) {
      case "circle":
        (o.cx += i), (o.cy += r);
        break;
      case "polygon":
        for (var s = 0, c = o.vertices.length; s < c; s++) {
          var u = o.vertices[s];
          (u.x += i), (u.y += r);
        }
        break;
      case "geopolygon":
        for (var l = 0; l < o.vertices.length; l++)
          for (var h = o.vertices[l], f = 0, d = h.length; f < d; f++) {
            var g = h[f];
            (g.x += i), (g.y += r);
          }
        break;
      case "line":
        (o.x1 += i), (o.y1 += r), (o.x2 += i), (o.y2 += r);
        break;
      case "point":
      case "rect":
        (o.x += i), (o.y += r);
    }
    return o;
  }
  function Zs(t, e) {
    var n = [],
      i = 0;
    e.forEach(function (t) {
      n.push(i), i++;
      var e =
        t.instance.def.additionalElements &&
        t.instance.def.additionalElements();
      e && (i += e.length);
    }),
      e.forEach(function (e, i) {
        return (function (t, e, n) {
          var i = e.instance.renderer().element();
          if (!isNaN(n) && i && t && t.children) {
            var r = t.children[Math.max(0, n)];
            if (i !== r) {
              var a =
                e.instance.def.additionalElements &&
                e.instance.def.additionalElements().filter(Boolean);
              t.insertBefore && void 0 !== r
                ? (t.insertBefore(i, r),
                  a &&
                    a.forEach(function (e) {
                      t.insertBefore(e, i);
                    }))
                : (a &&
                    a.forEach(function (e) {
                      t.appendChild(e, i);
                    }),
                  t.appendChild(i));
            }
          }
        })(t, e, n[i]);
      });
  }
  function Js(t, e) {
    var n = t.element,
      i = t.data,
      r = void 0 === i ? [] : i,
      a = t.settings,
      o = void 0 === a ? {} : a,
      s = t.on,
      c = void 0 === s ? {} : s,
      u = e.registries,
      l = e.logger,
      h = (function () {
        var t =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          e = {},
          n = t,
          i = function (t) {
            t.forEach(function (t) {
              var n = Array.isArray(t.colors[0]) ? t.colors : [t.colors];
              e[t.key] = {
                colors: n,
                sizes: n.map(function (t) {
                  return t ? t.length : 0;
                }),
              };
            });
          },
          r = function (t, n) {
            var i = e[t];
            if (!i) return [];
            for (var r = i.sizes, a = 0; a < r.length; a++)
              if (n <= r[a]) return i.colors[a];
            return i.colors[r.length - 1];
          },
          a = {
            palette: function (t, e) {
              return r(t, e);
            },
            setPalettes: i,
            style: function (t) {
              return Ss(t, n);
            },
            setStyle: function (e) {
              n = k({}, t, e);
            },
          };
        return (
          i(
            arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : []
          ),
          a
        );
      })(e.style, e.palettes),
      f = [],
      d = k({}, t),
      g = (function () {
        var t = {};
        return ja(t), t;
      })(),
      p = [],
      y = null,
      v = null,
      m = null,
      x = [],
      b = function () {},
      _ = function () {},
      w = {},
      M = !1,
      R = Xs({
        createComponent: function (t) {
          if (!u.component.has(t.type))
            return l.warn("Unknown component: ".concat(t.type)), !1;
          var e = (function (t) {
            var e,
              n,
              i,
              r,
              a,
              o =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : {},
              s = t.defaultSettings,
              c = void 0 === s ? {} : s,
              u = t._DO_NOT_USE_getInfo,
              l =
                void 0 === u
                  ? function () {
                      return {};
                    }
                  : u,
              h = o.chart,
              f = o.container,
              d = o.mediator,
              g = o.registries,
              p = o.theme,
              y = o.renderer,
              v = ja({}),
              m = o.settings || {},
              x = k(!0, {}, c, m),
              b = [],
              _ = go({ chart: h }),
              w = !1,
              M = {
                nodes: [],
                chart: h,
                config: x.brush || {},
                renderer: null,
              },
              R = { tap: [], over: [] },
              S = [],
              z = {},
              C = k({}, m);
            function A(e) {
              var n =
                  arguments.length > 1 && void 0 !== arguments[1]
                    ? arguments[1]
                    : function () {},
                i =
                  arguments.length > 2 &&
                  void 0 !== arguments[2] &&
                  arguments[2];
              return function () {
                for (
                  var r,
                    a,
                    o,
                    s = void 0 !== t[e],
                    c = void 0 !== m[e],
                    u = arguments.length,
                    l = new Array(u),
                    h = 0;
                  h < u;
                  h++
                )
                  l[h] = arguments[h];
                return (
                  s &&
                    ("function" == typeof t[e]
                      ? (r = (a = t[e]).call.apply(a, [z].concat(l)))
                      : i && (r = t[e])),
                  c &&
                    ("function" == typeof m[e]
                      ? (r = (o = m[e]).call.apply(o, [C].concat(l)))
                      : i && (r = m[e])),
                  s || c || (r = n.call.apply(n, [z].concat(l))),
                  r
                );
              };
            }
            var E = A(
                "preferredSize",
                function () {
                  return 0;
                },
                !0
              ),
              O = A("resize", function (t) {
                return t.inner;
              }),
              T = A("created"),
              j = A("beforeMount"),
              P = A("mounted"),
              B = A("beforeUnmount"),
              D = A("beforeUpdate"),
              F = A("updated"),
              L = A("beforeRender"),
              I = A("beforeDestroy"),
              W = A("destroyed"),
              V = t.render,
              H = function () {
                x.brush &&
                  (x.brush.consume || []).forEach(function (t) {
                    t.context && t.style && S.push(yo(M, t));
                  });
              },
              q = function () {
                x.brush &&
                  (x.brush.trigger || []).forEach(function (t) {
                    "over" === t.on ? R.over.push(t) : R.tap.push(t);
                  });
              };
            Object.defineProperty(M, "data", {
              get: function () {
                return b;
              },
            });
            var U = x.renderer || t.renderer,
              Y = U ? y || g.renderer(U)() : y || g.renderer()();
            "function" == typeof Y.settings && Y.settings(x.rendererSettings),
              (M.renderer = Y);
            var X = { resources: h.logger ? { logger: h.logger() } : {} },
              G = $o(qo(x, E, h.logger()), X),
              Z = function (t) {
                (t.key = x.key), (t.element = Y.element());
              },
              J = function () {};
            (J.dockConfig = function () {
              return G;
            }),
              (J.set = function () {
                var t =
                  arguments.length > 0 && void 0 !== arguments[0]
                    ? arguments[0]
                    : {};
                t.settings &&
                  ((m = t.settings),
                  (x = k(!0, {}, c, t.settings)),
                  (G = $o(qo(x, E, h.logger()), X))),
                  x.scale && (e = h.scale(x.scale)),
                  (b = x.data
                    ? vi(
                        x.data,
                        { dataset: h.dataset, collection: h.dataCollection },
                        { logger: h.logger() },
                        h.dataCollection
                      )
                    : e
                    ? e.data()
                    : []),
                  "string" == typeof x.formatter || "object" === N(x.formatter)
                    ? (n = h.formatter(x.formatter))
                    : e &&
                      e.data().fields &&
                      (n = e.data().fields[0].formatter()),
                  (a = p.style(x.style || {}));
              }),
              (J.resize = function () {
                var t =
                    arguments.length > 0 && void 0 !== arguments[0]
                      ? arguments[0]
                      : {},
                  e =
                    arguments.length > 1 && void 0 !== arguments[1]
                      ? arguments[1]
                      : {},
                  n = O({ inner: t, outer: e });
                (r = n ? Y.size(n) : Y.size(t)),
                  (C.rect = k(
                    !0,
                    {
                      computedPhysical: r.computedPhysical,
                      computedOuter: e.computed || e,
                      computedInner: t.computed || t,
                    },
                    t
                  )),
                  (r = k(
                    !0,
                    {
                      computedOuter: e.computed || e,
                      computedInner: t.computed || t,
                    },
                    r
                  ));
              }),
              (J.getRect = function () {
                return C.rect;
              });
            var K,
              Q,
              tt,
              et = function () {
                var t = Y.renderArgs ? Y.renderArgs.slice(0) : [];
                return t.push({ data: b }), t;
              };
            (J.beforeMount = j),
              (J.beforeRender = function () {
                L({ size: r });
              }),
              (J.render = function () {
                var t = (M.nodes = V.call.apply(V, [z].concat($(et()))));
                Y.render(t), (K = t), (Q = C.rect.computed);
              }),
              (J.hide = function () {
                J.unmount(),
                  Y.size({ x: 0, y: 0, width: 0, height: 0 }),
                  Y.clear();
              }),
              (J.beforeUpdate = function () {
                D({ settings: x, data: b });
              }),
              (J.update = function () {
                tt && tt.stop();
                var t = x,
                  e = t.rendererSettings,
                  n = t.brush,
                  i = t.animations;
                if (
                  "function" == typeof (null == e ? void 0 : e.transform) &&
                  e.transform()
                )
                  return Y.render(), void (K = null);
                var r = (M.nodes = V.call.apply(V, [z].concat($(et()))));
                S.forEach(function (t) {
                  return t.cleanUp();
                }),
                  (S.length = 0),
                  (R.tap = []),
                  (R.over = []),
                  n && (H(), q()),
                  S.forEach(function (t) {
                    t.isActive() && t.update();
                  }),
                  K &&
                  i &&
                  ("function" == typeof i.enabled ? i.enabled() : i.enabled)
                    ? (i.compensateForLayoutChanges &&
                        i.compensateForLayoutChanges({
                          currentNodes: K,
                          currentRect: C.rect.computed,
                          previousRect: Q,
                        }),
                      (tt = oo(
                        { old: K, current: r },
                        { renderer: Y },
                        i
                      )).start())
                    : Y.render(r),
                  (K = r),
                  (Q = C.rect.computed),
                  Y.setKey && "string" == typeof m.key && Y.setKey(m.key);
              }),
              (J.updated = F),
              (J.destroy = function () {
                J.unmount(), I(i), Y.destroy(), W(), (i = null);
              });
            var nt = function (t) {
              (M.nodes = t),
                S.forEach(function (t) {
                  t.isActive() && t.update();
                }),
                Y.render(t);
            };
            return (
              Ho(z, t, {
                settings: function () {
                  return x;
                },
                data: function () {
                  return b;
                },
                scale: function () {
                  return e;
                },
                formatter: function () {
                  return n;
                },
                renderer: function () {
                  return Y;
                },
                chart: function () {
                  return h;
                },
                dockConfig: function () {
                  return G;
                },
                mediator: function () {
                  return d;
                },
                instance: function () {
                  return C;
                },
                rect: function () {
                  return C.rect;
                },
                style: function () {
                  return a;
                },
                update: function () {
                  return nt;
                },
                registries: function () {
                  return g;
                },
                resolver: function () {
                  return _;
                },
                symbol: function () {
                  return Vo(g.symbol);
                },
              }),
              Ho(C, m, {
                settings: function () {
                  return x;
                },
                data: function () {
                  return b;
                },
                scale: function () {
                  return e;
                },
                formatter: function () {
                  return n;
                },
                renderer: function () {
                  return Y;
                },
                chart: function () {
                  return h;
                },
                dockConfig: function () {
                  return G;
                },
                mediator: function () {
                  return d;
                },
                style: function () {
                  return a;
                },
                _DO_NOT_USE_getInfo: l.bind(z),
                isVisible: function () {
                  return w;
                },
              }),
              (J.getBrushedShapes = function (t, e, n) {
                var i = [];
                if (x.brush && x.brush.consume) {
                  var r = h.brush(t),
                    a = Y.findShapes("*");
                  x.brush.consume
                    .filter(function (e) {
                      return e.context === t;
                    })
                    .forEach(function (t) {
                      for (var o = 0; o < a.length; o++) {
                        var s = a[o];
                        s.data &&
                          r.containsMappedData(s.data, n || t.data, e) &&
                          (Z(s), i.push(s), a.splice(o, 1), o--);
                      }
                    });
                }
                return i;
              }),
              (J.findShapes = function (t) {
                for (
                  var e,
                    n = (
                      null !== (e = tt) && void 0 !== e && e.inProgress()
                        ? tt.targetScene
                        : Y
                    ).findShapes(t),
                    i = 0,
                    r = n.length;
                  i < r;
                  i++
                )
                  Z(n[i]);
                return n;
              }),
              (J.shapesAt = function (t) {
                var e,
                  n =
                    arguments.length > 1 && void 0 !== arguments[1]
                      ? arguments[1]
                      : {},
                  i = Y.itemsAt(t);
                e =
                  n && "stop" === n.propagation && i.length > 0
                    ? [i.pop().node]
                    : i.map(function (t) {
                        return t.node;
                      });
                for (var r = 0, a = e.length; r < a; r++) Z(e[r]);
                return e;
              }),
              (J.brushFromShapes = function (t) {
                var e =
                  arguments.length > 1 && void 0 !== arguments[1]
                    ? arguments[1]
                    : {};
                (e.contexts = Array.isArray(e.contexts) ? e.contexts : []),
                  vo({
                    nodes: t,
                    action: e.action || "toggle",
                    trigger: e,
                    chart: h,
                    data: M.data,
                  });
              }),
              (J.mount = function () {
                (i = Y.element && Y.element() ? i : Y.appendTo(f)),
                  Y.setKey && "string" == typeof m.key && Y.setKey(m.key),
                  x.brush && (H(), q()),
                  Uo(C, v, m),
                  Uo(z, v, t),
                  (w = !0);
              }),
              (J.mounted = function () {
                return P(i);
              }),
              (J.unmount = function () {
                [C, z].forEach(function (t) {
                  Yo(t, v);
                }),
                  (R.tap = []),
                  (R.over = []),
                  S.forEach(function (t) {
                    t.cleanUp();
                  }),
                  (S.length = 0),
                  B(),
                  (w = !1);
              }),
              (J.onBrushTap = function (t) {
                R.tap.forEach(function (e) {
                  _o({ e: t, t: e, config: M }) &&
                    "stop" === e.globalPropagation &&
                    h.toggleBrushing(!0);
                });
              }),
              (J.onBrushOver = function (t) {
                R.over.forEach(function (e) {
                  wo({ e: t, t: e, config: M }) &&
                    "stop" === e.globalPropagation &&
                    h.toggleBrushing(!0);
                });
              }),
              (J.def = z),
              (J.ctx = C),
              (J.renderer = function () {
                return Y;
              }),
              J.set({ settings: m }),
              T(),
              J
            );
          })(u.component(t.type), {
            settings: t,
            chart: d,
            mediator: g,
            registries: u,
            theme: h,
            container: n,
          });
          return {
            instance: e,
            settings: k(!0, {}, t),
            key: t.key,
            hasKey: void 0 !== t.key,
          };
        },
      });
    function z(e) {
      var n =
        arguments.length > 1 && void 0 !== arguments[1]
          ? arguments[1]
          : function () {};
      return function () {
        for (
          var i,
            r,
            a = "function" == typeof t[e],
            o = arguments.length,
            s = new Array(o),
            c = 0;
          c < o;
          c++
        )
          s[c] = arguments[c];
        a
          ? (i = (r = t[e]).call.apply(r, [d].concat(s)))
          : (i = n.call.apply(n, [d].concat(s)));
        return i;
      };
    }
    var C = function () {
        var t;
        o.dockLayout
          ? (l.warn(
              'Deprecation Warning: "dockLayout" property should be renamed to "strategy"'
            ),
            (t = o.dockLayout))
          : (t = o.strategy);
        var e = (function (t) {
          if ("function" == typeof t.getBoundingClientRect) {
            var e = t.getBoundingClientRect();
            return { x: 0, y: 0, width: e.width, height: e.height };
          }
          return { x: 0, y: 0, width: 0, height: 0 };
        })(n);
        return R.layout({ layoutSettings: t, rect: e });
      },
      A = z("created"),
      E = z("beforeMount"),
      O = z("mounted"),
      T = z("beforeUpdate"),
      j = z("updated"),
      P = z("beforeRender"),
      B = z("beforeDestroy"),
      D = z("destroyed"),
      F = function (t, e) {
        var n =
            arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
          i = n.partialData,
          r = e.formatters,
          a = void 0 === r ? {} : r,
          o = e.scales,
          s = void 0 === o ? {} : o,
          c = e.scroll,
          f = void 0 === c ? {} : c;
        (b = nt(t, { logger: l, types: u.data })),
          i ||
            Object.keys(w).forEach(function (t) {
              return w[t].clear();
            }),
          e.palettes && h.setPalettes(e.palettes),
          e.style && h.setStyle(e.style),
          (_ = mi(e.collections, { dataset: b }, { logger: l }));
        var d = { theme: h, logger: l };
        (y = Aa(
          s,
          { dataset: b, collection: _ },
          S(S({}, d), {}, { scale: u.scale })
        )),
          (v = bi(
            a,
            { dataset: b, collection: _ },
            S(S({}, d), {}, { formatter: u.formatter })
          )),
          (m = Da(f, m));
      };
    function L() {
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
        e = {},
        i = t
          .filter(function (t) {
            return !!t.key;
          })
          .map(function (t) {
            return t.key;
          });
      x.forEach(function (t) {
        t.key && -1 !== i.indexOf(t.key) ? (e[t.key] = t) : t.destroy();
      }),
        (x = t.map(function (t) {
          var i = t.key && e[t.key] ? e[t.key] : u.interaction(t.type)(d, g, n);
          return i.set(t), i;
        }));
    }
    var I = function (t) {
        var e = n.getBoundingClientRect(),
          i = "clientX" in t ? t.clientX : t.x,
          r = "clientY" in t ? t.clientY : t.y,
          a = { x: i - e.left, y: r - e.top },
          o = [];
        return (
          p.forEach(function (t) {
            var e = t.instance.getRect();
            fs(
              e.computedPhysical
                ? e.computedPhysical
                : {
                    x: e.margin.left + e.x * e.scaleRatio.x,
                    y: e.margin.top + e.y * e.scaleRatio.y,
                    width: e.width * e.scaleRatio.x,
                    height: e.height * e.scaleRatio.y,
                  },
              a
            ) && o.push(t);
          }),
          o
        );
      },
      W = function () {
        if (!f.length && n) {
          Object.keys(c).forEach(function (t) {
            var e = c[t].bind(d);
            n.addEventListener(t, e), f.push({ key: t, listener: e });
          });
          var t = {},
            e = function (e) {
              e.touches
                ? ((t.x = e.touches[0].clientX),
                  (t.y = e.touches[0].clientY),
                  (t.multiTouch = e.touches.length > 1))
                : ((t.x = e.clientX), (t.y = e.clientY), (t.multiTouch = !1)),
                (t.time = Date.now()),
                (t.comps = I(t));
            },
            i = function (e) {
              var n = t.comps || I(e);
              if (
                !n.every(function (t) {
                  return t.instance.def.disableTriggers;
                }) &&
                ("touchend" === e.type && e.preventDefault(),
                (function (t, e) {
                  var n = U(t),
                    i = n ? t.changedTouches[0] : t,
                    r = Date.now() - e.time,
                    a = isNaN(e.x) ? 0 : Math.abs(i.clientX - e.x),
                    o = isNaN(e.y) ? 0 : Math.abs(i.clientY - e.y);
                  return (
                    (0 === t.button || n) &&
                    !e.multiTouch &&
                    a <= 12 &&
                    o <= 12 &&
                    r <= 300
                  );
                })(e, t))
              )
                for (var i = n.length - 1; i >= 0; i--) {
                  if ((n[i].instance.onBrushTap(e), M)) {
                    M = !1;
                    break;
                  }
                }
            },
            r = [];
          r.push({ key: "mousedown", listener: e }),
            r.push({ key: "mouseup", listener: i }),
            (function (t) {
              return (
                ("ontouchstart" in t && "ontouchend" in t) ||
                navigator.maxTouchPoints > 1
              );
            })(n) &&
              (r.push({ key: "touchstart", listener: e }),
              r.push({ key: "touchend", listener: i })),
            r.push({
              key: "mousemove",
              listener: function (t) {
                for (var e = I(t), n = e.length - 1; n >= 0; n--) {
                  if ((e[n].instance.onBrushOver(t), M)) {
                    M = !1;
                    break;
                  }
                }
              },
            }),
            r.forEach(function (t) {
              n.addEventListener(t.key, t.listener), f.push(t);
            });
        }
      },
      V = function () {
        f.forEach(function (t) {
          var e = t.key,
            i = t.listener;
          return n.removeEventListener(e, i);
        }),
          (f.length = 0);
      };
    return (
      (d.layoutComponents = function () {
        var t =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          e = t.excludeFromUpdate,
          n = void 0 === e ? [] : e;
        t.data && (r = t.data),
          t.settings && ((o = t.settings), L(t.settings.interactions)),
          T(),
          F(r, o);
        var i = o,
          a = i.formatters,
          s = i.scales,
          c = i.components,
          u = void 0 === c ? [] : c;
        R.update({
          components: u,
          data: r,
          excludeFromUpdate: n,
          formatters: a,
          scales: s,
        }),
          R.forEach(function (t) {
            t.updateWith && t.instance.set(t.updateWith);
          }),
          R.forEach(function (t) {
            t.updateWith && t.instance.beforeUpdate();
          }),
          C();
      }),
      (d.update = function () {
        var t,
          e =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          i = e.partialData,
          a = e.excludeFromUpdate,
          s = void 0 === a ? [] : a;
        e.data && (r = e.data),
          e.settings && ((o = e.settings), L(e.settings.interactions)),
          T(),
          F(r, o, { partialData: i });
        var c = o,
          u = c.formatters,
          l = c.scales,
          h = c.components,
          f = void 0 === h ? [] : h;
        R.update({
          components: f,
          data: r,
          excludeFromUpdate: s,
          formatters: u,
          scales: l,
        }),
          R.forEach(function (t) {
            t.updateWith && t.instance.set(t.updateWith);
          }),
          R.forEach(function (t) {
            t.updateWith && t.instance.beforeUpdate();
          });
        var d,
          g = [],
          y = [];
        if (i)
          R.forEach(function (t) {
            (t.updateWith || t.applyTransform) && t.visible && g.push(t);
          }),
            (d = g);
        else {
          var v = C(),
            m = v.visible,
            x = v.hidden,
            b = v.ordered;
          (p = m),
            (d = m),
            (t = b),
            m.forEach(function (t) {
              t.updateWith && t.visible ? g.push(t) : y.push(t);
            }),
            x.forEach(function (t) {
              t.instance.hide(),
                (t.visible = !1),
                delete t.updateWith,
                (t.applyTransform = !1);
            });
        }
        y.forEach(function (t) {
          return t.instance.beforeMount();
        }),
          y.forEach(function (t) {
            return t.instance.mount();
          }),
          d.forEach(function (t) {
            return t.instance.beforeRender();
          }),
          d.forEach(function (t) {
            (t.updateWith || t.applyTransform) && t.visible
              ? t.instance.update()
              : t.instance.render();
          }),
          i || Zs(n, t),
          y.forEach(function (t) {
            return t.instance.mounted();
          }),
          g.forEach(function (t) {
            return t.instance.updated();
          }),
          p.forEach(function (t) {
            delete t.updateWith, (t.visible = !0), (t.applyTransform = !1);
          }),
          j();
      }),
      (d.destroy = function () {
        B(), R.destroy(), V(), L(), delete d.update, delete d.destroy, D();
      }),
      (d.getAffectedShapes = function (t) {
        var e =
            arguments.length > 1 && void 0 !== arguments[1]
              ? arguments[1]
              : "and",
          n = arguments.length > 2 ? arguments[2] : void 0,
          i = arguments.length > 3 ? arguments[3] : void 0,
          r = [];
        return (
          p
            .filter(function (t) {
              return null == i || t.key === i;
            })
            .forEach(function (i) {
              r.push.apply(r, $(i.instance.getBrushedShapes(t, e, n)));
            }),
          r
        );
      }),
      (d.findShapes = function (t) {
        var e = [];
        return (
          p.forEach(function (n) {
            e.push.apply(e, $(n.instance.findShapes(t)));
          }),
          e
        );
      }),
      (d.componentsFromPoint = function (t) {
        return I(t).map(function (t) {
          return t.instance.ctx;
        });
      }),
      (d.shapesAt = function (t) {
        var e =
            arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
          i = [],
          r = n.getBoundingClientRect(),
          a = p;
        if (Array.isArray(e.components) && e.components.length > 0) {
          var o = e.components.map(function (t) {
            return t.key;
          });
          a = p
            .filter(function (t) {
              return -1 !== o.indexOf(t.key);
            })
            .map(function (t) {
              return {
                instance: t.instance,
                opts: e.components[o.indexOf(t.key)],
              };
            });
        }
        for (var s = a.length - 1; s >= 0; s--) {
          var c = a[s],
            u = c.instance.renderer().element().getBoundingClientRect(),
            l = Gs(t, r, u),
            h = c.instance.shapesAt(l, c.opts),
            f = h.length > 0 && "stop" === e.propagation;
          if ((i.push.apply(i, $(h)), i.length > 0 && f)) return i;
        }
        return i;
      }),
      (d.brushFromShapes = function (t) {
        for (
          var e =
              arguments.length > 1 && void 0 !== arguments[1]
                ? arguments[1]
                : { components: [] },
            n = function (n) {
              var i = e.components[n].key;
              p.filter(function (t) {
                return i === t.key;
              }).forEach(function (i) {
                var r = t.filter(function (t) {
                  return t.key === i.key;
                });
                i.instance.brushFromShapes(r, e.components[n]);
              });
            },
            i = 0;
          i < e.components.length;
          i++
        )
          n(i);
      }),
      (d.scroll = function () {
        var t =
          arguments.length > 0 && void 0 !== arguments[0]
            ? arguments[0]
            : "default";
        return Fa(t, m);
      }),
      (d.dataset = function (t) {
        return b(t);
      }),
      (d.dataCollection = function (t) {
        return _(t);
      }),
      (d.scales = function () {
        return y.all();
      }),
      (d.formatters = function () {
        return v.all();
      }),
      (d.brush = function () {
        var t =
          arguments.length > 0 && void 0 !== arguments[0]
            ? arguments[0]
            : "default";
        return w[t] || (w[t] = io()), w[t];
      }),
      (d.scale = function (t) {
        return y.get(t);
      }),
      (d.formatter = function (t) {
        return v.get(t);
      }),
      (d.toggleBrushing = function (t) {
        M = void 0 !== t ? t : !M;
      }),
      (d.component = function (t) {
        var e = R.findComponentByKey(t);
        return null == e ? void 0 : e.instance.ctx;
      }),
      (d.logger = function () {
        return l;
      }),
      (d.theme = function () {
        return h;
      }),
      Object.defineProperty(d, "interactions", {
        get: function () {
          return {
            instances: x,
            on: function () {
              W(),
                x.forEach(function (t) {
                  return t.on();
                });
            },
            off: function () {
              V(),
                x.forEach(function (t) {
                  return t.off();
                });
            },
          };
        },
      }),
      A(),
      n &&
        (E(),
        (n.innerHTML = ""),
        (function () {
          var t = o.components,
            e = void 0 === t ? [] : t;
          P(), F(r, o), R.set({ components: e });
          var i = C(),
            a = i.visible,
            s = i.hidden,
            c = i.ordered;
          (p = a),
            s.forEach(function (t) {
              t.instance.hide(), (t.visible = !1);
            }),
            a.forEach(function (t) {
              return t.instance.beforeMount();
            }),
            a.forEach(function (t) {
              return t.instance.mount();
            }),
            a.forEach(function (t) {
              return t.instance.beforeRender();
            }),
            a.forEach(function (t) {
              return t.instance.render();
            }),
            a.forEach(function (t) {
              return t.instance.mounted();
            }),
            a.forEach(function (t) {
              t.visible = !0;
            }),
            Zs(n, c);
        })(),
        W(),
        L(o.interactions),
        O(n),
        (d.element = n)),
      d
    );
  }
  var Ks = function (t) {
      var e = oe(t);
      return (
        (e.prio = function (t) {
          return t ? e.default(t[0]) : [e.default()];
        }),
        (e.types = function () {
          return e.getKeys();
        }),
        e
      );
    },
    Qs = oe();
  function tc(t, e) {
    var n = e.cache;
    if ("number" == typeof t) return n.fields[t];
    for (var i = 0; i < n.fields.length; i++)
      if (n.fields[i].key() === t) return n.fields[i];
    for (var r = 0; r < n.fields.length; r++)
      if (n.fields[r].title() === t) return n.fields[r];
    return null;
  }
  var ec = function (t) {
      return t.filter(function (t) {
        return "number" == typeof t && !isNaN(t);
      });
    },
    nc = function (t) {
      return t.reduce(function (t, e) {
        return t + e;
      }, 0);
    },
    ic = {
      first: function (t) {
        return t[0];
      },
      last: function (t) {
        return t[t.length - 1];
      },
      min: function (t) {
        var e = ec(t);
        return e.length ? Math.min.apply(null, e) : NaN;
      },
      max: function (t) {
        var e = ec(t);
        return e.length ? Math.max.apply(null, e) : NaN;
      },
      sum: function (t) {
        var e = ec(t);
        return e.length
          ? e.reduce(function (t, e) {
              return t + e;
            }, 0)
          : NaN;
      },
      avg: function (t) {
        var e = ec(t),
          n = e.length;
        return n ? nc(e) / n : NaN;
      },
    };
  function rc(t, e, n, i) {
    var r = {},
      a = i.field || (void 0 !== t.field ? e.field(t.field) : null);
    return (
      Object.keys(n).forEach(function (o) {
        var s = n[o],
          c = (r[o] = {});
        -1 !== ["number", "string", "boolean"].indexOf(N(s))
          ? ((c.type = "primitive"), (c.value = s))
          : "function" == typeof s
          ? ((c.type = "function"), (c.value = s), (c.label = s), (c.field = a))
          : "object" === N(s) &&
            (s.fields
              ? (c.fields = s.fields.map(function (n) {
                  return rc(t, e, { main: n }, i).main;
                }))
              : void 0 !== s.field
              ? ((c.type = "field"),
                (c.field = e.field(s.field)),
                (c.value = c.field.value),
                (c.label = c.field.label))
              : a && ((c.value = a.value), (c.label = a.label), (c.field = a)),
            "function" == typeof s.filter && (c.filter = s.filter),
            void 0 !== s.value && (c.value = s.value),
            void 0 !== s.label && (c.label = s.label),
            "function" == typeof s.reduce
              ? (c.reduce = s.reduce)
              : s.reduce
              ? (c.reduce = ic[s.reduce])
              : c.field &&
                c.field.reduce &&
                (c.reduce =
                  "string" == typeof c.field.reduce
                    ? ic[c.field.reduce]
                    : c.field.reduce),
            "function" == typeof s.reduceLabel
              ? (c.reduceLabel = s.reduceLabel)
              : s.reduceLabel
              ? (c.reduceLabel = ic[s.reduceLabel])
              : c.field &&
                c.field.reduceLabel &&
                (c.reduceLabel =
                  "string" == typeof c.field.reduceLabel
                    ? ic[c.field.reduceLabel]
                    : c.field.reduceLabel));
      }),
      r
    );
  }
  function ac(t, e) {
    var n = rc(
      t,
      e,
      {
        main: {
          value: t.value,
          label: t.label,
          reduce: t.reduce,
          filter: t.filter,
        },
      },
      {}
    ).main;
    return { props: rc(t, e, t.props || {}, n), main: n };
  }
  function oc(t, e, n, i) {
    for (
      var r, a = Array(t.length), o = Array(t.length), s = 0;
      s < t.length;
      s++
    )
      (r = i ? t[s][i] : t[s]), (a[s] = r.value), (o[s] = r.label);
    var c = e.reduce,
      u = e.reduceLabel,
      l = c ? c(a) : a,
      h = { value: l, label: u ? u(o, l) : n ? n(l) : String(l) };
    return i && t[0][i].source
      ? ((h.source = t[0][i].source), h)
      : !i && t[0].source
      ? ((h.source = t[0].source), h)
      : h;
  }
  function sc(t, e) {
    var n = e.main,
      i = e.propsArr,
      r = e.props,
      a = [],
      o = n.field.formatter(),
      s = {};
    return (
      i.forEach(function (t) {
        s[t] = r[t].field
          ? r[t].field.formatter()
          : function (t) {
              return t;
            };
      }),
      a.push.apply(
        a,
        $(
          t.map(function (t) {
            var e = oc(t.items, n, o);
            return (
              i.forEach(function (n) {
                e[n] = oc(t.items, r[n], s[n], n);
              }),
              e
            );
          })
        )
      ),
      a
    );
  }
  function cc(t) {
    var e = t.cfg,
      n = t.itemData,
      i = t.obj,
      r = t.target,
      a = t.tracker,
      o = "function" === t.trackType ? e.trackBy(n) : n[e.trackBy],
      s = a[o];
    s || ((s = a[o] = { items: [], id: o }), r.push(s)), s.items.push(i);
  }
  function uc(t, e, n) {
    var i = n.key,
      r = {
        value:
          "function" == typeof t.value
            ? t.value(e)
            : void 0 !== t.value
            ? t.value
            : e,
      };
    return (
      (r.label =
        "function" == typeof t.label
          ? t.label(e)
          : void 0 !== t.label
          ? String(t.label)
          : String(r.value)),
      t.field && (r.source = { key: i, field: t.field.key() }),
      r
    );
  }
  function lc(t, e) {
    var n = Array.isArray(t) ? t : [t],
      i = [];
    return (
      n.forEach(function (t) {
        void 0 !== t.field &&
          (function () {
            var n = e.field(t.field),
              r = e.key();
            if (!n) throw Error("Field '".concat(t.field, "' not found"));
            for (
              var a = ac(t, e),
                o = a.props,
                s = a.main,
                c = Object.keys(o),
                u = !!t.trackBy,
                l = N(t.trackBy),
                h = {},
                f = [],
                d = n.items(),
                g = [],
                p = function (e) {
                  var n = d[e];
                  if (s.filter && !s.filter(n)) return "continue";
                  var i = uc(s, n, { key: r });
                  c.forEach(function (t) {
                    var a = o[t],
                      s = a.field ? a.field.items()[e] : n;
                    i[t] = uc(a, s, { key: r });
                  }),
                    u &&
                      cc({
                        cfg: t,
                        itemData: n,
                        obj: i,
                        target: f,
                        tracker: h,
                        trackType: l,
                      }),
                    g.push(i);
                },
                y = 0;
              y < d.length;
              y++
            )
              p(y);
            u
              ? i.push.apply(i, $(sc(f, { main: s, propsArr: c, props: o })))
              : i.push.apply(i, g);
          })();
      }),
      i
    );
  }
  var hc = function (t) {
    return t.filter(function (t) {
      return "number" == typeof t && !isNaN(t);
    });
  };
  var fc = function (t) {
    var e = t.key,
      n = t.data,
      i = t.cache,
      r = t.config;
    if (n) {
      var a = n;
      "string" == typeof a &&
        (a = (function (t) {
          var e = t.data,
            n = t.config,
            i = e.split("\n"),
            r = i[0],
            a = i[1],
            o = ",";
          if (n && n.parse && n.parse.delimiter) o = n.parse.delimiter;
          else if (r)
            for (var s = [/,/, /\t/, /;/], c = 0; c < s.length; c++) {
              var u = s[c];
              if (r && a) {
                if (
                  u.test(r) &&
                  u.test(a) &&
                  r.split(u).length === a.split(u).length
                ) {
                  o = u;
                  break;
                }
              } else u.test(r) && (o = u);
            }
          return i.map(function (t) {
            return t.split(o);
          });
        })({ data: n, config: r })),
        Array.isArray(a) &&
          (function (t) {
            var e,
              n = t.source,
              i = t.data,
              r = t.cache,
              a = t.config,
              o = i,
              s = a && a.parse;
            Array.isArray(i[0])
              ? s && !1 === s.headers
                ? (e = i[0].map(function (t, e) {
                    return e;
                  }))
                : ((e = i[0]), (o = i.slice(1)))
              : (e = Object.keys(i[0]));
            var c,
              u = !!s && "function" == typeof s.row && s.row,
              l = e;
            (l =
              s && "function" == typeof s.fields
                ? s.fields(l.slice())
                : e.map(function (t) {
                    return { key: t, title: t };
                  })),
              Array.isArray(i[0])
                ? (c = l.map(function () {
                    return [];
                  }))
                : ((c = {}),
                  l.forEach(function (t) {
                    c[t.key] = [];
                  }));
            for (var h = 0; h < o.length; h++) {
              var f = u ? u(o[h], h, l) : o[h];
              if (f)
                if (Array.isArray(f))
                  for (var d = 0; d < l.length; d++) c[d].push(f[d]);
                else
                  for (var g = 0; g < l.length; g++)
                    c[l[g].key].push(f[l[g].key]);
            }
            for (
              var p = Array.isArray(c)
                  ? function (t) {
                      return c[t];
                    }
                  : function (t) {
                      return c[l[t].key];
                    },
                y = 0;
              y < l.length;
              y++
            ) {
              var v = p(y),
                m = hc(v),
                x = m.length > 0,
                b = x ? "measure" : "dimension",
                _ = x ? Math.min.apply(Math, $(m)) : NaN,
                w = x ? Math.max.apply(Math, $(m)) : NaN;
              r.fields.push(
                di(
                  k(
                    {
                      source: n,
                      key: y,
                      title: l[y].title,
                      values: v,
                      min: _,
                      max: w,
                      type: b,
                    },
                    l[y]
                  ),
                  { value: l[y].value, label: l[y].label }
                )
              );
            }
          })({ data: a, cache: i, source: e, config: r });
    }
  };
  function dc() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = t.key,
      n = t.data,
      i = t.config,
      r = { fields: [] },
      a = {
        key: function () {
          return e;
        },
        raw: function () {
          return n;
        },
        field: function (t) {
          return tc(t, { cache: r, matrix: n });
        },
        fields: function () {
          return r.fields.slice();
        },
        extract: function (t) {
          return lc(t, a);
        },
        hierarchy: function () {
          return null;
        },
      };
    return fc({ key: e, data: n, config: i, cache: r }), a;
  }
  dc.util = { normalizeConfig: ac, collect: sc, track: cc };
  var gc = oe();
  gc.default("matrix"), gc("matrix", dc), gc("default", dc);
  var pc = oe();
  function yc(t, e, n) {
    return Math.max(t, Math.min(e, n));
  }
  function vc(t) {
    var e,
      n = t.item,
      i = t.value,
      r = t.boxCenter,
      a = t.rendWidth,
      o = t.rendHeight,
      s = "x",
      c = "y",
      u = a,
      l = o,
      h = i < 0.5 ? 90 : -90;
    return (
      t.flipXY &&
        ((s = "y"), (c = "x"), (u = o), (l = a), (h = i < 0.5 ? 180 : 0)),
      (0, t.symbol)(
        k(
          {},
          n.oob,
          (E((e = {}), s, r * u),
          E(
            e,
            c,
            Math.max(n.oob.size / 2, Math.min(i * l, l - n.oob.size / 2))
          ),
          E(e, "startAngle", h),
          e)
        )
      )
    );
  }
  function mc(t) {
    var e,
      n = t.item,
      i = t.boxWidth,
      r = t.boxPadding,
      a = t.rendWidth,
      o = t.rendHeight,
      s = "x",
      c = "y",
      u = "width",
      l = "height",
      h = a,
      f = o;
    t.flipXY &&
      ((s = "y"), (c = "x"), (u = "height"), (l = "width"), (h = o), (f = a));
    var d = (function (t) {
        var e = t.start,
          n = t.end,
          i = t.minPx,
          r = void 0 === i ? 0.1 : i,
          a = t.maxPx,
          o = void 0 === a ? 1 : a,
          s = Math.max(e, n),
          c = Math.min(e, n),
          u = yc(-0.1, 1.2, s),
          l = yc(-0.1, 1.2, c),
          h = u * o - l * o,
          f = Math.max(r, h),
          d = (f - h) / 2;
        return { actualDiff: f, startModifier: d, actualLow: l * o - d };
      })({ start: n.start, end: n.end, minPx: n.box.minHeightPx, maxPx: f }),
      g = d.actualDiff,
      p = d.actualLow;
    return k(
      {},
      n.box,
      (E((e = { type: "rect" }), s, (r + n.major) * h),
      E(e, c, p),
      E(e, l, g),
      E(e, u, i * h),
      E(e, "data", n.data || {}),
      E(e, "collider", { type: null }),
      e)
    );
  }
  function xc(t) {
    var e,
      n = t.item,
      i = t.from,
      r = t.to,
      a = t.boxCenter,
      o = t.rendWidth,
      s = t.rendHeight,
      c = "x1",
      u = "y1",
      l = "x2",
      h = "y2",
      f = o,
      d = s;
    return (
      t.flipXY &&
        ((c = "y1"), (u = "x1"), (l = "y2"), (h = "x2"), (f = s), (d = o)),
      k(
        {},
        n.line,
        (E((e = { type: "line" }), h, Math.floor(i * d)),
        E(e, c, a * f),
        E(e, u, Math.floor(r * d)),
        E(e, l, a * f),
        E(e, "data", n.data || {}),
        E(e, "collider", { type: null }),
        e)
      )
    );
  }
  function bc(t) {
    var e,
      n = t.item,
      i = t.key,
      r = t.position,
      a = t.width,
      o = t.boxCenter,
      s = t.rendWidth,
      c = t.rendHeight,
      u = "x1",
      l = "y1",
      h = "x2",
      f = "y2",
      d = s,
      g = c;
    t.flipXY &&
      ((u = "y1"), (l = "x1"), (h = "y2"), (f = "x2"), (d = c), (g = s));
    var p = a / 2;
    return k(
      { type: "line" },
      n[i],
      (E((e = {}), l, Math.floor(r * g)),
      E(e, u, (o - p) * d),
      E(e, f, Math.floor(r * g)),
      E(e, h, (o + p) * d),
      E(e, "r", p * d),
      E(e, "cx", o * d),
      E(e, "cy", r * g),
      E(e, "width", a * d),
      E(e, "data", n.data || {}),
      E(e, "collider", { type: null }),
      e)
    );
  }
  function _c(t, e, n) {
    var i = e.box,
      r = i.width,
      a = i.maxWidthPx,
      o = i.minWidthPx,
      s = t >= 0 ? 1 : -1,
      c = Math.min(s * t * r, isNaN(a) ? n : a / n);
    return (c = isNaN(o) ? c : Math.max(o / n, c)) * s;
  }
  pc("native", function (t, e, n) {
    var i,
      r,
      a = { chart: t, mediator: e, element: n },
      o = [],
      s = !0;
    function c() {
      "function" == typeof i.enable && (i.enable = i.enable.bind(a)()),
        i.enable &&
          Object.keys(i.events).forEach(function (t) {
            var e = i.events[t].bind(a);
            n.addEventListener(t, e), o.push({ key: t, listener: e });
          });
    }
    function u() {
      o.forEach(function (t) {
        var e = t.key,
          i = t.listener;
        n.removeEventListener(e, i);
      }),
        (o = []);
    }
    return {
      get key() {
        return r;
      },
      set: function (t) {
        !(function (t) {
          (r = t.key),
            ((i = t).events = i.events || []),
            void 0 === i.enable && (i.enable = !0);
        })(t),
          u(),
          s && c();
      },
      off: function () {
        (s = !1), u();
      },
      on: function () {
        (s = !0), 0 === o.length && c();
      },
      destroy: function () {
        u(), (a = null), (i = null);
      },
    };
  });
  var wc = {
      oob: {
        show: !0,
        type: "n-polygon",
        fill: "#999",
        stroke: "#000",
        strokeWidth: 0,
        size: 10,
        sides: 3,
        startAngle: -90,
      },
      box: {
        show: !0,
        fill: "#fff",
        stroke: "#000",
        strokeWidth: 1,
        strokeLinejoin: "miter",
        width: 1,
        maxWidthPx: void 0,
        minWidthPx: 1,
        minHeightPx: 1,
      },
      line: { show: !0, stroke: "#000", strokeWidth: 1 },
      median: { show: !0, stroke: "#000", strokeWidth: 1 },
      whisker: {
        show: !0,
        stroke: "#000",
        strokeWidth: 1,
        fill: "",
        type: "line",
        width: 1,
      },
    },
    kc = Object.keys(wc),
    Mc = {
      require: ["chart", "resolver", "symbol"],
      defaultSettings: {
        settings: {},
        data: {},
        style: {
          box: "$shape",
          line: "$shape-guide",
          whisker: "$shape-guide",
          median: "$shape-guide--inverted",
        },
      },
      created: function () {
        this.state = {};
      },
      render: function (t) {
        var e = this,
          n = t.data,
          i = this.rect,
          r = i.width,
          a = i.height,
          o = "horizontal" === this.settings.settings.orientation,
          s = this.style,
          c = this.resolver,
          u = this.symbol,
          l = kc.filter(function (t) {
            return (
              !e.settings.settings[t] || !1 !== e.settings.settings[t].show
            );
          }),
          h = {};
        l.forEach(function (t) {
          return (h[t] = wc[t]);
        });
        var f = (function (t) {
            var e,
              n = t.keys,
              i = t.data,
              r = t.defaultSettings,
              a = t.style,
              o = t.settings,
              s = t.width,
              c = t.height,
              u = t.resolver,
              l = k(!0, {}, r || {}, a || {}),
              h = {
                major: "horizontal" === o.orientation ? c : s,
                minor: "horizontal" === o.orientation ? s : c,
              },
              f = o.major;
            e =
              "object" === N(f) &&
              "object" === N(f.ref) &&
              void 0 !== f.ref.start &&
              void 0 !== f.ref.end
                ? u.resolve({
                    data: i,
                    defaults: { start: 0, end: 1 },
                    scaled: h,
                    settings: k(
                      !0,
                      {},
                      {
                        binStart: {
                          scale: o.major.scale,
                          ref: o.major.ref.start,
                        },
                        binEnd: { scale: o.major.scale, ref: o.major.ref.end },
                      }
                    ),
                  })
                : "object" === N(f) &&
                  void 0 !== f.binStart &&
                  void 0 !== f.binEnd
                ? u.resolve({
                    data: i,
                    defaults: { start: 0, end: 1 },
                    scaled: h,
                    settings: k(
                      !0,
                      {},
                      {
                        binStart: { scale: o.major.scale, ref: "binStart" },
                        binEnd: { scale: o.major.scale, ref: "binEnd" },
                      },
                      o.major
                    ),
                  })
                : u.resolve({
                    data: i,
                    scaled: h,
                    defaults: { major: 0.5 },
                    settings: { major: o.major },
                  });
            var d = o.minor || {},
              g = {};
            ["start", "end", "min", "max", "med"].forEach(function (t) {
              (d[t] || (i.items && i.items.length && i.items[0][t])) &&
                (g[t] = { scale: d.scale, ref: t });
            });
            for (
              var p,
                y = {
                  major: e,
                  minor: u.resolve({
                    data: i,
                    defaults: { start: 0, end: 1 },
                    scaled: h,
                    settings: k(!0, {}, g, d),
                  }),
                },
                v = 0,
                m = n.length;
              v < m;
              v++
            )
              (o[p] && !1 === o[p].show) ||
                (y[(p = n[v])] = u.resolve({
                  data: i,
                  defaults: l[p],
                  settings: o[p],
                  scaled: h,
                }));
            return y;
          })({
            keys: l,
            data: n,
            defaultSettings: h,
            style: s,
            settings: this.settings.settings,
            width: r,
            height: a,
            resolver: c,
          }),
          d = f.settings,
          g = (function (t) {
            var e = t.width,
              n = t.height,
              i = t.flipXY,
              r = t.resolved,
              a = t.keys,
              o = t.symbol,
              s = [],
              c = r.major.items;
            if (!c.length) return s;
            var u,
              l,
              h,
              f,
              d,
              g,
              p,
              y,
              v,
              m = e,
              x = n,
              b = i ? n : e,
              _ = r.major.settings,
              w = ["start", "end", "min", "max", "med"].filter(function (t) {
                return void 0 !== r.minor.settings[t];
              }),
              M = w.length,
              R = a.filter(function (t) {
                return "oob" !== t;
              }),
              S = a ? a.length : 0,
              N = R ? R.length : 0,
              z = {
                box: function () {
                  h.box &&
                    Y(h.start) &&
                    Y(h.end) &&
                    u.push(
                      mc({
                        item: h,
                        boxWidth: f,
                        boxPadding: d,
                        rendWidth: m,
                        rendHeight: x,
                        flipXY: i,
                      })
                    );
                },
                line: function () {
                  Y(h.min) &&
                    Y(h.start) &&
                    u.push(
                      xc({
                        item: h,
                        from: h.min,
                        to: h.start,
                        boxCenter: g,
                        rendWidth: m,
                        rendHeight: x,
                        flipXY: i,
                      })
                    ),
                    Y(h.max) &&
                      Y(h.end) &&
                      u.push(
                        xc({
                          item: h,
                          from: h.max,
                          to: h.end,
                          boxCenter: g,
                          rendWidth: m,
                          rendHeight: x,
                          flipXY: i,
                        })
                      );
                },
                median: function () {
                  h.median &&
                    Y(h.med) &&
                    u.push(
                      bc({
                        item: h,
                        key: "median",
                        position: h.med,
                        width: f,
                        boxCenter: g,
                        rendWidth: m,
                        rendHeight: x,
                        flipXY: i,
                      })
                    );
                },
                whisker: function () {
                  if (h.whisker) {
                    var t = f * h.whisker.width;
                    Y(h.min) &&
                      u.push(
                        bc({
                          item: h,
                          key: "whisker",
                          position: h.min,
                          width: t,
                          boxCenter: g,
                          rendWidth: m,
                          rendHeight: x,
                          flipXY: i,
                        })
                      ),
                      Y(h.max) &&
                        u.push(
                          bc({
                            item: h,
                            key: "whisker",
                            position: h.max,
                            width: t,
                            boxCenter: g,
                            rendWidth: m,
                            rendHeight: x,
                            flipXY: i,
                          })
                        );
                  }
                },
              };
            function C() {
              for (
                var t, e = -Number.MAX_VALUE, n = Number.MAX_VALUE, i = 0;
                i < M;
                i++
              )
                Y((t = h[w[i]])) && (e < t && (e = t), n > t && (n = t));
              (p = e < 0 && e !== -Number.MAX_VALUE),
                (y = n > 1 && n !== Number.MAX_VALUE),
                (v = p || y);
            }
            for (var A = 0, E = c.length; A < E; A++) {
              (u = []), (l = null);
              var O = c[A],
                T = O.data,
                j = null,
                P = null;
              void 0 !== _.binStart
                ? ((j = O.binStart), (P = O.binEnd), (l = _.binStart.scale))
                : (j = (l = _.major.scale) ? O.major : 0);
              var B = 0;
              l
                ? l.bandwidth
                  ? (j -= (B = l.bandwidth()) / 2)
                  : (B = P - j)
                : (B = 1),
                (h = k({}, { major: j, majorEnd: P }, r.minor.items[A]));
              for (var D = 0; D < S; D++) h[a[D]] = r[a[D]].items[A];
              if (
                ((f = _c(B, h, b)),
                (g = (d = (B - f) / 2) + h.major + f / 2),
                C(),
                v)
              )
                h.oob &&
                  (p
                    ? u.push(
                        vc({
                          item: h,
                          value: 0,
                          boxCenter: g,
                          rendWidth: m,
                          rendHeight: x,
                          flipXY: i,
                          symbol: o,
                        })
                      )
                    : y &&
                      u.push(
                        vc({
                          item: h,
                          value: 1,
                          boxCenter: g,
                          rendWidth: m,
                          rendHeight: x,
                          flipXY: i,
                          symbol: o,
                        })
                      ));
              else
                for (var F = 0; F < N; F++)
                  (h[R[F]] && !1 === h[R[F]].show) || z[R[F]]();
              var L = {
                type: "container",
                data: T,
                collider: { type: "bounds" },
                children: u,
              };
              s.push(L);
            }
            return s;
          })({
            items: f.items,
            settings: d,
            width: r,
            height: a,
            flipXY: o,
            resolved: f,
            keys: l,
            symbol: u,
          });
        return g;
      },
    };
  var Rc = {
      shape: "saltire",
      width: 2,
      size: 0.5,
      fill: "#333",
      stroke: "#333",
      strokeWidth: 0,
    },
    Sc = /px$/,
    Nc = {
      shape: "circle",
      label: "",
      fill: "#333",
      stroke: "#ccc",
      strokeDasharray: "",
      strokeWidth: 0,
      strokeLinejoin: "miter",
      opacity: 1,
      x: 0.5,
      y: 0.5,
      size: 1,
      show: !0,
    },
    zc = {
      maxPx: 1e4,
      minPx: 1,
      maxRelExtent: 0.1,
      minRelExtent: 0.01,
      maxRelDiscrete: 1,
      minRelDiscrete: 0.1,
    };
  function Cc(t, e) {
    return t && "function" == typeof t.bandwidth
      ? { isBandwidth: !0, value: Math.max(1, t.bandwidth() * e) }
      : { isBandwidth: !1, value: Math.max(1, e) };
  }
  function Ac(t, e, n, i) {
    var r = e.width,
      a = e.height;
    return t
      .filter(function (t) {
        return !1 !== t.show && !isNaN(t.x + t.y);
      })
      .map(function (t) {
        var e = t,
          o = Sc.test(t.size)
            ? parseInt(t.size, 10)
            : n.min + e.size * (n.max - n.min);
        X(o) && ((e = Rc), (o = n.min + e.size * (n.max - n.min)));
        var s = (function (t) {
            var e = Nc.shape,
              n = {};
            return (
              "object" === N(t.shape) && "string" == typeof t.shape.type
                ? ((e = t.shape.type), (n = t.shape))
                : "string" == typeof t.shape && (e = t.shape),
              [e, n]
            );
          })(e),
          c = V(s, 2),
          u = c[0],
          l = S(
            S({}, c[1]),
            {},
            {
              type: u,
              label: t.label,
              x: t.x * r,
              y: t.y * a,
              fill: e.fill,
              size: Math.min(n.maxGlobal, Math.max(n.minGlobal, o)),
              stroke: e.stroke,
              strokeWidth: e.strokeWidth,
              strokeDasharray: e.strokeDasharray,
              opacity: e.opacity,
            }
          );
        e === t.errorShape && (l.width = e.width);
        var h = i(l);
        return (h.data = t.data), h;
      });
  }
  var Ec = {
    require: ["chart", "resolver", "symbol"],
    defaultSettings: {
      settings: {},
      data: {},
      animations: {
        enabled: !1,
        trackBy: function (t) {
          return t.data.value;
        },
      },
      style: { item: "$shape" },
    },
    render: function (t) {
      var e = t.data,
        n = this.resolver.resolve({
          data: e,
          defaults: k({}, Nc, this.style.item),
          settings: this.settings.settings,
          scaled: { x: this.rect.width, y: this.rect.height },
        }),
        i = this.rect,
        r = i.width,
        a = i.height,
        o = k({}, zc, this.settings.settings.sizeLimits),
        s = n.items,
        c = (function (t, e, n, i, r) {
          var a = Cc(t ? t.scale : void 0, n),
            o = Cc(e ? e.scale : void 0, i),
            s = Math.min(
              a.value * r[a.isBandwidth ? "maxRelDiscrete" : "maxRelExtent"],
              o.value * r[o.isBandwidth ? "maxRelDiscrete" : "maxRelExtent"]
            ),
            c = Math.min(
              a.value * r[a.isBandwidth ? "minRelDiscrete" : "minRelExtent"],
              o.value * r[o.isBandwidth ? "minRelDiscrete" : "minRelExtent"]
            );
          return {
            min: Math.max(1, Math.floor(c)),
            max: Math.max(1, Math.floor(s)),
            maxGlobal: r.maxPx,
            minGlobal: r.minPx,
          };
        })(n.settings.x, n.settings.y, r, a, o);
      return Ac(s, this.rect, c, this.settings.shapeFn || this.symbol);
    },
  };
  Math.PI;
  var Oc = {
    label: "",
    arc: 1,
    show: !0,
    fill: "#333",
    stroke: "#ccc",
    strokeWidth: 1,
    strokeLinejoin: "round",
    opacity: 1,
    innerRadius: 0,
    outerRadius: 0.8,
    cornerRadius: 0,
    offset: 0,
  };
  function Tc(t, e, n, i) {
    var r = e.x,
      a = e.y,
      o = e.width,
      s = e.height,
      c = (function () {
        var t = kt,
          e = Mt,
          n = ut(0),
          i = null,
          r = Rt,
          a = St,
          o = Nt,
          s = null;
        function c() {
          var c,
            u,
            l = +t.apply(this, arguments),
            h = +e.apply(this, arguments),
            f = r.apply(this, arguments) - xt,
            d = a.apply(this, arguments) - xt,
            g = lt(d - f),
            p = d > f;
          if (
            (s || (s = c = ct()), h < l && ((u = h), (h = l), (l = u)), h > vt)
          )
            if (g > bt - vt)
              s.moveTo(h * ft(f), h * pt(f)),
                s.arc(0, 0, h, f, d, !p),
                l > vt &&
                  (s.moveTo(l * ft(d), l * pt(d)), s.arc(0, 0, l, d, f, p));
            else {
              var y,
                v,
                m = f,
                x = d,
                b = f,
                _ = d,
                w = g,
                k = g,
                M = o.apply(this, arguments) / 2,
                R =
                  M > vt && (i ? +i.apply(this, arguments) : yt(l * l + h * h)),
                S = gt(lt(h - l) / 2, +n.apply(this, arguments)),
                N = S,
                z = S;
              if (R > vt) {
                var C = wt((R / l) * pt(M)),
                  A = wt((R / h) * pt(M));
                (w -= 2 * C) > vt
                  ? ((b += C *= p ? 1 : -1), (_ -= C))
                  : ((w = 0), (b = _ = (f + d) / 2)),
                  (k -= 2 * A) > vt
                    ? ((m += A *= p ? 1 : -1), (x -= A))
                    : ((k = 0), (m = x = (f + d) / 2));
              }
              var E = h * ft(m),
                O = h * pt(m),
                T = l * ft(_),
                j = l * pt(_);
              if (S > vt) {
                var P,
                  B = h * ft(x),
                  D = h * pt(x),
                  F = l * ft(b),
                  L = l * pt(b);
                if (g < mt && (P = zt(E, O, F, L, B, D, T, j))) {
                  var I = E - P[0],
                    W = O - P[1],
                    V = B - P[0],
                    $ = D - P[1],
                    H =
                      1 /
                      pt(
                        _t(
                          (I * V + W * $) /
                            (yt(I * I + W * W) * yt(V * V + $ * $))
                        ) / 2
                      ),
                    q = yt(P[0] * P[0] + P[1] * P[1]);
                  (N = gt(S, (l - q) / (H - 1))),
                    (z = gt(S, (h - q) / (H + 1)));
                }
              }
              k > vt
                ? z > vt
                  ? ((y = Ct(F, L, E, O, h, z, p)),
                    (v = Ct(B, D, T, j, h, z, p)),
                    s.moveTo(y.cx + y.x01, y.cy + y.y01),
                    z < S
                      ? s.arc(
                          y.cx,
                          y.cy,
                          z,
                          ht(y.y01, y.x01),
                          ht(v.y01, v.x01),
                          !p
                        )
                      : (s.arc(
                          y.cx,
                          y.cy,
                          z,
                          ht(y.y01, y.x01),
                          ht(y.y11, y.x11),
                          !p
                        ),
                        s.arc(
                          0,
                          0,
                          h,
                          ht(y.cy + y.y11, y.cx + y.x11),
                          ht(v.cy + v.y11, v.cx + v.x11),
                          !p
                        ),
                        s.arc(
                          v.cx,
                          v.cy,
                          z,
                          ht(v.y11, v.x11),
                          ht(v.y01, v.x01),
                          !p
                        )))
                  : (s.moveTo(E, O), s.arc(0, 0, h, m, x, !p))
                : s.moveTo(E, O),
                l > vt && w > vt
                  ? N > vt
                    ? ((y = Ct(T, j, B, D, l, -N, p)),
                      (v = Ct(E, O, F, L, l, -N, p)),
                      s.lineTo(y.cx + y.x01, y.cy + y.y01),
                      N < S
                        ? s.arc(
                            y.cx,
                            y.cy,
                            N,
                            ht(y.y01, y.x01),
                            ht(v.y01, v.x01),
                            !p
                          )
                        : (s.arc(
                            y.cx,
                            y.cy,
                            N,
                            ht(y.y01, y.x01),
                            ht(y.y11, y.x11),
                            !p
                          ),
                          s.arc(
                            0,
                            0,
                            l,
                            ht(y.cy + y.y11, y.cx + y.x11),
                            ht(v.cy + v.y11, v.cx + v.x11),
                            p
                          ),
                          s.arc(
                            v.cx,
                            v.cy,
                            N,
                            ht(v.y11, v.x11),
                            ht(v.y01, v.x01),
                            !p
                          )))
                    : s.arc(0, 0, l, _, b, p)
                  : s.lineTo(T, j);
            }
          else s.moveTo(0, 0);
          if ((s.closePath(), c)) return (s = null), c + "" || null;
        }
        return (
          (c.centroid = function () {
            var n = (+t.apply(this, arguments) + +e.apply(this, arguments)) / 2,
              i =
                (+r.apply(this, arguments) + +a.apply(this, arguments)) / 2 -
                mt / 2;
            return [ft(i) * n, pt(i) * n];
          }),
          (c.innerRadius = function (e) {
            return arguments.length
              ? ((t = "function" == typeof e ? e : ut(+e)), c)
              : t;
          }),
          (c.outerRadius = function (t) {
            return arguments.length
              ? ((e = "function" == typeof t ? t : ut(+t)), c)
              : e;
          }),
          (c.cornerRadius = function (t) {
            return arguments.length
              ? ((n = "function" == typeof t ? t : ut(+t)), c)
              : n;
          }),
          (c.padRadius = function (t) {
            return arguments.length
              ? ((i = null == t ? null : "function" == typeof t ? t : ut(+t)),
                c)
              : i;
          }),
          (c.startAngle = function (t) {
            return arguments.length
              ? ((r = "function" == typeof t ? t : ut(+t)), c)
              : r;
          }),
          (c.endAngle = function (t) {
            return arguments.length
              ? ((a = "function" == typeof t ? t : ut(+t)), c)
              : a;
          }),
          (c.padAngle = function (t) {
            return arguments.length
              ? ((o = "function" == typeof t ? t : ut(+t)), c)
              : o;
          }),
          (c.context = function (t) {
            return arguments.length ? ((s = null == t ? null : t), c) : s;
          }),
          c
        );
      })(),
      u = { x: r + o / 2, y: a + s / 2 },
      l = Math.min(o, s) / 2,
      h = Math.min(o, s) / 2,
      f = h / 100;
    return t.map(function (t, e) {
      var r = n[e];
      r.type = "path";
      var a = h * r.outerRadius,
        o = l * r.innerRadius;
      c.innerRadius(o),
        c.outerRadius(a),
        c.cornerRadius(f * r.cornerRadius),
        (r.d = c(t));
      var s = c.centroid(t),
        d = r.offset
          ? (function (t, e, n, i) {
              var r = V(t, 2),
                a = r[0],
                o = r[1],
                s = Math.sqrt(a * a + o * o),
                c = n - i;
              return { x: (a /= s) * e * c, y: (o /= s) * e * c };
            })(s, r.offset, a, o)
          : { x: 0, y: 0 };
      return (
        (r.transform = "translate("
          .concat(d.x, ", ")
          .concat(d.y, ") translate(")
          .concat(u.x, ", ")
          .concat(u.y, ")")),
        (r.desc = {
          share: t.value / i,
          slice: {
            start: t.startAngle,
            end: t.endAngle,
            innerRadius: o,
            outerRadius: a,
            offset: { x: u.x + d.x, y: u.y + d.y },
          },
        }),
        r
      );
    });
  }
  function jc(t, e) {
    return t.slice && "arc" in t.slice ? e.arc : e.data.value;
  }
  var Pc = {
    require: ["chart", "resolver"],
    defaultSettings: {
      settings: {
        startAngle: 0,
        endAngle: 2 * Math.PI,
        padAngle: 0,
        slice: {},
      },
      style: { slice: "$shape" },
      data: {},
    },
    render: function (t) {
      for (
        var e = t.data,
          n = [],
          i = [],
          r = this.settings.settings,
          a = this.resolver.resolve({
            data: e,
            defaults: k({}, Oc, this.style.slice),
            settings: r.slice,
          }).items,
          o = 0,
          s = 0,
          c = a.length;
        s < c;
        s++
      ) {
        var u = jc(r, a[s]);
        u > 0 &&
          a[s].outerRadius >= a[s].innerRadius &&
          (n.push(u), i.push(a[s]), (o += u));
      }
      var l = (function () {
        var t = Bt,
          e = Pt,
          n = null,
          i = ut(0),
          r = ut(bt),
          a = ut(0);
        function o(o) {
          var s,
            c,
            u,
            l,
            h,
            f = o.length,
            d = 0,
            g = new Array(f),
            p = new Array(f),
            y = +i.apply(this, arguments),
            v = Math.min(bt, Math.max(-bt, r.apply(this, arguments) - y)),
            m = Math.min(Math.abs(v) / f, a.apply(this, arguments)),
            x = m * (v < 0 ? -1 : 1);
          for (s = 0; s < f; ++s)
            (h = p[(g[s] = s)] = +t(o[s], s, o)) > 0 && (d += h);
          for (
            null != e
              ? g.sort(function (t, n) {
                  return e(p[t], p[n]);
                })
              : null != n &&
                g.sort(function (t, e) {
                  return n(o[t], o[e]);
                }),
              s = 0,
              u = d ? (v - f * x) / d : 0;
            s < f;
            ++s, y = l
          )
            (c = g[s]),
              (l = y + ((h = p[c]) > 0 ? h * u : 0) + x),
              (p[c] = {
                data: o[c],
                index: s,
                value: h,
                startAngle: y,
                endAngle: l,
                padAngle: m,
              });
          return p;
        }
        return (
          (o.value = function (e) {
            return arguments.length
              ? ((t = "function" == typeof e ? e : ut(+e)), o)
              : t;
          }),
          (o.sortValues = function (t) {
            return arguments.length ? ((e = t), (n = null), o) : e;
          }),
          (o.sort = function (t) {
            return arguments.length ? ((n = t), (e = null), o) : n;
          }),
          (o.startAngle = function (t) {
            return arguments.length
              ? ((i = "function" == typeof t ? t : ut(+t)), o)
              : i;
          }),
          (o.endAngle = function (t) {
            return arguments.length
              ? ((r = "function" == typeof t ? t : ut(+t)), o)
              : r;
          }),
          (o.padAngle = function (t) {
            return arguments.length
              ? ((a = "function" == typeof t ? t : ut(+t)), o)
              : a;
          }),
          o
        );
      })().sortValues(null);
      return (
        l.startAngle(r.startAngle),
        l.endAngle(r.endAngle),
        l.padAngle(r.padAngle),
        Tc(l(n), k({}, this.rect, { x: 0, y: 0 }), i, o)
      );
    },
  };
  var Bc = (function (t) {
      function e(e) {
        if (t[e.type] && t[e.type].condition(e)) {
          var n = t[e.type],
            i = void 0 === n.conditionAppend || n.conditionAppend(e);
          n.items.forEach(function (t) {
            var n = Math.round(e[t.key]),
              r = e[t.key] - n;
            (e[t.key] = n),
              i &&
                "append" === t.type &&
                (r > 0 ? (e[t.key] += 0.5) : (e[t.key] -= 0.5));
          });
        }
      }
      return (
        void 0 === t &&
          (((t = {}).line = {
            append: ["x1", "x2", "y1", "y2"],
            round: [],
            condition: function (t) {
              return t.x1 === t.x2 || t.y1 === t.y2;
            },
            conditionAppend: function (t) {
              return t.strokeWidth % 2 != 0;
            },
          }),
          (t.rect = {
            append: ["x", "y"],
            round: ["width", "height"],
            condition: function () {
              return !0;
            },
            conditionAppend: function (t) {
              return t.strokeWidth % 2 != 0;
            },
          })),
        Object.keys(t).forEach(function (e) {
          var n = t[e];
          (n.items = []),
            n.append.forEach(function (t) {
              n.items.push({ key: t, type: "append" });
            }),
            n.round.forEach(function (t) {
              n.items.push({ key: t, type: "round" });
            });
        }),
        (e.multiple = function (t) {
          return t.forEach(function (t) {
            return e(t);
          });
        }),
        e
      );
    })(),
    Dc = (function () {
      function t() {
        z(this, t), this.reset(), this.push.apply(this, arguments);
      }
      return (
        A(
          t,
          [
            {
              key: "transposeCoordinate",
              value: function (t, e, n) {
                if ("number" == typeof e && isFinite(e)) {
                  var i = t.substring(0, 1);
                  if ("x" === i || "cx" === t) return e * this.width;
                  if ("width" === t) return e * this.width;
                  if ("r" === t) return e * (n ? this.height : this.width);
                  if ("y" === i || "cy" === t) return e * this.height;
                  if ("height" === t) return e * this.height;
                }
                return e;
              },
            },
            {
              key: "push",
              value: function () {
                var t;
                return (t = this.storage).push.apply(t, arguments), this;
              },
            },
            {
              key: "processItem",
              value: function (e) {
                var n = {},
                  i = void 0 !== e.flipXY ? e.flipXY : this.flipXY,
                  r = void 0 !== e.crisp ? e.crisp : this.crisp;
                if (e.fn && "function" == typeof e.fn) {
                  var a = i ? this.height : this.width,
                    o = i ? this.width : this.height;
                  e = e.fn({ width: a, height: o, flipXY: i });
                  for (
                    var s = Object.keys(e), c = 0, u = s.length;
                    c < u;
                    c++
                  ) {
                    var l = s[c];
                    n[t.evaluateKey(l, i)] = e[l];
                  }
                } else
                  for (
                    var h = Object.keys(e), f = 0, d = h.length;
                    f < d;
                    f++
                  ) {
                    var g = h[f],
                      p = t.evaluateKey(g, i),
                      y = this.transposeCoordinate(p, e[g], i);
                    n[p] = y;
                  }
                return r && Bc(n), n;
              },
            },
            {
              key: "output",
              value: function () {
                for (var t = [], e = 0, n = this.storage.length; e < n; e++) {
                  var i = this.processItem(this.storage[e]);
                  t.push(i);
                }
                return t;
              },
            },
            {
              key: "reset",
              value: function () {
                (this.storage = []),
                  (this.flipXY = !1),
                  (this.crisp = !1),
                  (this.width = 0),
                  (this.height = 0);
              },
            },
          ],
          [
            {
              key: "evaluateKey",
              value: function (t, e) {
                if (e) {
                  var n = t.substring(0, 1),
                    i = t.substring(1);
                  if ("x" === n) return "y".concat(i);
                  if ("y" === n) return "x".concat(i);
                  if ("cx" === t) return "cy";
                  if ("cy" === t) return "cx";
                  if ("width" === t) return "height";
                  if ("height" === t) return "width";
                }
                return t;
              },
            },
          ]
        ),
        t
      );
    })();
  function Fc() {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(Dc, e);
  }
  function Lc(t, e) {
    return t && e
      ? (t.cachedTicks && t.cachedTicks()) || t.ticks({ distance: e })
      : [];
  }
  var Ic = {
    created: function () {},
    require: ["chart", "renderer", "resolver"],
    defaultSettings: {
      layout: { displayOrder: 0 },
      style: { ticks: "$guide-line", minorTicks: "$guide-line--minor" },
    },
    beforeRender: function () {
      (this.blueprint = Fc()),
        (this.blueprint.width = this.rect.width),
        (this.blueprint.height = this.rect.height),
        (this.blueprint.x = this.rect.x),
        (this.blueprint.y = this.rect.y),
        (this.blueprint.crisp = !0);
    },
    render: function () {
      var t = this;
      if (
        ((this.x = this.settings.x ? this.chart.scale(this.settings.x) : null),
        (this.y = this.settings.y ? this.chart.scale(this.settings.y) : null),
        lo(this, "x", this.rect.width),
        lo(this, "y", this.rect.height),
        !this.x && !this.y)
      )
        return [];
      (this.settings.ticks = k(
        { show: !0 },
        this.style.ticks,
        this.settings.ticks || {}
      )),
        (this.settings.minorTicks = k(
          { show: !1 },
          this.style.minorTicks,
          this.settings.minorTicks || {}
        )),
        (this.lines = { x: [], y: [] }),
        (this.lines.x = Lc(this.x, this.rect.width)),
        (this.lines.y = Lc(this.y, this.rect.height)),
        (this.lines.y = this.lines.y.map(function (t) {
          return k(t, { flipXY: !0 });
        }));
      var e = function (e) {
        var n = e.dir,
          i = e.isMinor,
          r = t.lines[n].filter(function (t) {
            return !!t.isMinor === i;
          }),
          a = i ? t.settings.minorTicks : t.settings.ticks;
        t.resolver
          .resolve({ settings: a, data: { items: r, dir: n } })
          .items.forEach(function (e) {
            var i = e.data;
            e.show &&
              t.blueprint.push({
                type: "line",
                x1: i.position,
                y1: 0,
                x2: i.position,
                y2: 1,
                stroke: e.stroke || "black",
                strokeWidth: void 0 !== e.strokeWidth ? e.strokeWidth : 1,
                strokeDasharray:
                  void 0 !== e.strokeDasharray ? e.strokeDasharray : void 0,
                flipXY: i.flipXY || !1,
                value: i.value,
                dir: n,
              });
          });
      };
      return (
        e({ dir: "x", isMinor: !1 }),
        e({ dir: "x", isMinor: !0 }),
        e({ dir: "y", isMinor: !1 }),
        e({ dir: "y", isMinor: !0 }),
        this.blueprint.output()
      );
    },
  };
  function Wc(t, e, n) {
    var i =
      arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : "bottom";
    if (((n *= 0.8), "left" === i || "right" === i)) {
      var r = "right" === i;
      return "\n      M "
        .concat(t, " ")
        .concat(e - n, "\n      A ")
        .concat(1.25 * n, " ")
        .concat(1.25 * n, ", 0, 1, ")
        .concat(r ? 0 : 1, ", ")
        .concat(t, " ")
        .concat(e + n, "\n      L ")
        .concat(r ? t + n : t - n, " ")
        .concat(e, " Z\n    ");
    }
    var a = "bottom" === i;
    return "\n    M "
      .concat(t - n, " ")
      .concat(e, "\n    A ")
      .concat(1.25 * n, " ")
      .concat(1.25 * n, ", 0, 1, ")
      .concat(a ? 1 : 0, ", ")
      .concat(t + n, " ")
      .concat(e, "\n    L ")
      .concat(t, " ")
      .concat(a ? e + n : e - n, " Z\n  ");
  }
  function Vc(t, e, n) {
    var i =
      arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : "bottom";
    if (((n *= 0.75), "left" === i || "right" === i)) {
      var r = "right" === i;
      return "\n      M "
        .concat((t += r ? 1.5 * n : -1.5 * n), " ")
        .concat(e - n, "\n      L ")
        .concat(t, " ")
        .concat(e + n, "\n      L ")
        .concat(r ? t + n : t - n, " ")
        .concat(e, " Z\n    ");
    }
    var a = "bottom" === i;
    return (
      (e += a ? 1.5 * n : -1.5 * n),
      "\n    M "
        .concat(t - n, " ")
        .concat(e, "\n    L ")
        .concat(t + n, " ")
        .concat(e, "\n    L ")
        .concat(t, " ")
        .concat(a ? e + n : e - n, " Z\n  ")
    );
  }
  function $c(t) {
    if (void 0 === t) return 0;
    if ("number" == typeof t && isFinite(t)) return t;
    if ("string" == typeof t)
      switch (t) {
        case "center":
        case "middle":
          return 0.5;
        case "bottom":
        case "right":
          return 1;
        default:
          return 0;
      }
    return 0;
  }
  function Hc(t) {
    var e = { value: t.value };
    return t.label && (e.label = t.label.text), e;
  }
  function qc(t) {
    return void 0 !== ("function" == typeof t.value ? t.value() : t.value);
  }
  function Uc(t, e) {
    var n = t.min();
    if (n === t.max()) {
      var i = (function (t) {
        var e = t.range();
        return 2 === (null == e ? void 0 : e.length) && e[0] > e[1];
      })(t);
      if (e < n) return i ? 2 : -1;
      if (e > n) return i ? -1 : 2;
    }
    return t(e);
  }
  var Yc = {
    require: ["chart", "renderer"],
    defaultSettings: {
      layout: { displayOrder: 0 },
      style: {
        oob: {
          show: !0,
          width: 10,
          fill: "#1A1A1A",
          stroke: "transparent",
          strokeWidth: 0,
          opacity: 1,
          text: {
            fontFamily: "Arial",
            stroke: "transparent",
            fill: "#fff",
            strokeWidth: 0,
            opacity: 1,
          },
          triangle: {
            fill: "#4D4D4D",
            stroke: "transparent",
            strokeWidth: 0,
            opacity: 1,
          },
          padding: { x: 28, y: 5 },
        },
        line: { stroke: "#000" },
        label: { strokeWidth: 0 },
      },
    },
    preferredSize: function () {
      return 30;
    },
    beforeRender: function () {
      (this.blueprint = Fc()),
        (this.blueprint.width = this.rect.width),
        (this.blueprint.height = this.rect.height),
        (this.blueprint.x = this.rect.x),
        (this.blueprint.y = this.rect.y),
        (this.blueprint.crisp = !0);
    },
    render: function () {
      var t = this,
        e = this.settings;
      if (
        ((this.lines = { x: [], y: [] }),
        (this.lines.x = (e.lines && e.lines.x) || []),
        (this.lines.y = (e.lines && e.lines.y) || []),
        0 === this.lines.x.length && 0 === this.lines.y.length)
      )
        return [];
      var n = { x0: [], x1: [], y0: [], y1: [] };
      (this.lines.x = this.lines.x.filter(qc).map(function (e) {
        if (("function" == typeof e.value && (e.value = e.value()), e.scale)) {
          var n = t.chart.scale(e.scale),
            i = Uc(n, e.value);
          return k(e, { scale: n, position: i });
        }
        return k(e, { position: e.value });
      })),
        (this.lines.y = this.lines.y.filter(qc).map(function (e) {
          if (
            ("function" == typeof e.value && (e.value = e.value()), e.scale)
          ) {
            var n = t.chart.scale(e.scale),
              i = Uc(n, e.value);
            return k(e, { scale: n, position: i, flipXY: !0 });
          }
          return k(e, { position: e.value, flipXY: !0 });
        })),
        (this.lines.x = this.lines.x.filter(function (t) {
          return (
            !(t.position < 0 || t.position > 1) ||
            (n["x".concat(t.position > 1 ? 1 : 0)].push(Hc(t)), !1)
          );
        })),
        (this.lines.y = this.lines.y.filter(function (t) {
          return (
            !(t.position < 0 || t.position > 1) ||
            (n["y".concat(t.position > 1 ? 1 : 0)].push(Hc(t)), !1)
          );
        }));
      var i = [];
      return (
        [].concat($(this.lines.x), $(this.lines.y)).forEach(function (n) {
          (!0 === n.show || void 0 === n.show) &&
            (function (t) {
              var e,
                n = t.chart,
                i = t.blueprint,
                r = t.renderer,
                a = t.p,
                o = t.settings,
                s = t.items,
                c = !0,
                u = !1,
                l = !1,
                h = !1,
                f = k(!0, {}, o.style.line, a.line || {});
              if (
                ((e = i.processItem({
                  type: "line",
                  x1: a.position,
                  y1: 0,
                  x2: a.position,
                  y2: 1,
                  stroke: f.stroke || "black",
                  strokeWidth: f.strokeWidth || 1,
                  strokeDasharray: f.strokeDasharray,
                  flipXY: a.flipXY || !1,
                })),
                a.label)
              ) {
                var d,
                  g = k(
                    !0,
                    {
                      fill: "#000",
                      fontFamily: "Arial",
                      fontSize: "12px",
                      opacity: 1,
                      maxWidth: 1,
                      maxWidthPx: 9999,
                      padding: 5,
                      background: {
                        fill: "#fff",
                        stroke: "transparent",
                        strokeWidth: 0,
                        opacity: 0.5,
                      },
                    },
                    o.style.label || {},
                    { fill: f.stroke },
                    a.label
                  ),
                  p = { width: 0, height: 0 },
                  y = "";
                if ("string" == typeof a.formatter)
                  d = n.formatter(a.formatter);
                else if ("object" === N(a.formatter))
                  d = n.formatter(a.formatter);
                else if (void 0 !== a.scale && a.scale.data) {
                  var v = a.scale.data() && a.scale.data().fields;
                  d = v && v[0] ? v[0].formatter() : null;
                }
                !1 !== a.label.showValue &&
                  (d
                    ? (y = " (".concat(d(a.value), ")"))
                    : a.scale && (y = " (".concat(a.value, ")"))),
                  y &&
                    (p = r.measureText({
                      text: y,
                      fontFamily: g.fontFamily,
                      fontSize: g.fontSize,
                    }));
                var m = r.measureText({
                    text: g.text || "",
                    fontFamily: g.fontFamily,
                    fontSize: g.fontSize,
                  }),
                  x = {
                    width: m.width + p.width,
                    height: Math.max(m.height, p.height),
                  },
                  b = g.padding,
                  _ = $c(a.flipXY ? g.vAlign : g.align),
                  w = $c(a.flipXY ? g.align : g.vAlign),
                  M = Math.min(
                    1 + x.width + 2 * b,
                    g.maxWidth * i.width,
                    g.maxWidthPx
                  ),
                  R = x.height + 2 * b,
                  S = a.flipXY ? R : M,
                  z = a.flipXY ? M : R;
                if (
                  ((u = i.processItem({
                    fn: function (t) {
                      var e = t.width,
                        n = t.height,
                        r = a.position * e - (a.flipXY ? R : M) * (1 - _);
                      return {
                        type: "rect",
                        x: (r = a.flipXY ? r : Math.max(r, 0)),
                        y: Math.max(Math.abs(w * n - z * w), 0),
                        width: a.flipXY ? S : Math.min(S, i.width - r),
                        height: z,
                        stroke: g.background.stroke,
                        strokeWidth: g.background.strokeWidth,
                        fill: g.background.fill,
                        opacity: g.background.opacity,
                      };
                    },
                    flipXY: a.flipXY || !1,
                  })),
                  u.x < -1 ||
                    u.x + u.width > i.width + 1 ||
                    u.y < -1 ||
                    u.y + u.height > i.height + 1)
                )
                  u = void 0;
                else {
                  (l = {
                    type: "text",
                    text: g.text || "",
                    fill: g.fill,
                    opacity: g.opacity,
                    fontFamily: g.fontFamily,
                    fontSize: g.fontSize,
                    x: u.x + b,
                    y: u.y + u.height / 2 + x.height / 3,
                    maxWidth: u.width - 2 * b - p.width,
                    anchor: "start",
                  }),
                    y &&
                      (h = {
                        type: "text",
                        text: y || "",
                        fill: g.fill,
                        opacity: g.opacity,
                        fontFamily: g.fontFamily,
                        fontSize: g.fontSize,
                        x: l.x + 3 + (u.width - (p.width + 2 * b)),
                        y: l.y,
                      });
                  for (var C = 0, A = s.length; C < A; C++) {
                    var E = s[C];
                    "rect" === E.type
                      ? ls(u, E) && (c = !1)
                      : "line" === E.type &&
                        a.flipXY === E.flipXY &&
                        ds(u, E) &&
                        (c = !1);
                  }
                }
              }
              s.push(e), c && u && l && (s.push(u, l), h && s.push(h));
            })({
              chart: t.chart,
              blueprint: t.blueprint,
              renderer: t.renderer,
              p: n,
              settings: e,
              items: i,
            });
        }),
        e.style.oob.show &&
          (function (t) {
            for (
              var e = t.blueprint,
                n = t.oob,
                i = t.settings,
                r = t.items,
                a = Object.keys(n),
                o = i.style.oob || {},
                s = function (t, i) {
                  var s = a[t],
                    c = n[s];
                  if (c.length > 0) {
                    var u = s.charAt(1),
                      l = "y" === s.charAt(0),
                      h = o.padding.x + o.width,
                      f = o.padding.y + o.width,
                      d = "bottom";
                    d = l
                      ? "1" === u
                        ? "bottom"
                        : "top"
                      : "1" === u
                      ? "right"
                      : "left";
                    var g = e.processItem({
                        fn: function (t) {
                          var e = t.width,
                            n = t.height,
                            i = u * e + ("1" === u ? -h : h),
                            r = l ? f : n - f;
                          return "arc" === o.type
                            ? {
                                type: "path",
                                d: Wc(l ? r : i, l ? i : r, o.width, d),
                                x: i,
                                y: r,
                                stroke: o.stroke,
                                fill: o.fill,
                                strokeWidth: o.strokeWidth || 0,
                              }
                            : {
                                type: "circle",
                                cx: i,
                                cy: r,
                                r: o.width,
                                stroke: o.stroke,
                                fill: o.fill,
                                strokeWidth: o.strokeWidth || 0,
                                opacity: o.opacity,
                                data: c,
                              };
                        },
                        flipXY: l,
                      }),
                      p = g.cx || g.x,
                      y = g.cy || g.y,
                      v = {
                        type: "text",
                        text: c.length || "",
                        x: p - 0.4 * o.width,
                        y: y + 0.4 * o.width,
                        fontFamily: o.text.fontFamily,
                        fontSize: "".concat(1.3 * o.width, "px"),
                        stroke: o.text.stroke,
                        fill: o.text.fill,
                        strokeWidth: o.text.strokeWidth || 0,
                        opacity: o.text.opacity,
                      },
                      m = {
                        type: "path",
                        d: Vc(p, y, o.width, d),
                        x: p,
                        y: y,
                        stroke: o.triangle.stroke,
                        fill: o.triangle.fill,
                        strokeWidth: o.triangle.strokeWidth || 0,
                        opacity: o.triangle.opacity,
                      };
                    r.push(g, v, m);
                  }
                },
                c = 0,
                u = a.length;
              c < u;
              c++
            )
              s(c);
          })({ blueprint: this.blueprint, oob: n, settings: e, items: i }),
        i
      );
    },
  };
  function Xc(t) {
    var e = {
      type: "line",
      x1: 0,
      x2: 0,
      y1: 0,
      y2: 0,
      collider: { type: null },
    };
    return (
      "top" === t.align || "bottom" === t.align
        ? ((e.x1 = t.innerRect.x - t.outerRect.x),
          (e.x2 = e.x1 + t.innerRect.width),
          (e.y1 = e.y2 =
            "top" === t.align ? t.innerRect.height - t.padding : t.padding))
        : ((e.x1 = e.x2 =
            "left" === t.align ? t.innerRect.width - t.padding : t.padding),
          (e.y1 = t.innerRect.y - t.outerRect.y),
          (e.y2 = e.y1 + t.innerRect.height)),
      (function (t, e) {
        k(t, e.style);
      })(e, t),
      e
    );
  }
  function Gc(t) {
    return "string" == typeof t || "number" == typeof t ? t : "-";
  }
  function Zc(t, e, n) {
    n.layered || !n.stepSize
      ? (function (t, e) {
          e.collider = {
            type: "polygon",
            vertices: [
              { x: e.boundingRect.x, y: e.boundingRect.y },
              {
                x: e.boundingRect.x + e.boundingRect.width,
                y: e.boundingRect.y,
              },
              {
                x: e.boundingRect.x + e.boundingRect.width,
                y: e.boundingRect.y + e.boundingRect.height,
              },
              {
                x: e.boundingRect.x,
                y: e.boundingRect.y + e.boundingRect.height,
              },
            ],
          };
        })(0, e)
      : n.tilted
      ? (function (t, e, n) {
          var i = n.angle * (Math.PI / 180),
            r = Math.max(n.stepSize / 2, e.boundingRect.height / 2),
            a = "start" === e.anchor,
            o = "end" === e.anchor && i < 0,
            s = "start" === e.anchor && i >= 0,
            c = e.boundingRect.y + (s || o ? e.boundingRect.height : 0),
            u = [
              { x: e.x - r, y: c },
              { x: e.x + r, y: c },
            ].map(function (t) {
              return Do(t, i, { x: e.x, y: e.y });
            }),
            l = {
              x: a
                ? e.boundingRect.x + e.boundingRect.width + 10
                : e.boundingRect.x - 10,
              y: e.boundingRect.y + e.boundingRect.height,
            },
            h = {
              x: a
                ? e.boundingRect.x + e.boundingRect.width + 10
                : e.boundingRect.x - 10,
              y: e.boundingRect.y,
            },
            f = i >= 0 ? [l, h] : [h, l];
          u.push.apply(u, f), (e.collider = { type: "polygon", vertices: u });
        })(0, e, n)
      : (function (t, e, n) {
          if ("bottom" === n.align || "top" === n.align) {
            var i =
              t.position * n.innerRect.width +
              (n.innerRect.x - n.outerRect.x - n.stepSize / 2);
            e.collider = {
              type: "rect",
              x: i,
              y: 0,
              width: i < 0 ? n.stepSize + i : n.stepSize,
              height: n.innerRect.height,
            };
          } else {
            var r =
              t.position * n.innerRect.height +
              (n.innerRect.y - n.outerRect.y - n.stepSize / 2);
            e.collider = {
              type: "rect",
              x: 0,
              y: r,
              width: n.innerRect.width,
              height: r < 0 ? n.stepSize + r : n.stepSize,
            };
          }
          var a = e.collider;
          (a.x = Math.max(a.x, 0)), (a.y = Math.max(a.y, 0));
          var o = a.x + a.width - (n.outerRect.x + n.outerRect.width);
          a.width = o > 0 ? a.width - o : a.width;
          var s = a.y + a.height - (n.outerRect.y + n.outerRect.height);
          a.height = s > 0 ? a.height - s : a.height;
        })(t, e, n);
  }
  function Jc(t, e) {
    var n = isNaN(t.style.align)
        ? 0.5
        : Math.min(Math.max(t.style.align, 0), 1),
      i = 0;
    if (t.tilted) i = t.stepSize * n;
    else {
      var r = e ? t.textRect.height : t.textRect.width;
      i = Math.max(0, t.stepSize - r) * n;
    }
    return i;
  }
  function Kc(t, e) {
    var n = {
      type: "text",
      text: Gc(t.label),
      x: 0,
      y: 0,
      maxWidth: e.maxWidth,
      maxHeight: e.maxHeight,
      tickValue: t.value,
    };
    return (
      "top" === e.align || "bottom" === e.align
        ? ((n.x =
            t.start * e.innerRect.width +
            (e.innerRect.x - e.outerRect.x) +
            Jc(e, !1)),
          (n.y = "top" === e.align ? e.innerRect.height : 0),
          (n.anchor = e.stepSize ? "start" : "middle"),
          (n.x += isNaN(e.style.offset) ? 0 : +e.style.offset))
        : ((n.y =
            t.start * e.innerRect.height +
            (e.innerRect.y - e.outerRect.y) +
            Jc(e, !0)),
          (n.x = "left" === e.align ? e.innerRect.width : 0),
          (n.anchor = "left" === e.align ? "end" : "start"),
          (n.baseline = e.stepSize ? "text-before-edge" : "central"),
          (n.y += isNaN(e.style.offset) ? 0 : +e.style.offset)),
      (function (t, e) {
        ["fill", "fontSize", "fontFamily"].forEach(function (n) {
          t[n] = e.style[n];
        });
      })(n, e),
      (function (t, e) {
        if (!e.tilted && !e.stepSize)
          if ("top" === e.align || "bottom" === e.align) {
            var n = e.outerRect.width,
              i = Math.min(e.maxWidth / 2, e.textRect.width / 2),
              r = t.x - i,
              a = t.x + i;
            r < 0
              ? ((t.anchor = "start"), (t.x = e.innerRect.x - e.outerRect.x))
              : a > n &&
                ((t.anchor = "end"), (t.x = e.innerRect.width + e.innerRect.x));
          } else {
            var o = e.outerRect.height,
              s = e.maxHeight / 2,
              c = t.y - s,
              u = t.y + s;
            c < 0
              ? ((t.y = e.innerRect.y - e.outerRect.y),
                (t.baseline = "text-before-edge"))
              : u > o &&
                ((t.y = e.innerRect.height + (e.innerRect.y - e.outerRect.y)),
                (t.baseline = "text-after-edge"));
          }
      })(n, e),
      (function (t, e) {
        "top" === e.align
          ? (t.y -= e.padding)
          : "bottom" === e.align
          ? (t.y += e.padding + e.maxHeight)
          : "left" === e.align
          ? (t.x -= e.padding)
          : "right" === e.align && (t.x += e.padding);
      })(n, e),
      (function (t, e) {
        if (e.tilted) {
          var n = -e.angle,
            i = n * (Math.PI / 180);
          "bottom" === e.align
            ? ((t.x -= (e.maxHeight * Math.sin(i)) / 2),
              (t.y -= e.maxHeight),
              (t.y += (e.maxHeight * Math.cos(i)) / 2))
            : (t.x -= (e.maxHeight * Math.sin(i)) / 3),
            (t.transform = "rotate("
              .concat(n, ", ")
              .concat(t.x, ", ")
              .concat(t.y, ")")),
            (t.anchor =
              ("bottom" === e.align) == e.angle < 0 ? "start" : "end");
          var r = Math.cos(i) * e.maxWidth;
          if (("bottom" === e.align) == e.angle < 0) {
            var a = e.outerRect.width - e.paddingEnd;
            t.x + r > a && (t.maxWidth = (a - t.x - 10) / Math.cos(i));
          } else {
            var o = e.paddingEnd;
            t.x - r < o && (t.maxWidth = (t.x - o - 10) / Math.cos(i));
          }
        }
      })(n, e),
      (function (t, e) {
        t.boundingRect = e.textBounds(t);
      })(n, e),
      Zc(t, n, e),
      n
    );
  }
  function Qc(t, e) {
    var n = {
      type: "line",
      x1: 0,
      x2: 0,
      y1: 0,
      y2: 0,
      collider: { type: null },
      tickValue: t.value,
    };
    return (
      "top" === e.align || "bottom" === e.align
        ? ((n.x1 = n.x2 =
            t.position * e.innerRect.width + (e.innerRect.x - e.outerRect.x)),
          (n.y1 = "top" === e.align ? e.innerRect.height : 0),
          (n.y2 = "top" === e.align ? n.y1 - e.tickSize : n.y1 + e.tickSize))
        : ((n.y1 = n.y2 =
            t.position * e.innerRect.height + (e.innerRect.y - e.outerRect.y)),
          (n.x1 = "left" === e.align ? e.innerRect.width : 0),
          (n.x2 = "left" === e.align ? n.x1 - e.tickSize : n.x1 + e.tickSize)),
      (function (t, e) {
        k(t, e.style);
      })(n, e),
      (function (t, e) {
        "top" === e.align
          ? ((t.y1 -= e.padding), (t.y2 -= e.padding))
          : "bottom" === e.align
          ? ((t.y1 += e.padding), (t.y2 += e.padding))
          : "left" === e.align
          ? ((t.x1 -= e.padding), (t.x2 -= e.padding))
          : "right" === e.align && ((t.x1 += e.padding), (t.x2 += e.padding));
      })(n, e),
      (function (t, e) {
        var n = t.strokeWidth / 2;
        t.x1 === e.innerRect.width
          ? ((t.x1 -= n), (t.x2 -= n))
          : 0 === t.x1
          ? ((t.x1 += n), (t.x2 += n))
          : t.y1 === e.innerRect.height
          ? ((t.y1 -= n), (t.y2 -= n))
          : 0 === t.y1 && ((t.y1 += n), (t.y2 += n));
      })(n, e),
      n
    );
  }
  function tu(t) {
    return !t.isMinor && t.position >= 0 && t.position <= 1;
  }
  function eu(t) {
    var e = t.rect,
      n = t.state,
      i = t.majorTicks,
      r = t.measure;
    return t.horizontal
      ? (function (t) {
          for (
            var e = t.majorTicks,
              n = t.measureText,
              i = t.rect,
              r = "layered" === t.state.labels.activeMode ? 2 : 1,
              a = i.width,
              o = e
                .map(function (t) {
                  return t.label;
                })
                .map(function (t) {
                  return ""
                    .concat(t.slice(0, 1))
                    .concat(t.length > 1 ? "…" : "");
                })
                .map(n)
                .map(function (t) {
                  return t.width;
                }),
              s = 0;
            s < e.length;
            ++s
          ) {
            var c = e[s];
            if (r * a * Math.abs(c.start - c.end) < o[s]) return !0;
          }
          return !1;
        })({ majorTicks: i, measureText: r, rect: e, state: n })
      : (function (t) {
          var e = t.majorTicks,
            n = t.measureText,
            i = t.rect.height,
            r = n("M").height;
          return (
            !(e.length < 2) && i * Math.abs(e[0].position - e[1].position) < r
          );
        })({ majorTicks: i, measureText: r, rect: e, state: n });
  }
  function nu(t) {
    var e = t.value,
      n = t.maxValue,
      i = t.minValue,
      r = t.range,
      a = t.modifier;
    return (
      isNaN(r) || isNaN(a) || (e = r * a), e > n && (e = n), e < i && (e = i), e
    );
  }
  function iu(t) {
    var e = t.isDiscrete,
      n = t.rect,
      i = t.formatter,
      r = t.measureText,
      a = t.scale,
      o = t.settings,
      s = t.state,
      c = 0,
      u = { left: 0, top: 0, right: 0, bottom: 0 },
      l = o.labels,
      h = l.maxLengthPx,
      f = l.minLengthPx;
    if (o.labels.show) {
      var d,
        g,
        p = o.align,
        y = "top" === p || "bottom" === p,
        v = y ? n.inner.width : n.inner.height,
        m = a.ticks({ settings: o, distance: v, formatter: i }).filter(tu),
        x = function (t) {
          var e = r({
            text: t,
            fontSize: o.labels.fontSize,
            fontFamily: o.labels.fontFamily,
          });
          return (
            (e.width = nu({ value: e.width, maxValue: h, minValue: f })), e
          );
        };
      if (
        (e &&
          y &&
          "auto" === o.labels.mode &&
          (!(function (t) {
            var e = t.majorTicks,
              n = t.measure,
              i = t.rect,
              r = t.state,
              a = t.settings,
              o = a.labels.maxGlyphCount,
              s = "layered" === r.labels.activeMode ? 2 : 1,
              c = a.labels.tiltThreshold ? a.labels.tiltThreshold : 0.7,
              u = n("…").width,
              l = i.width,
              h = 0;
            if (isNaN(o))
              for (var f = 0; f < e.length; f++) {
                var d = e[f],
                  g = d.label;
                if (
                  n(g).width * (g.length > 1 ? c : 1) + u >
                  (h = s * l * Math.abs(d.start - d.end))
                )
                  return !0;
              }
            else if (
              ((h =
                s *
                l *
                e.reduce(function (t, e) {
                  return Math.min(Math.abs(e.start - e.end), t);
                }, 1 / 0)),
              n("M").width * c * o + u > h)
            )
              return !0;
            return !1;
          })({
            majorTicks: m,
            measure: x,
            rect: n.inner,
            state: s,
            settings: o,
          })
            ? (s.labels.activeMode = "horizontal")
            : (s.labels.activeMode = "tilted")),
        !o.labels.filterOverlapping &&
          "tilted" !== s.labels.activeMode &&
          eu({
            rect: n.inner,
            state: s,
            majorTicks: m,
            measure: x,
            horizontal: y,
          }))
      )
        return { size: Math.max(n.outer.width, n.outer.height), isToLarge: !0 };
      if ("tilted" === s.labels.activeMode) {
        var b = Math.abs(o.labels.tiltAngle) * (Math.PI / 180);
        d = function (t) {
          return (
            nu({ value: t.width, maxValue: h, minValue: f }) * Math.sin(b) +
            t.height * Math.cos(b)
          );
        };
      } else
        d = y
          ? function (t) {
              return t.height;
            }
          : function (t) {
              return nu({ value: t.width, maxValue: h, minValue: f });
            };
      if (y && "tilted" !== s.labels.activeMode) g = ["M"];
      else if (isNaN(o.labels.maxGlyphCount))
        g = m.map(function (t) {
          return t.label;
        });
      else {
        for (var _ = "", w = 0; w < o.labels.maxGlyphCount; w++) _ += "M";
        g = [_];
      }
      var k = g.map(x),
        M = k.map(d),
        R = Math.max.apply(Math, $(M).concat([0]));
      if (
        ((c += R),
        (c += o.labels.margin),
        "layered" === s.labels.activeMode && (c *= 2),
        "tilted" === s.labels.activeMode)
      ) {
        var S = ("bottom" === o.align) == o.labels.tiltAngle >= 0,
          N = Math.abs(o.labels.tiltAngle) * (Math.PI / 180),
          z = (R - r("M").height * Math.cos(N)) / Math.sin(N),
          C =
            Math.min(
              o.labels.maxEdgeBleed,
              Math.max.apply(
                Math,
                $(
                  k
                    .map(function (t) {
                      return Math.min(z, t.width) * Math.cos(N) + t.height;
                    })
                    .map(function (t, e) {
                      var i = m[e] ? m[e].position : 0;
                      return S
                        ? t - i * n.inner.width
                        : t - (1 - i) * n.inner.width;
                    })
                ).concat([0])
              )
            ) + o.paddingEnd;
        if (
          ((u[S ? "left" : "right"] = C),
          !o.labels.filterOverlapping &&
            (function (t) {
              var e = t.majorTicks,
                n = t.measureText,
                i = t.rect,
                r = t.bleedSize,
                a = t.angle;
              if (e.length < 2) return !1;
              if (0 === a) return !0;
              var o = Math.abs(a),
                s =
                  1 /
                  (Math.min(i.outer.width - r, i.inner.width) *
                    Math.abs(e[0].position - e[1].position));
              return n("M").height > Math.sin(o * (Math.PI / 180)) / s;
            })({
              majorTicks: m,
              measureText: r,
              rect: n,
              bleedSize: C,
              angle: o.labels.tiltAngle,
            }))
        )
          return {
            size: Math.max(n.outer.width, n.outer.height),
            isToLarge: !0,
          };
      }
    }
    return { size: c, edgeBleed: u };
  }
  var ru = function (t, e, n) {
    return t.width * Math.abs(e.position - n.position);
  };
  function au(t) {
    var e = t.layered,
      n = t.major,
      i = t.innerRect,
      r = t.outerRect,
      a = t.tick,
      o = t.index,
      s = e ? 2 : 1,
      c = n[o - s],
      u = n[o + s],
      l = c ? ru(i, a, c) / 2 - 2 : 1 / 0,
      h = u ? ru(i, a, u) / 2 - 2 : 1 / 0;
    return n.length < 2
      ? i.width
      : c
      ? u
        ? 2 * Math.min(l, h)
        : (function (t) {
            var e = t.innerRect,
              n = t.outerRect,
              i = t.tick,
              r = t.prevWidth,
              a = e.x - n.x,
              o = n.width - e.width - a,
              s = e.width - e.width * i.position + o,
              c = 2 * Math.min(r, s),
              u = 1 === i.position ? e.width / 2 - 2 : 0;
            return Math.max(r, c, u);
          })({ innerRect: i, outerRect: r, tick: a, prevWidth: l })
      : (function (t) {
          var e = t.innerRect,
            n = t.outerRect,
            i = t.tick,
            r = t.nextWidth,
            a = e.x - n.x,
            o = e.width * i.position + a,
            s = 2 * Math.min(r, o),
            c = 0 === i.position ? e.width / 2 - 2 : 0;
          return Math.max(r, s, c);
        })({ innerRect: i, outerRect: r, tick: a, nextWidth: h });
  }
  function ou(t) {
    var e = 0;
    return (
      (e += t.paddingStart),
      (e += t.line.show ? t.line.strokeWidth / 2 : 0),
      (e += t.ticks.show ? t.ticks.margin : 0)
    );
  }
  function su(t) {
    var e = 0;
    return (
      (e += t.ticks.show ? t.ticks.tickSize : 0), (e += ou(t) + t.labels.margin)
    );
  }
  function cu(t, e) {
    return t.map(function (t) {
      return Qc(t, e);
    });
  }
  function uu(t, e) {
    return e ? Math.abs(e.end - e.start) : t.bandwidth();
  }
  function lu(t) {
    var e = t.textMetrics,
      n = t.settings,
      i = t.innerRect,
      r = t.scale,
      a = t.tilted,
      o = t.layered,
      s = t.tick,
      c = e.height,
      u = uu(r, s),
      l = { width: 0, height: c };
    if ("left" === n.align || "right" === n.align)
      l.width = i.width - su(n) - n.paddingEnd;
    else if (o) l.width = u * i.width * 2;
    else if (a) {
      var h = Math.abs(n.labels.tiltAngle) * (Math.PI / 180);
      l.width =
        (i.height - su(n) - n.paddingEnd - c * Math.cos(h)) / Math.sin(h);
    } else l.width = u * i.width;
    return (
      (l.width = nu({
        value: l.width,
        maxValue: n.labels.maxLengthPx,
        minValue: n.labels.minLengthPx,
      })),
      l
    );
  }
  function hu(t) {
    var e = t.textMetrics,
      n = t.settings,
      i = t.innerRect,
      r = t.outerRect,
      a = t.tilted,
      o = t.layered,
      s = t.tick,
      c = t.index,
      u = t.major,
      l = e.height,
      h = { width: 0, height: l };
    if ("left" === n.align || "right" === n.align)
      h.width = i.width - su(n) - n.paddingEnd;
    else if (a) {
      var f = Math.abs(n.labels.tiltAngle) * (Math.PI / 180);
      h.width =
        (i.height - su(n) - n.paddingEnd - l * Math.cos(f)) / Math.sin(f);
    } else
      h.width = au({
        layered: o,
        major: u,
        innerRect: i,
        outerRect: r,
        tick: s,
        index: c,
      });
    return (
      (h.width = nu({
        value: h.width,
        maxValue: n.labels.maxLengthPx,
        minValue: n.labels.minLengthPx,
      })),
      h
    );
  }
  function fu(t) {
    var e;
    function n() {
      return (e = hu), n;
    }
    function i() {
      return (e = lu), i;
    }
    function r(t) {
      var n,
        i = t.settings,
        r = t.scale,
        a = t.innerRect,
        o = t.outerRect,
        s = t.measureText,
        c = t.ticks,
        u = t.state,
        l = t.textBounds,
        h = [],
        f = (function (t) {
          return t.filter(function (t) {
            return !t.isMinor;
          });
        })(c),
        d = (function (t) {
          return t.filter(function (t) {
            return t.isMinor;
          });
        })(c),
        g = { innerRect: a, align: i.align, outerRect: o },
        p = "tilted" === u.labels.activeMode,
        y = "layered" === u.labels.activeMode;
      if (
        (i.line.show &&
          ((g.style = i.line), (g.padding = i.paddingStart), h.push(Xc(g))),
        i.ticks.show &&
          ((g.style = i.ticks),
          (g.tickSize = i.ticks.tickSize),
          (g.padding = ou(i)),
          (n = cu(f, g))),
        i.labels.show)
      ) {
        var v = su(i);
        (g.style = i.labels),
          (g.padding = v),
          (g.tilted = p),
          (g.layered = y),
          (g.angle = i.labels.tiltAngle),
          (g.paddingEnd = i.paddingEnd),
          (g.textBounds = l);
        var m = function (t, n) {
            g.textRect = (function (t) {
              var e = t.style;
              return (0, t.measureText)({
                text: t.tick.label,
                fontSize: e.fontSize,
                fontFamily: e.fontFamily,
              });
            })({ tick: t, measureText: s, style: g.style });
            var u = e({
              textMetrics: g.textRect,
              settings: i,
              innerRect: a,
              outerRect: o,
              scale: r,
              tilted: p,
              layered: y,
              tick: t,
              major: f,
              index: n,
            });
            (g.maxWidth = u.width),
              (g.maxHeight = u.height),
              (g.stepSize = (function (t) {
                var e = t.innerRect,
                  n = t.scale,
                  i = t.settings,
                  r = t.tick;
                return (
                  ("top" === i.align || "bottom" === i.align
                    ? e.width
                    : e.height) * uu(n, r)
                );
              })({ innerRect: a, scale: r, ticks: c, settings: i, tick: t }));
          },
          x = [];
        (x =
          !y || ("top" !== i.align && "bottom" !== i.align)
            ? (function (t, e, n) {
                return t.map(function (t, i) {
                  n(t, i);
                  var r = Kc(t, e);
                  return (r.data = t.data), r;
                });
              })(f, g, m)
            : (function (t, e, n, i) {
                var r = e.padding,
                  a = su(n);
                return t.map(function (t, o) {
                  i(t, o);
                  var s = a + e.maxHeight + n.labels.margin;
                  (e.layer = o % 2), (e.padding = o % 2 == 0 ? r : s);
                  var c = Kc(t, e);
                  return (c.data = t.data), c;
                });
              })(f, g, i, m)),
          i.labels.filterOverlapping &&
            (function (t, e, n) {
              var i = function (e, n) {
                return ls(et(1, t[e].boundingRect), et(1, t[n].boundingRect));
              };
              if (n && n.tilted) {
                var r = Math.abs(n.angle);
                i = function (e, n) {
                  var i = 1 / Math.abs(t[e].x - t[n].x),
                    a = Math.sin(r * (Math.PI / 180)) / i;
                  return t[e].boundingRect.height > a;
                };
              }
              for (var a = 0; a <= t.length - 1; a++)
                for (
                  var o = a + 1;
                  o <= Math.min(a + 5, a + (t.length - 1));
                  o++
                )
                  t[a] &&
                    t[o] &&
                    i(a, o) &&
                    (o === t.length - 1
                      ? (t.splice(a, 1), e && e.splice(a, 1))
                      : (t.splice(o, 1), e && e.splice(o, 1)),
                    o--,
                    a--);
            })(x, n, g),
          h.push.apply(h, $(x));
      }
      return (
        i.minorTicks &&
          i.minorTicks.show &&
          d.length > 0 &&
          ((g.style = i.minorTicks),
          (g.tickSize = i.minorTicks.tickSize),
          (g.padding = (function (t) {
            return t.line.strokeWidth + t.minorTicks.margin;
          })(i)),
          h.push.apply(h, $(cu(d, g)))),
        n && h.push.apply(h, $(n)),
        h
      );
    }
    return (n.build = r), (i.build = r), t ? i() : n();
  }
  var du = {
      labels: {
        show: !0,
        tiltAngle: 40,
        tiltThreshold: 0.7,
        maxEdgeBleed: 1 / 0,
        margin: 4,
        maxLengthPx: 150,
        minLengthPx: 0,
        mode: "auto",
        maxGlyphCount: NaN,
        align: 0.5,
        offset: 0,
        filterOverlapping: !1,
      },
      ticks: { show: !1, margin: 0, tickSize: 4 },
      line: { show: !1 },
      paddingStart: 0,
      paddingEnd: 10,
      align: "auto",
    },
    gu = {
      labels: {
        show: !0,
        margin: 4,
        maxLengthPx: 150,
        minLengthPx: 0,
        align: 0.5,
        offset: 0,
        filterOverlapping: !0,
      },
      ticks: { show: !0, margin: 0, tickSize: 8 },
      minorTicks: { show: !1, tickSize: 3, margin: 0 },
      line: { show: !0 },
      paddingStart: 0,
      paddingEnd: 10,
      align: "auto",
    };
  var pu = {
    require: ["chart", "renderer", "dockConfig"],
    defaultSettings: {
      layout: { displayOrder: 0, prioOrder: 0 },
      settings: {},
      style: {
        labels: "$label",
        ticks: "$guide-line",
        minorTicks: "$guide-line--minor",
        line: "$guide-line",
      },
    },
    created: function () {
      (this.state = {
        isDiscrete: !!this.scale.bandwidth,
        isHorizontal: !1,
        labels: { activeMode: "horizontal" },
        ticks: [],
        innerRect: { width: 0, height: 0, x: 0, y: 0 },
        outerRect: { width: 0, height: 0, x: 0, y: 0 },
        defaultDock: void 0,
        concreteNodeBuilder: void 0,
        settings: void 0,
      }),
        this.state.isDiscrete
          ? (this.state.defaultDock = "bottom")
          : (this.state.defaultDock = "left"),
        this.setState(this.settings);
    },
    setState: function () {
      var t, e, n, i;
      (this.state.isDiscrete = !!this.scale.bandwidth),
        (this.state.settings = (function (t) {
          var e = t.state,
            n = t.style,
            i = t.settings,
            r = k(!0, {}, e.isDiscrete ? du : gu, n),
            a = k(!0, {}, r, i.settings),
            o = i.layout.dock || e.defaultDock;
          (a.dock = o),
            (a.align = (function (t, e) {
              var n = ["top", "bottom"],
                i = ["left", "right"];
              return (-1 !== n.indexOf(t) && -1 === i.indexOf(e)) ||
                (-1 !== i.indexOf(t) && -1 === n.indexOf(e))
                ? t
                : e;
            })(i.settings.align, o)),
            (a.labels.tiltAngle = Math.max(
              -90,
              Math.min(a.labels.tiltAngle, 90)
            ));
          var s = a.paddingStart,
            c = a.paddingEnd;
          return (
            (a.paddingStart = "function" == typeof s ? s.call(null) : s),
            (a.paddingEnd = "function" == typeof c ? c.call(null) : c),
            a
          );
        })(this)),
        (this.state.concreteNodeBuilder = fu(this.state.isDiscrete)),
        this.dockConfig.dock(this.state.settings.dock),
        (this.state.isHorizontal =
          "top" === this.state.settings.align ||
          "bottom" === this.state.settings.align),
        (this.state.labels.activeMode =
          ((t = this.state),
          (e = this.state.settings),
          (n = this.state.isDiscrete),
          (i = e.labels.mode),
          n && t.isHorizontal
            ? "auto" === i
              ? t.labels.activeMode
              : -1 !== ["layered", "tilted"].indexOf(e.labels.mode) &&
                -1 !== ["top", "bottom"].indexOf(e.dock)
              ? i
              : "horizontal"
            : "horizontal"));
    },
    preferredSize: function (t) {
      var e = this.formatter,
        n = this.state,
        i = this.scale,
        r = this.state.isHorizontal ? t.inner.width : t.inner.height;
      this.state.pxScale = ho(i, r);
      var a = (function (t) {
        var e = t.isDiscrete,
          n = t.rect,
          i = t.formatter,
          r = t.measureText,
          a = t.scale,
          o = t.settings,
          s = 0,
          c = iu({
            isDiscrete: e,
            rect: n,
            formatter: i,
            measureText: r,
            scale: a,
            settings: o,
            state: t.state,
          }),
          u = c.size,
          l = c.edgeBleed;
        if (((s += u), c.isToLarge)) return { size: s };
        if (
          (o.ticks.show && ((s += o.ticks.margin), (s += o.ticks.tickSize)),
          o.minorTicks && o.minorTicks.show)
        ) {
          var h = o.minorTicks.margin + o.minorTicks.tickSize;
          h > s && (s = h);
        }
        if (o.line.show) {
          var f = o.line.strokeWidth / 2;
          (s += f),
            (l[
              (function (t) {
                switch (t) {
                  case "top":
                    return "bottom";
                  case "bottom":
                    return "top";
                  case "left":
                    return "right";
                  case "right":
                    return "left";
                  default:
                    return t;
                }
              })(o.align)
            ] = Math.ceil(f));
        }
        return (
          (s += o.paddingStart), { size: (s += o.paddingEnd), edgeBleed: l }
        );
      })({
        isDiscrete: this.state.isDiscrete,
        rect: { inner: t.inner, outer: t.outer },
        formatter: e,
        measureText: this.renderer.measureText,
        scale: this.state.pxScale,
        settings: this.state.settings,
        state: n,
      });
      return a;
    },
    beforeUpdate: function () {
      var t =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        e = t.settings;
      this.setState(e);
    },
    resize: function (t) {
      var e = t.inner,
        n = t.outer,
        i = k(
          {},
          e,
          (function (t) {
            var e = t.align,
              n = t.inner;
            return "left" === e
              ? { x: n.width + n.x }
              : "right" === e || "bottom" === e
              ? n
              : { y: n.y + n.height };
          })({ align: this.state.settings.align, inner: e })
        ),
        r = n || i;
      return k(this.state.innerRect, i), k(this.state.outerRect, r), n;
    },
    beforeRender: function () {
      var t = this.scale,
        e = this.formatter,
        n = this.state.isHorizontal
          ? this.state.innerRect.width
          : this.state.innerRect.height;
      (this.state.pxScale = ho(t, n)),
        (this.state.ticks = this.state.pxScale
          .ticks({ distance: n, formatter: e })
          .filter(function (t) {
            return t.position >= 0 && t.position <= 1;
          }));
    },
    render: function () {
      var t = this.state,
        e = [];
      return (
        e.push.apply(
          e,
          $(
            this.state.concreteNodeBuilder.build({
              settings: this.state.settings,
              scale: this.state.pxScale,
              innerRect: this.state.innerRect,
              outerRect: this.state.outerRect,
              measureText: this.renderer.measureText,
              textBounds: this.renderer.textBounds,
              ticks: this.state.ticks,
              state: t,
            })
          )
        ),
        Bc.multiple(e),
        e
      );
    },
  };
  var yu = {
    render: function () {
      return [];
    },
  };
  function vu(t, e, n) {
    var i = "";
    if ("function" == typeof t) i = t();
    else if ("string" == typeof t) i = t;
    else if (n) {
      var r = (n.data().fields || []).map(function (t) {
        return t.title();
      });
      i = r.join(e);
    }
    return i;
  }
  function mu(t, e) {
    var n = "middle";
    return (
      "left" === t
        ? "top" === e
          ? (n = "end")
          : "bottom" === e && (n = "start")
        : "right" === t
        ? "top" === e
          ? (n = "start")
          : "bottom" === e && (n = "end")
        : "left" === e
        ? (n = "start")
        : "right" === e && (n = "end"),
      n
    );
  }
  var xu = {
    require: ["renderer", "chart"],
    defaultSettings: {
      layout: { dock: "bottom", displayOrder: 0, prioOrder: 0 },
      settings: {
        paddingStart: 5,
        paddingEnd: 5,
        paddingLeft: 0,
        paddingRight: 0,
        anchor: "center",
        join: ", ",
        maxLengthPx: NaN,
      },
      style: { text: "$title" },
    },
    created: function () {
      this.definitionSettings = this.settings.settings;
      var t = this.settings.text,
        e = this.definitionSettings.join;
      this.title = vu(t, e, this.scale);
    },
    preferredSize: function () {
      return (
        this.renderer.measureText({
          text: this.title,
          fontSize: this.style.text.fontSize,
          fontFamily: this.style.text.fontFamily,
        }).height +
        this.definitionSettings.paddingStart +
        this.definitionSettings.paddingEnd
      );
    },
    render: function () {
      var t = this.title,
        e = this.definitionSettings,
        n = this.rect,
        i = [];
      return (
        i.push(
          (function (t) {
            var e = t.title,
              n = t.definitionSettings,
              i = t.dock,
              r = t.rect,
              a = t.measureText,
              o = t.style,
              s = {
                type: "text",
                text: e,
                x: 0,
                y: 0,
                dx: 0,
                dy: 0,
                anchor: mu(i, n.anchor),
                baseline: "alphabetical",
              };
            k(s, o.text);
            var c = a(s);
            if ("top" === i || "bottom" === i) {
              var u = r.width / 2;
              "left" === n.anchor
                ? (u = n.paddingLeft || 0)
                : "right" === n.anchor && (u = r.width - (n.paddingRight || 0)),
                (s.x = u),
                (s.y =
                  "top" === i
                    ? r.height - n.paddingStart
                    : n.paddingStart + c.height),
                (s.dy = "top" === i ? -c.height / 6 : -c.height / 3),
                (s.maxWidth = 0.8 * r.width);
            } else {
              var l = r.height / 2;
              "top" === n.anchor
                ? (l = n.paddingStart)
                : "bottom" === n.anchor && (l = r.height - n.paddingStart),
                (s.y = l),
                (s.x =
                  "left" === i ? r.width - n.paddingStart : n.paddingStart),
                (s.dx = "left" === i ? -c.height / 3 : c.height / 3);
              var h = "left" === i ? 270 : 90;
              (s.transform = "rotate("
                .concat(h, ", ")
                .concat(s.x + s.dx, ", ")
                .concat(s.y + s.dy, ")")),
                (s.maxWidth = 0.8 * r.height);
            }
            return (
              isNaN(n.maxLengthPx) ||
                (s.maxWidth = Math.min(s.maxWidth, n.maxLengthPx)),
              s
            );
          })({
            title: t,
            dock: this.settings.layout.dock,
            definitionSettings: e,
            rect: n,
            measureText: this.renderer.measureText,
            style: this.style,
          })
        ),
        i
      );
    },
    beforeUpdate: function (t) {
      t.settings &&
        (k(this.settings, t.settings),
        (this.definitionSettings = t.settings.settings));
      var e = this.settings.text,
        n = this.definitionSettings.join;
      this.title = vu(e, n, this.scale);
    },
  };
  function bu(t, e) {
    var n,
      i = t.settings.layout.dock,
      r = t.settings.settings.invert,
      a = "top" === i || "bottom" === i,
      o = a ? "width" : "height",
      s = t.rect[o],
      c = t.chart.scroll(t.settings.scroll),
      u = e[a ? "x" : "y"];
    r && (u = s - u);
    var l = c.getState();
    n = { startOffset: u, startScroll: l.start, swipe: !1 };
    var h = (u / s) * (l.max - l.min) + l.min;
    h < l.start
      ? (n.startScroll = h)
      : h > l.start + l.viewSize && (n.startScroll = h - l.viewSize);
    return {
      update: function (t) {
        var e = t[a ? "x" : "y"];
        if ((r && (e = s - e), !n.swipe)) {
          if (Math.abs(n.startOffset - e) <= 1) return;
          n.swipe = !0;
        }
        var i = c.getState(),
          o = ((e - n.startOffset) / s) * (i.max - i.min),
          u = n.startScroll + o;
        c.moveTo(u);
      },
      end: function (t) {
        var e = t[a ? "x" : "y"];
        r && (e = s - e);
        var i = c.getState();
        if (n.swipe) {
          var o = ((e - n.startOffset) / s) * (i.max - i.min),
            u = n.startScroll + o;
          c.moveTo(u);
        } else {
          var l = (e / s) * (i.max - i.min) + i.min - i.viewSize / 2;
          c.moveTo(l);
        }
      },
    };
  }
  function _u(t, e) {
    var n = e.element().getBoundingClientRect();
    return { x: t.center.x - n.left, y: t.center.y - n.top };
  }
  var wu = {
    require: ["chart", "renderer"],
    on: {
      panStart: function (t) {
        var e = _u(t, this.renderer),
          n = { x: e.x - t.deltaX, y: e.y - t.deltaY };
        (this.currentMove = bu(this, n)), this.currentMove.update(e);
      },
      panMove: function (t) {
        if (this.currentMove) {
          var e = _u(t, this.renderer);
          this.currentMove.update(e);
        }
      },
      panEnd: function (t) {
        if (this.currentMove) {
          var e = _u(t, this.renderer);
          this.currentMove.end(e), (this.currentMove = null);
        }
      },
      panCancel: function () {
        this.currentMove = null;
      },
      tap: function (t) {
        var e = _u(t, this.renderer);
        bu(this, e).end(e);
      },
    },
    defaultSettings: {
      settings: { backgroundColor: "#eee", thumbColor: "#ccc", width: 16 },
    },
    preferredSize: function (t) {
      var e = this.chart.scroll(this.settings.scroll).getState();
      return e.viewSize >= e.max - e.min
        ? Math.max(t.width, t.height)
        : this.settings.settings.width;
    },
    render: function (t) {
      var e,
        n = this.settings.layout.dock,
        i = this.settings.settings.invert,
        r = "top" === n || "bottom" === n,
        a = r ? "width" : "height",
        o = this.rect[a],
        s = this.chart.scroll(this.settings.scroll).getState(),
        c = (o * (s.start - s.min)) / (s.max - s.min),
        u = (o * s.viewSize) / (s.max - s.min);
      return (
        i && (c = o - c - u),
        t(
          "div",
          {
            style: {
              position: "relative",
              width: "100%",
              height: "100%",
              background: this.settings.settings.backgroundColor,
              pointerEvents: "auto",
            },
          },
          [].concat(
            t("div", {
              class: "scroller",
              style:
                ((e = { position: "absolute" }),
                E(e, r ? "left" : "top", "".concat(c, "px")),
                E(e, r ? "top" : "left", "25%"),
                E(e, r ? "height" : "width", "50%"),
                E(e, a, "".concat(Math.max(1, u), "px")),
                E(e, "background", this.settings.settings.thumbColor),
                e),
            })
          )
        )
      );
    },
    renderer: "dom",
  };
  function ku(t) {
    var e = t.h,
      n = t.isVertical,
      i = t.value,
      r = t.pos,
      a = t.align,
      o = t.borderHit,
      s = t.state,
      c = t.idx,
      u = "end" !== a,
      l = u ? { left: "0", top: "0" } : { right: "0", bottom: "0" },
      h = 0,
      f = "100%",
      d = "100%";
    return (
      s.targetRect && "start" === s.settings.bubbles.align
        ? ((f = "".concat(s.targetRect.x + s.targetRect.width, "px")),
          (d = "".concat(s.targetRect.y + s.targetRect.height, "px")))
        : s.targetRect &&
          "end" === s.settings.bubbles.align &&
          ((h = n ? s.targetRect.x : s.targetRect.y),
          (f = "".concat(s.rect.width - h, "px")),
          (d = "".concat(s.rect.height - h, "px"))),
      u || (r -= o),
      e(
        "div",
        {
          onmouseover: function (t) {
            (t.srcElement.children[0].style.backgroundColor = "#000"),
              (t.srcElement.children[0].style[n ? "height" : "width"] = "2px");
          },
          onmouseout: function (t) {
            (t.srcElement.children[0].style.backgroundColor =
              s.style.line.stroke),
              (t.srcElement.children[0].style[n ? "height" : "width"] = "1px");
          },
          "data-value": i,
          "data-key": [s.key, "edge", c].join("-"),
          style: {
            cursor: n ? "ns-resize" : "ew-resize",
            position: "absolute",
            left: "".concat(n ? h : r, "px"),
            top: "".concat(n ? r : h, "px"),
            height: n ? "".concat(o, "px") : d,
            width: n ? f : "".concat(o, "px"),
            pointerEvents: "auto",
          },
        },
        [
          e("div", {
            style: k(
              {
                backgroundColor: s.style.line.stroke,
                position: "absolute",
                height: n ? "".concat(1, "px") : "100%",
                width: n ? "100%" : "".concat(1, "px"),
                pointerEvents: "none",
              },
              l
            ),
          }),
        ]
      )
    );
  }
  function Mu(t) {
    var e,
      n,
      i = t.h,
      r = t.isVertical,
      a = t.label,
      o = t.otherValue,
      s = t.rangeIdx,
      c = t.idx,
      u = t.pos,
      l = t.align,
      h = t.state,
      f = t.value,
      d = "end" !== l,
      g = "outside" === h.settings.bubbles.placement,
      p = "none";
    r
      ? ((n = d ? "left" : "right"),
        g && (p = d ? "translate(-100%,  0px)" : "translate(100%,  0px)"))
      : ((n = d ? "top" : "bottom"),
        g && (p = d ? "translate(0px, -100%)" : "translate(0px,  100%)"));
    var y = h.edit && h.edit.rangeIdx === s && h.edit.bubbleIdx === c,
      v = {
        position: "relative",
        borderRadius: "".concat(h.style.bubble.borderRadius, "px"),
        border: ""
          .concat(h.style.bubble.strokeWidth, "px solid ")
          .concat(h.style.bubble.stroke),
        backgroundColor: h.style.bubble.fill,
        color: h.style.bubble.color,
        fontFamily: h.style.bubble.fontFamily,
        fontSize: h.style.bubble.fontSize,
        padding: "4px 8px",
        textAlign: "center",
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        maxWidth: "150px",
        minWidth: "50px",
        minHeight: "1em",
        pointerEvents: "auto",
        transform: r ? "translate(0,-50%)" : "translate(-50%,0)",
        cursor: r ? "ns-resize" : "ew-resize",
      },
      m = h.style.bubble.stroke,
      x = y
        ? i("input", {
            type: "text",
            value: f,
            style: S(
              S({}, v),
              {},
              { textAlign: "start", textOverflow: "", fontSize: "13px" }
            ),
            onkeyup: function (t) {
              if ("Enter" === t.key) {
                t.preventDefault(), t.stopPropagation();
                var e = parseFloat(t.target.value);
                isNaN(e)
                  ? ((m = "rgba(230, 78, 78, 0.6)"),
                    (t.target.style.border = ""
                      .concat(h.style.bubble.strokeWidth, "px solid ")
                      .concat(m)))
                  : h.onEditConfirmed(s, e, o);
              } else
                "Escape" === t.key &&
                  (t.preventDefault(), t.stopPropagation(), h.onEditCanceled());
            },
          })
        : i(
            "div",
            {
              "data-key": [h.key, "bubble", s].join("-"),
              "data-other-value": o,
              "data-idx": s,
              "data-bidx": c,
              style: v,
            },
            [a]
          );
    return i(
      "div",
      {
        style:
          ((e = { position: "absolute" }),
          E(e, n, "0"),
          E(e, r ? "top" : "left", "".concat(u, "px")),
          E(e, "transform", p),
          e),
      },
      [x]
    );
  }
  function Ru(t) {
    var e = t.h,
      n = t.isVertical,
      i = t.top,
      r = t.height,
      a = t.color,
      o = t.on,
      s = t.opacity;
    return e(
      "div",
      k(
        {
          style: {
            backgroundColor: a,
            opacity: s,
            position: "absolute",
            left: n ? 0 : "".concat(i, "px"),
            top: n ? "".concat(i, "px") : 0,
            height: n ? "".concat(r, "px") : "100%",
            width: n ? "100%" : "".concat(r, "px"),
            pointerEvents: "auto",
          },
        },
        o
      ),
      []
    );
  }
  function Su(t) {
    var e = t.borderHit,
      n = t.els,
      i = t.isVertical,
      r = t.state,
      a = t.vStart,
      o = t.vEnd,
      s = t.idx,
      c = 0;
    r.targetRect && (c = i ? r.targetRect.y : r.targetRect.x);
    var u = !!r.scale,
      l = u ? r.scale.norm(a) * r.size : a,
      h = u ? r.scale.norm(o) * r.size : o,
      f = Math.abs(l - h),
      d = Math.min(l, h) + c,
      g = d + f;
    if (r.targetRect) {
      var p = r.targetFillRect || r.targetRect,
        y = i ? p.height : p.width,
        v = u ? r.scale.norm(a) * y : a,
        m = u ? r.scale.norm(o) * y : o,
        x = Math.abs(v - m),
        b = Math.min(v, m),
        _ = {
          h: r.h,
          isVertical: i,
          top: b,
          height: x,
          color: r.style.target.fill,
          opacity: r.style.target.opacity,
        };
      r.style.target.opacity < 0.8 &&
        (_.on = {
          onmouseover: function (t) {
            t.srcElement.style.opacity = r.style.target.opacity + 0.1;
          },
          onmouseout: function (t) {
            t.srcElement.style.opacity = r.style.target.opacity;
          },
        }),
        n.push(
          r.h(
            "div",
            {
              style: {
                position: "absolute",
                left: "".concat(p.x, "px"),
                top: "".concat(p.y, "px"),
                height: "".concat(p.height, "px"),
                width: "".concat(p.width, "px"),
              },
            },
            [Ru(_)]
          )
        );
    }
    var w = l < h ? a : o,
      k = l < h ? o : a,
      M = V(u ? r.scale.domain() : [Math.min(a, o), Math.max(a, o)], 2),
      R = M[0],
      S = M[1],
      N = w + 1e-5 >= R && w - 1e-5 <= S,
      z = k - 1e-5 <= S && k + 1e-5 >= R;
    N &&
      n.push(
        ku({
          h: r.h,
          isVertical: i,
          borderHit: e,
          value: w,
          pos: d,
          align: "start",
          state: r,
          idx: s,
        })
      ),
      z &&
        n.push(
          ku({
            h: r.h,
            isVertical: i,
            borderHit: e,
            value: k,
            pos: g,
            align: "end",
            state: r,
            idx: s,
          })
        );
    var C = r.settings.bubbles;
    if (C && C.show) {
      var A = { fontSize: C.fontSize, fontFamily: C.fontFamily, color: C.fill },
        E = [a, o];
      N &&
        n.push(
          Mu({
            h: r.h,
            isVertical: i,
            align: C.align,
            style: A,
            rangeIdx: s,
            idx: 0,
            otherValue: k,
            value: w,
            label: "".concat(r.format(w, E)),
            pos: d,
            state: r,
          })
        ),
        z &&
          n.push(
            Mu({
              h: r.h,
              isVertical: i,
              align: C.align,
              style: A,
              rangeIdx: s,
              idx: 1,
              otherValue: w,
              value: k,
              label: "".concat(r.format(k, E)),
              pos: g,
              state: r,
            })
          );
    }
  }
  function Nu(t) {
    var e = t.active.limitHigh - t.active.end,
      n = t.active.limitLow - t.active.start,
      i = t.current - t.start;
    return (i = i < 0 ? Math.max(i, n) : Math.min(i, e));
  }
  function zu(t) {
    var e = [],
      n = 0 === t.direction;
    if (
      (Array.isArray(t.ranges) &&
        t.ranges.forEach(function (i, r) {
          (t.active && r === t.active.idx) ||
            Su({
              borderHit: 5,
              els: e,
              isVertical: n,
              state: t,
              vStart: Math.min(i.min, i.max),
              vEnd: Math.max(i.min, i.max),
              idx: r,
            });
        }),
      t.active)
    ) {
      var i = t.start,
        r = t.current;
      if (-1 !== t.active.idx)
        if ("foo" === t.active.mode)
          (i = Math.min(t.active.start, t.active.end)),
            (r = Math.max(t.active.start, t.active.end));
        else if ("modify" === t.active.mode)
          (i = Math.min(t.start, t.current)),
            (r = Math.max(t.start, t.current));
        else {
          var a = Nu(t);
          (i = t.active.start + a), (r = t.active.end + a);
        }
      Su({
        borderHit: 5,
        els: e,
        isVertical: n,
        state: t,
        vStart: i,
        vEnd: r,
        idx: t.active.idx,
      });
    }
    return e;
  }
  function Cu(t) {
    return { min: t.scale.min(), max: t.scale.max() };
  }
  function Au(t) {
    return { min: 0, max: t.direction ? t.rect.width : t.rect.height };
  }
  function Eu(t, e, n) {
    var i,
      r,
      a = t.ranges,
      o = -1;
    for (i = 0; i < a.length; i++) {
      if (a[i].min <= e && e <= a[i].max) {
        (o = i),
          (n.min = i ? a[i - 1].max : n.min),
          (n.max = i + 1 < a.length ? a[i + 1].min : n.max);
        break;
      }
      if (e < a[i].min) {
        (n.max = a[i].min), (n.min = i ? a[i - 1].max : n.min);
        break;
      }
    }
    -1 === o && a.length && i >= a.length && (n.min = a[a.length - 1].max),
      -1 !== o &&
        (r = {
          idx: o,
          start: a[o].min,
          end: a[o].max,
          limitLow: n.min,
          limitHigh: n.max,
          mode: "foo",
        }),
      (t.active = r);
  }
  function Ou(t) {
    t.renderer.render(zu(t));
  }
  function Tu(t, e) {
    if (!e || !e.isActive()) return [];
    var n = t.scale.data(),
      i = ((n && n.fields) || []).map(function (t) {
        return t.id();
      }),
      r = e.brushes().filter(function (t) {
        return "range" === t.type && -1 !== i.indexOf(t.id);
      })[0];
    return r ? r.brush.ranges() : [];
  }
  function ju(t, e) {
    for (
      var n, i = 1 / 0, r = e.domain(), a = e.bandwidth() / 2, o = 0;
      o < r.length;
      ++o
    ) {
      var s = Math.abs(t - a - e(r[o]));
      s < i && ((i = s), (n = r[o]));
    }
    return n;
  }
  function Pu(t, e) {
    return (
      Math.min.apply(Math, $(t)) <= Math.max.apply(Math, $(e)) &&
      Math.max.apply(Math, $(t)) >= Math.min.apply(Math, $(e))
    );
  }
  function Bu(t) {
    return K(
      t.reduce(function (t, e) {
        return t.push.apply(t, $(J(e.bounds))), t;
      }, [])
    );
  }
  var Du = {
    require: ["chart", "settings", "renderer"],
    defaultSettings: {
      settings: { bubbles: { show: !0, align: "start" } },
      style: {
        bubble: "$label-overlay",
        line: "$shape-guide--inverted",
        target: "$selection-area-target",
      },
    },
    renderer: "dom",
    on: {
      rangeStart: function (t) {
        this.start(t);
      },
      rangeMove: function (t) {
        this.move(t);
      },
      rangeEnd: function (t) {
        this.end(t);
      },
      rangeClear: function (t) {
        this.clear(t);
      },
      bubbleStart: function (t) {
        this.bubbleStart(t);
      },
    },
    created: function () {
      this.state = { key: this.settings.key || "brush-range" };
    },
    beforeRender: function (t) {
      this.state.rect = t.size.computedInner;
    },
    renderRanges: function () {
      this.state.started ||
        ((this.state.ranges = Tu(this.state, this.state.brushInstance)),
        (this.state.active = null),
        Ou(this.state));
    },
    render: function (t) {
      var e = this,
        n = this.settings.settings;
      this.state.direction = "vertical" === n.direction ? 0 : 1;
      var i = this.renderer.element().getBoundingClientRect(),
        r = this.state.rect[0 === this.state.direction ? "height" : "width"],
        a = ho(this.chart.scale(n.scale), r),
        o = (function (t) {
          var e = {
              targetRect: null,
              targetFillRect: null,
              scale: null,
              size: null,
            },
            n = t.settings.settings,
            i =
              n.target &&
              (
                n.target.components ||
                (n.target.component ? [n.target.component] : [])
              )
                .map(function (e) {
                  return t.chart.component(e);
                })
                .filter(function (t) {
                  return !!t && !!t.rect;
                }),
            r =
              n.target && n.target.selector
                ? t.chart.findShapes(n.target.selector)
                : [],
            a =
              n.target && n.target.fillSelector
                ? t.chart.findShapes(n.target.fillSelector)
                : [];
          if (r.length > 0) {
            var o = Bu(r);
            if (
              ((e.size = o[0 === t.state.direction ? "height" : "width"]),
              (e.scale = ho(t.chart.scale(n.scale), e.size)),
              (e.targetRect = o),
              a.length > 0)
            ) {
              var s = Bu(a);
              e.targetFillRect = s;
            }
          } else if (i && i.length > 0) {
            var c = i.slice(1).reduce(
              function (t, e) {
                return {
                  x0: Math.min(t.x0, e.rect.computedInner.x),
                  y0: Math.min(t.y0, e.rect.computedInner.y),
                  x1: Math.max(
                    t.x1,
                    e.rect.computedInner.x + e.rect.computedInner.width
                  ),
                  y1: Math.max(
                    t.y1,
                    e.rect.computedInner.y + e.rect.computedInner.height
                  ),
                };
              },
              {
                x0: i[0].rect.computedInner.x,
                y0: i[0].rect.computedInner.y,
                x1: i[0].rect.computedInner.x + i[0].rect.computedInner.width,
                y1: i[0].rect.computedInner.y + i[0].rect.computedInner.height,
              }
            );
            e.targetRect = {
              x: c.x0 - t.state.rect.x,
              y: c.y0 - t.state.rect.y,
              width: c.x1 - c.x0,
              height: c.y1 - c.y0,
            };
          }
          return e;
        })(this);
      if (
        ((a = o.scale ? o.scale : a),
        (this.state.targetRect = o.targetRect),
        (this.state.targetFillRect = o.targetFillRect),
        (this.state.size = null === o.size ? r : o.size),
        (this.state.settings = n),
        (this.state.style = this.style),
        (this.state.offset = i),
        (this.state.brush =
          "object" === N(n.brush) ? n.brush.context : n.brush),
        (this.state.brushInstance = this.chart.brush(this.state.brush)),
        (this.state.renderer = this.renderer),
        (this.state.multi = !!n.multiple),
        (this.state.h = t),
        (this.state.onEditConfirmed = function (t, n, i) {
          (e.state.edit = null),
            (function (t, e, n, i) {
              var r = t.ranges.map(function (t) {
                  return { min: t.min, max: t.max };
                }),
                a = t.scale.min(),
                o = t.scale.max();
              (r[e] = {
                min: Math.max(a, Math.min(n, i)),
                max: Math.min(o, Math.max(n, i)),
              }),
                (t.ranges[e] = S({}, r[e]));
              var s = t.scale.data();
              s &&
                s.fields &&
                s.fields.forEach(function (e) {
                  t.fauxBrushInstance || t.brushInstance.setRange(e.id(), r);
                });
            })(e.state, t, n, i),
            e.emit("bubbleEnd"),
            Ou(e.state);
        }),
        (this.state.onEditCanceled = function () {
          (e.state.edit = null), Ou(e.state);
        }),
        (this.state.cssCoord = {
          offset: 0 === this.state.direction ? "top" : "left",
          coord: 0 === this.state.direction ? "y" : "x",
          pos: 0 === this.state.direction ? "deltaY" : "deltaX",
        }),
        (this.state.format =
          "function" == typeof n.bubbles.label &&
          function (t, i) {
            return n.bubbles.label.call(void 0, {
              datum: t,
              data: i,
              scale: a,
              resources: { scale: e.chart.scale, formatter: e.chart.formatter },
            });
          }),
        {}.hasOwnProperty.call(a, "norm"))
      ) {
        (this.state.editable = !0),
          (this.state.observeBrush =
            "object" === N(n.brush) && n.brush.observe),
          (this.state.fauxBrushInstance = null),
          (this.state.findValues = null),
          (this.state.scale = a);
        var s = this.state.scale.data();
        !this.state.format &&
          s &&
          s.fields &&
          s.fields[0] &&
          (this.state.format = s.fields[0].formatter());
      } else
        (this.state.editable = !1),
          (this.state.scale = ca()),
          (this.state.scale.data = a.data),
          this.state.format ||
            (this.state.format = function (t, e) {
              return Pu(a.range(), e)
                ? (function (t, e) {
                    var n = e.ticks(),
                      i = e.domain().indexOf(ju(t, e));
                    return -1 !== i ? n[i].label : "-";
                  })(t, a)
                : "-";
            }),
          (this.state.fauxBrushInstance = io()),
          (this.state.findValues = function (t) {
            return (function (t, e) {
              var n = e.domain(),
                i = e.range(),
                r = [];
              return (
                t.forEach(function (t) {
                  if (Pu(i, [t.min, t.max])) {
                    var a = n.indexOf(ju(t.min, e)),
                      o = n.indexOf(ju(t.max, e));
                    r.push.apply(
                      r,
                      n.slice(Math.min(a, o), Math.max(a, o) + 1)
                    );
                  }
                }),
                r
              );
            })(t, a);
          });
      return (
        (this.state.ranges = Tu(this.state, this.state.brushInstance)),
        this.state.observeBrush || this.state.sourcedFromThisComponent
          ? [zu(this.state)]
          : []
      );
    },
    mounted: function () {
      this.state.observeBrush &&
        this.state.brushInstance &&
        this.state.brushInstance.on("update", this.renderRanges);
    },
    beforeDestroy: function () {
      this.state.observeBrush &&
        this.state.brushInstance &&
        this.state.brushInstance.removeListener("update", this.renderRanges);
    },
    start: function (t) {
      !(function (t) {
        var e = t.state,
          n = t.e,
          i = t.renderer,
          r = t.ranges,
          a = t.targetSize;
        if (!e.started) {
          e.edit = null;
          var o = n.center.x - n.deltaX,
            s = n.center.y - n.deltaY,
            c = document.elementFromPoint(o, s);
          i.element().contains(c) || (c = null);
          var u = { started: !0 };
          e.offset = k({}, i.element().getBoundingClientRect());
          var l = o - e.offset.left,
            h = s - e.offset.top;
          (e.offset.left += e.targetRect ? e.targetRect.x : 0),
            (e.offset.top += e.targetRect ? e.targetRect.y : 0),
            (e.ranges = r(e, e.fauxBrushInstance || e.brushInstance));
          var f =
              n.center[e.cssCoord.coord] -
              n[e.cssCoord.pos] -
              e.offset[e.cssCoord.offset],
            d = n.center[e.cssCoord.coord] - e.offset[e.cssCoord.offset];
          (u.current = e.scale.normInvert(d / e.size)),
            (u.start = e.scale.normInvert(f / e.size));
          var g,
            p,
            y = e.ranges,
            v = Cu(e),
            m = -1;
          if (c && c.hasAttribute("data-idx"))
            (m = parseInt(c.getAttribute("data-idx"), 10)),
              (v.min = m > 0 ? y[m - 1].max : v.min),
              (v.max = m + 1 < y.length ? y[m + 1].min : v.max);
          else {
            for (g = 0; g < y.length; g++) {
              if (y[g].min <= u.start && u.start <= y[g].max) {
                (m = g),
                  (v.min = g ? y[g - 1].max : v.min),
                  (v.max = g + 1 < y.length ? y[g + 1].min : v.max);
                break;
              }
              if (u.start < y[g].min) {
                (v.max = y[g].min), (v.min = g ? y[g - 1].max : v.min);
                break;
              }
            }
            -1 === m &&
              y.length &&
              g >= y.length &&
              (v.min = y[y.length - 1].max);
          }
          if (
            (-1 !== m ||
              e.multi ||
              ((u.ranges = []),
              (v.min = e.scale.min()),
              (v.max = e.scale.max())),
            -1 !== m)
          )
            if (
              ((p = {
                idx: m,
                start: y[m].min,
                end: y[m].max,
                limitLow: v.min,
                limitHigh: v.max,
                mode: "move",
              }),
              c && c.hasAttribute("data-other-value"))
            )
              (u.start = parseFloat(c.getAttribute("data-other-value"))),
                (p.mode = "modify");
            else {
              var x = e.scale.norm(p.start) * e.size,
                b = e.scale.norm(p.end) * e.size;
              Math.abs(f - x) <= a
                ? ((u.start = p.end), (p.mode = "modify"))
                : Math.abs(f - b) <= a &&
                  ((u.start = p.start), (p.mode = "modify"));
            }
          else
            p = {
              idx: -1,
              start: u.start,
              end: u.current,
              limitLow: v.min,
              limitHigh: v.max,
              mode: "current",
            };
          (u.active = p),
            ("modify" !== p.mode &&
              e.targetRect &&
              !fs(e.targetRect, { x: l, y: h })) ||
              Object.keys(u).forEach(function (t) {
                return (e[t] = u[t]);
              });
        }
      })({
        e: t,
        state: this.state,
        renderer: this.renderer,
        ranges: Tu,
        targetSize: 5,
      });
    },
    end: function () {
      this.state.started &&
        (!(function (t, e) {
          (t.started = !1),
            (t.ranges = e(t, t.fauxBrushInstance || t.brushInstance));
          var n = Cu(t);
          Eu(t, t.current, n);
        })(this.state, Tu),
        Ou(this.state),
        (this.state.sourcedFromThisComponent = !0),
        (this.state.active = null));
    },
    move: function (t) {
      this.state.started &&
        (!(function (t, e) {
          var n =
              (e.center[t.cssCoord.coord] - t.offset[t.cssCoord.offset]) /
              t.size,
            i = t.scale.normInvert(n);
          t.current = Math.max(
            Math.min(i, t.active.limitHigh),
            t.active.limitLow
          );
        })(this.state, t),
        (function (t) {
          var e = t.ranges.map(function (t) {
            return { min: t.min, max: t.max };
          });
          if (-1 !== t.active.idx)
            if ("modify" === t.active.mode)
              (e[t.active.idx].min = Math.min(t.start, t.current)),
                (e[t.active.idx].max = Math.max(t.start, t.current));
            else {
              var n = Nu(t);
              (e[t.active.idx].min += n), (e[t.active.idx].max += n);
            }
          else
            e.push({
              min: Math.min(t.start, t.current),
              max: Math.max(t.start, t.current),
            });
          var i = t.scale.data();
          i &&
            i.fields &&
            i.fields.forEach(function (n) {
              if (t.fauxBrushInstance) {
                var i = Tu(t, t.fauxBrushInstance),
                  r = t.findValues(i),
                  a = t.findValues(e),
                  o = a.filter(function (t) {
                    return -1 === r.indexOf(t);
                  }),
                  s = r.filter(function (t) {
                    return -1 === a.indexOf(t);
                  }),
                  c = o.map(function (t) {
                    return { key: n.id(), value: t };
                  }),
                  u = s.map(function (t) {
                    return { key: n.id(), value: t };
                  });
                t.brushInstance.addAndRemoveValues(c, u),
                  t.fauxBrushInstance.setRange(n.id(), e);
              } else t.brushInstance.setRange(n.id(), e);
            });
        })(this.state),
        Ou(this.state));
    },
    clear: function () {
      this.state.fauxBrushInstance && this.state.fauxBrushInstance.clear(),
        this.state.renderer.render([]),
        (this.state.started = !1),
        (this.state.active = null),
        (this.state.sourcedFromThisComponent = !1);
    },
    bubbleStart: function (t) {
      if (this.state.editable) {
        var e = t.srcEvent || t,
          n = e.target,
          i = {
            rangeIdx: parseInt(n.getAttribute("data-idx"), 10),
            bubbleIdx: parseInt(n.getAttribute("data-bidx"), 10),
          };
        if (
          !isNaN(i.rangeIdx) &&
          JSON.stringify(i) !== JSON.stringify(this.state.edit)
        ) {
          (this.state.edit = i),
            e.stopPropagation(),
            e.stopImmediatePropagation(),
            e.preventDefault();
          var r = n.parentNode;
          Ou(this.state);
          var a = r.querySelector("input");
          a.focus(), a.select();
        }
      }
    },
  };
  function Fu(t) {
    t.renderer.render(zu(t));
  }
  function Lu(t) {
    return t.rc.ranges();
  }
  var Iu = {
    require: ["chart", "settings", "renderer"],
    defaultSettings: {
      settings: { bubbles: { show: !0, align: "start" } },
      style: {
        bubble: "$label-overlay",
        line: "$shape-guide--inverted",
        target: "$selection-area-target",
      },
    },
    renderer: "dom",
    on: {
      areaStart: function (t) {
        this.start(t);
      },
      areaMove: function (t) {
        this.move(t);
      },
      areaEnd: function (t) {
        this.end(t);
      },
      areaClear: function (t) {
        this.clear(t);
      },
    },
    created: function () {
      this.state = { key: this.settings.key || "brush-area-dir" };
    },
    render: function (t) {
      var e = this;
      this.state.rect = this.rect;
      var n = this.settings.settings,
        i = "vertical" === n.direction ? 0 : 1,
        r = this.state.rect[0 === i ? "height" : "width"],
        a = this.renderer.element().getBoundingClientRect(),
        o = (n.target ? n.target.components || [n.target.component] : [])
          .map(function (t) {
            return e.chart.component(t);
          })
          .filter(function (t) {
            return !!t && !!t.rect;
          }),
        s = o[0]
          ? o.slice(1).reduce(
              function (t, e) {
                return {
                  x0: Math.min(t.x0, e.rect.x),
                  y0: Math.min(t.y0, e.rect.y),
                  x1: Math.max(t.x1, e.rect.x + e.rect.width),
                  y1: Math.max(t.y1, e.rect.y + e.rect.height),
                };
              },
              {
                x0: o[0].rect.x,
                y0: o[0].rect.y,
                x1: o[0].rect.x + o[0].rect.width,
                y1: o[0].rect.y + o[0].rect.height,
              }
            )
          : null;
      return (
        (this.state.targetRect = s
          ? {
              x: s.x0 - this.rect.x,
              y: s.y0 - this.rect.y,
              width: s.x1 - s.x0,
              height: s.y1 - s.y0,
            }
          : null),
        (this.state.style = this.style),
        (this.state.chart = this.chart),
        (this.state.direction = i),
        (this.state.settings = n),
        (this.state.offset = a),
        (this.state.rc = $a()),
        (this.state.renderer = this.renderer),
        (this.state.multi = !!n.multiple),
        (this.state.h = t),
        (this.state.size = r),
        (this.state.cssCoord = {
          offset: 0 === this.state.direction ? "top" : "left",
          coord: 0 === this.state.direction ? "y" : "x",
          pos: 0 === this.state.direction ? "deltaY" : "deltaX",
          area: 0 === this.state.direction ? "height" : "width",
        }),
        (this.state.format = function (t, e) {
          return (function (t, e, n) {
            var i = Math.min.apply(Math, $(n)),
              r = Math.max.apply(Math, $(n)),
              a = {
                x: t.direction ? i + t.rect.x : t.rect.x,
                y: t.direction ? t.rect.y : i + t.rect.y,
                width: t.direction ? r - i : t.rect.width + t.rect.x,
                height: t.direction ? t.rect.height + t.rect.y : r - i,
              },
              o = t.chart.shapesAt(a, t.settings.brush);
            if (0 === o.length) return "-";
            var s = o.reduce(function (n, r) {
                var a = n.bounds,
                  o = r.bounds;
                return e === i
                  ? a[t.cssCoord.coord] <= o[t.cssCoord.coord]
                    ? n
                    : r
                  : a[t.cssCoord.coord] + a[t.cssCoord.area] >=
                    o[t.cssCoord.coord] + o[t.cssCoord.area]
                  ? n
                  : r;
              }),
              c = t.settings.brush.components.reduce(function (t, e) {
                return t.key === s.key ? t : e;
              });
            return "function" == typeof t.settings.bubbles.label
              ? t.settings.bubbles.label(s.data)
              : Array.isArray(c.data) && c.data.length
              ? s.data[c.data[0]].label
              : s.data && s.data.label
              ? s.data.label
              : "-";
          })(this, t, e);
        }),
        []
      );
    },
    start: function (t) {
      !(function (t) {
        var e = t.state,
          n = t.e,
          i = t.renderer,
          r = t.ranges,
          a = t.targetSize;
        if (!e.started) {
          var o = n.center.x - n.deltaX,
            s = n.center.y - n.deltaY,
            c = document.elementFromPoint(o, s);
          i.element().contains(c) || (c = null);
          var u = { started: !0 };
          (e.offset = i.element().getBoundingClientRect()), (e.ranges = r(e));
          var l = o - e.offset.left,
            h = s - e.offset.top,
            f =
              n.center[e.cssCoord.coord] -
              n[e.cssCoord.pos] -
              e.offset[e.cssCoord.offset],
            d = n.center[e.cssCoord.coord] - e.offset[e.cssCoord.offset],
            g = d,
            p = f;
          (u.start = f), (u.current = d);
          var y,
            v,
            m = e.ranges,
            x = Au(e),
            b = -1;
          if (c && c.hasAttribute("data-idx"))
            (b = parseInt(c.getAttribute("data-idx"), 10)),
              (x.min = b > 0 ? m[b - 1].max : x.min),
              (x.max = b + 1 < m.length ? m[b + 1].min : x.max);
          else {
            for (y = 0; y < m.length; y++) {
              if (m[y].min <= p && p <= m[y].max) {
                (b = y),
                  (x.min = y ? m[y - 1].max : x.min),
                  (x.max = y + 1 < m.length ? m[y + 1].min : x.max);
                break;
              }
              if (p < m[y].min) {
                (x.max = m[y].min), (x.min = y ? m[y - 1].max : x.min);
                break;
              }
            }
            -1 === b &&
              m.length &&
              y >= m.length &&
              (x.min = m[m.length - 1].max);
          }
          if (
            (-1 !== b ||
              e.multi ||
              ((u.ranges = []),
              (x.min = 0),
              (x.max = e.direction ? e.rect.width : e.rect.height)),
            -1 !== b)
          )
            if (
              ((v = {
                idx: b,
                start: m[b].min,
                end: m[b].max,
                limitLow: x.min,
                limitHigh: x.max,
                mode: "move",
              }),
              c && c.hasAttribute("data-other-value"))
            )
              (u.start = parseFloat(c.getAttribute("data-other-value"))),
                (v.mode = "modify");
            else {
              var _ = v.start,
                w = v.end;
              Math.abs(f - _) <= a
                ? ((u.start = v.end), (v.mode = "modify"))
                : Math.abs(f - w) <= a &&
                  ((u.start = v.start), (v.mode = "modify"));
            }
          else
            v = {
              idx: -1,
              start: p,
              end: g,
              limitLow: x.min,
              limitHigh: x.max,
              mode: "current",
            };
          (u.active = v),
            ("modify" !== v.mode &&
              e.targetRect &&
              !fs(e.targetRect, { x: l, y: h })) ||
              Object.keys(u).forEach(function (t) {
                return (e[t] = u[t]);
              });
        }
      })({
        e: t,
        state: this.state,
        renderer: this.renderer,
        ranges: Lu,
        targetSize: 5,
      });
    },
    end: function () {
      this.state.started &&
        (!(function (t, e) {
          (t.started = !1), (t.ranges = e(t));
          var n = Au(t);
          Eu(t, t.current, n);
        })(this.state, Lu),
        Fu(this.state));
    },
    move: function (t) {
      this.state.started &&
        (!(function (t, e) {
          var n = e.center[t.cssCoord.coord] - t.offset[t.cssCoord.offset];
          t.current = Math.max(
            Math.min(n, t.active.limitHigh),
            t.active.limitLow
          );
        })(this.state, t),
        (function (t) {
          var e = t.ranges.map(function (t) {
            return { min: t.min, max: t.max };
          });
          if (-1 !== t.active.idx)
            if ("modify" === t.active.mode)
              (e[t.active.idx].min = Math.min(t.start, t.current)),
                (e[t.active.idx].max = Math.max(t.start, t.current));
            else {
              var n = Nu(t);
              (e[t.active.idx].min = t.active.start + n),
                (e[t.active.idx].max = t.active.end + n);
            }
          else
            e.push({
              min: Math.min(t.start, t.current),
              max: Math.max(t.start, t.current),
            });
          t.rc.set(e);
          var i = [];
          e.forEach(function (e) {
            i.push.apply(
              i,
              $(
                (function (t, e) {
                  var n = {
                    x: t.direction ? e.min + t.rect.x : t.rect.x,
                    y: t.direction ? t.rect.y : e.min + t.rect.y,
                    width: t.direction
                      ? e.max - e.min
                      : t.rect.width + t.rect.x,
                    height: t.direction
                      ? t.rect.height + t.rect.y
                      : e.max - e.min,
                  };
                  return t.chart.shapesAt(n, t.settings.brush);
                })(t, e)
              )
            );
          }),
            (function (t, e) {
              t.chart.brushFromShapes(e, t.settings.brush);
            })(t, i);
        })(this.state),
        Fu(this.state));
    },
    clear: function () {
      this.state.rc && this.state.rc.clear(), this.state.renderer.render([]);
    },
  };
  function Wu(t) {
    var e = "vertical" === t.direction,
      n = t.rect[e ? "height" : "width"],
      i = t.rect[e ? "width" : "height"];
    return (function (t) {
      var e = t.brush;
      if (!e || !e.isActive()) return [];
      var n = t.scale.data(),
        i = ((n && n.fields) || []).map(function (t) {
          return t.id();
        }),
        r = e.brushes().filter(function (t) {
          return "range" === t.type && -1 !== i.indexOf(t.id);
        })[0];
      return r ? r.brush.ranges() : [];
    })(t).map(function (r) {
      var a = t.scale(r.min) * n,
        o = t.scale(r.max) * n,
        s = Math.min(a, o),
        c = Math.abs(a - o);
      return {
        type: "rect",
        fill: t.fill,
        opacity: t.opacity,
        x: e ? 0 : s,
        width: e ? i : c,
        y: e ? s : 0,
        height: e ? c : i,
      };
    });
  }
  function Vu(t, e, n, i) {
    function r() {
      !(function (t) {
        t.renderer.render(Wu(t));
      })(t);
    }
    function a() {
      !(function (t) {
        t.renderer.render(Wu(t));
      })(t);
    }
    function o() {
      !(function (t) {
        t.renderer.render(Wu(t));
      })(t);
    }
    (t.brush = e),
      e &&
        (e.on("start", r),
        e.on("update", a),
        e.on("end", o),
        (t.start = r),
        (t.update = a),
        (t.end = o),
        (t.brush = e),
        (t.scale = n),
        (t.renderer = i));
  }
  function $u(t) {
    t.brush &&
      (t.brush.removeListener("start", t.start),
      t.brush.removeListener("update", t.update),
      t.brush.removeListener("end", t.end)),
      (t.start = void 0),
      (t.update = void 0),
      (t.end = void 0),
      (t.brush = void 0),
      (t.scale = void 0),
      (t.renderer = void 0);
  }
  var Hu = {
    require: ["chart", "settings", "renderer"],
    defaultSettings: { settings: {} },
    preferredSize: function () {
      return 50;
    },
    created: function () {
      this.state = {};
    },
    render: function () {
      var t = this.settings.settings,
        e = this.chart.brush(t.brush),
        n = t.direction || "horizontal",
        i = "horizontal" === n ? this.rect.width : this.rect.height,
        r = ho(this.chart.scale(t.scale), i);
      return (
        $u(this.state),
        Vu(this.state, e, r, this.renderer),
        (this.state.rect = this.rect),
        (this.state.fill = t.fill || "#ccc"),
        (this.state.opacity = void 0 !== t.opacity ? t.opacity : 1),
        (this.state.direction = n),
        Wu(this.state)
      );
    },
    beforeDestroy: function () {
      $u(this.state);
    },
  };
  function qu(t, e) {
    var n = e.center.x,
      i = e.center.y;
    return { x: n - t.left, y: i - t.top };
  }
  function Uu(t, e, n) {
    return jo(t, e.points[0]) < Math.pow(n.settings.snapIndicator.threshold, 2);
  }
  function Yu(t, e) {
    null == t.path.d
      ? (t.path.d = "M".concat(e.x, " ").concat(e.y, " "))
      : (t.path.d += "L".concat(e.x, " ").concat(e.y, " ")),
      t.points.push(e);
  }
  function Xu(t) {
    var e = t.state,
      n = t.start,
      i = void 0 === n ? null : n,
      r = t.end,
      a = void 0 === r ? null : r;
    null !== i && ((e.snapIndicator.x1 = i.x), (e.snapIndicator.y1 = i.y)),
      null !== a && ((e.snapIndicator.x2 = a.x), (e.snapIndicator.y2 = a.y));
  }
  function Gu(t, e) {
    t.snapIndicator.visible = e;
  }
  var Zu = {
    require: ["chart", "renderer", "settings"],
    defaultSettings: {
      layout: { displayOrder: 0 },
      settings: {
        brush: { components: [] },
        snapIndicator: {
          threshold: 75,
          strokeDasharray: "5, 5",
          stroke: "black",
          strokeWidth: 2,
          opacity: 0.5,
        },
        lasso: {
          fill: "transparent",
          stroke: "black",
          strokeWidth: 2,
          opacity: 0.7,
          strokeDasharray: "20, 10",
        },
        startPoint: {
          r: 10,
          fill: "green",
          stroke: "black",
          strokeWidth: 1,
          opacity: 1,
        },
      },
    },
    on: {
      lassoStart: function (t) {
        this.start(t);
      },
      lassoEnd: function (t) {
        this.end(t);
      },
      lassoMove: function (t) {
        this.move(t);
      },
      lassoCancel: function () {
        this.cancel();
      },
    },
    created: function () {
      this.state = {
        points: [],
        active: !1,
        path: null,
        snapIndicator: null,
        startPoint: null,
        rendererBounds: null,
        componentDelta: null,
        brushConfig: null,
        lineBrushShape: { x1: 0, y1: 0, x2: 0, y2: 0 },
      };
    },
    start: function (t) {
      var e, n, i, r;
      (this.state.active = !0),
        (this.state.path = {
          visible: !0,
          type: "path",
          d: null,
          fill: (e = this.settings.settings.lasso).fill,
          stroke: e.stroke,
          strokeWidth: e.strokeWidth,
          opacity: e.opacity,
          strokeDasharray: e.strokeDasharray,
          collider: { type: null },
        }),
        (this.state.snapIndicator = (function (t) {
          return {
            visible: !1,
            type: "line",
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 0,
            strokeDasharray: t.strokeDasharray,
            stroke: t.stroke,
            strokeWidth: t.strokeWidth,
            opacity: t.opacity,
            collider: { type: null },
          };
        })(this.settings.settings.snapIndicator)),
        (this.state.startPoint = (function (t) {
          return {
            visible: !0,
            type: "circle",
            cx: 0,
            cy: 0,
            r: t.r,
            fill: t.fill,
            opacity: t.opacity,
            stroke: t.stroke,
            strokeWidth: t.strokeWidth,
            collider: { type: null },
          };
        })(this.settings.settings.startPoint)),
        (this.state.rendererBounds = this.renderer
          .element()
          .getBoundingClientRect()),
        (this.state.componentDelta =
          ((n = this.chart),
          (i = this.state.rendererBounds),
          (r = n.element.getBoundingClientRect()),
          { x: i.left - r.left, y: i.top - r.top })),
        (this.state.brushConfig = this.settings.settings.brush.components.map(
          function (t) {
            return {
              key: t.key,
              contexts: t.contexts || ["lassoBrush"],
              data: t.data || [""],
              action: t.action || "add",
            };
          }
        ));
      var a = qu(this.state.rendererBounds, t);
      Yu(this.state, a),
        Xu({ state: this.state, start: a }),
        (function (t, e) {
          (t.startPoint.cx = e.x), (t.startPoint.cy = e.y);
        })(this.state, a);
    },
    move: function (t) {
      if (this.state.active) {
        var e = qu(this.state.rendererBounds, t);
        Uu(e, this.state, this.settings)
          ? Gu(this.state, !0)
          : Gu(this.state, !1),
          Yu(this.state, e),
          Xu({ state: this.state, end: e }),
          (function (t, e) {
            var n = [t.startPoint, t.path, t.snapIndicator].filter(function (
              t
            ) {
              return t.visible;
            });
            e.render(n);
          })(this.state, this.renderer),
          (function (t, e) {
            if (t.active) {
              var n = t.points[t.points.length - 2],
                i = t.points[t.points.length - 1];
              (t.lineBrushShape.x1 = n.x + t.componentDelta.x),
                (t.lineBrushShape.y1 = n.y + t.componentDelta.y),
                (t.lineBrushShape.x2 = i.x + t.componentDelta.x),
                (t.lineBrushShape.y2 = i.y + t.componentDelta.y);
              var r = e.shapesAt(t.lineBrushShape, {
                components: t.brushConfig,
              });
              e.brushFromShapes(r, { components: t.brushConfig });
            }
          })(this.state, this.chart);
      }
    },
    end: function (t) {
      this.state.active &&
        (Gu(this.state, !1),
        Uu(qu(this.state.rendererBounds, t), this.state, this.settings) &&
          (function (t, e) {
            if (t.active) {
              var n = t.componentDelta.x,
                i = t.componentDelta.y,
                r = t.points.map(function (t) {
                  return { x: t.x + n, y: t.y + i };
                }),
                a = e.shapesAt({ vertices: r }, { components: t.brushConfig });
              e.brushFromShapes(a, { components: t.brushConfig });
            }
          })(this.state, this.chart),
        (this.state = {
          points: [],
          active: !1,
          path: null,
          snapIndicator: null,
          startPoint: null,
          rendererBounds: null,
          componentDelta: null,
          brushConfig: null,
          lineBrushShape: { x1: 0, y1: 0, x2: 0, y2: 0 },
        }),
        this.renderer.render([]));
    },
    cancel: function () {
      var t, e;
      this.state.active &&
        ((t = this.state),
        (e = this.chart),
        t.brushConfig.forEach(function (t) {
          t.contexts.forEach(function (t) {
            e.brush(t).end();
          });
        }),
        (this.state = {
          points: [],
          active: !1,
          path: null,
          snapIndicator: null,
          startPoint: null,
          rendererBounds: null,
          componentDelta: null,
          brushConfig: null,
          lineBrushShape: { x1: 0, y1: 0, x2: 0, y2: 0 },
        }),
        this.renderer.render([]));
    },
    render: function () {},
  };
  function Ju(t, e, n, i, r) {
    for (var a, o = 0, s = e.length - 1; o < s; ) {
      var c = Math.floor((o + s) / 2);
      (a = r(e[c]))[n] + a[i] < t[n] ? (o = c + 1) : (s = c);
    }
    return o;
  }
  function Ku(t) {
    var e = t.orientation,
      n = t.targetNodes,
      i = t.labels,
      r = t.container,
      a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : Ju,
      o = [],
      s = "v" === e ? "x" : "y",
      c = "v" === e ? "width" : "height",
      u = function (t) {
        return t.textBounds;
      },
      l = function (t) {
        return t.node.localBounds;
      };
    return function (t, e) {
      var h = i[e],
        f = h.textBounds,
        d = h.node;
      if (f[s] < r[s] || f[s] + f[c] > r[s] + r[c]) return !1;
      for (var g = a(f, o, s, c, u); g < o.length; g++)
        if (ls(f, o[g].textBounds)) return !1;
      for (
        var p = a(f, n, s, c, l), y = f[s] + f[c], v = p;
        v < n.length;
        v++
      ) {
        var m = n[v].node;
        if (y < m.localBounds[s]) break;
        if (ls(f, m.localBounds) && d !== m) return !1;
      }
      return o.push(i[e]), !0;
    };
  }
  function Qu(t, e) {
    return {
      node: t,
      data: t.data,
      scale: e.scale,
      formatter: e.formatter,
      dataset: e.dataset,
    };
  }
  function tl(t) {
    var e = N(t);
    return ("string" === e || "number" === e) && "" !== t;
  }
  function el(t) {
    return S(
      { type: "rect", rx: 2, ry: 2, fill: t.backgroundColor },
      t.backgroundBounds
    );
  }
  function nl(t, e, n) {
    return n ? t.width >= e.height : t.width >= e.width;
  }
  function il(t, e, n) {
    return n ? t.height >= e.width : t.height >= e.height;
  }
  function rl(t, e, n, i, r) {
    var a, o;
    return (
      "v" === t
        ? ((a = i || r || nl(e, n, !0)), (o = il(e, n, !i)))
        : ((a = nl(e, n)), (o = r || il(e, n, !1))),
      a && o
    );
  }
  function al(t, e, n, i) {
    return t + 0.5 * e + (i - 0.5) * (e - n) - 0.5 * n;
  }
  function ol(t, e, n) {
    var i = {
        type: "text",
        text: e,
        maxWidth: n.rotate ? t.height : t.width,
        x: 0,
        y: t.y,
        dx: 0,
        dy: 0,
        fill: n.fill,
        anchor: n.rotate ? "end" : "start",
        baseline: "central",
        fontSize: "".concat(n.fontSize, "px"),
        fontFamily: n.fontFamily,
      },
      r = n.textMetrics;
    if (
      !n.overflow &&
      !(function (t, e, n) {
        return nl(t, e, n.rotate) && il(t, e, n.rotate);
      })(t, r, n)
    )
      return !1;
    var a = 0.5 * r.height;
    return (
      n.rotate
        ? ((i.x = al(t.x, t.width, r.height, n.align) + a),
          (i.y = al(t.y, t.height, r.width, n.justify)),
          (i.transform = "rotate(-90, "
            .concat(i.x + i.dx, ", ")
            .concat(i.y + i.dy, ")")))
        : ((i.x = al(t.x, t.width, r.width, n.align)),
          (i.y = al(t.y, t.height, r.height, n.justify) + a)),
      i
    );
  }
  function sl(t) {
    var e = t.bar,
      n = t.view,
      i = t.direction,
      r = t.position,
      a = t.padding,
      o = void 0 === a ? 4 : a,
      s = {};
    if ((k(s, e), r && "inside" !== r))
      if ("up" === i || "down" === i) {
        var c = Math.max(0, Math.min(e.y, n.height)),
          u = Math.max(0, Math.min(e.y + e.height, n.height));
        ("outside" === r && "up" === i) || ("opposite" === r && "down" === i)
          ? ((s.y = 0), (s.height = c))
          : (("outside" === r && "down" === i) ||
              ("opposite" === r && "up" === i)) &&
            ((s.y = u), (s.height = n.height - u));
      } else {
        var l = Math.max(0, Math.min(e.x, n.width)),
          h = Math.max(0, Math.min(e.x + e.width, n.width));
        ("outside" === r && "left" === i) || ("opposite" === r && "right" === i)
          ? ((s.x = 0), (s.width = l))
          : (("outside" === r && "right" === i) ||
              ("opposite" === r && "left" === i)) &&
            ((s.x = h), (s.width = n.width - h));
      }
    else;
    return (
      (function (t, e) {
        var n = Math.max(0, Math.min(t.y, e.height)),
          i = Math.max(0, Math.min(t.y + t.height, e.height)),
          r = Math.max(0, Math.min(t.x, e.width)),
          a = Math.max(0, Math.min(t.x + t.width, e.width));
        (t.x = r), (t.width = a - r), (t.y = n), (t.height = i - n);
      })(s, n),
      (function (t) {
        var e =
            arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
          n = e.top,
          i = void 0 === n ? 4 : n,
          r = e.bottom,
          a = void 0 === r ? 4 : r,
          o = e.left,
          s = void 0 === o ? 4 : o,
          c = e.right,
          u = void 0 === c ? 4 : c;
        (t.x += s), (t.width -= s + u), (t.y += i), (t.height -= i + a);
      })(s, o),
      s
    );
  }
  function cl(t) {
    var e,
      n,
      i,
      r,
      a,
      o = t.direction,
      s = t.fitsHorizontally,
      c = t.measured,
      u = t.node,
      l = t.orientation,
      h = t.placementSettings,
      f = t.rect,
      d = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : sl,
      g = [],
      p = "h" === l ? "width" : "height";
    for (a = 0; a < h.length; a++)
      if (
        ((i = h[a]),
        (r = d({
          bar: u.localBounds,
          view: f,
          direction: o,
          position: i.position,
          padding: i.padding,
        })),
        g.push(r),
        (e = !a || r[p] > e[p] ? r : e),
        rl(l, r, c, s, i.overflow))
      ) {
        n = r;
        break;
      }
    return (
      n || ((n = e), (a = g.indexOf(n))), { bounds: n, placement: (i = h[a]) }
    );
  }
  function ul(t, e, n, i) {
    var r = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : {},
      a = r.top,
      o = void 0 === a ? 4 : a,
      s = r.bottom,
      c = void 0 === s ? 4 : s,
      u = r.left,
      l = void 0 === u ? 4 : u,
      h = r.right,
      f = void 0 === h ? 4 : h,
      d = t.x + t.dx,
      g = t.y + t.dy,
      p = n ? Math.min(e.width, i.height) : Math.min(e.height, i.width),
      y = n ? Math.min(e.height, i.height) : Math.min(e.width, i.width),
      v = 0.5 * e.height,
      m = 1e-9,
      x = n ? d - v : d,
      b = n ? g : g - v,
      _ = {
        x: x - l - m,
        y: b - o - m,
        width: y + (l + f) - m,
        height: p + (o + c) - m,
      };
    return _;
  }
  function ll(t) {
    for (
      var e,
        n,
        i,
        r,
        a,
        o,
        s,
        c,
        u,
        l,
        h,
        f,
        d,
        g = t.chart,
        p = t.targetNodes,
        y = t.rect,
        v = t.fitsHorizontally,
        m = t.collectiveOrientation,
        x = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : cl,
        b = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : ol,
        _ = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : Ku,
        w = [],
        k = { container: y, targetNodes: p, labels: [], orientation: m },
        M = 0,
        R = p.length;
      M < R;
      M++
    ) {
      (o = null),
        (f = Qu((i = (n = p[M]).node), g)),
        (d = "left" === (u = n.direction) || "right" === u ? "h" : "v");
      for (var z = 0; z < n.texts.length; z++)
        if (tl((r = n.texts[z]))) {
          var C = x({
            direction: u,
            fitsHorizontally: v,
            lblStngs: (l = n.labelSettings[z]),
            measured: (c = n.measurements[z]),
            node: i,
            orientation: d,
            placements: l.placements,
            placementSettings: n.placementSettings[z],
            rect: y,
          });
          if (((o = C.bounds), (h = C.placement), o && h)) {
            a = h.justify;
            var A = "function" == typeof l.linkData ? l.linkData(f, M) : void 0,
              E =
                "function" == typeof h.overflow ? h.overflow(f, M) : h.overflow;
            "up" === u && (a = 1 - a),
              "opposite" === h.position && (a = 1 - a),
              "left" === u && (a = 1 - a);
            var O = !("h" === m || v);
            if (
              (e = b(o, r, {
                justify: "h" === d ? h.align : a,
                align: "h" === d ? a : h.align,
                fontSize: l.fontSize,
                fontFamily: l.fontFamily,
                textMetrics: c,
                rotate: O,
                overflow: !!E,
              }))
            ) {
              var T = ul(e, c, O, o, h.padding);
              (s =
                "function" == typeof h.fill
                  ? h.fill(S(S({}, f), {}, { textBounds: T }), M)
                  : h.fill),
                (e.fill = s),
                void 0 !== A && (e.data = A),
                "object" === N(h.background) &&
                  ((e.backgroundColor =
                    "function" == typeof h.background.fill
                      ? h.background.fill(S(S({}, f), {}, { textBounds: T }), M)
                      : h.background.fill),
                  void 0 !== e.backgroundColor &&
                    (e.backgroundBounds = ul(
                      e,
                      c,
                      O,
                      o,
                      h.background.padding
                    ))),
                w.push(e),
                k.labels.push({ node: i, textBounds: T });
            }
          }
        }
    }
    var j = w.filter(_(k)),
      P = j
        .filter(function (t) {
          return void 0 !== t.backgroundBounds;
        })
        .map(el);
    return [].concat($(P), $(j));
  }
  function hl(t) {
    for (
      var e,
        n,
        i,
        r,
        a,
        o,
        s,
        c = t.nodes,
        u = t.rect,
        l = t.chart,
        h = t.labelSettings,
        f = t.placementSettings,
        d = t.settings,
        g = t.renderer,
        p = {},
        y = [],
        v = !0,
        m = !1,
        x = 0;
      x < c.length;
      x++
    )
      if (ls((r = (n = c[x]).localBounds), u)) {
        var b = Qu(n, l);
        e = {
          node: n,
          texts: [],
          measurements: [],
          labelSettings: [],
          placementSettings: [],
        };
        for (var _ = 0; _ < h.length; _++)
          tl(
            (i = "function" == typeof (o = h[_]).label ? o.label(b, x) : void 0)
          ) &&
            ((s =
              "function" == typeof d.direction
                ? d.direction(b, x)
                : d.direction || "up"),
            (m = m || "left" === s || "right" === s),
            (p.fontFamily = o.fontFamily),
            (p.fontSize = "".concat(o.fontSize, "px")),
            (p.text = i),
            (a = g.measureText(p)),
            e.measurements.push(a),
            e.texts.push(i),
            e.labelSettings.push(o),
            e.placementSettings.push(f[_]),
            (e.direction = s),
            (v = v && a.width <= r.width - 8));
        y.push(e);
      }
    return { targetNodes: y, fitsHorizontally: v, hasHorizontalDirection: m };
  }
  function fl(t) {
    var e = t.orientation,
      n = void 0 === e ? "auto" : e,
      i = t.defaultOrientation,
      r = void 0 === i ? "h" : i;
    switch (n.toLocaleLowerCase()) {
      case "vertical":
        return "v";
      case "horizontal":
        return "h";
      default:
        return r;
    }
  }
  function dl(t) {
    var e = 2 * Math.PI;
    return ((t % e) + e) % e;
  }
  function gl(t, e) {
    (t.x += e), (t.width -= 2 * e), (t.y += e), (t.height -= 2 * e);
  }
  function pl(t) {
    return { x: t.x, y: t.y - t.height / 2, width: t.width, height: t.height };
  }
  function yl(t, e, n) {
    var i = e.x,
      r = e.y;
    if (i * i + r * r > t * t) return null;
    var a = Math.sin(n),
      o = Math.cos(n),
      s = i * o - r * a,
      c = t * t - s * s;
    if (c < 0) return null;
    var u = Math.sqrt(c);
    return { x: s * o + a * u, y: -s * a + o * u };
  }
  function vl(t) {
    var e,
      n,
      i = t.radius,
      r = t.size,
      a = t.angle,
      o = r.width,
      s = r.height,
      c = { x: o / 2, y: s / 2 };
    switch (Math.floor(a / (Math.PI / 2))) {
      case 0:
        if (!(e = yl(i, c, a))) return null;
        (e.y *= -1), (n = { x: -o, y: 0 });
        break;
      case 1:
        if (!(e = yl(i, c, Math.PI - a))) return null;
        n = { x: -o, y: -s };
        break;
      case 2:
        if (!(e = yl(i, c, a - Math.PI))) return null;
        (e.x *= -1), (n = { x: 0, y: -s });
        break;
      case 3:
        if (!(e = yl(i, c, 2 * Math.PI - a))) return null;
        (e.x *= -1), (e.y *= -1), (n = { x: 0, y: 0 });
        break;
      default:
        throw new Error("invalid angle");
    }
    return { x: e.x + n.x, y: e.y + n.y, width: o, height: s };
  }
  function ml(t) {
    var e = t.slice,
      n = t.measured,
      i = t.padding,
      r = e.start,
      a = e.end,
      o = e.innerRadius,
      s = e.outerRadius,
      c = s - o - 2 * i,
      u = a - r;
    if (u < Math.PI) {
      var l = (n.height / 2 + i) / Math.tan(u / 2);
      l > o && (c = s - l - 2 * i);
    }
    if (c < 0 || c < n.minReqWidth) return null;
    var h = dl((r + a) / 2),
      f = s - i,
      d = {
        x: Math.sin(h) * f,
        y: -Math.cos(h) * f,
        width: c,
        height: n.height,
      };
    return (
      h < Math.PI
        ? ((d.angle = h - Math.PI / 2), (d.anchor = "end"))
        : ((d.angle = h + Math.PI / 2), (d.anchor = "start")),
      d
    );
  }
  function xl(t) {
    var e = t.context,
      n = t.node,
      i = t.bounds,
      r = n.desc.slice,
      a = dl((r.start + r.end) / 2);
    switch (Math.floor(a / (Math.PI / 2))) {
      case 0:
        (e.q1maxY = i.y - 2),
          void 0 === e.q2minY && (e.q2minY = i.y + i.height + 2);
        break;
      case 1:
        e.q2minY = i.y + i.height + 2;
        break;
      case 2:
        e.q3minY = i.y + i.height + 2;
        break;
      case 3:
        (e.q4maxY = i.y - 2),
          void 0 === e.q3minY && (e.q3minY = i.y + i.height + 2);
    }
  }
  function bl(t, e) {
    return {
      node: t,
      data: t.data,
      scale: e.scale,
      formatter: e.formatter,
      dataset: e.dataset,
    };
  }
  function _l(t, e, n) {
    var i = {
      type: "text",
      text: e,
      maxWidth: t.width,
      x: t.x,
      y: t.y + ("top" === t.baseline ? t.height / 2 : 0),
      fill: n.fill,
      anchor: t.anchor || "start",
      baseline: "middle",
      fontSize: "".concat(n.fontSize, "px"),
      fontFamily: n.fontFamily,
    };
    if (!isNaN(t.angle)) {
      var r = t.angle * (360 / (2 * Math.PI));
      i.transform = "rotate("
        .concat(r, ", ")
        .concat(i.x, ", ")
        .concat(i.y, ")");
    }
    return i;
  }
  function wl(t) {
    var e,
      n,
      i = t.slice,
      r = t.direction,
      a = t.position,
      o = t.padding,
      s = t.measured,
      c = t.view,
      u = t.context,
      l = t.store,
      h = i.start,
      f = i.end,
      d = i.innerRadius,
      g = i.offset;
    switch (a) {
      case "into":
        e =
          "rotate" === r
            ? ml({ slice: i, measured: s, padding: o })
            : (function (t) {
                var e = t.slice,
                  n = t.padding,
                  i = t.measured,
                  r = e.start,
                  a = e.end,
                  o = e.innerRadius,
                  s = e.outerRadius,
                  c = dl((r + a) / 2),
                  u = vl({
                    radius: s,
                    size: { width: i.width + 2 * n, height: i.height + 2 * n },
                    angle: c,
                  });
                return u
                  ? ((u.baseline = "top"),
                    ds(u, {
                      x1: 0,
                      y1: 0,
                      x2: Math.sin(r) * s,
                      y2: -Math.cos(r) * s,
                    }) ||
                    ds(u, {
                      x1: 0,
                      y1: 0,
                      x2: Math.sin(a) * s,
                      y2: -Math.cos(a) * s,
                    }) ||
                    rs({ cx: 0, cy: 0, r: o }, u)
                      ? null
                      : (gl(u, n), u))
                  : null;
              })({ slice: i, measured: s, padding: o });
        break;
      case "inside":
        (n = { start: h, end: f, innerRadius: 0, outerRadius: d }),
          (e =
            "rotate" === r
              ? ml({ slice: n, measured: s, padding: o })
              : (function (t) {
                  var e = t.slice,
                    n = t.padding,
                    i = t.measured,
                    r = t.store,
                    a = e.start,
                    o = e.end,
                    s = e.outerRadius,
                    c = dl((a + o) / 2),
                    u = vl({
                      radius: s,
                      size: {
                        width: i.width + 2 * n,
                        height: i.height + 2 * n,
                      },
                      angle: c,
                    });
                  return u
                    ? ((u.baseline = "top"),
                      gl(u, n),
                      r.insideLabelBounds.some(function (t) {
                        return ls(t, u);
                      })
                        ? null
                        : (r.insideLabelBounds.push({
                            x: u.x - 4,
                            y: u.y,
                            width: u.width + 8,
                            height: u.height,
                          }),
                          u))
                    : null;
                })({ slice: n, measured: s, padding: o, store: l }));
        break;
      case "outside":
        e =
          "rotate" === r
            ? (function (t) {
                var e = t.slice,
                  n = t.measured,
                  i = t.padding,
                  r = t.view,
                  a = e.start,
                  o = e.end,
                  s = e.outerRadius,
                  c = e.offset,
                  u = s + i,
                  l = o - a;
                if (l < Math.PI && (n.height / 2 + i) / Math.tan(l / 2) > u)
                  return null;
                var h = dl((a + o) / 2),
                  f = Math.sin(h) * u,
                  d = -Math.cos(h) * u,
                  g = n.width,
                  p = h % Math.PI;
                if (
                  (p > Math.PI / 2 && (p = Math.PI - p), Math.cos(p) > 0.001)
                ) {
                  var y = d < 0 ? r.y : r.y + r.height,
                    v =
                      Math.abs(y - c.y) / Math.cos(p) -
                      Math.tan(p) * (n.height / 2) -
                      2 * i -
                      s;
                  v < g && (g = v);
                }
                if (Math.sin(p) > 0.001) {
                  var m = f < 0 ? r.x : r.x + r.width,
                    x =
                      Math.abs(m - c.x) / Math.sin(p) -
                      n.height / 2 / Math.tan(p) -
                      2 * i -
                      s;
                  x < g && (g = x);
                }
                if (g <= 0 || g < n.minReqWidth) return null;
                var b = { x: f, y: d, width: g, height: n.height };
                return (
                  h < Math.PI
                    ? ((b.angle = h - Math.PI / 2), (b.anchor = "start"))
                    : ((b.angle = h + Math.PI / 2), (b.anchor = "end")),
                  b
                );
              })({ slice: i, measured: s, padding: o, view: c })
            : (function (t) {
                var e = t.slice,
                  n = t.measured,
                  i = t.padding,
                  r = t.view,
                  a = t.context,
                  o = e.start,
                  s = e.end,
                  c = e.outerRadius,
                  u = e.offset,
                  l = dl((o + s) / 2);
                if (
                  (function (t, e, n) {
                    switch (e) {
                      case 0:
                        return t.q1maxY < 0;
                      case 1:
                        return t.q2minY > n.height;
                      case 2:
                        return t.q3minY > n.height;
                      case 3:
                        return t.q4maxY < 0;
                      default:
                        return !0;
                    }
                  })(a, Math.floor(l / (Math.PI / 2)), r)
                )
                  return null;
                var h = c + i + n.height / 2,
                  f = Math.sin(l) * h,
                  d = -Math.cos(l) * h,
                  g = n.width;
                if (l < Math.PI) {
                  var p = Math.abs(r.x + r.width - (f + u.x));
                  p < g && (g = p);
                } else {
                  var y = Math.abs(r.x - (f + u.x));
                  y < g && (g = y);
                }
                if (g < n.minReqWidth) return null;
                var v = { x: f, y: d, width: g, height: n.height };
                return (
                  l < Math.PI ? (v.anchor = "start") : (v.anchor = "end"), v
                );
              })({ slice: i, measured: s, padding: o, view: c, context: u });
        break;
      default:
        throw new Error("not implemented");
    }
    return (
      e &&
        ((e.x += g.x),
        (e.y += g.y),
        "outside" === a &&
          "rotate" !== r &&
          (function (t, e, n) {
            var i = n.start,
              r = n.end,
              a = n.offset,
              o = n.outerRadius,
              s = dl((i + r) / 2);
            switch (Math.floor(s / (Math.PI / 2))) {
              case 0:
                if (void 0 !== e.q1maxY) {
                  var c = Math.min(t.y, e.q1maxY - t.height),
                    u = t.y - c;
                  if (((t.y = c), u > 1)) {
                    var l = o + 2;
                    t.line = {
                      type: "line",
                      x1: t.x - 2,
                      y1: t.y + 2,
                      x2: a.x + Math.sin(s) * l,
                      y2: a.y - Math.cos(s) * l,
                      strokeWidth: 1,
                    };
                  }
                }
                break;
              case 1:
                if (void 0 !== e.q2minY) {
                  var h = Math.max(t.y, e.q2minY),
                    f = h - t.y;
                  if (((t.y = h), f > 1)) {
                    var d = o + 2;
                    t.line = {
                      type: "line",
                      x1: t.x - 2,
                      y1: t.y - 2,
                      x2: a.x + Math.sin(s) * d,
                      y2: a.y - Math.cos(s) * d,
                      strokeWidth: 1,
                    };
                  }
                }
                break;
              case 2:
                if (void 0 !== e.q3minY) {
                  var g = Math.max(t.y, e.q3minY),
                    p = g - t.y;
                  if (((t.y = g), p > 1)) {
                    var y = o + 2;
                    t.line = {
                      type: "line",
                      x1: t.x + 2,
                      y1: t.y - 2,
                      x2: a.x + Math.sin(s) * y,
                      y2: a.y - Math.cos(s) * y,
                      strokeWidth: 1,
                    };
                  }
                }
                break;
              case 3:
                if (void 0 !== e.q4maxY) {
                  var v = Math.min(t.y, e.q4maxY - t.height),
                    m = t.y - v;
                  if (((t.y = v), m > 1)) {
                    var x = o + 2;
                    t.line = {
                      type: "line",
                      x1: t.x + 2,
                      y1: t.y + 2,
                      x2: a.x + Math.sin(s) * x,
                      y2: a.y - Math.cos(s) * x,
                      strokeWidth: 1,
                    };
                  }
                }
            }
          })(e, u, i)),
      e
    );
  }
  function kl(t) {
    for (
      var e = t.context,
        n = t.direction,
        i = t.measured,
        r = t.node,
        a = t.placementSettings,
        o = t.rect,
        s = t.store,
        c = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : wl,
        u = 0;
      u < a.length;
      u++
    ) {
      var l = a[u],
        h = c({
          context: e,
          slice: r.desc.slice,
          view: o,
          direction: n,
          position: l.position,
          measured: i,
          padding: l.padding,
          store: s,
        });
      if (h) return { bounds: h, placement: l };
    }
    return { bounds: null, placement: null };
  }
  function Ml(t) {
    for (var e = [], n = [], i = [], r = [], a = 0; a < t.length; ++a) {
      var o = t[a].desc.slice,
        s = dl((o.start + o.end) / 2);
      switch (Math.floor(s / (Math.PI / 2))) {
        case 0:
          e.push(t[a]);
          break;
        case 1:
          n.push(t[a]);
          break;
        case 2:
          i.push(t[a]);
          break;
        case 3:
          r.push(t[a]);
      }
    }
    var c = function (t, e) {
        return (
          dl((t.desc.slice.start + t.desc.slice.end) / 2) -
          dl((e.desc.slice.start + e.desc.slice.end) / 2)
        );
      },
      u = function (t, e) {
        return c(e, t);
      };
    return e.sort(u), n.sort(c), i.sort(u), r.sort(c), e.concat(n, r, i);
  }
  function Rl(t, e, n) {
    var i = e.fontFamily,
      r = "".concat(e.fontSize, "px"),
      a = n.measureText({ text: t, fontFamily: i, fontSize: r });
    return (
      (a.minReqWidth = Math.min(
        a.width,
        n.measureText({
          text: "".concat(t[0], "…"),
          fontFamily: i,
          fontSize: r,
        }).width
      )),
      a
    );
  }
  var Sl = /\n+|\r+|\r\n/,
    Nl = /\s/,
    zl = /[a-zA-Z\u00C0-\u00F6\u00F8-\u00FF\u00AD]/;
  function Cl(t) {
    return "string" == typeof t
      ? -1 !== t.search(Sl)
      : -1 !== String(t).search(Sl);
  }
  function Al(t) {
    return -1 !== t.search(Nl);
  }
  function El(t) {
    return -1 !== t.search(zl);
  }
  function Ol(t, e, n, i, r) {
    return i.some(function (i) {
      return i(t, e, n);
    })
      ? 1
      : r.some(function (i) {
          return i(t, e, n);
        })
      ? 0
      : 2;
  }
  function Tl(t) {
    "" === t[0] && t.shift(), "" === t[t.length - 1] && t.pop();
  }
  function jl(t, e, n) {
    return Math.max(e, Math.min(n, t));
  }
  function Pl() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = t.string,
      n = t.separator,
      i = void 0 === n ? "" : n,
      r = t.reverse,
      a = void 0 !== r && r,
      o = t.measureText,
      s =
        void 0 === o
          ? function (t) {
              return { width: t.length, height: 1 };
            }
          : o,
      c = t.mandatoryBreakIdentifiers,
      u = void 0 === c ? [Cl] : c,
      l = t.noBreakAllowedIdentifiers,
      h = void 0 === l ? [] : l,
      f = t.suppressIdentifier,
      d =
        void 0 === f
          ? [
              Al,
              Cl,
              function (t) {
                return "" === t;
              },
            ]
          : f,
      g = t.hyphenationIdentifiers,
      p = void 0 === g ? [El] : g,
      y = String(e).split(i);
    Tl(y);
    var v = y.length,
      m = a
        ? function (t) {
            return t >= 0;
          }
        : function (t) {
            return t < v;
          },
      x = a ? v : -1;
    function b(t) {
      var e = jl(t, 0, v - 1),
        n = y[e],
        i = s(n),
        r = Ol(n, e, y, u, h);
      return {
        index: e,
        value: n,
        breakOpportunity: r,
        suppress: d.some(function (t) {
          return t(n, e, y);
        }),
        hyphenation: p.some(function (t) {
          return t(n, e, y);
        }),
        width: i.width,
        height: i.height,
        done: !1,
      };
    }
    function _(t) {
      return (
        isNaN(t) ? (a ? x-- : x++) : (x = jl(t, 0, v - 1)),
        m(x) ? b(x) : { done: !0 }
      );
    }
    return { next: _, peek: b, length: v };
  }
  var Bl = "…",
    Dl = /^\s*\d+(\.\d+)?px\s*$/i;
  function Fl(t) {
    return "string" === N(t) && Dl.test(t);
  }
  function Ll(t) {
    var e = t["font-size"] || t.fontSize;
    return Fl(e)
      ? parseFloat(e) * Math.max(isNaN(t.lineHeight) ? 1.2 : t.lineHeight, 0)
      : 19.2;
  }
  function Il(t) {
    var e = t.maxHeight,
      n = Math.max(t.maxLines, 1) || 1 / 0;
    if (isNaN(e)) return n;
    var i = Ll(t);
    return Math.max(1, Math.min(Math.floor(e / i), n));
  }
  function Wl(t, e) {
    return {
      lines: [],
      line: "",
      width: 0,
      maxLines: Il(t),
      maxWidth: t.maxWidth,
      hyphens: { enabled: "auto" === t.hyphens, char: "‐", metrics: e("‐") },
    };
  }
  function Vl(t) {
    t.lines.push(t.line), (t.line = ""), (t.width = 0);
  }
  function $l(t, e) {
    (t.line += e.value), (t.width += e.width);
  }
  function Hl(t, e, n) {
    if (e.width > t.maxWidth) return e;
    for (var i = e.index, r = 1; r < 5; r++) {
      var a = n.peek(e.index - 1);
      if (!e.hyphenation || !a.hyphenation || 0 === e.index) return e;
      if (t.width + t.hyphens.metrics.width <= t.maxWidth)
        return (t.line += t.hyphens.char), e;
      if (1 === t.line.length) return e;
      (e = n.next(i - r)), (t.line = t.line.slice(0, -1)), (t.width -= e.width);
    }
    return e;
  }
  function ql(t, e, n) {
    for (
      var i = Pl({ string: e.value, measureText: n });
      t.lines.length < t.maxLines;

    ) {
      var r = i.next();
      if (r.done) break;
      t.width + r.width > t.maxWidth &&
      2 === r.breakOpportunity &&
      t.line.length > 0
        ? ((r = t.hyphens.enabled ? Hl(t, r, i) : r), Vl(t), $l(t, r))
        : $l(t, r);
    }
  }
  function Ul(t, e) {
    for (
      var n = Pl({
          string: t.text,
          separator: "",
          measureText: e,
          noBreakAllowedIdentifiers: [
            function (t, e) {
              return 0 === e;
            },
          ],
        }),
        i = Wl(t, e),
        r = !0;
      i.lines.length < i.maxLines;

    ) {
      var a = n.next();
      if (a.done) {
        Vl(i), (r = !1);
        break;
      }
      1 === a.breakOpportunity
        ? Vl(i)
        : i.width + a.width > i.maxWidth && 2 === a.breakOpportunity
        ? a.suppress
          ? (i.width += a.width)
          : ((a = i.hyphens.enabled ? Hl(i, a, n) : a), Vl(i), $l(i, a))
        : $l(i, a);
    }
    return { lines: i.lines, reduced: r };
  }
  function Yl(t, e) {
    for (
      var n = Pl({
          string: t.text,
          separator: /(\s|-|\u2010)/,
          measureText: e,
        }),
        i = Wl(t, e),
        r = !0;
      i.lines.length < i.maxLines;

    ) {
      var a = n.next();
      if (a.done) {
        Vl(i), (r = !1);
        break;
      }
      1 === a.breakOpportunity
        ? Vl(i)
        : i.width + a.width > i.maxWidth && 2 === a.breakOpportunity
        ? a.suppress
          ? Vl(i)
          : a.width > i.maxWidth
          ? ql(i, a, e)
          : (Vl(i), $l(i, a))
        : $l(i, a);
    }
    return { lines: i.lines, reduced: r };
  }
  function Xl(t) {
    return { "break-all": Ul, "break-word": Yl }[t.wordBreak];
  }
  function Gl(t) {
    return function (e) {
      var n = e.node;
      if (
        (function (t) {
          return "text" === t.type && !t._lineBreak;
        })(n)
      ) {
        var i = Xl(n);
        if (!i) return;
        var r = t(n);
        if (r.width > n.maxWidth || Cl(n.text)) {
          var a = (Ll(n) - r.height) / 2,
            o = i(
              n,
              (function (t, e) {
                return function (n) {
                  return e({
                    text: n,
                    fontSize: t.fontSize,
                    fontFamily: t.fontFamily,
                  });
                };
              })(n, t)
            );
          e.node = (function (t, e, n, i) {
            var r = { type: "container", children: [] };
            void 0 !== e.id && (r.id = e.id);
            var a = 0;
            return (
              t.lines.forEach(function (o, s) {
                var c = k({}, e);
                (c.text = o),
                  (c._lineBreak = !0),
                  (a += n),
                  t.reduced && s === t.lines.length - 1
                    ? (c.text += Bl)
                    : delete c.maxWidth,
                  (c.dy = isNaN(c.dy) ? a : c.dy + a),
                  (a += i),
                  (a += n),
                  r.children.push(c);
              }),
              r
            );
          })(o, n, a, r.height);
        }
      }
    };
  }
  function Zl(t, e) {
    var n = t.text,
      i = t["font-size"],
      r = t["font-family"],
      a = t.maxWidth;
    if (((n = "string" == typeof n ? n : "".concat(n)), void 0 === a)) return n;
    var o = e({ text: n, fontSize: i, fontFamily: r }).width;
    if (o <= a) return n;
    for (var s = 0, c = n.length - 1; s <= c; ) {
      var u = Math.floor((s + c) / 2);
      (o = e({
        text: n.substr(0, u) + Bl,
        fontSize: i,
        fontFamily: r,
      }).width) <= a
        ? (s = u + 1)
        : (c = u - 1);
    }
    return n.substr(0, c) + Bl;
  }
  function Jl(t) {
    var e = t.baseline || t["dominant-baseline"],
      n = 0,
      i = parseInt(t.fontSize || t["font-size"], 10) || 0;
    switch (e) {
      case "hanging":
        n = 0.75 * i;
        break;
      case "text-before-edge":
        n = 0.85 * i;
        break;
      case "middle":
        n = 0.25 * i;
        break;
      case "central":
        n = 0.35 * i;
        break;
      case "mathemetical":
        n = i / 2;
        break;
      case "text-after-edge":
      case "ideographic":
        n = 0.2 * -i;
        break;
      default:
        n = 0;
    }
    return n;
  }
  var Kl,
    Ql = {},
    th = {},
    eh = { fontSize: void 0, fontFamily: void 0 };
  function nh(t, e, n) {
    var i = t + e + n;
    return (
      "number" != typeof th[i] &&
        ((Kl = Kl || document.createElement("canvas").getContext("2d")),
        (function (t, e) {
          (eh.fontSize === t && eh.fontFamily === e) ||
            ((Kl.font = t + " " + e), (eh.fontSize = t), (eh.fontFamily = e));
        })(e, n),
        (th[i] = Kl.measureText(t).width)),
      th[i]
    );
  }
  function ih(t) {
    return (
      "number" != typeof Ql[t] &&
        (Ql[t] = (function (t) {
          if (Fl(t)) {
            var e = parseFloat(t);
            return e + 4 * Math.ceil((e + 1e-12) / 24);
          }
          return 16;
        })(t)),
      Ql[t]
    );
  }
  function rh(t) {
    var e = t.text,
      n = t.fontSize;
    return { width: nh(e, n, t.fontFamily), height: ih(n) };
  }
  function ah(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : rh,
      n = t["font-size"] || t.fontSize,
      i = t["font-family"] || t.fontFamily,
      r = e({ text: t.text, fontFamily: i, fontSize: n }),
      a = Math.min(t.maxWidth || r.width, r.width),
      o = t.x || 0,
      s = t.y || 0,
      c = t.dx || 0,
      u = (t.dy || 0) + Jl(t),
      l = { x: 0, y: s + u - 0.75 * r.height, width: a, height: r.height },
      h = t["text-anchor"] || t.anchor;
    return (
      (l.x = "middle" === h ? o + c - a / 2 : "end" === h ? o + c - a : o + c),
      l
    );
  }
  function oh(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : rh,
      n = Xl(t),
      i = t["font-size"] || t.fontSize,
      r = t["font-family"] || t.fontFamily,
      a = e({ text: t.text, fontFamily: r, fontSize: i });
    if (n && (a.width > t.maxWidth || Cl(t.text))) {
      for (
        var o = n(t, function (t) {
            return e({ text: t, fontFamily: r, fontSize: i });
          }),
          s = k({}, t),
          c = 0,
          u = "",
          l = 0,
          h = o.lines.length;
        l < h;
        l++
      ) {
        var f = o.lines[l];
        f += l === h - 1 && o.reduced ? Bl : "";
        var d = e({ text: f, fontSize: i, fontFamily: r }).width;
        d >= c && ((c = d), (u = f));
      }
      s.text = u;
      var g = ah(s, e);
      return (g.height = Ll(t) * o.lines.length), g;
    }
    return ah(t, e);
  }
  var sh = 0.9;
  function ch(t, e) {
    return {
      node: t,
      data: t.data,
      scale: e.scale,
      formatter: e.formatter,
      dataset: e.dataset,
    };
  }
  function uh(t, e, n) {
    var i = {
        type: "text",
        text: e,
        maxWidth: t.width,
        x: 0,
        y: t.y,
        dx: 0,
        dy: 0,
        fill: n.fill,
        anchor: "start",
        baseline: "alphabetical",
        fontSize: "".concat(n.fontSize, "px"),
        fontFamily: n.fontFamily,
      },
      r = n.textMetrics;
    if (t.width < n.fontSize) return !1;
    var a = Math.max(0, t.width - r.width);
    return (i.x = t.x + n.align * a), (i.y = t.y + r.height / 1.2), i;
  }
  function lh(t) {
    return { type: "circle", bounds: { cx: t.cx, cy: t.cy, r: t.r } };
  }
  function hh(t) {
    return t.desc && t.desc.slice
      ? (function (t) {
          var e = t.start,
            n = t.end,
            i = t.innerRadius,
            r = t.outerRadius,
            a = t.offset;
          if (Math.abs(e + 2 * Math.PI - n) > 1e-12)
            return { type: null, bounds: null };
          var o = 0 !== i ? i : r;
          return lh({ cx: a.x, cy: a.y, r: o });
        })(t.desc.slice)
      : "circle" === t.type
      ? lh(t.attrs)
      : "rect" === t.type
      ? { type: "rect", bounds: t.bounds }
      : { type: null, bounds: null };
  }
  var fh = {
    bar: function (t) {
      var e = t.settings,
        n = t.chart,
        i = t.nodes,
        r = t.rect,
        a = t.renderer,
        o = t.style,
        s = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : ll,
        c = k(
          {
            fontSize: 12,
            fontFamily: "Arial",
            align: 0.5,
            justify: 0,
            fill: "#333",
          },
          o.label
        );
      c.fontSize = parseInt(c.fontSize, 10);
      var u = e.labels.map(function (t) {
          return k({}, c, e, t);
        }),
        l = e.labels.map(function (t) {
          return t.placements.map(function (n) {
            return k({}, c, e, t, n);
          });
        }),
        h = hl({
          nodes: i,
          chart: n,
          renderer: a,
          settings: e,
          rect: r,
          labelSettings: u,
          placementSettings: l,
        }),
        f = h.fitsHorizontally,
        d = h.hasHorizontalDirection,
        g = h.targetNodes,
        p = fl({
          orientation: e.orientation,
          defaultOrientation: d ? "h" : "v",
        }),
        y = "h" === p ? "y" : "x",
        v = "h" === p ? "height" : "width";
      return (
        g.sort(function (t, e) {
          return (
            t.node.localBounds[y] +
            t.node.localBounds[v] -
            (e.node.localBounds[y] + e.node.localBounds[v])
          );
        }),
        s({
          chart: n,
          targetNodes: g,
          stngs: e,
          rect: r,
          fitsHorizontally: f,
          collectiveOrientation: p,
        })
      );
    },
    slice: function (t) {
      var e = t.settings,
        n = t.chart,
        i = t.nodes,
        r = t.rect,
        a = t.renderer,
        o = t.style,
        s = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : kl,
        c = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : _l,
        u = k(
          {
            fontSize: 12,
            fontFamily: "Arial",
            fill: "#333",
            padding: 4,
            position: "into",
          },
          o.label
        );
      u.fontSize = parseInt(u.fontSize, 10);
      for (
        var l = e.labels.map(function (t) {
            return k({}, u, e, t);
          }),
          h = e.labels.map(function (t) {
            return t.placements.map(function (n) {
              return k({}, u, e, t, n);
            });
          }),
          f = [],
          d = { insideLabelBounds: [] },
          g = {},
          p = 0,
          y = (i = Ml(i)).length;
        p < y;
        p++
      )
        for (var v = i[p], m = bl(v, n), x = 0; x < l.length; x++) {
          var b = l[x],
            _ = "function" == typeof b.label ? b.label(m, p) : "";
          if (_) {
            var w =
                "function" == typeof b.direction
                  ? b.direction(m, p)
                  : b.direction || "horizontal",
              M = "function" == typeof b.linkData ? b.linkData(m, p) : void 0,
              R = Rl(_, b, a),
              S = s({
                context: g,
                direction: w,
                lblStngs: b,
                measured: R,
                node: v,
                placementSettings: h[x],
                rect: r,
                store: d,
              }),
              N = S.bounds,
              z = S.placement;
            if (N && z) {
              if ("outside" === z.position && "rotate" !== w) {
                xl({ context: g, node: v, bounds: N });
                var C = pl(N);
                if (!Zo(C, r)) continue;
              }
              var A = "function" == typeof z.fill ? z.fill(m, p) : z.fill,
                E = c(N, _, {
                  fill: A,
                  fontSize: b.fontSize,
                  fontFamily: b.fontFamily,
                  textMetrics: R,
                });
              E &&
                (void 0 !== M && (E.data = M),
                f.push(E),
                N.line && ((N.line.stroke = A), f.push(N.line)));
            }
          }
        }
      return f;
    },
    rows: function (t) {
      var e = t.settings,
        n = t.chart,
        i = t.nodes,
        r = t.renderer,
        a = t.style,
        o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : uh,
        s = k(
          {
            fontSize: 12,
            fontFamily: "Arial",
            fill: "#333",
            padding: 4,
            align: 0.5,
            justify: 0.5,
          },
          a.label
        );
      s.fontSize = parseInt(s.fontSize, 10);
      for (
        var c = k({}, s, e),
          u = e.labels.map(function (t) {
            return k({}, c, t);
          }),
          l = {},
          h = [],
          f = 0,
          d = i.length;
        f < d;
        f++
      ) {
        var g = i[f],
          p = ch(g, n),
          y = hh(g),
          v = y.type,
          m = y.bounds;
        if (m) {
          var x = 0,
            b = [],
            _ = [],
            w = "circle" === v ? 2 * m.r * sh : m.height;
          x += c.padding;
          var M = void 0;
          for (M = 0; M < u.length; M++) {
            var R = u[M],
              S = "function" == typeof R.label ? R.label(p, f) : "";
            (l.fontFamily = R.fontFamily),
              (l.fontSize = "".concat(R.fontSize, "px")),
              (l.text = S);
            var N = r.measureText(l);
            if ((x += N.height + R.padding) > w) break;
            _.push(S), b.push(N);
          }
          var z = M,
            C = Math.max(0, w - x),
            A = void 0;
          for (
            A = "circle" === v ? m.cy - m.r * sh : m.y,
              A += c.justify * C + c.padding,
              M = 0;
            M < z;
            M++
          ) {
            var E = u[M],
              O = void 0;
            if ("circle" === v) {
              var T = Math.max(
                  Math.abs(A - m.cy),
                  Math.abs(A + b[M].height - m.cy)
                ),
                j = Math.sqrt(m.r * m.r - T * T);
              O = {
                x: m.cx - j + c.padding,
                y: A,
                width: 2 * j - 2 * c.padding,
                height: b[M].height,
              };
            } else
              O = {
                x: m.x + c.padding,
                y: A,
                width: m.width - 2 * c.padding,
                height: b[M].height,
              };
            A += b[M].height + c.padding;
            var P = "function" == typeof E.fill ? E.fill(p, f) : E.fill,
              B = "function" == typeof E.linkData ? E.linkData(p, f) : void 0,
              D = o(O, _[M], {
                fill: P,
                align: E.align,
                fontSize: E.fontSize,
                fontFamily: E.fontFamily,
                textMetrics: b[M],
              });
            if (D) {
              if (D.text && D.text !== Bl) {
                var F = Zl(D, r.measureText);
                if (Bl === F) continue;
                D.ellipsed = F;
              }
              void 0 !== B && (D.data = B), h.push(D);
            }
          }
        }
      }
      return h;
    },
  };
  var dh = {
    require: ["chart", "renderer", "settings"],
    defaultSettings: { settings: {}, style: { label: "$label" } },
    render: function () {
      var t = this,
        e = this.settings.settings,
        n = [];
      return (
        (e.sources || []).forEach(function (e) {
          e.strategy &&
            fh[e.strategy.type] &&
            e.component &&
            n.push.apply(
              n,
              $(
                (function (t, e) {
                  var n = t.chart,
                    i = t.source,
                    r = t.rect,
                    a = t.renderer,
                    o = t.style;
                  if (!n.component(i.component)) return [];
                  var s = n.findShapes(i.selector).filter(function (t) {
                    return t.key === i.component;
                  });
                  return e({
                    chart: n,
                    settings: i.strategy.settings,
                    nodes: s,
                    rect: { x: 0, y: 0, width: r.width, height: r.height },
                    renderer: a,
                    style: o,
                  });
                })(
                  {
                    chart: t.chart,
                    rect: t.rect,
                    renderer: t.renderer,
                    source: e,
                    style: t.style,
                  },
                  fh[e.strategy.type]
                )
              )
            );
        }),
        n
      );
    },
  };
  var gh = {
    layout: { size: 1, direction: "ltr", scrollOffset: 0 },
    item: {
      show: !0,
      justify: 0.5,
      align: 0.5,
      label: {
        fontSize: "12px",
        fontFamily: "Arial",
        fill: "#595959",
        wordBreak: "none",
        maxLines: 2,
        maxWidth: 136,
        lineHeight: 1.2,
      },
      shape: { type: "square", size: 12 },
    },
    title: {
      show: !0,
      text: void 0,
      anchor: "start",
      fontSize: "16px",
      fontFamily: "Arial",
      fill: "#595959",
      wordBreak: "none",
      maxLines: 2,
      maxWidth: 156,
      lineHeight: 1.25,
    },
    navigation: {
      button: { class: void 0, content: void 0, tabIndex: void 0 },
      disabled: !1,
    },
  };
  function ph(t) {
    var e = t.x,
      n = void 0 === e ? 0 : e,
      i = t.y,
      r = t.item,
      a = t.globalMetrics,
      o = t.createSymbol,
      s = t.direction,
      c = void 0 === s ? "ltr" : s,
      u = r.label.displayObject,
      l = r.label.bounds,
      h = r.symbol.meta,
      f = "rtl" === c,
      d = {
        x: f ? n + a.maxLabelBounds.width : n + a.maxSymbolSize + a.spacing,
        y: i,
        width: a.maxLabelBounds.width,
        height: Math.max(a.maxSymbolSize, a.maxLabelBounds.height),
      },
      g = (function (t, e, n) {
        var i = Math.max(0, t.width - e),
          r = Math.max(0, t.height - e);
        return { x: t.x + e / 2 + n.align * i, y: t.y + e / 2 + n.justify * r };
      })(
        {
          x: f ? n + a.maxLabelBounds.width + a.spacing : n,
          y: i,
          width: a.maxSymbolSize,
          height: d.height,
        },
        h.size,
        {
          align: void 0 === h.align ? 0.5 : h.align,
          justify: void 0 === h.justify ? 0.5 : h.justify,
        }
      ),
      p = o(k({}, h, g));
    return (
      delete p.collider,
      (u.anchor = f ? "end" : "start"),
      (function (t, e, n) {
        var i = n.textMetrics;
        if (t.height < i.height) return !1;
        var r = Math.max(0, t.width - i.width);
        e.baseline = "text-before-edge";
        var a = Math.max(0, t.height - i.height);
        (e.x = t.x + n.align * r),
          (e.y = t.y + n.justify * a + 0.175 * parseInt(e.fontSize, 10));
      })(d, u, {
        textMetrics: l,
        fontSize: parseInt(u.fontSize, 10),
        align: 0,
        justify: 0.5,
      }),
      {
        item: {
          type: "container",
          data: r.label.displayObject.data,
          children: [p, u],
          collider: {
            type: "rect",
            x: n,
            y: i,
            width: a.maxItemBounds.width,
            height: a.maxItemBounds.height,
          },
        },
        metrics: d,
      }
    );
  }
  function yh(t, e) {
    var n = t.items.length,
      i = Math.ceil(n / e),
      r = "horizontal" === t.layout.orientation ? "width" : "height",
      a = "width" === r ? "horizontal" : "vertical";
    return t.globalMetrics.maxItemBounds[r] * i + (i - 1) * t.layout.margin[a];
  }
  function vh(t, e) {
    var n,
      i,
      r,
      a,
      o = e.onScroll,
      s = void 0 === o ? function () {} : o,
      c = null,
      u = 0,
      l = {
        itemize: function (e) {
          (n = (function (t, e) {
            for (
              var n,
                i,
                r = t.resolved,
                a = t.dock,
                o = [],
                s = r.items.items,
                c = r.symbols.items,
                u = r.labels.items,
                l = 0,
                h = 0,
                f = 0,
                d = 0;
              d < s.length;
              d++
            )
              if (!1 !== s[d].show) {
                var g =
                  void 0 !== u[d].text ? u[d].text : u[d].data.label || "";
                (n = k({}, u[d], {
                  type: "text",
                  fontSize: "".concat(parseInt(u[d].fontSize, 10), "px"),
                  text: g,
                  title: g,
                })),
                  (i = {
                    symbol: { meta: c[d] },
                    label: { displayObject: n, bounds: e.textBounds(n) },
                  }),
                  o.push(i),
                  (l = Math.max(c[d].size, l)),
                  (h = Math.max(i.label.bounds.width, h)),
                  (f = Math.max(i.label.bounds.height, f));
              }
            return {
              items: o,
              globalMetrics: {
                spacing: 8,
                maxSymbolSize: l,
                maxItemBounds: { height: Math.max(l, f), width: l + 8 + h },
                maxLabelBounds: { width: h, height: f },
              },
              layout: {
                margin: {
                  vertical:
                    void 0 !== r.layout.item.vertical
                      ? r.layout.item.vertical
                      : 4,
                  horizontal:
                    void 0 !== r.layout.item.horizontal
                      ? r.layout.item.horizontal
                      : 4,
                },
                mode: r.layout.item.mode,
                size: r.layout.item.size,
                orientation:
                  "top" === a || "bottom" === a ? "horizontal" : "vertical",
                direction: r.layout.item.direction,
                scrollOffset: r.layout.item.scrollOffset,
              },
            };
          })(e, t.renderer)),
            (c = isNaN(n.layout.scrollOffset) ? c : n.layout.scrollOffset);
        },
        getItemsToRender: function (e) {
          (r = e.viewRect), (u = l.getContentOverflow(r));
          var o = l.extent();
          (c = Math.max(0, Math.min(c, u))), (a = k({}, r));
          var s = "horizontal" === l.orientation() ? "x" : "y";
          return (
            (a[s] -= c),
            (a["x" === s ? "width" : "height"] = o),
            (function (t, e, n) {
              for (
                var i = t.viewRect,
                  r = n.itemized,
                  a = n.create,
                  o = void 0 === a ? ph : a,
                  s = n.parallels,
                  c = n.createSymbol,
                  u = r.layout.direction,
                  l = r.globalMetrics,
                  h = r.items,
                  f = "horizontal" === r.layout.orientation,
                  d = 0,
                  g = [],
                  p = l.maxItemBounds.height,
                  y = l.maxItemBounds.width,
                  v = r.layout.margin.vertical + p,
                  m = r.layout.margin.horizontal + y,
                  x = e.x,
                  b = e.y,
                  _ = i.x - e.x,
                  w = 0;
                w < h.length;
                w++
              ) {
                var k = o({
                  y: b,
                  x: "rtl" === u ? i.x + _ + i.width - y - (x - e.x) : x,
                  item: h[w],
                  globalMetrics: l,
                  direction: u,
                  createSymbol: c,
                });
                if (
                  (((f && x >= i.x - y) || (!f && b >= i.y - p)) &&
                    g.push(k.item),
                  ++d >= s
                    ? ((d = 0),
                      f ? ((x += m), (b = e.y)) : ((b += v), (x = e.x)))
                    : f
                    ? (b += v)
                    : (x += m),
                  !f && b > i.y + i.height)
                )
                  break;
                if (f && x > i.x + i.width) break;
              }
              return g;
            })(e, a, { itemized: n, parallels: i, createSymbol: t.symbol })
          );
        },
        parallelize: function (t, e) {
          return (
            (i = (function (t, e, n) {
              var i = n.items.length,
                r = "horizontal" === n.layout.orientation ? "width" : "height",
                a = "width" === r ? "horizontal" : "vertical",
                o =
                  n.globalMetrics.maxItemBounds[r] * i +
                  (i - 1) * n.layout.margin[a],
                s = Math.ceil(o / t);
              if (null != e) {
                var c =
                    "horizontal" === n.layout.orientation ? "height" : "width",
                  u = "width" === c ? "horizontal" : "vertical",
                  l = n.layout.margin[u] || 4,
                  h = Math.floor(
                    (e + l) / (l + n.globalMetrics.maxItemBounds[c])
                  );
                s = Math.min(s, h);
              }
              var f = isNaN(n.layout.size) ? 1 : n.layout.size;
              return Math.max(1, Math.min(s, f));
            })(t, e, n)),
            i
          );
        },
        hasContentOverflow: function () {
          var t = "horizontal" === n.layout.orientation ? "width" : "height";
          return yh(n, i) > r[t];
        },
        getContentOverflow: function () {
          var t =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : r,
            e = "horizontal" === n.layout.orientation ? "width" : "height";
          return Math.max(0, yh(n, i) - t[e]);
        },
        getNextSize: function () {
          var t = "horizontal" === n.layout.orientation ? "width" : "height",
            e = "width" === t ? "horizontal" : "vertical";
          return n.globalMetrics.maxItemBounds[t] + n.layout.margin[e];
        },
        getPrevSize: function () {
          var t = "horizontal" === n.layout.orientation ? "width" : "height",
            e = "width" === t ? "horizontal" : "vertical";
          return n.globalMetrics.maxItemBounds[t] + n.layout.margin[e];
        },
        hasNext: function () {
          return "horizontal" === l.orientation()
            ? r.x + r.width < a.x + a.width
            : r.y + r.height < a.y + a.height;
        },
        hasPrev: function () {
          return "horizontal" === l.orientation() ? a.x < r.x : a.y < r.y;
        },
        next: function () {
          l.scroll(-l.getNextSize());
        },
        prev: function () {
          l.scroll(l.getPrevSize());
        },
        scroll: function (t) {
          var e = Math.max(0, Math.min(u, c - t));
          e !== c && ((c = e), s());
        },
        offset: function () {
          return c;
        },
        orientation: function () {
          return n.layout.orientation;
        },
        direction: function () {
          return n.layout.direction;
        },
        extent: function () {
          return yh(n, i);
        },
        spread: function () {
          return (function (t, e) {
            var n = e,
              i = "horizontal" === t.layout.orientation ? "height" : "width",
              r = "width" === i ? "horizontal" : "vertical";
            return (
              t.globalMetrics.maxItemBounds[i] * n +
              (n - 1) * t.layout.margin[r]
            );
          })(n, i);
        },
      };
    return l;
  }
  function mh(t) {
    return Object.keys(t)
      .filter(function (e) {
        return t[e];
      })
      .join(" ");
  }
  var xh = { up: "▲", right: "▶", down: "▼", left: "◀" };
  function bh(t, e) {
    var n = e.size,
      i = e.isActive,
      r = e.direction,
      a = e.nav,
      o = {},
      s = "",
      c = e.attrs;
    a &&
      a.button &&
      ("function" == typeof a.button.class
        ? (o = a.button.class({ direction: r }))
        : a.button.class && (o = a.button.class),
      "function" == typeof a.button.content &&
        (s = a.button.content(t, { direction: r })),
      void 0 !== a.button.tabIndex && (c.tabindex = a.button.tabIndex));
    var u = {
      width: "".concat(n, "px"),
      minWidth: "".concat(n, "px"),
      height: "".concat(n, "px"),
    };
    return (
      Object.keys(o).length || ((u.border = "0"), (u.background = "none")),
      (!i || (a && a.disabled)) && (c.disabled = "disabled"),
      t("button", k({ class: mh(o), style: u }, c), [
        s || t("span", { style: { pointerEvents: "none" } }, [xh[r]]),
      ])
    );
  }
  function _h(t) {
    var e,
      n = {
        itemize: function (t) {
          var n, i;
          (i = (n = t).dock),
            (e = {
              layout: {
                orientation:
                  "top" === i || "bottom" === i ? "vertical" : "horizontal",
              },
              navigation: n.navigation,
            });
        },
        render: function (i) {
          return (function (t, e, n, i) {
            var r = e.rect,
              a = e.itemRenderer;
            if (t && t.renderArgs) {
              t.size(r);
              var o = t.renderArgs[0],
                s = "vertical" === n.layout.orientation,
                c = "rtl" === a.direction(),
                u = a.hasNext(),
                l = a.hasPrev();
              if (l || u) {
                var h = s ? ["right", "left"] : ["down", "up"];
                c && s && h.reverse();
                var f = [
                  o(
                    "div",
                    {
                      style: {
                        position: "relative",
                        display: "flex",
                        "flex-direction": s ? "column" : "row",
                        "justify-content": "center",
                        height: "100%",
                        pointerEvents: "auto",
                      },
                      dir: c && !s ? "rtl" : "ltr",
                    },
                    [
                      bh(o, {
                        size: 32,
                        isActive: u,
                        direction: h[0],
                        attrs: {
                          "data-action": "next",
                          "data-component-key": i.settings.key,
                        },
                        nav: n.navigation,
                      }),
                      bh(o, {
                        size: 32,
                        isActive: l,
                        direction: h[1],
                        attrs: {
                          "data-action": "prev",
                          "data-component-key": i.settings.key,
                        },
                        nav: n.navigation,
                      }),
                    ]
                  ),
                ];
                t.render(f);
              } else t.render([]);
            }
          })(n.renderer, i, e, t);
        },
        extent: function () {
          return 32;
        },
        spread: function () {
          return 64;
        },
      };
    return n;
  }
  function wh(t) {
    var e,
      n = {
        itemize: function (n) {
          e = (function (t, e) {
            var n = t.resolved;
            if (!1 === n.title.item.show) return null;
            var i = k({}, n.title.item, { type: "text" });
            if (
              ("rtl" === n.layout.item.direction &&
                (i.anchor && "start" !== i.anchor
                  ? "end" === i.anchor && (i.anchor = "start")
                  : (i.anchor = "end")),
              void 0 === n.title.settings.text)
            ) {
              var r = e.scale.data().fields;
              i.text = r && r[0] ? r[0].title() : "";
            }
            return { displayObject: i, bounds: e.renderer.textBounds(i) };
          })(n, t);
        },
        render: function (t) {
          !(function (t, e, n) {
            var i = t.rect;
            if (e) {
              var r = [];
              if ((e.size(i), n)) {
                var a = { start: 0, end: i.width, middle: i.width / 2 };
                r.push(
                  k({}, n.displayObject, {
                    x: a[n.displayObject.anchor] || 0,
                    y: 0,
                    baseline: "text-before-edge",
                    title: n.displayObject.text,
                  })
                );
              }
              e.render(r);
            }
          })(t, n.renderer, e);
        },
        spread: function () {
          return e ? e.bounds.height : 0;
        },
        extent: function () {
          return e ? e.bounds.width : 0;
        },
      };
    return n;
  }
  function kh(t, e, n, i) {
    var r,
      a,
      o,
      s = i.itemRenderer,
      c = i.navigationRenderer,
      u = i.titleRenderer,
      l = i.isPreliminary,
      h = void 0 !== l && l,
      f = 0,
      d = {
        x: e.spacing,
        y: e.spacing,
        width: t.width - 2 * e.spacing,
        height: t.height - 2 * e.spacing,
      };
    if (
      ((r = { x: d.x, y: d.y, width: d.width, height: u.spread() }),
      "horizontal" === n)
    ) {
      r = { x: d.x, y: d.y, width: u.extent(), height: u.spread() };
      var g = d.width - r.width - (r.width ? e.spacing : 0),
        p = d.height;
      s.parallelize(g, h ? void 0 : p);
      var y = s.extent() > g ? c.extent() : 0,
        v = s.spread(),
        m = y ? c.spread() : 0;
      (o = {
        x:
          (a = {
            x: r.x + r.width + (r.width ? e.spacing : 0),
            y: d.y + Math.max(0, (m - v) / 2),
            width:
              d.width -
              y -
              r.width -
              (y ? e.spacing : 0) -
              (r.width ? e.spacing : 0),
            height: p,
          }).x +
          a.width +
          (y ? e.spacing : 0),
        y: d.y,
        width: y,
        height: d.height,
      }),
        (r.y = a.y),
        "rtl" === s.direction() &&
          ((o.x = d.x),
          (a.x = o.x + o.width + (o.width ? e.spacing : 0)),
          (r.x = a.x + a.width + (r.width ? e.spacing : 0))),
        (f = Math.max(r.height, m, s.spread()));
    } else {
      var x = d.height - r.height - (r.height ? e.spacing : 0),
        b = d.width;
      s.parallelize(x, h ? void 0 : b);
      var _ = s.extent() > x ? c.extent() : 0;
      (o = { x: d.x, y: d.y + d.height - _, width: d.width, height: _ }),
        (a = {
          x: d.x,
          y: r.y + r.height + (r.height ? e.spacing : 0),
          width: d.width,
          height:
            d.height -
            r.height -
            (r.height ? e.spacing : 0) -
            o.height -
            (o.height ? e.spacing : 0),
        }),
        (f = Math.max(u.extent(), _ ? c.spread() : 0, s.spread()));
    }
    return (
      (a = k({}, t, {
        x: t.x + a.x,
        y: t.y + a.y,
        width: a.width,
        height: a.height,
      })),
      (o.x += t.x),
      (o.y += t.y),
      (r.x += t.x),
      (r.y += t.y),
      {
        title: k({}, t, r),
        content: k({}, t, a),
        navigation: k({}, t, o),
        orientation: n,
        preferredSize: f,
      }
    );
  }
  function Mh(t) {
    (t.state.resolved = (function (t) {
      var e = t.scale.domain(),
        n = { items: [] },
        i = t.settings.layout.dock;
      if ("threshold-color" === t.scale.type) {
        var r = t.scale.data().fields[0],
          a = function (t) {
            return String(t);
          };
        t.settings.formatter
          ? (a = t.chart.formatter(t.settings.formatter))
          : r && (a = r.formatter());
        for (var o = 0; o < e.length - 1; o++) {
          var s = {
            value: e[o],
            label: "".concat(a(e[o]), " - < ").concat(a(e[o + 1])),
          };
          r && (s.source = { field: r.id() }), n.items.push(s);
        }
        "vertical" ==
          ("top" === i || "bottom" === i ? "horizontal" : "vertical") &&
          n.items.reverse();
      } else {
        var c = t.scale.labels ? t.scale.labels() : null;
        n.items = e.map(function (e, n) {
          var i = t.scale.datum ? k({}, t.scale.datum(e)) : { value: e };
          return (
            (i.value = e),
            t.scale.label
              ? (i.label = t.scale.label(e))
              : c && (i.label = c[n]),
            i
          );
        });
      }
      var u = t.resolver.resolve({
          data: { fields: t.scale.data().fields },
          defaults: k(!0, {}, gh.title, t.style.title),
          settings: t.settings.settings.title,
        }),
        l = t.resolver.resolve({
          data: { fields: t.scale.data().fields },
          defaults: gh.layout,
          settings: t.settings.settings.layout,
        }),
        h = t.resolver.resolve({
          data: n,
          defaults: k(!0, {}, gh.item.label, t.style.item.label),
          settings: (t.settings.settings.item || {}).label,
        }),
        f = k(!0, {}, (t.settings.settings.item || {}).shape);
      void 0 === f.fill &&
        t.settings.scale &&
        (f.fill = { scale: t.settings.scale });
      var d = t.resolver.resolve({
          data: n,
          defaults: k(!0, {}, gh.item.shape, t.style.item.shape),
          settings: f,
        }),
        g = t.resolver.resolve({
          data: n,
          defaults: k(!0, {}, { show: gh.item.show }),
          settings: { show: (t.settings.settings.item || {}).show },
        });
      function p(t, n) {
        var i = t.data.value,
          r = e[n + 1];
        t.data.value = [i, r];
      }
      return (
        "threshold-color" === t.scale.type &&
          ("vertical" ==
          ("top" === i || "bottom" === i ? "horizontal" : "vertical")
            ? (g.items.reverse().forEach(p), g.items.reverse())
            : g.items.forEach(p)),
        { title: u, labels: h, symbols: d, items: g, layout: l }
      );
    })(t)),
      t.titleRenderer.itemize({
        resolved: t.state.resolved,
        dock: t.settings.layout.dock || "center",
      }),
      t.itemRenderer.itemize({
        resolved: t.state.resolved,
        dock: t.settings.layout.dock || "center",
      }),
      t.navigationRenderer.itemize({
        resolved: t.state.resolved,
        dock: t.settings.layout.dock || "center",
        navigation: t.settings.settings.navigation,
      }),
      (t.state.display = { spacing: 8 });
  }
  function Rh(t) {
    var e = t.rect,
      n = t.settings,
      i = t.state,
      r = t.itemRenderer,
      a = t.navigationRenderer,
      o = t.titleRenderer,
      s = n.layout.dock,
      c = "top" === s || "bottom" === s ? "horizontal" : "vertical",
      u = kh(e, i.display, c, {
        itemRenderer: r,
        navigationRenderer: a,
        titleRenderer: o,
      });
    t.renderer.size(u.content);
    var l = r.getItemsToRender({ viewRect: k({}, u.content, { x: 0, y: 0 }) });
    return (
      a.render({ rect: u.navigation, itemRenderer: r }),
      o.render({ rect: u.title }),
      (t.state.views = { layout: u }),
      l
    );
  }
  var Sh = {
    require: [
      "chart",
      "settings",
      "renderer",
      "update",
      "resolver",
      "registries",
      "symbol",
    ],
    defaultSettings: {
      settings: {},
      style: { item: { label: "$label", shape: "$shape" }, title: "$title" },
    },
    mounted: function (t) {
      t &&
        t.parentNode &&
        (this.navigationRenderer.renderer.appendTo(t.parentNode),
        this.titleRenderer.renderer.appendTo(t.parentNode),
        t.parentNode.insertBefore(
          this.navigationRenderer.renderer.element(),
          t
        ),
        t.parentNode.insertBefore(this.titleRenderer.renderer.element(), t)),
        this.navigationRenderer.render({
          rect: this.state.views.layout.navigation,
          itemRenderer: this.itemRenderer,
        }),
        this.titleRenderer.render({ rect: this.state.views.layout.title });
    },
    beforeUnmount: function () {
      this.navigationRenderer.renderer.clear(),
        this.titleRenderer.renderer.clear();
    },
    on: {
      panstart: function () {
        this.state.interaction.started ||
          (this.itemRenderer.getContentOverflow() &&
            ((this.state.interaction.started = !0),
            (this.state.interaction.delta = 0)));
      },
      panmove: function (t) {
        if (this.state.interaction.started) {
          var e =
            "horizontal" === this.itemRenderer.orientation()
              ? ("rtl" === this.itemRenderer.direction() ? -1 : 1) * t.deltaX
              : t.deltaY;
          this.itemRenderer.scroll(e - this.state.interaction.delta),
            (this.state.interaction.delta = e);
        }
      },
      panend: function () {
        this.state.interaction.started = !1;
      },
      scroll: function (t) {
        this.itemRenderer.scroll(-t);
      },
      next: function () {
        this.itemRenderer.next();
      },
      prev: function () {
        this.itemRenderer.prev();
      },
    },
    created: function () {
      var t = this;
      (this.state = { interaction: {} }),
        (this.onScroll = function () {
          var e = Rh(t);
          t.update(e);
        }),
        (this.itemRenderer = vh(this, { onScroll: this.onScroll })),
        (this.navigationRenderer = _h(this)),
        (this.titleRenderer = wh(this)),
        (this.navigationRenderer.renderer = this.registries.renderer("dom")()),
        (this.titleRenderer.renderer = this.registries.renderer(
          this.settings.renderer
        )()),
        Mh(this);
    },
    preferredSize: function (t) {
      return (function (t, e) {
        var n = 0,
          i = t.settings.layout.dock || "center",
          r = "top" === i || "bottom" === i ? "horizontal" : "vertical",
          a = t.state.display,
          o = kh(e.inner, a, r, {
            itemRenderer: t.itemRenderer,
            navigationRenderer: t.navigationRenderer,
            titleRenderer: t.titleRenderer,
            isPreliminary: !0,
          });
        return (n += a.spacing), (n += o.preferredSize), n + a.spacing;
      })(this, t);
    },
    beforeUpdate: function () {
      Mh(this);
    },
    render: function () {
      return Rh(this);
    },
    beforeDestroy: function () {
      this.navigationRenderer.renderer.destroy(),
        this.titleRenderer.renderer.destroy();
    },
    additionalElements: function () {
      return [
        this.titleRenderer.renderer.element(),
        this.navigationRenderer.renderer.element(),
      ];
    },
    _DO_NOT_USE_getInfo: function () {
      return { offset: this.itemRenderer.offset() };
    },
  };
  function Nh(t, e) {
    var n = 0,
      i = t.state.isVertical ? "justify" : "align",
      r = t.state.isVertical ? "y" : "x",
      a = t.state.isVertical ? "height" : "width",
      o = t.state.isVertical ? "requiredHeight" : "requiredWidth";
    (n = t.state.rect[a] - t.state.legend.length() - t.state.title[o]()),
      (n *= Math.min(1, Math.max(t.stgns[i], 0))),
      (e[r] += n);
  }
  function zh(t, e, n) {
    var i = n[t];
    return "object" === N(i)
      ? -1 !== i.valid.indexOf(e)
        ? e
        : i.default
      : n.default;
  }
  function Ch(t) {
    return zh(t.layout.dock, t.settings.title.anchor, {
      left: { valid: ["top"], default: "top" },
      right: { valid: ["top"], default: "top" },
      top: { valid: ["left", "right"], default: "left" },
      bottom: { valid: ["left", "right"], default: "left" },
      default: "top",
    });
  }
  function Ah(t, e) {
    var n = { x: 0, y: 0, width: 0, height: 0 },
      i = t.stgns.padding;
    return (
      (n.x = i.left),
      (n.y = i.top),
      (n.width = e.width - i.left - i.right),
      (n.height = e.height - i.top - i.bottom),
      n
    );
  }
  function Eh(t) {
    var e,
      n =
        "top" !== t.settings.layout.dock && "bottom" !== t.settings.layout.dock,
      i = t.stgns.title,
      r = t.chart.scale(t.stgns.fill),
      a = t.chart.scale(t.stgns.major),
      o = (function (t, e) {
        var n = e.domain(),
          i = n,
          r = t.stgns.tick.label;
        return (
          !r && t.formatter
            ? (r = t.formatter)
            : !r && e.data().fields && (r = e.data().fields[0].formatter()),
          "function" == typeof r && (i = n.map(r).map(String)),
          n.map(function (n, r) {
            var a = i[r];
            return {
              value: n,
              label: a,
              pos: e.norm(parseFloat(n, 10)),
              textMetrics: t.renderer.measureText({
                text: a,
                fontSize: t.stgns.tick.fontSize,
                fontFamily: t.stgns.tick.fontFamily,
              }),
            };
          })
        );
      })(t, a),
      s = zh((e = t.settings).layout.dock, e.settings.tick.anchor, {
        left: { valid: ["left", "right"], default: "left" },
        right: { valid: ["left", "right"], default: "right" },
        top: { valid: ["top", "bottom"], default: "top" },
        bottom: { valid: ["top", "bottom"], default: "bottom" },
        default: "right",
      });
    if (void 0 === i.text) {
      var c = a.data().fields;
      i.text = c && c[0] ? c[0].title() : "";
    }
    var u = t.renderer.measureText({
        text: i.text,
        fontSize: i.fontSize,
        fontFamily: i.fontFamily,
      }),
      l = t.renderer.textBounds({
        text: i.text,
        fontSize: i.fontSize,
        fontFamily: i.fontFamily,
        maxLines: i.maxLines,
        maxWidth: i.maxLengthPx,
        wordBreak: i.wordBreak,
        hyphens: i.hyphens,
        lineHeight: i.lineHeight,
      }),
      h = {
        isVertical: n,
        nodes: [],
        title: {
          anchor: Ch(t.settings),
          textMetrics: u,
          textBounds: l,
          requiredWidth: function () {
            if (!i.show) return 0;
            var t = l.width,
              e = i.maxLengthPx;
            return (
              n || ((t += i.padding), (e += i.padding)),
              Math.min(t, e, h.rect.width)
            );
          },
          requiredHeight: function () {
            if (!i.show) return 0;
            var t = l.height;
            return n && (t += i.padding), Math.min(t, h.rect.height);
          },
        },
        ticks: {
          values: o,
          anchor: s,
          length: Math.min(
            Math.max.apply(
              Math,
              $(
                o.map(function (t) {
                  return t.textMetrics.width;
                })
              )
            ),
            t.stgns.tick.maxLengthPx
          ),
          requiredHeight: function () {
            return "top" === s
              ? Math.max.apply(
                  Math,
                  $(
                    h.ticks.values.map(function (t) {
                      return t.textMetrics.height;
                    })
                  )
                ) + t.stgns.tick.padding
              : 0;
          },
          height: Math.max.apply(
            Math,
            $(
              o.map(function (t) {
                return t.textMetrics.height;
              })
            )
          ),
        },
        legend: {
          fillScale: r,
          majorScale: a,
          length: function () {
            var e = n ? "height" : "width",
              i = n ? "requiredHeight" : "requiredWidth",
              r =
                Math.min(h.rect[e], h.rect[e] * t.stgns.length) - h.title[i]();
            return Math.max(0, Math.min(r, t.stgns.maxLengthPx));
          },
        },
      };
    return h;
  }
  var Oh = {
    require: ["chart", "settings", "renderer"],
    defaultSettings: {
      layout: { displayOrder: 0, dock: "right" },
      settings: {
        size: 15,
        length: 0.5,
        maxLengthPx: 250,
        align: 0.5,
        justify: 0,
        padding: { left: 5, right: 5, top: 5, bottom: 5 },
        tick: {
          label: null,
          fill: "#595959",
          fontSize: "12px",
          fontFamily: "Arial",
          maxLengthPx: 100,
          anchor: null,
          padding: 5,
        },
        title: {
          show: !0,
          text: void 0,
          fill: "#595959",
          fontSize: "12px",
          fontFamily: "Arial",
          maxLengthPx: 100,
          padding: 5,
          maxLines: 2,
          wordBreak: "none",
          lineHeight: 1.2,
          hyphens: "auto",
          anchor: null,
        },
      },
    },
    preferredSize: function (t) {
      var e = this.state;
      e.rect = Ah(this, t.inner);
      var n = this.stgns.size,
        i = e.isVertical
          ? this.stgns.padding.left + this.stgns.padding.right
          : this.stgns.padding.top + this.stgns.padding.bottom;
      n += i;
      var r = Math.max(t.inner.width, t.inner.height);
      if ("left" === e.ticks.anchor || "right" === e.ticks.anchor) {
        var a = e.ticks.values.reduce(function (t, e) {
          return t + e.textMetrics.height;
        }, 0);
        if (a > this.state.legend.length()) return r;
        n += e.ticks.length;
      } else {
        if (e.ticks.length > this.state.legend.length()) return r;
        n += Math.max.apply(
          Math,
          $(
            e.ticks.values.map(function (t) {
              return t.textMetrics.height;
            })
          )
        );
      }
      return (
        (n += this.stgns.tick.padding),
        this.stgns.title.show &&
          (n =
            "left" === e.title.anchor || "right" === e.title.anchor
              ? Math.max(e.title.textBounds.height + i, n)
              : Math.max(n, e.title.requiredWidth() + i)),
        (this.state.preferredSize = n),
        n
      );
    },
    created: function () {
      (this.stgns = this.settings.settings), (this.state = Eh(this));
    },
    beforeUpdate: function (t) {
      (this.stgns = t.settings.settings), (this.state = Eh(this));
    },
    beforeRender: function (t) {
      if (
        ((this.state.nodes = []),
        (this.state.rect = Ah(this, t.size)),
        this.stgns.title.show)
      ) {
        var e = (function (t) {
          var e = t.state,
            n = t.stgns,
            i = "left" === e.ticks.anchor,
            r = "top" === e.ticks.anchor,
            a = e.rect.x,
            o = e.rect.y,
            s = "start";
          "left" === e.title.anchor
            ? ((a += e.title.requiredWidth() - n.title.padding),
              (o += e.title.textMetrics.height),
              (o += r ? e.rect.height - e.title.textBounds.height : 0),
              (s = "end"))
            : "right" === e.title.anchor
            ? ((a += e.legend.length()),
              (a += n.title.padding),
              (o += e.title.textMetrics.height),
              (o += r ? e.rect.height - e.title.textBounds.height : 0))
            : "top" === e.title.anchor &&
              ((a += i ? e.rect.width : 0),
              (o += e.title.textMetrics.height),
              (s = i ? "end" : "start"));
          var c = {
            tag: "legend-title",
            type: "text",
            x: a,
            y: Math.min(o, e.rect.y + e.rect.height),
            text: n.title.text,
            fill: n.title.fill,
            fontSize: n.title.fontSize,
            fontFamily: n.title.fontFamily,
            maxWidth: n.title.maxLengthPx,
            maxLines: n.title.maxLines,
            wordBreak: n.title.wordBreak,
            hyphens: n.title.hyphens,
            lineHeight: n.title.lineHeight,
            anchor: s,
            title: n.title.text,
          };
          return Nh(t, c), c;
        })(this);
        this.state.nodes.push(e);
      }
      var n,
        i,
        r,
        a,
        o =
          ((i = (n = this).state.legend.fillScale),
          (r = n.state.legend.majorScale),
          (a = i.domain().map(function (t) {
            return {
              type: "stop",
              color: i(t),
              offset: Math.min(1, Math.max(0, r.norm(t))),
            };
          })),
          a.sort(function (t, e) {
            return t.offset - e.offset;
          })),
        s = (function (t, e) {
          var n = t.state,
            i = t.stgns,
            r = n.rect,
            a = r.x,
            o = r.y,
            s = n.isVertical ? i.size : n.legend.length(),
            c = n.isVertical ? n.legend.length() : i.size;
          "left" === n.ticks.anchor
            ? (a += n.rect.width - i.size)
            : "top" === n.ticks.anchor && (o += n.rect.height - i.size),
            "top" === n.title.anchor
              ? (o += n.title.requiredHeight())
              : "left" === n.title.anchor && (a += n.title.requiredWidth());
          var u = {
            type: "rect",
            x: a,
            y: o,
            width: s,
            height: c,
            fill: {
              type: "gradient",
              stops: e,
              degree: n.isVertical ? 90 : 180,
            },
          };
          return Nh(t, u), u;
        })(this, o),
        c = (function (t, e) {
          var n = t.state,
            i = t.stgns,
            r = "start",
            a = {
              type: "rect",
              x: e.x,
              y: e.y,
              width: n.isVertical ? 0 : e.width,
              height: n.isVertical ? e.height : 0,
              fill: "transparent",
            },
            o = n.ticks.values.map(function (o) {
              var s = 0,
                c = 0,
                u = 0,
                l = "alphabetical";
              return (
                n.isVertical
                  ? ((c = e.y + e.height * o.pos),
                    (l = 0 === o.pos ? "text-before-edge" : "text-after-edge"))
                  : (s = e.x + e.width * o.pos),
                "right" === n.ticks.anchor
                  ? ((s = e.x + i.size + i.tick.padding), (a.x = e.x + e.width))
                  : "left" === n.ticks.anchor
                  ? ((s = e.x - i.tick.padding), (r = "end"))
                  : "top" === n.ticks.anchor
                  ? ((c = e.y - i.tick.padding),
                    (u -= 0.25 * o.textMetrics.height),
                    (r = 0 === o.pos ? "start" : "end"))
                  : "bottom" === n.ticks.anchor &&
                    ((c = e.y + e.height + i.tick.padding),
                    (u = 0.8 * o.textMetrics.height),
                    (r = 0 === o.pos ? "start" : "end"),
                    (a.y = e.y + e.height)),
                {
                  type: "text",
                  x: s,
                  y: c,
                  dx: 0,
                  dy: u,
                  text: o.label,
                  fontSize: i.tick.fontSize,
                  fontFamily: i.tick.fontFamily,
                  fill: i.tick.fill,
                  maxWidth: n.isVertical
                    ? i.tick.maxLengthPx
                    : Math.min(i.tick.maxLengthPx, n.legend.length() / 2),
                  anchor: r,
                  textBoundsFn: t.renderer.textBounds,
                  title: o.label,
                  baseline: l,
                }
              );
            });
          return {
            type: "container",
            id: "legend-seq-ticks",
            children: [].concat($(o), [a]),
          };
        })(this, s),
        u = { id: "legend-seq-target", type: "container", children: [s, c] };
      this.state.nodes.push(u);
    },
    render: function () {
      return this.state.nodes;
    },
  };
  var Th = {
      step: function (t) {
        return new te(t, 0.5);
      },
      stepAfter: function (t) {
        return new te(t, 1);
      },
      stepBefore: function (t) {
        return new te(t, 0);
      },
      linear: Et,
      basis: function (t) {
        return new Lt(t);
      },
      cardinal: Vt.tension(0),
      catmullRom: Ht,
      monotonex: function (t) {
        return new Gt(t);
      },
      monotoney: function (t) {
        return new Zt(t);
      },
      natural: function (t) {
        return new Kt(t);
      },
    },
    jh = {
      coordinates: { minor: 0.5, major: 0.5, layerId: 0, defined: !0 },
      connect: !1,
      orientation: "horizontal",
      layers: {
        curve: "linear",
        show: !0,
        sort: void 0,
        line: {
          stroke: "#ccc",
          strokeWidth: 1,
          strokeLinejoin: "miter",
          strokeDasharray: void 0,
          opacity: 1,
          show: !0,
          showMinor0: !0,
        },
        area: { fill: "#ccc", opacity: 0.8, show: !0 },
      },
    };
  function Ph(t, e) {
    var n = e.generator,
      i = e.item,
      r = e.data,
      a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "",
      o = n(t),
      s = {
        type: "path",
        d: o,
        opacity: i.opacity,
        stroke: i.stroke,
        strokeWidth: i.strokeWidth,
        strokeLinejoin: i.strokeLinejoin,
        fill: a || i.fill,
        data: r,
      };
    return i.strokeDasharray && (s.strokeDasharray = i.strokeDasharray), s;
  }
  function Bh(t) {
    var e = (function (t) {
        for (
          var e = t.data,
            n = t.stngs,
            i = t.rect,
            r = t.resolver,
            a = t.style,
            o = t.domain,
            s = i.width,
            c = i.height,
            u = r.resolve({
              data: e,
              defaults: jh.coordinates,
              settings: n.coordinates || {},
              scaled: {
                major: "vertical" === n.orientation ? c : s,
                minor: "vertical" === n.orientation ? s : c,
              },
            }),
            l =
              !n.connect &&
              o.length > 2 &&
              ("function" == typeof n.coordinates.layerId ||
                "object" === N(n.coordinates.layerId)),
            h = {},
            f = 0,
            d = 0;
          d < u.items.length;
          d++
        ) {
          var g = u.items[d],
            p = g.layerId;
          if (l) {
            var y = h[p] ? h[p].items[h[p].items.length - 1] : null,
              v = y
                ? o.indexOf(y.data.major ? y.data.major.value : y.data.value)
                : null;
            y &&
              o.indexOf(g.data.major ? g.data.major.value : g.data.value) -
                1 !==
                v &&
              h[p].items.push({ dummy: !0 });
          }
          (h[p] = h[p] || {
            order: f++,
            id: p,
            items: [],
            dataItems: [],
            consumableData: {},
          }),
            h[p].dataItems.push(g.data),
            h[p].items.push(g);
        }
        var m = Object.keys(h).map(function (t) {
            return (
              (h[t].consumableData = S(
                { points: h[t].dataItems },
                h[t].dataItems[0]
              )),
              h[t]
            );
          }),
          x = {
            items: m.map(function (t) {
              return t.consumableData;
            }),
          },
          b = n.layers || {};
        return {
          coordinates: u,
          metaLayers: m,
          layers: r.resolve({
            data: x,
            defaults: { curve: jh.layers.curve, show: jh.layers.show },
            settings: { curve: b.curve, show: b.show },
          }),
          lines: r.resolve({
            data: x,
            defaults: k({}, jh.layers.line, a.line),
            settings: b.line,
          }),
          areas: r.resolve({
            data: x,
            defaults: k({}, jh.layers.area, a.area),
            settings: b.area,
          }),
        };
      })(t),
      n = e.metaLayers,
      i = e.coordinates,
      r = e.layers,
      a = e.lines,
      o = e.areas,
      s = [];
    return (
      n.forEach(function (e, n) {
        var c = r.items[n];
        if (!1 !== c.show) {
          (c.datum = c.data), (c.data = []), (c.id = e.id);
          for (var u, l, h = [], f = [], d = 0; d < e.items.length; d++) {
            if (((l = (u = e.items[d]).data), !u.dummy)) {
              if (isNaN(u.major)) continue;
              t.missingMinor0 &&
                (u.minor0 = i.settings.minor.scale
                  ? i.settings.minor.scale(l.minor0 ? l.minor0.value : 0)
                  : 0),
                isNaN(u.minor) || h.push(u.minor),
                c.data.push(u.data);
            }
            f.push(u);
          }
          var g = h.sort(function (t, e) {
            return t - e;
          })[Math.floor((h.length - 1) / 2)];
          s.push({
            layerObj: c,
            lineObj: a.items[n],
            areaObj: o.items[n],
            median: g,
            points: f,
            consumableData: e.consumableData,
          });
        }
      }),
      s
    );
  }
  var Dh = {
    require: ["chart", "resolver"],
    defaultSettings: { style: { area: "$shape", line: "$shape-outline" } },
    created: function () {},
    render: function (t) {
      var e = t.data,
        n = this.rect,
        i = n.width,
        r = n.height;
      this.stngs = this.settings.settings || {};
      var a =
          !this.stngs.coordinates || void 0 === this.stngs.coordinates.minor0,
        o = Bh({
          data: e,
          stngs: this.stngs,
          rect: this.rect,
          resolver: this.resolver,
          style: this.style,
          missingMinor0: a,
          domain:
            this.stngs.coordinates &&
            this.stngs.coordinates.major &&
            this.stngs.coordinates.major.scale
              ? this.chart.scale(this.stngs.coordinates.major.scale).domain()
              : [],
        });
      if (this.stngs.layers && this.stngs.layers.sort) {
        var s = o
          .map(function (t) {
            return { id: t.layerObj.id, data: t.layerObj.data };
          })
          .sort(this.stngs.layers.sort)
          .map(function (t) {
            return t.id;
          });
        o.sort(function (t, e) {
          return s.indexOf(t.layerObj.id) - s.indexOf(e.layerObj.id);
        });
      } else
        o.sort(function (t, e) {
          return t.median - e.median;
        });
      return (function (t, e) {
        var n = e.width,
          i = e.height,
          r = e.missingMinor0,
          a = e.stngs,
          o = [],
          s = a.layers || {};
        return (
          t.forEach(function (t) {
            var e,
              c,
              u = t.lineObj,
              l = t.layerObj,
              h = t.areaObj,
              f = t.points,
              d = jt(),
              g = a.coordinates ? a.coordinates.defined : null,
              p = { size: i, p: "y" },
              y = { size: n, p: "x" };
            if ("vertical" === a.orientation) {
              var v = k(!0, {}, y);
              (y = k(!0, {}, p)), (p = k(!0, {}, v));
            }
            d[y.p](function (t) {
              return t.major * y.size;
            })
              ["".concat(p.p, "1")](function (t) {
                return t.minor * p.size;
              })
              ["".concat(p.p, "0")](function (t) {
                return t.minor0 * p.size;
              })
              .curve(
                Th["monotone" === l.curve ? "monotone".concat(y.p) : l.curve]
              ),
              g
                ? d.defined(function (t) {
                    return (
                      !t.dummy &&
                      "number" == typeof t.minor &&
                      !isNaN(t.minor) &&
                      t.defined
                    );
                  })
                : d.defined(function (t) {
                    return (
                      !t.dummy && "number" == typeof t.minor && !isNaN(t.minor)
                    );
                  });
            var m = a.connect ? f.filter(d.defined()) : f;
            (e = d["line".concat(p.p.toUpperCase(), "1")]()),
              (c = d["line".concat(p.p.toUpperCase(), "0")]()),
              s.area &&
                !1 !== h.show &&
                o.push(
                  Ph(m, { data: t.consumableData, item: h, generator: d })
                ),
              u &&
                !1 !== u.show &&
                (o.push(
                  Ph(
                    m,
                    { data: t.consumableData, item: u, generator: e },
                    "none"
                  )
                ),
                !r &&
                  s.area &&
                  !1 !== h.show &&
                  !1 !== u.showMinor0 &&
                  o.push(
                    Ph(
                      m,
                      { data: t.consumableData, item: u, generator: c },
                      "none"
                    )
                  ));
          }),
          o
        );
      })(o, { width: i, height: r, missingMinor0: a, stngs: this.stngs });
    },
  };
  function Fh(t, e) {
    var n,
      i,
      r = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2];
    "object" === N(e.center)
      ? ((n = e.center.x), (i = e.center.y))
      : ((n = e.clientX), (i = e.clientY));
    var a = n - t.state.boundingRect.left,
      o = i - t.state.boundingRect.top;
    return {
      x: r ? Math.max(0, Math.min(a, t.rect.width)) : a,
      y: r ? Math.max(0, Math.min(o, t.rect.height)) : o,
    };
  }
  function Lh(t, e) {
    return { x: e.x + t.rect.x, y: e.y + t.rect.y };
  }
  function Ih(t, e) {
    var n = Math.min(t.x, e.x),
      i = Math.min(t.y, e.y);
    return {
      x: n,
      y: i,
      width: Math.max(t.x, e.x) - n,
      height: Math.max(t.y, e.y) - i,
    };
  }
  var Wh = {
    require: ["chart", "renderer"],
    defaultSettings: {
      layout: { displayOrder: 99 },
      settings: { brush: { components: [] } },
      style: { area: "$selection-area-target" },
    },
    on: {
      areaStart: function (t) {
        this.start(t);
      },
      areaMove: function (t) {
        this.move(t);
      },
      areaEnd: function (t) {
        this.end(t);
      },
      areaCancel: function () {
        this.cancel();
      },
    },
    created: function () {
      this.state = { start: { x: 0, y: 0 }, end: { x: 0, y: 0 }, active: !1 };
    },
    preferredSize: function () {
      return 0;
    },
    render: function () {},
    start: function (t) {
      this.state.boundingRect = this.renderer.element().getBoundingClientRect();
      var e = Fh(this, t, !1);
      fs({ x: 0, y: 0, width: this.rect.width, height: this.rect.height }, e) &&
        ((this.state.brushConfig = this.settings.settings.brush.components.map(
          function (t) {
            return {
              key: t.key,
              contexts: t.contexts,
              data: t.data,
              action: t.action || "set",
            };
          }
        )),
        (this.state.start = Fh(this, t)),
        (this.state.active = !0));
    },
    move: function (t) {
      var e;
      this.state.active &&
        ((this.state.end = Fh(this, t)),
        (function (t) {
          if (t.state.active) {
            var e = Lh(t, t.state.start),
              n = Lh(t, t.state.end),
              i = t.chart.shapesAt(Ih(e, n), {
                components: t.state.brushConfig,
              });
            t.chart.brushFromShapes(i, { components: t.state.brushConfig });
          }
        })(this),
        (e = this).renderer.render([
          k({ type: "rect" }, Ih(e.state.start, e.state.end), e.style.area),
        ]));
    },
    end: function () {
      this.state.active &&
        ((this.state = {
          start: { x: 0, y: 0 },
          end: { x: 0, y: 0 },
          active: !1,
        }),
        this.renderer.render([]));
    },
    cancel: function () {
      var t, e;
      this.state.active &&
        ((t = this.state),
        (e = this.chart),
        t.brushConfig.forEach(function (t) {
          Array.isArray(t.contexts) &&
            t.contexts.forEach(function (t) {
              e.brush(t).end();
            });
        }),
        (this.state = {
          start: { x: 0, y: 0 },
          end: { x: 0, y: 0 },
          active: !1,
        }),
        this.renderer.render([]));
    },
  };
  function Vh(t, e, n) {
    var i = n.renderer,
      r = n.style,
      a = n.props,
      o = n.h,
      s = (function (t, e) {
        return {
          tooltip:
            "function" == typeof t.tooltipClass
              ? t.tooltipClass({ dock: e.dock })
              : t.tooltipClass,
          content:
            "function" == typeof t.contentClass
              ? t.contentClass({ dock: e.dock })
              : t.contentClass,
          arrow:
            "function" == typeof t.arrowClass
              ? t.arrowClass({ dock: e.dock })
              : t.arrowClass,
        };
      })(a, e),
      c = (function (t, e, n, i) {
        return i.content({ h: t, style: n, data: e });
      })(o, t, r, a),
      u = o(
        "div",
        {
          dir: a.direction,
          class: mh(k({ "pic-tooltip": !0 }, s.tooltip)),
          style: k(
            { position: "relative", display: "inline-block" },
            e.computedTooltipStyle
          ),
        },
        o(
          "div",
          {
            style: r.content,
            class: mh(k({ "pic-tooltip-content": !0 }, s.content)),
          },
          c
        ),
        o("div", {
          class: mh(k({ "pic-tooltip-arrow": !0 }, s.arrow)),
          style: k(
            {},
            r.arrow,
            r["arrow-".concat(e.dock)],
            e.computedArrowStyle
          ),
        })
      );
    return i.render(u), i.element().children[0];
  }
  function $h(t) {
    var e = t.defaultDuration,
      n = t.defaultDelay,
      i = null,
      r = null,
      a = !1,
      o = (function () {
        var t = function () {},
          e = {
            pending: [],
            debounced: [],
            active: [],
            cancelled: [],
            rejected: [],
            fulfilled: [],
          };
        return (
          (t.set = function (t) {
            e[t].forEach(function (e) {
              return e(t);
            });
          }),
          (t.on = function (t, n) {
            Array.isArray(t)
              ? t.forEach(function (t) {
                  return e[t].push(n);
                })
              : e[t].push(n);
          }),
          (t.destroy = function () {
            Object.keys(e).forEach(function (t) {
              e[t].length = 0;
            });
          }),
          t
        );
      })(),
      s = function () {},
      c = function () {
        (i = null), (r = null), (a = !1), o.set("fulfilled");
      };
    return (
      (s.invoke = function (t) {
        var s =
            arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : e,
          u =
            arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : n;
        i && (clearTimeout(i), o.set("debounced")),
          o.set("pending"),
          (i = setTimeout(function () {
            t(), (a = !0), (i = null), o.set("active");
          }, u)),
          s > 0 &&
            (r && clearTimeout(r), (r = setTimeout(c, s + Math.max(u, 0))));
      }),
      (s.clear = function () {
        a ? o.set("cancelled") : i && (clearTimeout(i), o.set("rejected")),
          r && clearTimeout(r),
          (i = null),
          (r = null),
          (a = !1);
      }),
      (s.on = function (t, e) {
        o.on(t, e);
      }),
      (s.destroy = function () {
        s.clear(), o.destroy();
      }),
      s
    );
  }
  function Hh() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
    return {
      left: "translate(-100%,-50%) translateX(".concat(-t, "px)"),
      right: "translate(".concat(t, "px, -50%)"),
      top: "translate(-50%, -100%) translateY(".concat(-t, "px)"),
      bottom: "translate(-50%, ".concat(t, "px)"),
    };
  }
  function qh(t, e) {
    var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0;
    return {
      left: { x: -t - n, y: -e / 2 },
      right: { x: n, y: -e / 2 },
      top: { x: -t / 2, y: -e - n },
      bottom: { x: -t / 2, y: n },
    };
  }
  function Uh(t) {
    return {
      left: {
        left: "100%",
        top: "calc(50% - ".concat(t, "px)"),
        borderWidth: "".concat(t, "px"),
      },
      right: {
        left: "".concat(2 * -t, "px"),
        top: "calc(50% - ".concat(t, "px)"),
        borderWidth: "".concat(t, "px"),
      },
      top: {
        left: "calc(50% - ".concat(t, "px)"),
        top: "100%",
        borderWidth: "".concat(t, "px"),
      },
      bottom: {
        left: "calc(50% - ".concat(t, "px)"),
        top: "".concat(2 * -t, "px"),
        borderWidth: "".concat(t, "px"),
      },
    };
  }
  function Yh(t, e, n, i, r, a) {
    var o = e + a.x,
      s = n + a.y,
      c = r;
    return !(o < 0 || s < 0) && !(o + i > t.width || s + c > t.height);
  }
  function Xh(t) {
    var e = t.area,
      n = t.vx,
      i = t.vy,
      r = t.width,
      a = t.height,
      o = t.offset,
      s = n + o.x,
      c = i + o.y,
      u = r,
      l = a,
      h = s < 0 ? -s : 0,
      f = c < 0 ? -c : 0;
    return {
      x: (h += s + u > e.width ? -(s + u - e.width) : 0),
      y: (f += c + l > e.height ? -(c + l - e.height) : 0),
    };
  }
  function Gh(t) {
    var e = t.options,
      n = t.pointer,
      i = t.width,
      r = t.height,
      a = t.dockOrder,
      o = t.x,
      s = t.y,
      c = n.targetBounds,
      u = Hh(e.offset),
      l = u[e.dock];
    if (l)
      return {
        computedTooltipStyle: {
          left: "".concat(o, "px"),
          top: "".concat(s, "px"),
          transform: l,
        },
        computedArrowStyle: Uh(e.offset)[e.dock],
        dock: e.dock,
      };
    for (
      var h = {
          width: "target" === e.area ? c.width : window.innerWidth,
          height: "target" === e.area ? c.height : window.innerHeight,
        },
        f = qh(i, r, e.offset),
        d = [],
        g = "target" === e.area ? o : c.left + o,
        p = "target" === e.area ? s : c.top + s,
        y = 0;
      y < a.length;
      y += 1
    ) {
      var v = a[y],
        m = Xh({ area: h, vx: g, vy: p, width: i, height: r, offset: f[v] }),
        x = {
          left: "".concat(o, "px"),
          top: "".concat(s, "px"),
          transform: u[v],
        },
        b = Uh(e.offset)[v];
      0 !== m.x &&
        ((x.width = "".concat(i - 20 - Math.abs(m.x), "px")),
        ("top" !== v && "bottom" !== v) ||
          ((x.left = "".concat(o + m.x, "px")),
          (b.left = "calc(50% "
            .concat(m.x > 0 ? "-" : "+", " ")
            .concat(Math.abs(m.x), "px)"))));
      var _ = {
        computedTooltipStyle: x,
        computedArrowStyle: b,
        dock: v,
        rect: { width: i, height: r },
      };
      if (0 === m.x && 0 === m.y) return _;
      (_.offset = m), d.push(_);
    }
    return (
      d.sort(function (t, e) {
        return Math.abs(t.offset.x) - Math.abs(e.offset.x);
      }),
      d[0]
    );
  }
  function Zh(t, e, n) {
    var i = t.key
      ? n.component(t.key)
      : n.componentsFromPoint({ x: e.clientX, y: e.clientY })[0];
    if (!i)
      return { x: 0, y: 0, width: 0, height: 0, scaleRatio: { x: 1, y: 1 } };
    var r = i.rect;
    return k({ scaleRatio: r.scaleRatio }, r.computedInner);
  }
  var Jh,
    Kh = {
      bounds: function (t) {
        var e = t.resources,
          n = t.nodes,
          i = t.pointer,
          r = t.width,
          a = t.height,
          o = t.options,
          s = i.targetBounds,
          c = e.getNodeBoundsRelativeToTarget(n[0]),
          u = c.x,
          l = c.y,
          h = c.width,
          f = c.height,
          d = {
            left: { x: u, y: l + f / 2 },
            right: { x: u + h, y: l + f / 2 },
            top: { x: u + h / 2, y: l },
            bottom: { x: u + h / 2, y: l + f },
          },
          g = Hh(o.offset),
          p = g[o.dock];
        if (p)
          return {
            computedTooltipStyle: {
              left: "".concat(d[o.dock].x, "px"),
              top: "".concat(d[o.dock].y, "px"),
              transform: p,
            },
            computedArrowStyle: Uh(o.offset)[o.dock],
            dock: o.dock,
          };
        for (
          var y = {
              width: "target" === o.area ? s.width : window.innerWidth,
              height: "target" === o.area ? s.height : window.innerHeight,
            },
            v = qh(r, a, o.offset),
            m = ["top", "left", "right", "bottom"],
            x = 0;
          x < m.length;
          x += 1
        ) {
          var b = m[x];
          if (
            Yh(
              y,
              "target" === o.area ? d[b].x : s.left + d[b].x,
              "target" === o.area ? d[b].y : s.top + d[b].y,
              r,
              a,
              v[b]
            )
          )
            return {
              computedTooltipStyle: {
                left: "".concat(d[b].x, "px"),
                top: "".concat(d[b].y, "px"),
                transform: g[b],
              },
              computedArrowStyle: Uh(o.offset)[b],
              dock: b,
            };
        }
        return {
          computedTooltipStyle: {
            left: "".concat(d.top.x, "px"),
            top: "".concat(d.top.y, "px"),
            transform: g.top,
          },
          computedArrowStyle: Uh(o.offset).top,
          dock: "top",
        };
      },
      pointer: function (t) {
        var e = t.options,
          n = t.pointer,
          i = t.width,
          r = t.height;
        return Gh({
          x: n.x,
          y: n.y,
          pointer: n,
          width: i,
          height: r,
          options: e,
          dockOrder: ["top", "left", "right", "bottom"],
        });
      },
      slice: function (t) {
        var e = t.options,
          n = t.pointer,
          i = t.width,
          r = t.height,
          a = t.nodes,
          o = t.resources,
          s = a[0],
          c = n.dx,
          u = n.dy,
          l = o.getComponentBoundsFromNode(s),
          h = c + l.x + l.width / 2,
          f = u + l.y + l.height / 2,
          d = s.desc.slice,
          g = d.start,
          p = d.end,
          y = d.outerRadius,
          v = (g + p) / 2 - Math.PI / 2,
          m = 2 * Math.PI,
          x = ((v % m) + m) % m,
          b = ["top", "left", "right", "bottom"];
        return (
          "auto" === e.dock &&
            (b =
              x <= Math.PI / 4 || x >= (7 * Math.PI) / 4
                ? ["right", "top", "bottom", "left"]
                : x <= (3 * Math.PI) / 4
                ? ["bottom", "left", "right", "top"]
                : x <= (5 * Math.PI) / 4
                ? ["left", "top", "bottom", "right"]
                : ["top", "left", "right", "bottom"]),
          Gh({
            x: h + y * l.scaleRatio.x * Math.cos(x),
            y: f + y * l.scaleRatio.y * Math.sin(x),
            pointer: n,
            width: i,
            height: r,
            options: e,
            dockOrder: b,
          })
        );
      },
    };
  function Qh(t, e) {
    var n = t.width,
      i = t.height,
      r = e.chart,
      a = e.state,
      o = e.props,
      s = {
        resources: {
          formatter: r.formatter,
          scale: r.scale,
          component: r.component,
          getComponentBoundsFromNode: function (t) {
            return Zh(t, a.pointer, r);
          },
          getNodeBoundsRelativeToTarget: function (t) {
            return (function (t, e, n) {
              var i = Zh(t, e, n),
                r = t.bounds;
              return {
                x: i.x + e.dx + r.x,
                y: i.y + e.dy + r.y,
                width: r.width,
                height: r.height,
              };
            })(t, a.pointer, r);
          },
        },
        nodes: a.activeNodes,
        pointer: a.pointer,
        width: n,
        height: i,
      },
      c = N(o.placement);
    if ("object" === c && "function" == typeof o.placement.fn)
      return o.placement.fn(s);
    var u = { type: "pointer", offset: 8, dock: "auto", area: "viewport" };
    "function" === c && (u = k(u, o.placement(s))),
      "object" === c && Kh[o.placement.type]
        ? (u = k(u, o.placement))
        : "string" === c &&
          Kh[o.placement] &&
          (u = k(u, { type: o.placement })),
      (s.options = u);
    var l = Kh[u.type](s),
      h = s.resources.getComponentBoundsFromNode(s.nodes[0]),
      f = h.x,
      d = h.y,
      g = h.width,
      p = h.height;
    return (
      (g += f += s.pointer.dx),
      (p += d += s.pointer.dy),
      (l.computedTooltipStyle.left = "".concat(
        Math.min(Math.max(0, f, parseFloat(l.computedTooltipStyle.left)), g),
        "px"
      )),
      (l.computedTooltipStyle.top = "".concat(
        Math.min(Math.max(0, d, parseFloat(l.computedTooltipStyle.top)), p),
        "px"
      )),
      l
    );
  }
  function tf() {
    Jh = null;
  }
  var ef = {
    duration: 8e3,
    delay: 500,
    filter: function (t) {
      return t.filter(function (t) {
        return t.data && void 0 !== t.data.value;
      });
    },
    extract: function (t) {
      return t.node.data.value;
    },
    content: function (t) {
      var e = t.h;
      return t.data.map(function (t) {
        return e("div", {}, t);
      });
    },
    isEqual: function (t, e) {
      return (
        t.length &&
        t.length === e.length &&
        t.every(function (t, n) {
          return e[n] && JSON.stringify(t.data) === JSON.stringify(e[n].data);
        })
      );
    },
    placement: { type: "pointer", dock: "auto", offset: 8, area: "viewport" },
    tooltipClass: {},
    contentClass: {},
    arrowClass: {},
    direction: "ltr",
    appendTo: void 0,
    beforeShow: void 0,
    afterShow: void 0,
    beforeHide: void 0,
    onHide: void 0,
    afterHide: void 0,
  };
  function nf(t, e) {
    var n = e.chart,
      i = e.state,
      r = 0,
      a = 0;
    t.center
      ? ((r += t.center.x), (a += t.center.y))
      : ((r += t.clientX), (a += t.clientY));
    var o = n.element.getBoundingClientRect(),
      s = i.targetElement.getBoundingClientRect(),
      c = r,
      u = a,
      l = o.left - s.left,
      h = o.top - s.top,
      f = r - o.left,
      d = a - o.top;
    return {
      x: (r -= s.left),
      y: (a -= s.top),
      dx: l,
      dy: h,
      cx: f,
      cy: d,
      clientX: c,
      clientY: u,
      targetBounds: s,
      chartBounds: o,
    };
  }
  var rf = {
    require: ["chart", "renderer"],
    defaultSettings: {
      settings: ef,
      style: {
        tooltip: {},
        content: {
          backgroundColor: "$gray-25",
          color: "$font-color--inverted",
          fontFamily: "$font-family",
          fontSize: "$font-size",
          lineHeight: "$line-height",
          borderRadius: "4px",
          padding: "8px",
          opacity: 0.9,
        },
        arrow: {
          position: "absolute",
          width: "0px",
          height: "0px",
          borderStyle: "solid",
          color: "$gray-25",
          opacity: 0.9,
        },
        "arrow-bottom": {
          borderTopColor: "transparent",
          borderLeftColor: "transparent",
          borderRightColor: "transparent",
        },
        "arrow-top": {
          borderBottomColor: "transparent",
          borderLeftColor: "transparent",
          borderRightColor: "transparent",
        },
        "arrow-right": {
          borderTopColor: "transparent",
          borderLeftColor: "transparent",
          borderBottomColor: "transparent",
        },
        "arrow-left": {
          borderTopColor: "transparent",
          borderBottomColor: "transparent",
          borderRightColor: "transparent",
        },
      },
    },
    renderer: "dom",
    on: {
      hide: function () {
        this.hide();
      },
      show: function (t) {
        var e =
          arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
        this.show(t, e);
      },
      prevent: function (t) {
        this.prevent(t);
      },
    },
    hide: function () {
      this.dispatcher.clear(),
        (this.state.activeNodes = []),
        (this.state.pointer = {});
    },
    show: function (t) {
      var e,
        n = this,
        i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        r = i.nodes,
        a = i.duration,
        o = i.delay;
      !this.state.prevent &&
        this.state.targetElement &&
        ((this.state.pointer = nf(t, this)),
        (e = Array.isArray(r)
          ? this.props.filter(r)
          : this.props.filter(
              this.chart.shapesAt({
                x: this.state.pointer.cx,
                y: this.state.pointer.cy,
              })
            )),
        this.props.isEqual(this.state.activeNodes, e) ||
          (this.dispatcher.clear(),
          (this.state.activeNodes = e),
          this.state.activeNodes.length &&
            this.dispatcher.invoke(
              function () {
                return n.invokeRenderer(n.state.activeNodes);
              },
              a,
              o
            )));
    },
    prevent: function (t) {
      this.state.prevent = !!t;
    },
    init: function (t) {
      var e = this;
      (this.state = {
        activeNodes: [],
        pointer: {},
        targetElement: null,
        prevent: !1,
      }),
        (this.props = t.settings),
        (this.dispatcher = $h({
          defaultDuration: this.props.duration,
          defaultDelay: this.props.delay,
        }));
      var n = this.dispatcher.clear;
      this.dispatcher.on("pending", function () {
        !(function (t) {
          Jh && Jh !== t && Jh();
        })(n),
          (Jh = n),
          "function" == typeof e.props.beforeShow &&
            e.props.beforeShow.call(void 0, {
              resources: { formatter: e.chart.formatter, scale: e.chart.scale },
            });
      }),
        this.dispatcher.on(["cancelled", "fulfilled"], function () {
          var t = {
            resources: { formatter: e.chart.formatter, scale: e.chart.scale },
          };
          "function" == typeof e.props.beforeHide &&
            e.props.beforeHide.call(
              void 0,
              k({ element: e.state.tooltipElm }, t)
            ),
            "function" == typeof e.props.onHide
              ? e.props.onHide.call(
                  void 0,
                  k({ element: e.state.tooltipElm }, t)
                )
              : e.renderer.clear([]),
            "function" == typeof e.props.afterHide &&
              e.props.afterHide.call(void 0, t),
            Jh === n && (Jh = null),
            (e.state.tooltipElm = void 0);
        }),
        this.dispatcher.on("active", function () {
          "function" == typeof e.props.afterShow &&
            e.props.afterShow.call(void 0, {
              element: e.state.tooltipElm,
              resources: { formatter: e.chart.formatter, scale: e.chart.scale },
            });
        });
    },
    created: function () {
      this.init(this.settings);
    },
    beforeUpdate: function (t) {
      var e = t.settings;
      this.dispatcher && (this.dispatcher.destroy(), tf()), this.init(e);
    },
    render: function (t) {
      return (this.h = t), [];
    },
    beforeDestroy: function () {
      this.dispatcher.destroy(), tf();
    },
    appendTo: function () {
      if (this.props.appendTo) {
        this.state.targetElement =
          "function" == typeof this.props.appendTo
            ? this.props.appendTo({
                resources: {
                  formatter: this.chart.formatter,
                  scale: this.chart.scale,
                },
              })
            : this.props.appendTo;
        var t = this.state.targetElement.getBoundingClientRect(),
          e = t.width,
          n = t.height;
        this.renderer.destroy(),
          this.renderer.size({ width: e, height: n }),
          this.renderer.appendTo(this.state.targetElement);
      } else this.state.targetElement = this.renderer.element();
    },
    mounted: function () {
      this.appendTo();
    },
    updated: function () {
      this.appendTo();
    },
    invokeRenderer: function (t) {
      var e = (function (t, e) {
          var n = e.chart,
            i = e.scale,
            r = e.props,
            a = e.h,
            o = {
              resources: {
                dataset: n.dataset,
                scale: n.scale,
                formatter: n.formatter,
              },
              scale: i,
              h: a,
            },
            s = [];
          return (
            t.forEach(function (t) {
              if ("function" == typeof r.extract) {
                var e = k({ node: t }, o);
                s.push(r.extract(e));
              }
            }),
            s
          );
        })(t, this),
        n = Qh(
          Vh(
            e,
            { style: { left: "0px", top: "0px", visibility: "hidden" } },
            this
          ).getBoundingClientRect(),
          this
        );
      this.state.tooltipElm = Vh(e, n, this);
    },
  };
  var af = {
      require: ["renderer", "chart"],
      defaultSettings: {
        settings: {
          target: "",
          selector: "*",
          fill: "rgba(0, 255, 0, 0.1)",
          stroke: "lime",
          opacity: 1,
          useOuterRect: !1,
        },
      },
      on: {
        update: function () {
          this.draw();
        },
      },
      draw: function () {
        var t = this,
          e = this.chart.findShapes(this.props.selector).filter(function (e) {
            return e.key === t.props.target;
          }),
          n = e
            .filter(function (t) {
              return t.collider;
            })
            .map(function (t) {
              return t.collider;
            });
        n.forEach(function (e) {
          (e.fill = t.props.fill),
            (e.stroke = t.props.stroke),
            (e.opacity = t.props.opacity),
            (e.collider = { type: null });
        }),
          this.renderer.render(n);
      },
      created: function () {
        this.props = this.settings.settings;
      },
      resize: function (t) {
        var e = t.outer,
          n = t.inner;
        return this.props.useOuterRect ? e : n;
      },
      render: function () {},
      mounted: function () {
        this.draw();
      },
      updated: function () {
        (this.props = this.settings.settings), this.draw();
      },
    },
    of = 2 * Math.PI;
  function sf(t, e, n, i) {
    return (
      Math.abs(t.x + n.x - (e.x + e.x)) +
      Math.abs(t.y + n.y - (e.y + e.y)) +
      Math.abs(e.x + i.x - (n.x + n.x)) +
      Math.abs(e.y + i.y - (n.y + n.y))
    );
  }
  function cf(t, e) {
    return { x: 0.5 * (t.x + e.x), y: 0.5 * (t.y + e.y) };
  }
  function uf(t, e, n, i, r) {
    var a = 1 - t;
    return (
      Math.pow(a, 3) * e +
      3 * Math.pow(a, 2) * t * n +
      3 * a * Math.pow(t, 2) * i +
      Math.pow(t, 3) * r
    );
  }
  function lf(t, e, n, i) {
    var r = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : [],
      a = arguments.length > 5 && void 0 !== arguments[5] ? arguments[5] : 8;
    if (a < 1 || sf(t, e, n, i) <= 10)
      return r[r.length - 1] !== t && r.push(t), r.push(i), r;
    var o = 0.5,
      s = cf(t, e),
      c = cf(e, n),
      u = cf(n, i),
      l = { x: uf(o, t.x, e.x, n.x, i.x), y: uf(o, t.y, e.y, n.y, i.y) },
      h = cf(s, c),
      f = cf(c, u);
    return lf(t, s, h, l, r, a - 1), lf(l, f, u, i, r, a - 1), r;
  }
  function hf(t, e, n) {
    var i = (function (t, e, n) {
      return {
        cp1: { x: t.x + (2 / 3) * (e.x - t.x), y: t.y + (2 / 3) * (e.y - t.y) },
        cp2: { x: n.x + (2 / 3) * (e.x - n.x), y: n.y + (2 / 3) * (e.y - n.y) },
      };
    })(t, e, n);
    return lf(t, i.cp1, i.cp2, n);
  }
  function ff(t, e, n) {
    var i,
      r,
      a,
      o,
      s = [],
      c = !!t[4],
      u = !!t[5],
      l = t[3],
      h = t[6],
      f = t[7],
      d = t[1],
      g = t[2];
    if (("a" === t[0] && ((h += e), (f += n)), e === f && n === f)) return s;
    if (!d || !g) return s.push({ x: h, y: f }), s;
    var p = (function (t, e, n, i, r, a, o, s, c) {
      var u,
        l,
        h,
        f,
        d,
        g,
        p = No(n % 360),
        y = Math.cos(p),
        v = Math.sin(p),
        m = (s - a) / 2,
        x = (c - o) / 2,
        b = y * m + v * x,
        _ = y * x - v * m;
      (t = Math.abs(t)),
        (e = Math.abs(e)),
        (g =
          Math.pow(b, 2) / Math.pow(t, 2) + Math.pow(_, 2) / Math.pow(e, 2)) >
          1 && ((t *= g = Math.sqrt(g)), (e *= g));
      var w = t * e,
        k = t * _,
        M = e * b,
        R = Math.pow(k, 2) + Math.pow(M, 2),
        S = Math.pow(w, 2) - R,
        N = Math.sqrt(Math.max(S / R, 0));
      i === r && (N = -N);
      var z = N * (k / e),
        C = N * (-M / t);
      (f = y * z - v * C + (s + a) / 2), (d = v * z + y * C + (c + o) / 2);
      var A = (b - z) / t,
        E = (_ - C) / e,
        O = (-b - z) / t,
        T = (-_ - C) / e;
      return (
        (u = Math.atan2(E, A)),
        (u += u < 0 ? of : 0),
        (l = Math.atan2(T, O)),
        (h = (l += l < 0 ? of : 0) - u),
        !r && u < l ? (h -= of) : r && l < u && (h += of),
        { startAngle: u, sweepAngle: (h %= of), cx: f, cy: d, rx: t, ry: e }
      );
    })(d, g, l, c, u, h, f, e, n);
    (i = p.cx),
      (r = p.cy),
      (d = p.rx),
      (g = p.ry),
      (a = p.sweepAngle),
      (o = p.startAngle);
    for (
      var y = Math.abs(a * Math.sqrt((Math.pow(d, 2) + Math.pow(g, 2)) / 2)),
        v = Math.ceil(y / 10),
        m = a / v,
        x = 1;
      x <= v;
      x++
    ) {
      var b = (o + m * x) % of,
        _ = Math.cos(b),
        w = Math.sin(b);
      s.push({ x: i + _ * d + -w * _, y: r + w * g + _ * w });
    }
    return s;
  }
  function df(t) {
    for (
      var e = g.parsePath(t),
        n = [],
        i = [],
        r = 0,
        a = 0,
        o = null,
        s = null,
        c = null,
        u = null,
        l = 0;
      l < e.length;
      ++l
    ) {
      var h = e[l],
        f = h[0];
      switch (
        ("S" !== f &&
          "s" !== f &&
          "C" !== f &&
          "c" !== f &&
          ((o = null), (s = null)),
        "T" !== f &&
          "t" !== f &&
          "Q" !== f &&
          "q" !== f &&
          ((c = null), (u = null)),
        f)
      ) {
        case "m":
          i.length && n.push(i.splice(0));
        case "l":
          (r += h[1]), (a += h[2]), i.push({ x: r, y: a });
          break;
        case "M":
          i.length && n.push(i.splice(0));
        case "L":
          (r = h[1]), (a = h[2]), i.push({ x: r, y: a });
          break;
        case "H":
          (r = h[1]), i.push({ x: r, y: a });
          break;
        case "h":
          (r += h[1]), i.push({ x: r, y: a });
          break;
        case "V":
          (a = h[1]), i.push({ x: r, y: a });
          break;
        case "v":
          (a += h[1]), i.push({ x: r, y: a });
          break;
        case "a":
          i.push.apply(i, $(ff(h, r, a))), (r += h[6]), (a += h[7]);
          break;
        case "A":
          i.push.apply(i, $(ff(h, r, a))), (r = h[6]), (a = h[7]);
          break;
        case "c":
          i.push.apply(
            i,
            $(
              lf(
                { x: r, y: a },
                { x: h[1] + r, y: h[2] + a },
                { x: h[3] + r, y: h[4] + a },
                { x: h[5] + r, y: h[6] + a }
              )
            )
          ),
            (o = h[3] + r),
            (s = h[4] + a),
            (r += h[5]),
            (a += h[6]);
          break;
        case "C":
          i.push.apply(
            i,
            $(
              lf(
                { x: r, y: a },
                { x: h[1], y: h[2] },
                { x: h[3], y: h[4] },
                { x: h[5], y: h[6] }
              )
            )
          ),
            (o = h[3]),
            (s = h[4]),
            (r = h[5]),
            (a = h[6]);
          break;
        case "s":
          (null !== o && null !== o) || ((o = r), (s = a)),
            i.push.apply(
              i,
              $(
                lf(
                  { x: r, y: a },
                  { x: 2 * r - o, y: 2 * a - s },
                  { x: h[1] + r, y: h[2] + a },
                  { x: h[3] + r, y: h[4] + a }
                )
              )
            ),
            (o = h[1] + r),
            (s = h[2] + a),
            (r += h[3]),
            (a += h[4]);
          break;
        case "S":
          (null !== o && null !== o) || ((o = r), (s = a)),
            i.push.apply(
              i,
              $(
                lf(
                  { x: r, y: a },
                  { x: 2 * r - o, y: 2 * a - s },
                  { x: h[1], y: h[2] },
                  { x: h[3], y: h[4] }
                )
              )
            ),
            (o = h[1]),
            (s = h[2]),
            (r = h[3]),
            (a = h[4]);
          break;
        case "Q":
          i.push.apply(
            i,
            $(hf({ x: r, y: a }, { x: h[1], y: h[2] }, { x: h[3], y: h[4] }))
          ),
            (c = h[1]),
            (u = h[2]),
            (r = h[3]),
            (a = h[4]);
          break;
        case "q":
          i.push.apply(
            i,
            $(
              hf(
                { x: r, y: a },
                { x: h[1] + r, y: h[2] + a },
                { x: h[3] + r, y: h[4] + a }
              )
            )
          ),
            (c = h[1] + r),
            (u = h[2] + a),
            (r += h[3]),
            (a += h[4]);
          break;
        case "T":
          (null !== c && null !== c) || ((c = r), (u = a)),
            (c = 2 * r - c),
            (u = 2 * a - u),
            i.push.apply(
              i,
              $(hf({ x: r, y: a }, { x: c, y: u }, { x: h[1], y: h[2] }))
            ),
            (r = h[1]),
            (a = h[2]);
          break;
        case "t":
          (null !== c && null !== c) || ((c = r), (u = a)),
            (c = 2 * r - c),
            (u = 2 * a - u),
            i.push.apply(
              i,
              $(
                hf({ x: r, y: a }, { x: c, y: u }, { x: h[1] + r, y: h[2] + a })
              )
            ),
            (r += h[1]),
            (a += h[2]);
          break;
        case "z":
        case "Z":
          i.length && i.push({ x: i[0].x, y: i[0].y });
      }
    }
    return (
      (function (t) {
        for (var e = 0; e < t.length - 1; e++) {
          var n = t[e],
            i = t[e + 1];
          Math.abs(n.x - i.x) < 1e-12 &&
            Math.abs(n.y - i.y) < 1e-12 &&
            (t.splice(e, 1), e--);
        }
      })(i),
      n.push(i.splice(0)),
      n
    );
  }
  var gf = {
    require: ["renderer", "chart"],
    defaultSettings: {
      settings: {
        target: "",
        fill: "transparent",
        stroke: "lime",
        opacity: 1,
        radius: 2,
        useOuterRect: !1,
      },
    },
    on: {
      update: function () {
        this.draw();
      },
    },
    draw: function () {
      var t = this,
        e = this.chart.findShapes("path").filter(function (e) {
          return e.key === t.props.target;
        }),
        n = [];
      e.forEach(function (e) {
        df(e.attrs.d).forEach(function (e) {
          e.forEach(function (e) {
            n.push({
              type: "circle",
              cx: e.x,
              cy: e.y,
              r: t.props.radius,
              fill: t.props.fill,
              stroke: t.props.stroke,
              opacity: t.props.opacity,
              collider: { type: null },
            });
          });
        });
      }),
        this.renderer.render(n);
    },
    created: function () {
      this.props = this.settings.settings;
    },
    resize: function (t) {
      var e = t.outer,
        n = t.inner;
      return this.props.useOuterRect ? e : n;
    },
    render: function () {},
    mounted: function () {
      this.draw();
    },
    updated: function () {
      (this.props = this.settings.settings), this.draw();
    },
  };
  var pf = [
      function (t) {
        t.component("box", Mc), t.component("box-marker", Mc);
      },
      function (t) {
        t.component("point", Ec), t.component("point-marker", Ec);
      },
      function (t) {
        t.component("pie", Pc);
      },
      function (t) {
        t.component("grid-line", Ic);
      },
      function (t) {
        t.component("ref-line", Yc);
      },
      function (t) {
        t.component("axis", pu);
      },
      function (t) {
        t.component("container", yu);
      },
      function (t) {
        t.component("text", xu);
      },
      function (t) {
        t.component("scrollbar", wu);
      },
      function (t) {
        t.component("brush-range", Du), t.component("brush-area-dir", Iu);
      },
      function (t) {
        t.component("range", Hu);
      },
      function (t) {
        t.component("brush-lasso", Zu);
      },
      function (t) {
        t.component("labels", dh);
      },
      function (t) {
        t.component("legend-cat", Sh);
      },
      function (t) {
        t.component("legend-seq", Oh);
      },
      function (t) {
        t.component("line", Dh);
      },
      function (t) {
        t.component("brush-area", Wh);
      },
      function (t) {
        t.component("tooltip", rf);
      },
      function (t) {
        t.component("debug-collider", af);
      },
      function (t) {
        t.component("debug-path-to-points", gf);
      },
    ],
    yf = (function () {
      function t(e) {
        z(this, t),
          (this._parent = null),
          (this._children = []),
          (this._ancestors = null),
          (this.type = e),
          (this.data = null);
      }
      return (
        A(t, [
          {
            key: "detach",
            value: function () {
              return this._parent && this._parent.removeChild(this), this;
            },
          },
          {
            key: "parent",
            get: function () {
              return this._parent;
            },
          },
          {
            key: "isBranch",
            get: function () {
              return this._children && this._children.length;
            },
          },
          {
            key: "children",
            get: function () {
              return this._children;
            },
          },
          {
            key: "ancestors",
            get: function () {
              var t;
              this._ancestors ||
                ((this._ancestors = []),
                this.parent &&
                  (t = this._ancestors).push.apply(
                    t,
                    [this.parent].concat($(this.parent.ancestors))
                  ));
              return this._ancestors;
            },
          },
          {
            key: "descendants",
            get: function () {
              var t,
                e,
                n = [],
                i = this.children.length;
              for (t = 0; t < i; t++)
                (e = this.children[t]),
                  n.push(e),
                  e.children.length && n.push.apply(n, $(e.descendants));
              return n;
            },
          },
          {
            key: "equals",
            value: function (t) {
              var e = this.children,
                n = t.children;
              if (e.length !== n.length) return !1;
              for (var i = 0; i < e.length; i++)
                if (!e[i].equals(n[i])) return !1;
              return !0;
            },
          },
          {
            key: "toJSON",
            value: function () {
              return {
                type: this.type,
                children: this.children.map(function (t) {
                  return t.toJSON();
                }),
              };
            },
          },
        ]),
        t
      );
    })(),
    vf = (function () {
      function t() {
        var e =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          n = e.x,
          i = void 0 === n ? 0 : n,
          r = e.y,
          a = void 0 === r ? 0 : r,
          o = e.width,
          s = void 0 === o ? 0 : o,
          c = e.height,
          u = void 0 === c ? 0 : c,
          l = e.minWidth,
          h = void 0 === l ? 0 : l,
          f = e.minHeight,
          d = void 0 === f ? 0 : f;
        z(this, t),
          this.set({
            x: i,
            y: a,
            width: s,
            height: u,
            minWidth: h,
            minHeight: d,
          });
      }
      return (
        A(t, [
          {
            key: "set",
            value: function () {
              var t =
                  arguments.length > 0 && void 0 !== arguments[0]
                    ? arguments[0]
                    : {},
                e = t.x,
                n = void 0 === e ? 0 : e,
                i = t.y,
                r = void 0 === i ? 0 : i,
                a = t.width,
                o = void 0 === a ? 0 : a,
                s = t.height,
                c = void 0 === s ? 0 : s,
                u = t.minWidth,
                l = void 0 === u ? 0 : u,
                h = t.minHeight,
                f = void 0 === h ? 0 : h;
              (this.type = "rect"),
                o >= 0
                  ? ((this.x = n), (this.width = Math.max(o, l)))
                  : ((this.x = n + Math.min(o, -l)),
                    (this.width = -Math.min(o, -l))),
                c >= 0
                  ? ((this.y = r), (this.height = Math.max(c, f)))
                  : ((this.y = r + Math.min(c, -f)),
                    (this.height = -Math.min(c, -f)));
            },
          },
          {
            key: "containsPoint",
            value: function (t) {
              return fs(this, t);
            },
          },
          {
            key: "intersectsLine",
            value: function (t) {
              return ds(this, Q(t));
            },
          },
          {
            key: "intersectsRect",
            value: function (t) {
              return ls(this, K(t));
            },
          },
          {
            key: "intersectsCircle",
            value: function (t) {
              return rs(t, this);
            },
          },
          {
            key: "intersectsPolygon",
            value: function (t) {
              return us(t, this);
            },
          },
          {
            key: "intersectsGeoPolygon",
            value: function (t) {
              return _s(t, this);
            },
          },
          {
            key: "points",
            value: function () {
              return [
                { x: this.x, y: this.y },
                { x: this.x + this.width, y: this.y },
                { x: this.x + this.width, y: this.y + this.height },
                { x: this.x, y: this.y + this.height },
              ];
            },
          },
        ]),
        t
      );
    })();
  var mf = (function () {
    function t() {
      var e =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = e.cx,
        i = void 0 === n ? 0 : n,
        r = e.cy,
        a = void 0 === r ? 0 : r,
        o = e.r,
        s = void 0 === o ? 0 : o,
        c = e.minRadius,
        u = void 0 === c ? 0 : c;
      z(this, t), this.set({ cx: i, cy: a, r: s, minRadius: u });
    }
    return (
      A(t, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.cx,
              n = void 0 === e ? 0 : e,
              i = t.cy,
              r = void 0 === i ? 0 : i,
              a = t.r,
              o = void 0 === a ? 0 : a,
              s = t.minRadius,
              c = void 0 === s ? 0 : s;
            (this.type = "circle"),
              (this.cx = n),
              (this.cy = r),
              (this.r = Math.max(o, c)),
              (this.vector = { x: this.cx, y: this.cy });
          },
        },
        {
          key: "containsPoint",
          value: function (t) {
            return is(this, t);
          },
        },
        {
          key: "intersectsLine",
          value: function (t) {
            return as(this, Q(t));
          },
        },
        {
          key: "intersectsRect",
          value: function (t) {
            return rs(this, K(t));
          },
        },
        {
          key: "intersectsCircle",
          value: function (t) {
            return (function (t, e) {
              if (Qo(t) || Qo(e)) return !1;
              var n = t.cx - e.cx,
                i = t.cy - e.cy;
              return Math.pow(n, 2) + Math.pow(i, 2) <= Math.pow(t.r + e.r, 2);
            })(this, t);
          },
        },
        {
          key: "intersectsPolygon",
          value: function (t) {
            return os(this, t);
          },
        },
        {
          key: "intersectsGeoPolygon",
          value: function (t) {
            return ms(this, t);
          },
        },
        {
          key: "points",
          value: function () {
            return [{ x: this.cx, y: this.cy }];
          },
        },
      ]),
      t
    );
  })();
  var xf = (function () {
    function t() {
      var e =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = e.x1,
        i = void 0 === n ? 0 : n,
        r = e.y1,
        a = void 0 === r ? 0 : r,
        o = e.x2,
        s = void 0 === o ? 0 : o,
        c = e.y2,
        u = void 0 === c ? 0 : c,
        l = e.tolerance,
        h = void 0 === l ? 0 : l;
      z(this, t), this.set({ x1: i, y1: a, x2: s, y2: u, tolerance: h });
    }
    return (
      A(t, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.x1,
              n = void 0 === e ? 0 : e,
              i = t.y1,
              r = void 0 === i ? 0 : i,
              a = t.x2,
              o = void 0 === a ? 0 : a,
              s = t.y2,
              c = void 0 === s ? 0 : s,
              u = t.tolerance,
              l = void 0 === u ? 0 : u;
            (this.type = "line"),
              (this.x1 = n),
              (this.y1 = r),
              (this.x2 = o),
              (this.y2 = c),
              (this.tolerance = Math.max(0, Math.round(l))),
              (this.vectors = this.points()),
              (this.zeroSize = n === o && r === c);
          },
        },
        {
          key: "containsPoint",
          value: function (t) {
            return this.tolerance > 0
              ? as({ cx: t.x, cy: t.y, r: this.tolerance }, this)
              : ps(this, t);
          },
        },
        {
          key: "intersectsLine",
          value: function (t) {
            return gs(this, Q(t));
          },
        },
        {
          key: "intersectsRect",
          value: function (t) {
            return ds(K(t), this);
          },
        },
        {
          key: "intersectsCircle",
          value: function (t) {
            return as(t, this);
          },
        },
        {
          key: "intersectsPolygon",
          value: function (t) {
            return cs(t, this);
          },
        },
        {
          key: "intersectsGeoPolygon",
          value: function (t) {
            return bs(t, this);
          },
        },
        {
          key: "points",
          value: function () {
            return [
              { x: this.x1, y: this.y1 },
              { x: this.x2, y: this.y2 },
            ];
          },
        },
      ]),
      t
    );
  })();
  function bf(t) {
    var e = t[0],
      n = t[t.length - 1];
    (e.x === n.x && e.y === n.y) || t.push(e);
  }
  function _f(t) {
    for (var e = 0; e < t.length - 1; e++) {
      var n = t[e],
        i = t[e + 1];
      n.x === i.x && n.y === i.y && (t.splice(e, 1), e--);
    }
  }
  var wf = (function () {
    function t() {
      var e =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = e.vertices,
        i = void 0 === n ? [] : n;
      z(this, t), this.set({ vertices: i });
    }
    return (
      A(t, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.vertices,
              n = void 0 === e ? [] : e;
            if (
              ((this.type = "polygon"),
              (this.vertices = n.slice()),
              (this.edges = []),
              _f(this.vertices),
              !(this.vertices.length <= 2))
            ) {
              bf(this.vertices),
                (this.xMin = NaN),
                (this.yMin = NaN),
                (this.xMax = NaN),
                (this.yMax = NaN);
              for (var i = 0; i < this.vertices.length; i++)
                i < this.vertices.length - 1 &&
                  this.edges.push([this.vertices[i], this.vertices[i + 1]]),
                  (this.xMin = isNaN(this.xMin)
                    ? this.vertices[i].x
                    : Math.min(this.xMin, this.vertices[i].x)),
                  (this.xMax = isNaN(this.xMax)
                    ? this.vertices[i].x
                    : Math.max(this.xMax, this.vertices[i].x)),
                  (this.yMin = isNaN(this.yMin)
                    ? this.vertices[i].y
                    : Math.min(this.yMin, this.vertices[i].y)),
                  (this.yMax = isNaN(this.yMax)
                    ? this.vertices[i].y
                    : Math.max(this.yMax, this.vertices[i].y));
              (this._bounds = null), (this._boundingRect = null);
            }
          },
        },
        {
          key: "containsPoint",
          value: function (t) {
            return ss(this, t);
          },
        },
        {
          key: "intersectsCircle",
          value: function (t) {
            return os(t, this);
          },
        },
        {
          key: "intersectsLine",
          value: function (t) {
            return cs(this, Q(t));
          },
        },
        {
          key: "intersectsRect",
          value: function (t) {
            return us(this, K(t));
          },
        },
        {
          key: "intersectsPolygon",
          value: function (t) {
            return ys(this, t);
          },
        },
        {
          key: "intersectsGeoPolygon",
          value: function (t) {
            return vs(t, this);
          },
        },
        {
          key: "points",
          value: function () {
            return this.vertices;
          },
        },
        {
          key: "bounds",
          value: function () {
            return (
              this._bounds ||
                (this._bounds = [
                  { x: this.xMin, y: this.yMin },
                  { x: this.xMax, y: this.yMin },
                  { x: this.xMax, y: this.yMax },
                  { x: this.xMin, y: this.yMax },
                ]),
              this._bounds
            );
          },
        },
        {
          key: "boundingRect",
          value: function () {
            return (
              this._boundingRect ||
                (this._boundingRect = {
                  x: this.xMin,
                  y: this.yMin,
                  width: this.xMax - this.xMin,
                  height: this.yMax - this.yMin,
                }),
              this._boundingRect
            );
          },
        },
      ]),
      t
    );
  })();
  function kf() {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(wf, e);
  }
  function Mf(t) {
    var e = t[0],
      n = t[t.length - 1];
    (e.x === n.x && e.y === n.y) || t.push(e);
  }
  function Rf(t) {
    for (var e = 0; e < t.length - 1; e++) {
      var n = t[e],
        i = t[e + 1];
      n.x === i.x && n.y === i.y && (t.splice(e, 1), e--);
    }
  }
  var Sf = (function () {
    function t() {
      var e =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = e.vertices,
        i = void 0 === n ? [[]] : n;
      z(this, t), this.set({ vertices: i });
    }
    return (
      A(t, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.vertices,
              n = void 0 === e ? [] : e;
            (this.type = "geopolygon"),
              (this.vertices = n.slice()),
              (this.numPolygons = this.vertices.length),
              (this.polygons = []),
              (this.xMin = NaN),
              (this.yMin = NaN),
              (this.xMax = NaN),
              (this.yMax = NaN);
            for (var i = 0; i < this.numPolygons; i++)
              Rf(this.vertices[i]),
                this.vertices[i].length > 2 && Mf(this.vertices[i]),
                (this.polygons[i] = kf({ vertices: this.vertices[i] })),
                (this.xMin = isNaN(this.xMin)
                  ? this.polygons[i].xMin
                  : Math.min(this.xMin, this.polygons[i].xMin)),
                (this.xMax = isNaN(this.xMax)
                  ? this.polygons[i].xMax
                  : Math.max(this.xMax, this.polygons[i].xMax)),
                (this.yMin = isNaN(this.yMin)
                  ? this.polygons[i].yMin
                  : Math.min(this.yMin, this.polygons[i].yMin)),
                (this.yMax = isNaN(this.yMax)
                  ? this.polygons[i].yMax
                  : Math.max(this.yMax, this.polygons[i].yMax));
            (this._bounds = null), (this._boundingRect = null);
          },
        },
        {
          key: "containsPoint",
          value: function (t) {
            return xs(this, t);
          },
        },
        {
          key: "intersectsCircle",
          value: function (t) {
            return ms(t, this);
          },
        },
        {
          key: "intersectsLine",
          value: function (t) {
            return bs(this, Q(t));
          },
        },
        {
          key: "intersectsRect",
          value: function (t) {
            return _s(this, K(t));
          },
        },
        {
          key: "intersectsPolygon",
          value: function (t) {
            return vs(this, t);
          },
        },
        {
          key: "intersectsGeoPolygon",
          value: function (t) {
            return (
              (n = t),
              (i = (e = this).boundingRect()),
              (r = n.boundingRect()),
              !!ls(i, r) && (hs(r, i) ? ns(n, e) : ns(e, n))
            );
            var e, n, i, r;
          },
        },
        {
          key: "points",
          value: function () {
            return this.vertices;
          },
        },
        {
          key: "bounds",
          value: function () {
            return (
              this._bounds ||
                (this._bounds = [
                  { x: this.xMin, y: this.yMin },
                  { x: this.xMax, y: this.yMin },
                  { x: this.xMax, y: this.yMax },
                  { x: this.xMin, y: this.yMax },
                ]),
              this._bounds
            );
          },
        },
        {
          key: "boundingRect",
          value: function () {
            return (
              this._boundingRect ||
                (this._boundingRect = {
                  x: this.xMin,
                  y: this.yMin,
                  width: this.xMax - this.xMin,
                  height: this.yMax - this.yMin,
                }),
              this._boundingRect
            );
          },
        },
      ]),
      t
    );
  })();
  function Nf() {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(Sf, e);
  }
  function zf(t, e) {
    return t.x !== e.x || t.y !== e.y;
  }
  var Cf = (function () {
    function t() {
      var e =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = e.points,
        i = void 0 === n ? [] : n;
      z(this, t), this.set({ points: i });
    }
    return (
      A(t, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.points,
              n = void 0 === e ? [] : e;
            if (
              ((this.type = "polyline"),
              (this.segments = []),
              (this._points = n.slice()),
              this._points.length > 1)
            )
              for (var i = 0, r = this._points.length - 1; i < r; i++)
                zf(this._points[i], this._points[i + 1]) &&
                  this.segments.push({
                    x1: this._points[i].x,
                    y1: this._points[i].y,
                    x2: this._points[i + 1].x,
                    y2: this._points[i + 1].y,
                  });
          },
        },
        {
          key: "containsPoint",
          value: function (t) {
            return this.segments.some(function (e) {
              return ps(e, t);
            });
          },
        },
        {
          key: "intersectsCircle",
          value: function (t) {
            return this.segments.some(function (e) {
              return as(t, e);
            });
          },
        },
        {
          key: "intersectsLine",
          value: function (t) {
            var e = Q(t);
            return this.segments.some(function (t) {
              return gs(t, e);
            });
          },
        },
        {
          key: "intersectsRect",
          value: function (t) {
            var e = K(t);
            return this.segments.some(function (t) {
              return ds(e, t);
            });
          },
        },
        {
          key: "intersectsPolygon",
          value: function (t) {
            return this.segments.some(function (e) {
              return cs(t, e);
            });
          },
        },
        {
          key: "intersectsGeoPolygon",
          value: function (t) {
            return this.segments.some(function (e) {
              return bs(t, e);
            });
          },
        },
        {
          key: "points",
          value: function () {
            return this._points;
          },
        },
      ]),
      t
    );
  })();
  var Af = oe();
  function Ef(t, e) {
    return Af.get(t)(e);
  }
  Af.add("rect", function () {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(vf, e);
  }),
    Af.add("circle", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(mf, e);
    }),
    Af.add("line", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(xf, e);
    }),
    Af.add("polygon", kf),
    Af.add("geopolygon", Nf),
    Af.add("polyline", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(Cf, e);
    });
  var Of = (function () {
    function t() {
      var e =
        arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
      z(this, t), this.set(e);
    }
    return (
      A(t, [
        {
          key: "set",
          value: function () {
            var t = this,
              e =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : [];
            (this.geometries = []),
              e.forEach(function (e) {
                var n = Ef(e.type, e);
                n && t.geometries.push(n);
              });
          },
        },
        {
          key: "containsPoint",
          value: function (t) {
            return this.geometries.some(function (e) {
              return e.containsPoint(t);
            });
          },
        },
        {
          key: "intersectsLine",
          value: function (t) {
            return this.geometries.some(function (e) {
              return e.intersectsLine(t);
            });
          },
        },
        {
          key: "intersectsRect",
          value: function (t) {
            return this.geometries.some(function (e) {
              return e.intersectsRect(t);
            });
          },
        },
        {
          key: "intersectsCircle",
          value: function (t) {
            return this.geometries.some(function (e) {
              return e.intersectsCircle(t);
            });
          },
        },
        {
          key: "intersectsPolygon",
          value: function (t) {
            return this.geometries.some(function (e) {
              return e.intersectsPolygon(t);
            });
          },
        },
        {
          key: "intersectsGeoPolygon",
          value: function (t) {
            return this.geometries.some(function (e) {
              return e.intersectsGeoPolygon(t);
            });
          },
        },
      ]),
      t
    );
  })();
  var Tf = (function () {
      function t() {
        z(this, t),
          (this._elements = [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
          ]),
          (this._stack = []);
      }
      return (
        A(t, [
          {
            key: "clone",
            value: function () {
              return new t().multiply(this);
            },
          },
          {
            key: "set",
            value: function (t) {
              return (this._elements = t), this;
            },
          },
          {
            key: "save",
            value: function () {
              return this._stack.push(this.elements), this;
            },
          },
          {
            key: "restore",
            value: function () {
              return (
                this._stack.length && (this._elements = this._stack.pop()), this
              );
            },
          },
          {
            key: "add",
            value: function (t) {
              var e, n;
              for (e = 0; e < this._elements.length; e++)
                for (n = 0; n < this._elements[e].length; n++)
                  this._elements[e][n] += t;
              return this;
            },
          },
          {
            key: "translate",
            value: function (t, e) {
              return (
                this.multiply([
                  [1, 0, t],
                  [0, 1, e],
                  [0, 0, 1],
                ]),
                this
              );
            },
          },
          {
            key: "rotate",
            value: function (t) {
              var e = Math.cos(-t),
                n = Math.sin(-t);
              return (
                this.multiply([
                  [e, n, 0],
                  [-n, e, 0],
                  [0, 0, 1],
                ]),
                this
              );
            },
          },
          {
            key: "multiply",
            value: function (e) {
              var n, i, r, a;
              if ((e instanceof t && (e = e._elements), Array.isArray(e))) {
                for (
                  r = [
                    [0, 0, 0],
                    [0, 0, 0],
                    [0, 0, 0],
                  ],
                    n = 0;
                  n < this._elements.length;
                  n++
                )
                  for (i = 0; i < this._elements[n].length; i++)
                    for (a = 0; a < 3; a++)
                      r[n][i] += this._elements[n][a] * e[a][i];
                this._elements = r;
              } else
                for (n = 0; n < this._elements.length; n++)
                  for (i = 0; i < this._elements[n].length; i++)
                    this._elements[n][i] *= e;
              return this;
            },
          },
          {
            key: "scale",
            value: function (t) {
              var e =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : t;
              return (
                this.multiply([
                  [t, 0, 0],
                  [0, e, 0],
                  [0, 0, 1],
                ]),
                this
              );
            },
          },
          {
            key: "transform",
            value: function (t, e, n, i, r, a) {
              return (
                this.multiply([
                  [t, n, r],
                  [e, i, a],
                  [0, 0, 1],
                ]),
                this
              );
            },
          },
          {
            key: "determinant",
            value: function () {
              var t = this._elements[0][0],
                e = this._elements[0][1],
                n = this._elements[0][2],
                i = this._elements[1][0],
                r = this._elements[1][1],
                a = this._elements[1][2],
                o = this._elements[2][0],
                s = this._elements[2][1],
                c = this._elements[2][2];
              return (
                t * r * c +
                e * a * o +
                n * i * s -
                n * r * o -
                e * i * c -
                t * a * s
              );
            },
          },
          {
            key: "invert",
            value: function () {
              var t = this.determinant(),
                e = this._elements[0][0],
                n = this._elements[0][1],
                i = this._elements[0][2],
                r = this._elements[1][0],
                a = this._elements[1][1],
                o = this._elements[1][2],
                s = this._elements[2][0],
                c = this._elements[2][1],
                u = this._elements[2][2];
              return (
                (this._elements = [
                  [a * u - o * c, i * c - n * u, n * o - i * a],
                  [o * s - r * u, e * u - i * s, i * r - e * o],
                  [r * c - a * s, s * n - e * c, e * a - n * r],
                ]),
                this.multiply(1 / t),
                this
              );
            },
          },
          {
            key: "transpose",
            value: function () {
              var t = Object.create(this._elements);
              return (
                (this._elements = [
                  [t[0][0], t[1][0], t[2][0]],
                  [t[0][1], t[1][1], t[2][1]],
                  [t[0][2], t[1][2], t[2][2]],
                ]),
                this
              );
            },
          },
          {
            key: "identity",
            value: function () {
              return (
                (this._elements = [
                  [1, 0, 0],
                  [0, 1, 0],
                  [0, 0, 1],
                ]),
                this
              );
            },
          },
          {
            key: "toString",
            value: function () {
              return "".concat(
                this._elements
                  .map(function (t) {
                    return t.join("\t");
                  })
                  .join("\n")
              );
            },
          },
          {
            key: "isIdentity",
            value: function () {
              var t = this._elements;
              return (
                1 === t[0][0] &&
                0 === t[0][1] &&
                0 === t[0][2] &&
                0 === t[1][0] &&
                1 === t[1][1] &&
                0 === t[1][2] &&
                0 === t[2][0] &&
                0 === t[2][1] &&
                1 === t[2][2]
              );
            },
          },
          {
            key: "transformPoint",
            value: function (t) {
              var e,
                n,
                i = [t.x, t.y, 1],
                r = this._elements,
                a = [0, 0, 0];
              for (e = 0; e < this._elements.length; e++)
                for (n = 0; n < this._elements[e].length; n++)
                  a[e] += i[n] * r[e][n];
              return { x: a[0], y: a[1] };
            },
          },
          {
            key: "transformPoints",
            value: function (t) {
              var e,
                n,
                i,
                r,
                a,
                o = this._elements,
                s = [];
              for (r = 0; r < t.length; r++) {
                for (
                  e = [t[r].x, t[r].y, 1], a = [0, 0, 0], n = 0;
                  n < this._elements.length;
                  n++
                )
                  for (i = 0; i < this._elements[n].length; i++)
                    a[n] += e[i] * o[n][i];
                s.push({ x: a[0], y: a[1] });
              }
              return s;
            },
          },
          {
            key: "elements",
            get: function () {
              var t = this._elements;
              return [
                [t[0][0], t[0][1], t[0][2]],
                [t[1][0], t[1][1], t[1][2]],
                [t[2][0], t[2][1], t[2][2]],
              ];
            },
          },
        ]),
        t
      );
    })(),
    jf = /(translate|scale|rotate|matrix)\(([0-9,.eE+-\s]+)(?:,|\s?)+\)/g;
  function Pf(t, e) {
    var n = e.args[0] * (Math.PI / 180);
    if (e.args.length > 2) {
      var i = e.args[1],
        r = e.args[2];
      t.translate(i, r), t.rotate(n), t.translate(-i, -r);
    } else 1 === e.args.length && t.rotate(n);
  }
  function Bf(t, e) {
    var n = e.args[0],
      i = isNaN(e.args[1]) ? e.args[0] : e.args[1];
    t.scale(n, i);
  }
  function Df(t, e) {
    var n = e.args[0],
      i = isNaN(e.args[1]) ? 0 : e.args[1];
    t.translate(n, i);
  }
  function Ff(t, e) {
    e.args.length >= 6 && t.transform.apply(t, $(e.args));
  }
  function Lf(t, e) {
    for (
      var n,
        i = (function (t) {
          for (var e, n = []; null !== (e = jf.exec(t)); ) {
            var i = e[2].trim(),
              r = -1 === i.indexOf(",") ? i.split(" ") : i.split(",");
            n.push({
              cmd: e[1],
              args: r
                .filter(function (t) {
                  return t.trim().length > 0;
                })
                .map(function (t) {
                  return Number(t);
                }),
            });
          }
          return n;
        })(t),
        r = 0,
        a = i.length;
      r < a;
      r++
    )
      "rotate" === (n = i[r]).cmd
        ? Pf(e, n)
        : "scale" === n.cmd
        ? Bf(e, n)
        : "matrix" === n.cmd
        ? Ff(e, n)
        : "translate" === n.cmd && Df(e, n);
  }
  var If = {
      type: /^\w[\w-]+/,
      attr: /^\[\w(?:[\w\._-]+)?(?:[!]?=['\"][\w\s*#_-]*['\"])?\]/,
      universal: /^(\*)/,
      tag: /^\.(\w+)/,
    },
    Wf = {
      type: function (t, e) {
        return e.filter(function (e) {
          var n = e.type;
          return !!n && n.toLowerCase() === t.toLowerCase();
        });
      },
      attr: function (t, e, n, i) {
        return i.filter(function (i) {
          var r = i.attrs[t];
          if (!e) return void 0 !== r;
          if (void 0 === r) return !1;
          switch (e) {
            case "=":
              return n === String(r);
            case "!=":
              return n !== String(r);
            default:
              return !1;
          }
        });
      },
      universal: function (t) {
        return t;
      },
      tag: function (t, e) {
        return e.filter(function (e) {
          var n = e.tag;
          return (
            !!n && -1 !== n.trim().split(/\s+/).indexOf(t.replace(".", ""))
          );
        });
      },
    };
  var Vf = {
    find: function (t, e) {
      var n,
        i,
        r = [],
        a = [];
      if (e.isBranch) {
        var o;
        (n = (function (t) {
          var e,
            n,
            i,
            r,
            a = [];
          return (
            t.split(/\s*,\s*/).forEach(function (t) {
              (t = t.trim()), (e = []);
              for (
                var o = function (a) {
                  (i = t.match(If[a])) &&
                    ((r = !0),
                    (t = t.slice(i[0].length)),
                    (n = { type: a, value: i[0] }),
                    "attr" === a &&
                      ((i = i[0].match(
                        /\[(\w[\w\._-]+)?(?:([!]?=)['\"]([\w\s#_-]*)['\"])?\]/
                      )),
                      (n.attribute = i[1]),
                      (n.operator = i[2]),
                      (n.attributeValue = i[3])),
                    e.push(n));
                };
                t &&
                ((r = !1),
                (i = t.match(/^\s*([>+~]|\s)\s*/)) &&
                  ((r = !0),
                  e.push({ type: " ", value: i[0] }),
                  (t = t.slice(i[0].length))),
                Object.keys(If).forEach(o),
                e && e.length && a.indexOf(e) < 0 && a.push(e),
                r);

              );
            }),
            a
          );
        })(t)),
          (i = e.descendants);
        for (
          var s = function (t, e) {
              o = n[t];
              var r = [],
                s = i.slice(),
                c = !1;
              o.reverse().forEach(function (t) {
                if (" " === t.type)
                  return r.push(s), (s = i.slice()), void (c = !1);
                (s = (function (t, e) {
                  if (!e || !e.length || !t || "function" != typeof Wf[t.type])
                    return [];
                  switch (t.type) {
                    case "type":
                    case "tag":
                      return Wf[t.type](t.value, e);
                    case "attr":
                      return Wf[t.type](
                        t.attribute,
                        t.operator,
                        t.attributeValue,
                        e
                      );
                    case "universal":
                      return Wf[t.type](e);
                    default:
                      return [];
                  }
                })(t, s)),
                  (c = !0);
              }),
                c && r.push(s);
              var u = r[0].filter(function (t) {
                for (var e, n = t.parent, i = 1; i < r.length; i++) {
                  for (e = r[i].indexOf(n); n && e < 0; )
                    (n = n.parent), (e = r[i].indexOf(n));
                  if (e < 0) return !1;
                }
                return !0;
              });
              a.push(u);
            },
            c = 0,
            u = n.length;
          c < u;
          c++
        )
          s(c);
        for (var l = 0, h = a.length; l < h; l++)
          for (var f = 0, d = a[l].length; f < d; f++)
            r.indexOf(a[l][f]) < 0 && r.push(a[l][f]);
      }
      return r || [];
    },
  };
  function $f(t, e, n) {
    var i = t.type,
      r = t.points();
    n &&
      (r = r.every(function (t) {
        return Array.isArray(t);
      })
        ? r.map(function (t) {
            return n.transformPoints(t);
          })
        : n.transformPoints(r)),
      (function (t, e) {
        for (var n = 0, i = t.length; n < i; n++) (t[n].x /= e), (t[n].y /= e);
      })(r, e);
    var a = null;
    if ("rect" === i || "bounds" === i) (a = K(r)).type = i;
    else if ("circle" === i)
      (a = (function (t, e) {
        return { cx: t[0].x, cy: t[0].y, r: e };
      })(r, t.r)),
        (a.type = i);
    else if ("line" === i) (a = Q(r)).type = i;
    else if ("polygon" === i || "polyline" === i) {
      a = { type: "path", d: Ro(r, "polygon" === i) };
    } else if ("geopolygon" === i) {
      for (var o = "", s = 0; s < r.length; s++) o += Ro(r[s], !0);
      a = { type: "path", d: o };
    }
    return a;
  }
  var Hf = (function () {
    function t(e) {
      var n = this;
      z(this, t),
        (this._bounds = function () {
          var t =
              !(arguments.length > 0 && void 0 !== arguments[0]) ||
              arguments[0],
            n = e.boundingRect
              ? e.boundingRect(t)
              : { x: 0, y: 0, width: 0, height: 0 },
            i = n.x,
            r = n.y,
            a = n.width,
            o = n.height;
          return { x: i, y: r, width: a, height: o };
        }),
        (this._attrs = e.attrs),
        (this._type = e.type),
        (this._data = e.data),
        (this._dpi = e.stage ? e.stage.dpi : 1),
        (this._collider = function () {
          return (function (t, e) {
            if (t.collider) {
              var n = t.modelViewMatrix;
              return "collection" === t.colliderType
                ? {
                    type: "container",
                    children: t.collider.geometries.map(function (t) {
                      return $f(t, e, n);
                    }),
                  }
                : $f(t.collider, e, n);
            }
            return null;
          })(e, n._dpi);
        }),
        (this._desc = e.desc),
        (this._tag = e.tag),
        (this._children = function () {
          return e.children.map(function (e) {
            return new t(e);
          });
        }),
        (this._parent = function () {
          return e.parent ? new t(e.parent) : null;
        }),
        (this._cache = { elementBoundingRect: null }),
        (this._getElementBoundingRect = function () {
          return (
            !n._cache.elementBoundingRect &&
              n.element &&
              (n._cache.elementBoundingRect =
                n.element.getBoundingClientRect()),
            n._cache.elementBoundingRect || { left: 0, top: 0 }
          );
        });
    }
    return (
      A(t, [
        {
          key: "children",
          get: function () {
            return this._children();
          },
        },
        {
          key: "parent",
          get: function () {
            return this._parent();
          },
        },
        {
          key: "type",
          get: function () {
            return this._type;
          },
        },
        {
          key: "data",
          get: function () {
            return this._data;
          },
        },
        {
          key: "attrs",
          get: function () {
            return this._attrs;
          },
        },
        {
          key: "element",
          get: function () {
            return this._element;
          },
          set: function (t) {
            (this._cache.elementBoundingRect = null), (this._element = t);
          },
        },
        {
          key: "key",
          get: function () {
            return this._key;
          },
          set: function (t) {
            this._key = t;
          },
        },
        {
          key: "bounds",
          get: function () {
            var t = this._bounds();
            return (
              (t.x /= this._dpi),
              (t.y /= this._dpi),
              (t.width /= this._dpi),
              (t.height /= this._dpi),
              t
            );
          },
        },
        {
          key: "localBounds",
          get: function () {
            return this._bounds(!1);
          },
        },
        {
          key: "boundsRelativeTo",
          value: function (t) {
            var e =
                !(arguments.length > 1 && void 0 !== arguments[1]) ||
                arguments[1],
              n = N(t),
              i = e ? this.bounds : this.localBounds,
              r = this._getElementBoundingRect(),
              a = r.left,
              o = r.top;
            if (
              "object" === n &&
              null !== t &&
              "function" == typeof t.getBoundingClientRect
            ) {
              var s = t.getBoundingClientRect(),
                c = s.left,
                u = void 0 === c ? 0 : c,
                l = s.top,
                h = void 0 === l ? 0 : l;
              (a -= u), (o -= h);
            }
            return (i.x += a), (i.y += o), i;
          },
        },
        {
          key: "collider",
          get: function () {
            return this._collider();
          },
        },
        {
          key: "desc",
          get: function () {
            return this._desc;
          },
        },
        {
          key: "tag",
          get: function () {
            return this._tag;
          },
        },
      ]),
      t
    );
  })();
  function qf() {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(Hf, e);
  }
  var Uf = (function () {
    function t(e) {
      z(this, t),
        (this._node = qf(e)),
        (this._parent = null),
        (this._input = null);
    }
    return (
      A(t, [
        {
          key: "node",
          get: function () {
            return this._node;
          },
        },
        {
          key: "parent",
          get: function () {
            return this._parent;
          },
          set: function (t) {
            this._parent = t;
          },
        },
        {
          key: "input",
          get: function () {
            return this._input;
          },
          set: function (t) {
            this._input = t;
          },
        },
      ]),
      t
    );
  })();
  function Yf() {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(Uf, e);
  }
  function Xf(t, e) {
    var n = t.parent;
    if (n && "stage" !== n.type) {
      e.parent = Yf(n);
      var i = n.parent;
      i && "stage" !== i.type && Xf(i, e.parent);
    }
  }
  function Gf(t, e, n) {
    if (null === t.colliderType) return null;
    var i = (function (t, e) {
      var n = {};
      if (t.modelViewMatrix)
        if (Array.isArray(e)) n = t.inverseModelViewMatrix.transformPoints(e);
        else if (isNaN(e.r))
          Array.isArray(e.vertices)
            ? (n.vertices = t.inverseModelViewMatrix.transformPoints(
                e.vertices
              ))
            : (n = t.inverseModelViewMatrix.transformPoint(e));
        else {
          var i = { x: e.cx, y: e.cy },
            r = t.inverseModelViewMatrix.transformPoint(i);
          (n.cx = r.x), (n.cy = r.y), (n.r = e.r);
        }
      else n = e;
      return (
        Array.isArray(n.vertices) &&
          (n = n.vertices.every(function (t) {
            return Array.isArray(t);
          })
            ? Nf(n)
            : kf(n)),
        n
      );
    })(t, n);
    return "frontChild" === t.colliderType
      ? (function (t, e, n) {
          for (var i = t.descendants.length - 1; i >= 0; i--) {
            var r = t.descendants[i];
            if (null !== r.collider && r.collider[e](n)) {
              var a = Yf(r);
              return Xf(r, a), a;
            }
          }
          return null;
        })(t, e, i)
      : (function (t, e, n) {
          if (t.collider[e](n)) {
            var i = Yf(t);
            return Xf(t, i), i;
          }
          return null;
        })(t, e, i);
  }
  function Zf(t, e, n, i) {
    for (var r = t.length, a = 0; a < r; a++) {
      var o = t[a],
        s = Gf(o, e, i);
      s && n.push(s), !o.children || s || o.collider || Zf(o.children, e, n, i);
    }
  }
  function Jf(t, e, n) {
    for (var i = t.length, r = 0; r < i; r++) {
      var a = t[r];
      if (null !== Gf(a, e, n)) return !0;
      if (a.children && !a.collider) return Jf(a.children, e, n);
    }
    return !1;
  }
  function Kf(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1,
      n = tt(t),
      i = {};
    switch (n) {
      case "circle":
        return (
          (i.cx = t.cx * e),
          (i.cy = t.cy * e),
          (i.r = t.r),
          ["intersectsCircle", i]
        );
      case "rect":
        return (
          (i = J(t).map(function (t) {
            return Eo(t, e);
          })),
          ["intersectsRect", i]
        );
      case "line":
        return (
          (i = Z(t).map(function (t) {
            return Eo(t, e);
          })),
          ["intersectsLine", i]
        );
      case "point":
        return ["containsPoint", (i = Eo(t, e))];
      case "polygon":
        return (
          (i.vertices = t.vertices.map(function (t) {
            return Eo(t, e);
          })),
          ["intersectsPolygon", i]
        );
      case "geopolygon":
        return (
          (i.vertices = t.vertices.map(function (t) {
            return t.map(function (t) {
              return Eo(t, e);
            });
          })),
          ["intersectsGeoPolygon", i]
        );
      default:
        return [];
    }
  }
  function Qf(t, e) {
    var n = V(Kf(e, t.dpi), 2),
      i = n[0],
      r = n[1],
      a = [];
    return (
      i &&
        (Zf([t], i, a, r),
        (function (t, e) {
          for (var n = 0, i = e.length; n < i; n++) e[n].input = t;
        })(e, a)),
      a
    );
  }
  function td(t, e) {
    var n = V(Kf(e, t.dpi), 2);
    return Jf([t], n[0], n[1]);
  }
  var ed = (function (t) {
      O(n, t);
      var e = L(n);
      function n(t) {
        var i;
        return (
          z(this, n),
          ((i = e.call(this, t))._stage = null),
          (i._collider = { type: null, definition: null, fn: null }),
          (i._attrs = {}),
          (i._node = null),
          i
        );
      }
      return (
        A(n, [
          {
            key: "set",
            value: function () {
              var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {};
              this.node = t;
              var e = t.data,
                n = t.desc,
                i = t.tag,
                r = t.strokeReference,
                a = t.fillReference;
              Mo(this.attrs, t),
                void 0 !== e && (this.data = e),
                "object" === N(n) && (this.desc = k(!0, {}, n)),
                "string" == typeof i && (this.tag = i),
                "string" == typeof r && (this.strokeReference = r),
                "string" == typeof a && (this.fillReference = a);
            },
          },
          {
            key: "findShapes",
            value: function (t) {
              return Vf.find(t, this).map(function (t) {
                return qf(t);
              });
            },
          },
          {
            key: "getItemsFrom",
            value: function (t) {
              return Qf(this, t);
            },
          },
          {
            key: "containsPoint",
            value: function (t) {
              return td(this, t);
            },
          },
          {
            key: "intersectsLine",
            value: function (t) {
              return td(this, t);
            },
          },
          {
            key: "intersectsRect",
            value: function (t) {
              return td(this, t);
            },
          },
          {
            key: "intersectsCircle",
            value: function (t) {
              return td(this, t);
            },
          },
          {
            key: "intersectsPolygon",
            value: function (t) {
              return td(this, t);
            },
          },
          {
            key: "intersectsGeoPolygon",
            value: function (t) {
              return td(this, t);
            },
          },
          {
            key: "resolveLocalTransform",
            value: function () {
              var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : new Tf();
              void 0 !== this.attrs.transform && Lf(this.attrs.transform, t),
                (this.modelViewMatrix = t.clone());
            },
          },
          {
            key: "resolveGlobalTransform",
            value: function () {
              var t =
                  arguments.length > 0 && void 0 !== arguments[0]
                    ? arguments[0]
                    : new Tf(),
                e = this.ancestors;
              if (e.length > 0)
                for (var n = e.length - 1; n >= 0; n--)
                  e[n].resolveLocalTransform(t), (t = e[n].modelViewMatrix);
              this.resolveLocalTransform(t);
            },
          },
          {
            key: "attr",
            value: function (t) {
              return this.attrs[t];
            },
          },
          {
            key: "equals",
            value: function (t) {
              var e = this.attrs,
                i = Object.keys(e),
                r = t.attrs,
                a = Object.keys(r);
              if (i.length !== a.length) return !1;
              for (var o = 0; o < i.length; o++) {
                var s = i[o];
                if (!Object.hasOwnProperty.call(r, s)) return !1;
                if (e[s] !== r[s]) return !1;
              }
              return W(T(n.prototype), "equals", this).call(this, t);
            },
          },
          {
            key: "toJSON",
            value: function () {
              var t = W(T(n.prototype), "toJSON", this).call(this);
              return (t.attrs = this.attrs), t;
            },
          },
          {
            key: "attrs",
            get: function () {
              return this._attrs;
            },
          },
          {
            key: "stage",
            get: function () {
              return (
                this._parent && !this._stage
                  ? (this._stage = this._parent.stage)
                  : this._parent ||
                    this._stage === this ||
                    (this._stage = null),
                this._stage
              );
            },
          },
          {
            key: "modelViewMatrix",
            get: function () {
              return this._mvm;
            },
            set: function (t) {
              (this._mvm = t), (this._imvm = null);
            },
          },
          {
            key: "inverseModelViewMatrix",
            get: function () {
              return (
                (this._imvm = this._imvm
                  ? this._imvm
                  : this._mvm.clone().invert()),
                this._imvm
              );
            },
          },
          {
            key: "node",
            get: function () {
              return this._node;
            },
            set: function (t) {
              this._node = t;
            },
          },
          {
            key: "collider",
            get: function () {
              if (null !== this._collider.fn) return this._collider.fn;
              switch (this._collider.type) {
                case "collection":
                  this._collider.fn = (function () {
                    for (
                      var t = arguments.length, e = new Array(t), n = 0;
                      n < t;
                      n++
                    )
                      e[n] = arguments[n];
                    return B(Of, e);
                  })(this._collider.definition);
                  break;
                case "frontChild":
                  return !0;
                case "bounds":
                  this._collider.fn = Ef("rect", this.boundingRect());
                  break;
                case "line":
                case "rect":
                case "circle":
                case "polygon":
                case "geopolygon":
                case "polyline":
                  this._collider.fn = Ef(
                    this._collider.type,
                    this._collider.definition
                  );
                  break;
                default:
                  return null;
              }
              return this._collider.fn;
            },
            set: function (t) {
              var e = Array.isArray(t) ? "collection" : t && t.type;
              return "string" != typeof e
                ? ((this._collider.type = null),
                  (this._collider.definition = null),
                  void (this._collider.fn = null))
                : null !== this._collider &&
                  this._collider.type === e &&
                  null !== this._collider.fn
                ? (this._collider.fn.set(t),
                  void (this._collider.definition = t))
                : ((this._collider.type = e),
                  void (this._collider.definition = t));
            },
          },
          {
            key: "colliderType",
            get: function () {
              return this._collider.type;
            },
          },
        ]),
        n
      );
    })(yf),
    nd = (function (t) {
      O(n, t);
      var e = L(n);
      function n() {
        return z(this, n), e.apply(this, arguments);
      }
      return (
        A(n, [
          {
            key: "addChild",
            value: function (t) {
              if (!(t && t instanceof yf))
                throw new TypeError(
                  "Expecting a Node as argument, but got ".concat(t)
                );
              if (t === this) throw new Error("Can not add itself as child!");
              if (
                t._children &&
                t._children.length &&
                this.ancestors.indexOf(t) >= 0
              )
                throw new Error("Can not add an ancestor as child!");
              return (
                t._parent && t._parent !== this && t._parent.removeChild(t),
                this._children.push(t),
                (t._parent = this),
                (t._ancestors = null),
                this
              );
            },
          },
          {
            key: "addChildren",
            value: function (t) {
              var e,
                n = t ? t.length : 0;
              for (e = 0; e < n; e++) this.addChild(t[e]);
              return this;
            },
          },
          {
            key: "removeChild",
            value: function (t) {
              var e = this._children.indexOf(t);
              return (
                e >= 0 &&
                  (this._children.splice(e, 1),
                  (t._parent = null),
                  (t._ancestors = null)),
                this
              );
            },
          },
          {
            key: "removeChildren",
            value: function (t) {
              var e, n;
              if (!this._children) return this;
              if (t)
                for (n = t.length, e = 0; e < n; e++) this.removeChild(t[e]);
              else
                for (; this._children.length; )
                  this.removeChild(this._children[0]);
              return this;
            },
          },
        ]),
        n
      );
    })(yf),
    id = nd.prototype,
    rd = (function (t) {
      O(n, t);
      var e = L(n);
      function n() {
        var t,
          i =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
        z(this, n);
        var r = i.type,
          a = void 0 === r ? "container" : r;
        return (t = e.call(this, a)).set(i), t;
      }
      return (
        A(n, [
          {
            key: "set",
            value: function () {
              var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {};
              W(T(n.prototype), "set", this).call(this, t);
              var e = t.collider,
                i = k({ type: null }, e);
              (this.collider = i),
                (this.__boundingRect = { true: null, false: null }),
                (this.__bounds = { true: null, false: null });
            },
          },
          {
            key: "appendChildRect",
            value: function (t, e) {
              if (void 0 !== t.bounds) {
                var n = this.__boundingRect[e] || {},
                  i = V(t.bounds(e), 3),
                  r = i[0],
                  a = i[2],
                  o = r.x,
                  s = r.y,
                  c = a.x,
                  u = a.y,
                  l = isNaN(n.width) ? c : Math.max(c, n.width + n.x),
                  h = isNaN(n.height) ? u : Math.max(u, n.height + n.y);
                (n.x = isNaN(n.x) ? o : Math.min(o, n.x)),
                  (n.y = isNaN(n.y) ? s : Math.min(s, n.y)),
                  (n.width = l - n.x),
                  (n.height = h - n.y),
                  (this.__boundingRect[e] = n);
              }
            },
          },
          {
            key: "boundingRect",
            value: function () {
              var t =
                arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
              if (null !== this.__boundingRect[t])
                return this.__boundingRect[t];
              for (var e = this.children.length, n = 0; n < e; n++)
                this.appendChildRect(this.children[n], t);
              return (
                (this.__boundingRect[t] = k(
                  { x: 0, y: 0, width: 0, height: 0 },
                  this.__boundingRect[t]
                )),
                this.__boundingRect[t]
              );
            },
          },
          {
            key: "bounds",
            value: function () {
              var t =
                arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
              if (null !== this.__bounds[t]) return this.__bounds[t];
              var e = this.boundingRect(t);
              return (
                (this.__bounds[t] = [
                  { x: e.x, y: e.y },
                  { x: e.x + e.width, y: e.y },
                  { x: e.x + e.width, y: e.y + e.height },
                  { x: e.x, y: e.y + e.height },
                ]),
                this.__bounds[t]
              );
            },
          },
          {
            key: "addChild",
            value: function (t) {
              var e = id.addChild.call(this, t);
              if (this._collider && "bounds" === this._collider.type) {
                this.appendChildRect(t, !0);
                var n = k(
                  { type: "bounds", x: 0, y: 0, width: 0, height: 0 },
                  this.__boundingRect.true
                );
                this.collider = n;
              }
              return e;
            },
          },
          {
            key: "addChildren",
            value: function (t) {
              var e = id.addChildren.call(this, t),
                n = t.length;
              if (this._collider && "bounds" === this._collider.type && n > 0) {
                for (var i = 0; i < n; i++) this.appendChildRect(t[i], !0);
                var r = k(
                  { type: "bounds", x: 0, y: 0, width: 0, height: 0 },
                  this.__boundingRect.true
                );
                this.collider = r;
              }
              return e;
            },
          },
          {
            key: "removeChild",
            value: function (t) {
              t._stage = null;
              var e,
                n = t.descendants,
                i = n ? n.length : 0;
              for (e = 0; e < i; e++) n[e]._stage = null;
              if (
                (id.removeChild.call(this, t),
                this._collider && "bounds" === this._collider.type)
              ) {
                (this.__boundingRect = { true: null, false: null }),
                  (this.__bounds = { true: null, false: null });
                var r = k(this.boundingRect(!0), { type: "bounds" });
                this.collider = r;
              }
              return this;
            },
          },
          {
            key: "removeChildren",
            value: function (t) {
              if (
                (id.removeChildren.call(this, t),
                this._collider && "bounds" === this._collider.type)
              ) {
                (this.__boundingRect = { true: null, false: null }),
                  (this.__bounds = { true: null, false: null });
                var e = k(this.boundingRect(!0), { type: "bounds" });
                this.collider = e;
              }
              return this;
            },
          },
        ]),
        n
      );
    })(ed);
  function ad() {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(rd, e);
  }
  var od = (function (t) {
    O(n, t);
    var e = L(n);
    function n(t) {
      var i;
      return (
        z(this, n),
        ((i = e.call(this, "stage"))._stage = D(i)),
        (i._dpiRatio = t || 1),
        i
      );
    }
    return (
      A(n, [
        {
          key: "dpi",
          get: function () {
            return this._dpiRatio;
          },
        },
      ]),
      n
    );
  })(rd);
  var sd = nd.prototype,
    cd = ["x1", "x2", "y1", "y2", "id", "offset", "style"],
    ud = (function (t) {
      O(n, t);
      var e = L(n);
      function n() {
        var t,
          i =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
        z(this, n);
        var r = i.type,
          a = void 0 === r ? "container" : r;
        return (t = e.call(this, a)).set(i), (t._boundingRect = {}), t;
      }
      return (
        A(n, [
          {
            key: "set",
            value: function () {
              var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {};
              W(T(n.prototype), "set", this).call(this, t);
              for (
                var e = this.attrs, i = "", r = 0, a = cd.length;
                r !== a;
                r++
              )
                void 0 !== t[(i = cd[r])] && (e[i] = t[i]);
            },
          },
          {
            key: "addChild",
            value: function (t) {
              return sd.addChild.call(this, t);
            },
          },
          {
            key: "addChildren",
            value: function (t) {
              return sd.addChildren.call(this, t);
            },
          },
          {
            key: "removeChild",
            value: function (t) {
              t._stage = null;
              var e,
                n = t.descendants,
                i = n ? n.length : 0;
              for (e = 0; e < i; e++) n[e]._stage = null;
              return sd.removeChild.call(this, t), this;
            },
          },
          {
            key: "removeChildren",
            value: function (t) {
              return sd.removeChildren.call(this, t), this;
            },
          },
        ]),
        n
      );
    })(ed);
  function ld() {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(ud, e);
  }
  var hd = nd.prototype,
    fd = ["patternUnits", "x", "y", "width", "height", "id"],
    dd = (function (t) {
      O(n, t);
      var e = L(n);
      function n() {
        var t,
          i =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
        z(this, n);
        var r = i.type,
          a = void 0 === r ? "container" : r;
        return (t = e.call(this, a)).set(i), (t._boundingRect = {}), t;
      }
      return (
        A(n, [
          {
            key: "set",
            value: function () {
              var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {};
              W(T(n.prototype), "set", this).call(this, t);
              for (
                var e = this.attrs, i = "", r = 0, a = fd.length;
                r !== a;
                r++
              )
                void 0 !== t[(i = fd[r])] && (e[i] = t[i]);
            },
          },
          {
            key: "addChild",
            value: function (t) {
              return hd.addChild.call(this, t);
            },
          },
          {
            key: "addChildren",
            value: function (t) {
              return hd.addChildren.call(this, t);
            },
          },
          {
            key: "removeChild",
            value: function (t) {
              t._stage = null;
              var e,
                n = t.descendants,
                i = n ? n.length : 0;
              for (e = 0; e < i; e++) n[e]._stage = null;
              return hd.removeChild.call(this, t), this;
            },
          },
          {
            key: "removeChildren",
            value: function (t) {
              return hd.removeChildren.call(this, t), this;
            },
          },
        ]),
        n
      );
    })(ed);
  var gd = (function (t) {
    O(n, t);
    var e = L(n);
    function n() {
      var t, i;
      return (
        z(this, n), (t = i = e.call(this, "rect")).set.apply(t, arguments), i
      );
    }
    return (
      A(n, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.x,
              i = void 0 === e ? 0 : e,
              r = t.y,
              a = void 0 === r ? 0 : r,
              o = t.width,
              s = void 0 === o ? 0 : o,
              c = t.height,
              u = void 0 === c ? 0 : c,
              l = t.rx,
              h = void 0 === l ? 0 : l,
              f = t.ry,
              d = void 0 === f ? 0 : f,
              g = t.collider,
              p = k({ type: "rect", x: i, y: a, width: s, height: u }, g);
            W(T(n.prototype), "set", this).call(this, t),
              s >= 0
                ? ((this.attrs.x = i), (this.attrs.width = s))
                : ((this.attrs.x = i + s), (this.attrs.width = -s)),
              u >= 0
                ? ((this.attrs.y = a), (this.attrs.height = u))
                : ((this.attrs.y = a + u), (this.attrs.height = -u)),
              h > 0 && (this.attrs.rx = h),
              d > 0 && (this.attrs.ry = d),
              (this.collider = p),
              (this.__boundingRect = { true: null, false: null }),
              (this.__bounds = { true: null, false: null });
          },
        },
        {
          key: "boundingRect",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__boundingRect[t]) return this.__boundingRect[t];
            var e = J(this.attrs),
              n =
                t && this.modelViewMatrix
                  ? this.modelViewMatrix.transformPoints(e)
                  : e,
              i = G(n),
              r = V(i, 4),
              a = r[0],
              o = r[1],
              s = r[2],
              c = r[3];
            return (
              (this.__boundingRect[t] = {
                x: a,
                y: o,
                width: s - a,
                height: c - o,
              }),
              this.__boundingRect[t]
            );
          },
        },
        {
          key: "bounds",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__bounds[t]) return this.__bounds[t];
            var e = this.boundingRect(t);
            return (
              (this.__bounds[t] = [
                { x: e.x, y: e.y },
                { x: e.x + e.width, y: e.y },
                { x: e.x + e.width, y: e.y + e.height },
                { x: e.x, y: e.y + e.height },
              ]),
              this.__bounds[t]
            );
          },
        },
      ]),
      n
    );
  })(ed);
  var pd = (function (t) {
    O(n, t);
    var e = L(n);
    function n() {
      var t, i;
      return (
        z(this, n), (t = i = e.call(this, "circle")).set.apply(t, arguments), i
      );
    }
    return (
      A(n, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.cx,
              i = void 0 === e ? 0 : e,
              r = t.cy,
              a = void 0 === r ? 0 : r,
              o = t.r,
              s = void 0 === o ? 0 : o,
              c = t.collider,
              u = k({ type: "circle", cx: i, cy: a, r: s }, c);
            W(T(n.prototype), "set", this).call(this, t),
              (this.attrs.cx = i),
              (this.attrs.cy = a),
              (this.attrs.r = s),
              (this.collider = u),
              (this.__boundingRect = { true: null, false: null }),
              (this.__bounds = { true: null, false: null });
          },
        },
        {
          key: "boundingRect",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__boundingRect[t]) return this.__boundingRect[t];
            var e = this.bounds(t);
            return (
              (this.__boundingRect[t] = {
                x: e[0].x,
                y: e[0].y,
                width: e[2].x - e[0].x,
                height: e[2].y - e[0].y,
              }),
              this.__boundingRect[t]
            );
          },
        },
        {
          key: "bounds",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__bounds[t]) return this.__bounds[t];
            var e = this.attrs,
              n = e.cx,
              i = e.cy,
              r = e.r,
              a = e.r,
              o = n - r,
              s = i - a,
              c = 2 * r,
              u = 2 * a,
              l = [
                { x: o, y: s },
                { x: o + c, y: s },
                { x: o + c, y: s + u },
                { x: o, y: s + u },
              ];
            if (t && this.modelViewMatrix) {
              var h = G((l = this.modelViewMatrix.transformPoints(l))),
                f = V(h, 4),
                d = f[0],
                g = f[1],
                p = f[2],
                y = f[3];
              (c = p - d),
                (u = y - g),
                (this.__bounds[t] = [
                  { x: d, y: g },
                  { x: d + c, y: g },
                  { x: d + c, y: g + u },
                  { x: d, y: g + u },
                ]);
            } else this.__bounds[t] = l;
            return this.__bounds[t];
          },
        },
      ]),
      n
    );
  })(ed);
  var yd = (function (t) {
    O(n, t);
    var e = L(n);
    function n() {
      var t, i;
      return (
        z(this, n), (t = i = e.call(this, "line")).set.apply(t, arguments), i
      );
    }
    return (
      A(n, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.x1,
              i = void 0 === e ? 0 : e,
              r = t.y1,
              a = void 0 === r ? 0 : r,
              o = t.x2,
              s = void 0 === o ? 0 : o,
              c = t.y2,
              u = void 0 === c ? 0 : c,
              l = t.collider;
            W(T(n.prototype), "set", this).call(this, t),
              (this.attrs.x1 = i),
              (this.attrs.y1 = a),
              (this.attrs.x2 = s),
              (this.attrs.y2 = u);
            var h = { type: "line", x1: i, y1: a, x2: s, y2: u };
            (this.collider = k(h, l)),
              (this.__boundingRect = { true: null, false: null }),
              (this.__bounds = { true: null, false: null });
          },
        },
        {
          key: "boundingRect",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__boundingRect[t]) return this.__boundingRect[t];
            var e = Z(this.attrs);
            t &&
              this.modelViewMatrix &&
              (e = this.modelViewMatrix.transformPoints(e));
            var n = G(e),
              i = V(n, 4),
              r = i[0],
              a = i[1],
              o = i[2],
              s = i[3],
              c = r !== o || a !== s;
            return (
              (this.__boundingRect[t] = {
                x: r,
                y: a,
                width: c ? Math.max(1, o - r) : 0,
                height: c ? Math.max(1, s - a) : 0,
              }),
              this.__boundingRect[t]
            );
          },
        },
        {
          key: "bounds",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__bounds[t]) return this.__bounds[t];
            var e = this.boundingRect(t);
            return (
              (this.__bounds[t] = [
                { x: e.x, y: e.y },
                { x: e.x + e.width, y: e.y },
                { x: e.x + e.width, y: e.y + e.height },
                { x: e.x, y: e.y + e.height },
              ]),
              this.__bounds[t]
            );
          },
        },
      ]),
      n
    );
  })(ed);
  var vd = Math.PI / 2;
  function md(t, e) {
    var n = Math.atan2(e.y - t.y, e.x - t.x);
    return n < 0 ? n + 2 * Math.PI : n;
  }
  function xd(t, e, n) {
    return { x: t.x + Math.cos(e) * n, y: t.y + Math.sin(e) * n };
  }
  function bd(t, e) {
    var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
      i = [],
      r = [];
    if ("h" === n.forceOrientation) {
      var a = t[0].x < t[1].x ? -1 : 1,
        o = t[t.length - 1].x > t[t.length - 2].x ? 1 : -1;
      t.unshift({ x: t[0].x + a, y: t[0].y }),
        t.push({ x: t[t.length - 1].x + o, y: t[t.length - 1].y });
    } else if ("v" === n.forceOrientation) {
      var s = t[0].y < t[1].y ? -1 : 1,
        c = t[t.length - 1].y > t[t.length - 2].y ? 1 : -1;
      t.unshift({ x: t[0].x, y: t[0].y + s }),
        t.push({ x: t[t.length - 1].x, y: t[t.length - 1].y + c });
    }
    for (var u = t.length - 1, l = 1; l < u; l++) {
      var h = t[l - 1],
        f = t[l],
        d = t[l + 1],
        g = md(f, h),
        p = md(f, d),
        y = (g + p) / 2,
        v = y + Math.PI,
        m = Math.max(y, v),
        x = Math.min(y, v),
        b = g > p;
      if (1 === l) {
        var _ = md(h, f);
        i.push(xd(h, _ - vd, e)), r.unshift(xd(h, _ + vd, e));
      }
      var w = b ? m : x,
        k = b ? x : m;
      if ((i.push(xd(f, w, e)), r.unshift(xd(f, k, e)), l === u - 1)) {
        var M = md(d, f);
        i.push(xd(d, M + vd, e)), r.unshift(xd(d, M - vd, e));
      }
    }
    return { type: "polygon", vertices: [].concat(i, r) };
  }
  function _d(t) {
    for (var e, n = [], i = t.length, r = 0; r < i; r++) {
      e = t[r];
      for (var a = 0; a < e.length; a++) n.push(e[a]);
    }
    return n;
  }
  function wd(t) {
    if (t.length < 2) return !1;
    var e = t[0],
      n = t[t.length - 1];
    return Math.abs(e.x - n.x) < 1e-12 && Math.abs(e.y - n.y) < 1e-12;
  }
  var kd = (function (t) {
    O(n, t);
    var e = L(n);
    function n() {
      var t, i;
      return (
        z(this, n), (t = i = e.call(this, "path")).set.apply(t, arguments), i
      );
    }
    return (
      A(n, [
        {
          key: "set",
          value: function () {
            var t = this,
              e =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {};
            if (
              (W(T(n.prototype), "set", this).call(this, e),
              (this.segments = []),
              (this.points = []),
              (this.attrs.d = e.d),
              (this.__boundingRect = { true: null, false: null }),
              (this.__bounds = { true: null, false: null }),
              Array.isArray(e.collider) ||
                ("object" === N(e.collider) && void 0 !== e.collider.type))
            )
              this.collider = e.collider;
            else if (e.d) {
              if (
                ((this.segments = df(e.d)),
                this.segments.length > 1 &&
                  this.segments.every(function (t) {
                    return wd(t);
                  }))
              )
                return void (this.collider = k(
                  { type: "geopolygon", vertices: this.segments },
                  e.collider
                ));
              this.segments.forEach(function (n) {
                if (n.length <= 1);
                else if (wd(n))
                  t.collider = k({ type: "polygon", vertices: n }, e.collider);
                else if ("object" === N(e.collider) && e.collider.visual) {
                  var i = t.attrs["stroke-width"] / 2;
                  t.collider = bd(n, i, e.collider);
                } else
                  t.collider = k({ type: "polyline", points: n }, e.collider);
              });
            }
          },
        },
        {
          key: "boundingRect",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__boundingRect[t]) return this.__boundingRect[t];
            this.points.length ||
              ((this.segments = this.segments.length
                ? this.segments
                : df(this.attrs.d)),
              (this.points = _d(this.segments)));
            var e =
                t && this.modelViewMatrix
                  ? this.modelViewMatrix.transformPoints(this.points)
                  : this.points,
              n = G(e),
              i = V(n, 4),
              r = i[0],
              a = i[1],
              o = i[2],
              s = i[3];
            return (
              (this.__boundingRect[t] = {
                x: r || 0,
                y: a || 0,
                width: o - r || 0,
                height: s - a || 0,
              }),
              this.__boundingRect[t]
            );
          },
        },
        {
          key: "bounds",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__bounds[t]) return this.__bounds[t];
            var e = this.boundingRect(t);
            return (
              (this.__bounds[t] = [
                { x: e.x, y: e.y },
                { x: e.x + e.width, y: e.y },
                { x: e.x + e.width, y: e.y + e.height },
                { x: e.x, y: e.y + e.height },
              ]),
              this.__bounds[t]
            );
          },
        },
      ]),
      n
    );
  })(ed);
  function Md(t) {
    var e = t.data,
      n = t._boundingRect,
      i = t._textBoundsFn;
    return null != e && (n || i);
  }
  var Rd = (function (t) {
    O(n, t);
    var e = L(n);
    function n() {
      var t, i;
      return (
        z(this, n), (t = i = e.call(this, "text")).set.apply(t, arguments), i
      );
    }
    return (
      A(n, [
        {
          key: "set",
          value: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.x,
              i = void 0 === e ? 0 : e,
              r = t.y,
              a = void 0 === r ? 0 : r,
              o = t.dx,
              s = void 0 === o ? 0 : o,
              c = t.dy,
              u = void 0 === c ? 0 : c,
              l = t.textBoundsFn,
              h = t.text,
              f = t.title,
              d = t.collider,
              g = t.boundingRect,
              p = t.ellipsed;
            W(T(n.prototype), "set", this).call(this, t),
              (this.attrs.x = i),
              (this.attrs.y = a),
              (this.attrs.dx = s),
              (this.attrs.dy = u),
              (this.attrs.text = h),
              void 0 !== f && (this.attrs.title = String(f)),
              "object" === N(g)
                ? (this._textBoundsFn = function () {
                    return g;
                  })
                : "function" == typeof l && (this._textBoundsFn = l),
              "string" == typeof p && (this.ellipsed = p),
              (this.collider = k({ type: Md(this) ? "bounds" : null }, d)),
              (this.__boundingRect = { true: null, false: null }),
              (this.__bounds = { true: null, false: null });
          },
        },
        {
          key: "boundingRect",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__boundingRect[t]) return this.__boundingRect[t];
            if ("function" != typeof this._textBoundsFn)
              return { x: 0, y: 0, width: 0, height: 0 };
            var e = J(this._textBoundsFn(this.attrs)),
              n =
                t && this.modelViewMatrix
                  ? this.modelViewMatrix.transformPoints(e)
                  : e,
              i = G(n),
              r = V(i, 4),
              a = r[0],
              o = r[1],
              s = r[2],
              c = r[3];
            return (
              (this.__boundingRect[t] = {
                x: a,
                y: o,
                width: s - a,
                height: c - o,
              }),
              this.__boundingRect[t]
            );
          },
        },
        {
          key: "bounds",
          value: function () {
            var t =
              arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            if (null !== this.__bounds[t]) return this.__bounds[t];
            var e = this.boundingRect(t);
            return (
              (this.__bounds[t] = [
                { x: e.x, y: e.y },
                { x: e.x + e.width, y: e.y },
                { x: e.x + e.width, y: e.y + e.height },
                { x: e.x, y: e.y + e.height },
              ]),
              this.__bounds[t]
            );
          },
        },
      ]),
      n
    );
  })(ed);
  var Sd = oe();
  function Nd(t, e) {
    return Sd.get(t)(e);
  }
  Sd.add("rect", function () {
    for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
      e[n] = arguments[n];
    return B(gd, e);
  }),
    Sd.add("circle", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(pd, e);
    }),
    Sd.add("text", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(Rd, e);
    }),
    Sd.add("line", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(yd, e);
    }),
    Sd.add("path", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(kd, e);
    }),
    Sd.add("stage", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(od, e);
    }),
    Sd.add("container", ad),
    Sd.add("defs", ad),
    Sd.add("linearGradient", ld),
    Sd.add("radialGradient", ld),
    Sd.add("stop", ld),
    Sd.add("pattern", function () {
      for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
        e[n] = arguments[n];
      return B(dd, e);
    });
  var zd = (function () {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
      e = [{}];
    function n() {
      return e[e.length - 1];
    }
    return (
      (n.save = function () {
        for (
          var i =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : {},
            r = n(),
            a = {},
            o = "",
            s = 0;
          s < t.length;
          s++
        )
          void 0 !== r[(o = t[s])] && (a[o] = r[o]);
        return k(a, i), e.push(a), n();
      }),
      (n.restore = function () {
        e.splice(e.length - 1, 1);
      }),
      n
    );
  })([
    "stroke",
    "fill",
    "strokeWidth",
    "opacity",
    "fontFamily",
    "fontSize",
    "baseline",
  ]);
  function Cd(t, e) {
    if (Array.isArray(e)) for (var n = 0, i = e.length; n < i; n++) e[n](t);
  }
  function Ad(t, e, n) {
    (t.node = n[e]), (t.index = e);
  }
  function Ed(t, e, n, i) {
    for (
      var r = { siblings: t, node: null, index: 0 }, a = 0, o = t.length;
      a < o;
      a++
    )
      if (
        (Ad(r, a, t),
        Cd(r, i.create),
        !("function" == typeof r.node.disabled
          ? r.node.disabled()
          : r.node.disabled))
      ) {
        r.node = zd.save(r.node);
        var s = Nd(r.node.type, r.node);
        s &&
          (r.node.transform && (n.save(), Lf(r.node.transform, n)),
          n.isIdentity() || (s.modelViewMatrix = n.clone()),
          e.addChild(s),
          r.node.children && Ed(r.node.children, s, n, i),
          r.node.transform && n.restore()),
          zd.restore();
      }
  }
  function Od(t) {
    var e = t.items,
      n = t.stage,
      i = t.dpi,
      r = t.on,
      a = void 0 === r ? {} : r;
    return n || (n = Nd("stage", i)), Ed(e, n, new Tf(), a), n;
  }
  function Td(t, e, n) {
    var i = n.orientation,
      r = n.degree,
      a = n.stops,
      o = void 0 === a ? [] : a,
      s = null;
    if ("radial" === i) {
      var c = e.boundingRect();
      s = t.createRadialGradient(
        c.x + c.width / 2,
        c.y + c.height / 2,
        1e-5,
        c.x + c.width / 2,
        c.y + c.height / 2,
        Math.max(c.width, c.height) / 2
      );
    } else {
      var u = zo(r);
      ["x1", "x2", "y1", "y2"].forEach(function (t) {
        t in n && (u[t] = n[t]);
      });
      var l = e.boundingRect();
      s = t.createLinearGradient(
        l.x + u.x1 * l.width,
        l.y + u.y1 * l.height,
        l.x + u.x2 * l.width,
        l.y + u.y2 * l.height
      );
    }
    for (var h = 0, f = o.length; h < f; h++) {
      var d = o[h];
      s.addColorStop(d.offset, d.color);
    }
    return s;
  }
  function jd(t, e, n) {
    return (
      (e.width = t.width),
      (e.height = t.height),
      n.save(),
      (n.fillStyle = t.fill),
      t.shapes.forEach(function (t) {
        if ("rect" === t.type) n.rect(t.x, t.y, t.width, t.height);
      }),
      n.fill(),
      n.restore(),
      n.createPattern(e, "repeat")
    );
  }
  function Pd(t) {
    var e = t.createElement("canvas"),
      n = e.getContext("2d"),
      i = {};
    return {
      create: function (t) {
        var r = t.key;
        return r ? ((i[r] = i[r] || jd(t, e, n)), i[r]) : jd(t, e, n);
      },
      clear: function () {
        i = {};
      },
    };
  }
  function Bd() {
    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      e = t.x,
      n = t.y,
      i = t.width,
      r = t.height,
      a = t.scaleRatio,
      o = t.margin,
      s = t.edgeBleed,
      c = {
        x: 0,
        y: 0,
        width: 0,
        height: 0,
        scaleRatio: { x: 1, y: 1 },
        margin: { left: 0, top: 0 },
        edgeBleed: { left: 0, right: 0, top: 0, bottom: 0, bool: !1 },
      };
    return (
      (c.x = isNaN(e) ? c.x : e),
      (c.y = isNaN(n) ? c.y : n),
      (c.width = isNaN(i) ? c.width : i),
      (c.height = isNaN(r) ? c.height : r),
      void 0 !== a &&
        ((c.scaleRatio.x = isNaN(a.x) ? c.scaleRatio.x : a.x),
        (c.scaleRatio.y = isNaN(a.y) ? c.scaleRatio.y : a.y)),
      void 0 !== o &&
        ((c.margin.left = isNaN(o.left) ? 0 : o.left),
        (c.margin.top = isNaN(o.top) ? 0 : o.top)),
      "object" === N(s) &&
        ["left", "right", "top", "bottom"].forEach(function (t) {
          !isNaN(s[t]) &&
            s[t] > 0 &&
            ((c.edgeBleed[t] = s[t]), (c.edgeBleed.bool = !0));
        }),
      (c.computedPhysical = {
        x: Math.round(
          c.margin.left + (c.x - c.edgeBleed.left) * c.scaleRatio.x
        ),
        y: Math.round(c.margin.top + (c.y - c.edgeBleed.top) * c.scaleRatio.y),
        width: Math.round(
          (c.width + c.edgeBleed.left + c.edgeBleed.right) * c.scaleRatio.x
        ),
        height: Math.round(
          (c.height + c.edgeBleed.top + c.edgeBleed.bottom) * c.scaleRatio.y
        ),
      }),
      c
    );
  }
  function Dd() {
    var t = {
      element: function () {},
      root: function () {},
      settings: function () {},
      appendTo: function () {},
      getScene: function () {
        return [];
      },
      render: function () {
        return !1;
      },
      itemsAt: function () {
        return [];
      },
      findShapes: function () {
        return [];
      },
      clear: function () {},
      destory: function () {},
      size: function () {},
      measureText: rh,
      textBounds: oh,
      setKey: function (e) {
        t.element().setAttribute("data-key", e);
      },
    };
    return t;
  }
  function Fd(t) {
    return function (e) {
      var n = e.node;
      "text" !== n.type ||
        null == n.data ||
        n.textBoundsFn ||
        (n.textBoundsFn = t.textBounds);
    };
  }
  var Ld = 200,
    Id = 200,
    Wd = (function () {
      function t(e) {
        z(this, t),
          (this.targetCanvas = e),
          (this.bufferCanvas = e.cloneNode());
      }
      return (
        A(t, [
          {
            key: "updateSize",
            value: function (t) {
              var e,
                n = t.rect,
                i = t.dpiRatio,
                r = t.canvasBufferSize;
              (e = r
                ? "function" == typeof r
                  ? r(n)
                  : r
                : {
                    width: n.computedPhysical.width + 2 * Ld,
                    height: n.computedPhysical.height + 2 * Id,
                  }),
                (this.bufferCanvas.style.width = "".concat(e.width, "px")),
                (this.bufferCanvas.style.height = "".concat(e.height, "px")),
                (this.bufferCanvas.width = Math.round(e.width * i)),
                (this.bufferCanvas.height = Math.round(e.height * i));
            },
          },
          {
            key: "apply",
            value: function () {
              this.targetCanvas
                .getContext("2d")
                .drawImage(this.bufferCanvas, 0, 0);
            },
          },
          {
            key: "clear",
            value: function () {
              this.bufferCanvas.width = this.bufferCanvas.width;
            },
          },
          {
            key: "getContext",
            value: function () {
              return this.bufferCanvas.getContext("2d");
            },
          },
        ]),
        t
      );
    })(),
    Vd = oe();
  function $d(t) {
    return Array.isArray(t)
      ? t
      : "string" == typeof t
      ? -1 !== t.indexOf(",")
        ? t.split(",")
        : t.split(" ")
      : [];
  }
  function Hd(t) {
    return (
      ("undefined" == typeof window ? 1 : window.devicePixelRatio || 1) /
      (t.webkitBackingStorePixelRatio ||
        t.mozBackingStorePixelRatio ||
        t.msBackingStorePixelRatio ||
        t.oBackingStorePixelRatio ||
        t.backingStorePixelRatio ||
        1)
    );
  }
  function qd(t, e) {
    e.setTransform(t[0][0], t[1][0], t[0][1], t[1][1], t[0][2], t[1][2]);
  }
  function Ud(t, e, n) {
    for (
      var i =
          arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {},
        r = Object.keys(i),
        a = 0,
        o = n.length;
      a < o;
      a++
    ) {
      var s = n[a],
        c = s[0],
        u = s[1],
        l = s[2];
      if (c in e.attrs && !(u in i) && t[u] !== e.attrs[c]) {
        var h = l ? l(e.attrs[c]) : e.attrs[c];
        "function" == typeof t[u] ? t[u](h) : (t[u] = h);
      }
    }
    for (var f = 0, d = r.length; f < d; f++) {
      var g = r[f];
      t[g] = i[g];
    }
  }
  function Yd(t, e, n, i) {
    for (var r = 0, a = t.length; r < a; r++) {
      var o = t[r],
        s = {};
      e.save(),
        o.attrs &&
          (o.attrs.fill || o.attrs.stroke) &&
          (o.attrs.fill &&
          "object" === N(o.attrs.fill) &&
          "gradient" === o.attrs.fill.type
            ? (s.fillStyle = Td(e, o, o.attrs.fill))
            : o.attrs.fill &&
              "object" === N(o.attrs.fill) &&
              "pattern" === o.attrs.fill.type &&
              (s.fillStyle = i.patterns.create(o.attrs.fill)),
          o.attrs.stroke &&
          "object" === N(o.attrs.stroke) &&
          "gradient" === o.attrs.stroke.type
            ? (s.strokeStyle = Td(e, o, o.attrs.stroke))
            : o.attrs.stroke &&
              "object" === N(o.attrs.stroke) &&
              "pattern" === o.attrs.stroke.type &&
              (s.strokeStyle = i.patterns.create(o.attrs.stroke))),
        Ud(e, o, n, s),
        o.modelViewMatrix && qd(o.modelViewMatrix.elements, e),
        Vd.has(o.type) &&
          Vd.get(o.type)(o.attrs, {
            g: e,
            doFill: "fill" in o.attrs && "none" !== o.attrs.fill,
            doStroke: "stroke" in o.attrs && 0 !== o.attrs["stroke-width"],
            ellipsed: o.ellipsed,
          }),
        o.children && Yd(o.children, e, n, i),
        e.restore();
    }
  }
  function Xd(t) {
    var e = t.el,
      n = t.dpiRatio,
      i = t.transform;
    if ("object" === N(i)) {
      var r = [
          i.horizontalScaling,
          i.horizontalSkewing,
          i.verticalSkewing,
          i.verticalScaling,
          i.horizontalMoving * n,
          i.verticalMoving * n,
        ],
        a = e.getContext("2d");
      a.setTransform.apply(a, r);
    }
  }
  function Gd() {
    var t,
      e,
      n,
      i,
      r = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : Od,
      a = { transform: void 0, canvasBufferSize: void 0 },
      o = !1,
      s = Bd(),
      c = [
        ["fill", "fillStyle"],
        ["stroke", "strokeStyle"],
        ["opacity", "globalAlpha"],
        ["globalAlpha", "globalAlpha"],
        ["stroke-width", "lineWidth"],
        ["stroke-linejoin", "lineJoin"],
        ["stroke-dasharray", "setLineDash", $d],
      ],
      u = Dd();
    return (
      (u.element = function () {
        return t;
      }),
      (u.root = function () {
        return t;
      }),
      (u.settings = function (t) {
        return (
          t &&
            Object.keys(a).forEach(function (e) {
              void 0 !== t[e] && (a[e] = t[e]);
            }),
          a
        );
      }),
      (u.appendTo = function (n) {
        return (
          t ||
            (((t = n.ownerDocument.createElement("canvas")).style.position =
              "absolute"),
            (t.style["-webkit-font-smoothing"] = "antialiased"),
            (t.style["-moz-osx-font-smoothing"] = "antialiased"),
            (t.style.pointerEvents = "none")),
          "function" != typeof a.transform || e || (e = new Wd(t)),
          n.appendChild(t),
          t
        );
      }),
      (u.getScene = function (n) {
        var i = Hd((e && e.getContext()) || t.getContext("2d")),
          a = s.scaleRatio.x,
          o = s.scaleRatio.y,
          c = {
            type: "container",
            children: n,
            transform: s.edgeBleed.bool
              ? "translate("
                  .concat(s.edgeBleed.left * i * a, ", ")
                  .concat(s.edgeBleed.top * i * o, ")")
              : "",
          };
        return (
          (1 === i && 1 === a && 1 === o) ||
            (c.transform += "scale(".concat(i * a, ", ").concat(i * o, ")")),
          r({ items: [c], dpi: i, on: { create: [Gl(u.measureText), Fd(u)] } })
        );
      }),
      (u.render = function (r) {
        if (!t) return !1;
        i || (i = Pd(t.ownerDocument));
        var l = (e && e.getContext()) || t.getContext("2d"),
          h = Hd(l),
          f = e && a.transform();
        if (f)
          return (
            (t.width = t.width),
            Xd({ el: t, dpiRatio: h, transform: f }),
            e.apply(),
            !0
          );
        o &&
          ((t.style.left = "".concat(s.computedPhysical.x, "px")),
          (t.style.top = "".concat(s.computedPhysical.y, "px")),
          (t.style.width = "".concat(s.computedPhysical.width, "px")),
          (t.style.height = "".concat(s.computedPhysical.height, "px")),
          (t.width = Math.round(s.computedPhysical.width * h)),
          (t.height = Math.round(s.computedPhysical.height * h)),
          e &&
            e.updateSize({
              rect: s,
              dpiRatio: h,
              canvasBufferSize: a.canvasBufferSize,
            }));
        var d = u.getScene(r),
          g = !n || !d.equals(n);
        i.clear();
        var p = o || g;
        return (
          p && (u.clear(), Yd(d.children, l, c, { patterns: i })),
          e && ((t.width = t.width), e.apply()),
          (o = !1),
          (n = d),
          p
        );
      }),
      (u.itemsAt = function (t) {
        return n ? n.getItemsFrom(t) : [];
      }),
      (u.findShapes = function (t) {
        return n ? n.findShapes(t) : [];
      }),
      (u.clear = function () {
        return t && (t.width = t.width), e && e.clear(), (n = null), u;
      }),
      (u.size = function (t) {
        if (t) {
          var e = Bd(t);
          JSON.stringify(s) !== JSON.stringify(e) && ((o = !0), (s = e));
        }
        return s;
      }),
      (u.destroy = function () {
        t && (t.parentElement && t.parentElement.removeChild(t), (t = null)),
          e && (e = null),
          (n = null);
      }),
      u
    );
  }
  function Zd(t, e) {
    Vd.add(t, e);
  }
  function Jd(t, e) {
    return Math.max(0, Math.min(t, e));
  }
  var Kd = new RegExp(
      "[A-Za-zªµºÀ-ÖØ-öø-ʸʻ-ˁː-ˑˠ-ˤˮͰ-ͳͶ-ͽΆΈ-ϵϷ-҂Ҋ-։ः-हऻऽ-ीॉ-ौॎ-ॐक़-ॡ।-ॿং-হঽ-ীে-ৌৎ-ৡ০-ৱ৴-৺ਃ-ਹਾ-ੀਖ਼-੯ੲ-ੴઃ-હઽ-ીૉ-ૌૐ-ૡ૦-૯ଂ-ହଽ-ାୀେ-ୌୗ-ୡ୦-୷ஃ-ிு-ௌௐ-௲ఁ-ఽు-ౄౘ-ౡ౦-౯౿-ಹಽ-ೋೕ-ೡ೦-ീെ-ൌൎ-ൡ൦-ෆා-ෑෘ-ะา-ำเ-ๆ๏-ະາ-ຳຽ-ໆ໐-༗༚-༴༶༸༾-ཬཿ྅ྈ-ྌ྾-࿅࿇-ာေးျ-ြဿ-ၗၚ-ၝၡ-ၰၵ-ႁႃ-ႄႇ-ႌႎ-ႜ႞-ፚ፠-ᎏᎠ-Ᏼᐁ-ᙿᚁ-ᚚᚠ-ᜑᜠ-ᜱ᜵-ᝑᝠ-ᝰក-ាើ-ៅះ-ៈ។-៚ៜ០-៩᠐-ᢨᢪ-ᤜᤣ-ᤦᤩ-ᤱᤳ-ᤸ᥆-᧚ᨀ-ᨖᨙ-ᩕᩗᩡᩣ-ᩤᩭ-ᩲ᪀-᪭ᬄ-ᬳᬵᬻᬽ-ᭁᭃ-᭪᭴-᭼ᮂ-ᮡᮦ-ᮧ᮪-ᯥᯧᯪ-ᯬᯮ᯲-ᰫᰴ-ᰵ᰻-᱿᳓᳡ᳩ-ᳬᳮ-ᶿḀ-ᾼιῂ-ῌῐ-Ίῠ-Ῥῲ-ῼ‎ⁱⁿₐ-ₜℂℇℊ-ℓℕℙ-ℝℤΩℨK-ℭℯ-ℹℼ-ℿⅅ-ⅉⅎ-⅏Ⅰ-ↈ⌶-⍺⎕⒜-ⓩ⚬⠀-⣿Ⰰ-ⳤⳫ-ⳮⴀ-⵰ⶀ-ⷞ々-〇〡-〩〱-〵〸-〼ぁ-ゖゝ-ゟァ-ヺー-ㆺㇰ-㈜㈠-㉏㉠-㉻㉿-㊰㋀-㋋㋐-㍶㍻-㏝㏠-㏾㐀-䶵一-ꒌꓐ-ꘌꘐ-ꙮꚀ-ꛯ꛲-꛷Ꜣ-ꞇ꞉-ꠁꠃ-ꠅꠇ-ꠊꠌ-ꠤꠧ꠰-꠷ꡀ-ꡳꢀ-ꣃ꣎-꣙ꣲ-ꤥ꤮-ꥆꥒ-ꥼꦃ-ꦲꦴ-ꦵꦺ-ꦻꦽ-ꨨꨯ-ꨰꨳ-ꨴꩀ-ꩂꩄ-ꩋꩍ-ꪯꪱꪵ-ꪶꪹ-ꪽꫀꫂ-ꯤꯦ-ꯧꯩ-꯬꯰-ﬗＡ-Ｚａ-ｚｦ-ￜ]"
    ),
    Qd = new RegExp("[־׀׃׆א-״߀-ߪߴ-ߵߺ-ࠕࠚࠤࠨ࠰-ࡘ࡞‏יִײַ-ﬨשׁ-ﭏ]"),
    tg = new RegExp("[؈؋؍؛-ي٭-ٯٱ-ەۥ-ۦۮ-ۯۺ-܍ܐܒ-ܯݍ-ޥޱﭐ-ﴽﵐ-﷼ﹰ-ﻼ]");
  function eg(t) {
    return Kd.test(t);
  }
  function ng(t) {
    return Qd.test(t) || tg.test(t);
  }
  function ig(t) {
    var e,
      n,
      i = t ? t.length : 0;
    for (e = 0; e < i; e++) {
      if (eg((n = t[e]))) return "ltr";
      if (ng(n)) return "rtl";
    }
    return "ltr";
  }
  var rg = { start: "end", end: "start", center: "center", middle: "middle" };
  function ag(t, e) {
    return "rtl" === e ? rg[t] : t;
  }
  function og(t, e) {
    var n,
      i,
      r,
      a = [],
      o = [],
      s = [],
      c = [],
      u = function (t) {
        return t.id;
      },
      l = function (t, e) {
        var n;
        return (
          (n = "object" === N(t) ? ("id" in t ? t.id : e) : t),
          { content: t, id: "".concat(n, "__").concat(t.type || "") }
        );
      };
    t.isTree || (t = t.map(l)), (e = e.map(l)), (i = t.map(u)), (r = e.map(u));
    for (var h = 0, f = e.length; h < f; h++) {
      -1 === i.indexOf(e[h].id) ? a.push(e[h]) : s.push(e[h]);
    }
    for (var d = 0, g = t.length; d < g; d++) {
      -1 === r.indexOf(t[d].id) ? o.push(t[d]) : c.push(t[d]);
    }
    for (var p = 0, y = a.length; p < y; p++)
      a[p].content.children &&
        ((a[p].diff = og([], a[p].content.children)),
        (a[p].children = a[p].diff.updatedNew.concat(a[p].diff.added)),
        (a[p].children.isTree = !0));
    for (var v = 0, m = s.length; v < m; v++)
      (s[v].diff = og(c[v].children || [], s[v].content.children || [])),
        (s[v].object = c[v].object),
        (s[v].children = s[v].diff.items);
    return (
      (n = s.concat(a)),
      (a.isTree = !0),
      (o.isTree = !0),
      (s.isTree = !0),
      (c.isTree = !0),
      (n.isTree = !0),
      { added: a, updatedNew: s, updatedOld: c, removed: o, items: n }
    );
  }
  function sg(t, e, n) {
    for (var i = 0, r = t.length; i < r; i++)
      t[i].object = n(t[i].content.type, e);
  }
  function cg(t, e) {
    for (var n = 0, i = t.length; n < i; n++)
      null !== t[n].object &&
        void 0 !== t[n].object &&
        (e(t[n].object), (t[n].object = null));
  }
  function ug(t, e, n, i) {
    for (var r, a = 0, o = t.length; a < o; a++)
      null !== (r = t[a]).object &&
        void 0 !== r.object &&
        (n(r.object, r.content),
        r.diff &&
          (sg(r.diff.added, r.object, e),
          cg(r.diff.removed, i),
          ug(r.diff.items, e, n, i)));
  }
  function lg(t, e, n, i, r, a) {
    var o = og(t, e);
    return sg(o.added, n, i), cg(o.removed, a), ug(o.items, i, r, a), o.items;
  }
  Zd("rect", function (t, e) {
    var n = e.g,
      i = e.doFill,
      r = e.doStroke;
    n.beginPath(),
      t.rx > 0 || t.ry > 0
        ? (function (t, e, n, i, r, a, o) {
            (a = Jd(i / 2, a > 0 ? a : o)),
              (o = Jd(r / 2, o > 0 ? o : a)),
              t.moveTo(e, n + o),
              t.lineTo(e, n + r - o),
              t.quadraticCurveTo(e, n + r, e + a, n + r),
              t.lineTo(e + i - a, n + r),
              t.quadraticCurveTo(e + i, n + r, e + i, n + r - o),
              t.lineTo(e + i, n + o),
              t.quadraticCurveTo(e + i, n, e + i - a, n),
              t.lineTo(e + a, n),
              t.quadraticCurveTo(e, n, e, n + o);
          })(n, t.x, t.y, t.width, t.height, t.rx, t.ry)
        : n.rect(t.x, t.y, t.width, t.height),
      i && n.fill(),
      r && n.stroke();
  }),
    Zd("circle", function (t, e) {
      var n = e.g,
        i = e.doFill,
        r = e.doStroke;
      n.beginPath(),
        n.moveTo(t.cx + t.r, t.cy),
        n.arc(t.cx, t.cy, t.r, 0, 2 * Math.PI, !1),
        i && n.fill(),
        r && n.stroke();
    }),
    Zd("line", function (t, e) {
      var n = e.g,
        i = e.doStroke;
      n.beginPath(),
        n.moveTo(t.x1, t.y1),
        n.lineTo(t.x2, t.y2),
        i && n.stroke();
    }),
    Zd("path", function (t, e) {
      var n = e.g,
        i = e.doStroke,
        r = e.doFill,
        a = new Path2D(t.d);
      r && n.fill(a), i && n.stroke(a);
    }),
    Zd("text", function (t, e) {
      var n = e.g,
        i = e.ellipsed || Zl(t, rh);
      n.font = "".concat(t["font-size"], " ").concat(t["font-family"]);
      var r = ig(t.text);
      n.canvas.dir !== r && (n.canvas.dir = r);
      var a = ag(
        "middle" === t["text-anchor"] ? "center" : t["text-anchor"],
        n.canvas.dir
      );
      a && n.textAlign !== a && (n.textAlign = a);
      var o = Jl(t);
      n.fillText(i, t.x + t.dx, t.y + t.dy + o);
    });
  var hg = "http://www.w3.org/2000/svg",
    fg = function (t, e) {
      if (!t || "string" != typeof t)
        throw new Error("Invalid type: ".concat(t));
      var n = e.ownerDocument.createElementNS(hg, "container" === t ? "g" : t);
      return e.appendChild(n), n;
    },
    dg = function (t) {
      t.parentNode && t.parentNode.removeChild(t);
    },
    gg = function (t, e) {
      if (
        (function (t) {
          switch (t.type) {
            case "circle":
              return (
                !isNaN(t.attrs.cx) && !isNaN(t.attrs.cy) && !isNaN(t.attrs.r)
              );
            case "line":
              return !(
                isNaN(t.attrs.x1) ||
                isNaN(t.attrs.y1) ||
                isNaN(t.attrs.x2) ||
                isNaN(t.attrs.y2)
              );
            case "rect":
              return !(
                isNaN(t.attrs.x) ||
                isNaN(t.attrs.y) ||
                isNaN(t.attrs.width) ||
                isNaN(t.attrs.height)
              );
            case "text":
              return !isNaN(t.attrs.x) && !isNaN(t.attrs.y);
            default:
              return !0;
          }
        })(e)
      ) {
        for (var n in e.attrs)
          if ("stroke" === n && e.strokeReference)
            t.setAttribute("stroke", e.strokeReference);
          else if ("fill" === n && e.fillReference)
            t.setAttribute("fill", e.fillReference);
          else if ("text" === n) {
            t.setAttribute("style", "white-space: pre"),
              (t.textContent = e.ellipsed || Zl(e.attrs, rh));
            var i = ig(e.attrs.text);
            "rtl" === i &&
              (t.setAttribute("direction", "rtl"),
              t.setAttribute("dir", "rtl"),
              t.setAttribute(
                "text-anchor",
                ag(t.getAttribute("text-anchor"), i)
              ));
          } else if (
            "text" !== e.type ||
            ("dy" !== n && "dominant-baseline" !== n)
          )
            if ("text" === e.type && "title" === n && e.attrs.title) {
              var r = t.ownerDocument.createElementNS(hg, "title");
              (r.textContent = e.attrs.title), t.appendChild(r);
            } else t.setAttribute(n, e.attrs[n]);
          else {
            var a = +t.getAttribute(n) || 0,
              o = 0;
            (o = "dominant-baseline" === n ? Jl(e.attrs) : e.attrs[n]),
              t.setAttribute("dy", o + a);
          }
        if (
          "string" == typeof e.data ||
          "number" == typeof e.data ||
          "boolean" == typeof e.data
        )
          t.setAttribute("data", e.data);
        else if ("object" === N(e.data) && null !== e.data)
          for (var s in e.data)
            ("string" != typeof e.data[s] &&
              "number" != typeof e.data[s] &&
              "boolean" != typeof e.data[s]) ||
              t.setAttribute("data-".concat(s), e.data[s]);
      }
    },
    pg = (function () {
      function t(e, n, i, r) {
        z(this, t),
          (this.create = e),
          (this.nodeCreator = n),
          (this.nodeMaintainer = i),
          (this.nodeDestroyer = r);
      }
      return (
        A(t, [
          {
            key: "render",
            value: function (t, e) {
              return this.create(
                [],
                t,
                e,
                this.nodeCreator,
                this.nodeMaintainer,
                this.nodeDestroyer
              );
            },
          },
        ]),
        t
      );
    })();
  function yg() {
    return new pg(lg, fg, gg, dg);
  }
  function vg(t) {
    var e,
      n,
      i = 0;
    if (0 === (t = JSON.stringify(t)).length) return i;
    for (e = 0, n = t.length; e < n; e++)
      (i = (i << 5) - i + t.charCodeAt(e)), (i &= i);
    return i;
  }
  function mg(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : vg,
      n = {},
      i = Date.now(),
      r = {
        getOrCreateGradient: function () {
          var r =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : {},
            a =
              arguments.length > 1 && void 0 !== arguments[1]
                ? arguments[1]
                : "fill",
            o =
              arguments.length > 2 && void 0 !== arguments[2]
                ? arguments[2]
                : "",
            s = e(r[a]),
            c = "picasso-gradient-".concat(i, "-").concat(s);
          if (!n[s]) {
            var u = r[a],
              l = u.orientation,
              h = u.degree,
              f = u.stops,
              d = void 0 === f ? [] : f,
              g = {};
            void 0 === h && (h = 90),
              "radial" === l
                ? (g.type = "radialGradient")
                : ((g = zo(h)),
                  ["x1", "x2", "y1", "y2"].forEach(function (t) {
                    t in r[a] && (g[t] = r[a][t]);
                  }),
                  (g.type = "linearGradient")),
              (g.id = c),
              (g.children = d.map(function (t) {
                var e = t.offset,
                  n = t.color,
                  i = t.opacity;
                return {
                  type: "stop",
                  offset: "".concat(100 * e, "%"),
                  style: "stop-color:"
                    .concat(n, ";stop-opacity:")
                    .concat(void 0 !== i ? i : 1),
                };
              })),
              t.push(g),
              (n[s] = c);
          }
          return "url('".concat(o, "#").concat(c, "')");
        },
        onCreate: function (t) {
          var e = "";
          "undefined" != typeof window &&
            (e = window.location.href.split("#")[0]);
          var n = t.node;
          n.fill &&
            "object" === N(n.fill) &&
            "gradient" === n.fill.type &&
            (n.fillReference = r.getOrCreateGradient(n, "fill", e)),
            n.stroke &&
              "object" === N(n.stroke) &&
              "gradient" === n.stroke.type &&
              (n.strokeReference = r.getOrCreateGradient(n, "stroke", e));
        },
        clear: function () {
          n = {};
        },
      };
    return r;
  }
  function xg(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : vg,
      n = {},
      i = Date.now(),
      r = {
        onCreate: function (r) {
          var a = {};
          r.node &&
            "object" === N(r.node.fill) &&
            "pattern" === r.node.fill.type &&
            r.node.fill.shapes &&
            (a.fill = r.node.fill),
            r.node &&
              "object" === N(r.node.stroke) &&
              "pattern" === r.node.stroke.type &&
              r.node.stroke.shapes &&
              (a.stroke = r.node.stroke),
            Object.keys(a).forEach(function (o) {
              var s = "",
                c = a[o],
                u = e(c),
                l = "picasso-pattern-".concat(i, "-").concat(u);
              if (
                ("undefined" != typeof window &&
                  (s = window.location.href.split("#")[0]),
                !n[u])
              ) {
                var h = {
                  patternUnits: "userSpaceOnUse",
                  x: 0,
                  y: 0,
                  width: c.width,
                  height: c.height,
                  type: "pattern",
                  id: l,
                  children: [],
                  fill: c.fill,
                };
                c.shapes.forEach(function (t) {
                  h.children.push(t);
                }),
                  t.push(h),
                  (n[u] = !0);
              }
              r.node["".concat(o, "Reference")] = "url('"
                .concat(s, "#")
                .concat(l, "')");
            });
        },
        clear: function () {
          n = {};
        },
      };
    return r;
  }
  function bg() {
    var t,
      e,
      n,
      i = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : yg,
      r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : hg,
      a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : Od,
      o = i(),
      s = !1,
      c = Bd(),
      u = { transform: void 0 },
      l = Dd(),
      h = { type: "defs", children: [] },
      f = xg(h.children),
      d = mg(h.children);
    return (
      (l.element = function () {
        return t;
      }),
      (l.root = function () {
        return e;
      }),
      (l.settings = function (t) {
        return (
          t &&
            Object.keys(u).forEach(function (e) {
              void 0 !== t[e] && (u[e] = t[e]);
            }),
          u
        );
      }),
      (l.appendTo = function (n) {
        return (
          t ||
            (((t = n.ownerDocument.createElementNS(r, "svg")).style.position =
              "absolute"),
            (t.style["-webkit-font-smoothing"] = "antialiased"),
            (t.style["-moz-osx-font-smoothing"] = "antialiased"),
            (t.style.pointerEvents = "none"),
            t.setAttribute("xmlns", r),
            ((e = n.ownerDocument.createElementNS(r, "g")).style.pointerEvents =
              "auto"),
            t.appendChild(e)),
          n.appendChild(t),
          t
        );
      }),
      (l.getScene = function (t) {
        var e = c.scaleRatio.x,
          n = c.scaleRatio.y,
          i = {
            type: "container",
            children: Array.isArray(t) ? [].concat($(t), [h]) : t,
            transform: c.edgeBleed.bool
              ? "translate("
                  .concat(c.edgeBleed.left * e, ", ")
                  .concat(c.edgeBleed.top * n, ")")
              : "",
          };
        return (
          (1 === e && 1 === n) ||
            (i.transform += "scale(".concat(e, ", ").concat(n, ")")),
          a({
            items: [i],
            on: {
              create: [
                function (t) {
                  (t.node.fillReference = void 0),
                    (t.node.strokeReference = void 0);
                },
                d.onCreate,
                f.onCreate,
                Gl(l.measureText),
                Fd(l),
              ],
            },
          })
        );
      }),
      (l.render = function (i) {
        if (!t) return !1;
        var r = "function" == typeof u.transform && u.transform();
        if (r) {
          var a = r.a,
            g = r.b,
            p = r.c,
            y = r.d,
            v = r.e,
            m = r.f;
          return (
            (e.style.transform = "matrix("
              .concat(a, ", ")
              .concat(g, ", ")
              .concat(p, ", ")
              .concat(y, ", ")
              .concat(v, ", ")
              .concat(m, ")")),
            !0
          );
        }
        (e.style.transform = ""),
          s &&
            ((t.style.left = "".concat(c.computedPhysical.x, "px")),
            (t.style.top = "".concat(c.computedPhysical.y, "px")),
            t.setAttribute("width", c.computedPhysical.width),
            t.setAttribute("height", c.computedPhysical.height)),
          d.clear(),
          f.clear(),
          (h.children.length = 0);
        var x = l.getScene(i),
          b = !n || !x.equals(n),
          _ = s || b;
        return _ && (l.clear(), o.render(x.children, e)), (s = !1), (n = x), _;
      }),
      (l.itemsAt = function (t) {
        return n ? n.getItemsFrom(t) : [];
      }),
      (l.findShapes = function (t) {
        return n ? n.findShapes(t) : [];
      }),
      (l.clear = function () {
        if (!e) return l;
        n = null;
        var i = e.cloneNode(!1);
        return t.replaceChild(i, e), (e = i), l;
      }),
      (l.destroy = function () {
        t && t.parentNode && t.parentNode.removeChild(t),
          (t = null),
          (e = null);
      }),
      (l.size = function (t) {
        if (t) {
          var e = Bd(t);
          JSON.stringify(c) !== JSON.stringify(e) && ((s = !0), (c = e));
        }
        return c;
      }),
      l
    );
  }
  var _g,
    wg,
    kg,
    Mg,
    Rg,
    Sg,
    Ng = {},
    zg = [],
    Cg = /acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord|itera/i;
  function Ag(t, e) {
    for (var n in e) t[n] = e[n];
    return t;
  }
  function Eg(t) {
    var e = t.parentNode;
    e && e.removeChild(t);
  }
  function Og(t, e, n) {
    var i,
      r,
      a,
      o = {};
    for (a in e)
      "key" == a ? (i = e[a]) : "ref" == a ? (r = e[a]) : (o[a] = e[a]);
    if (
      (arguments.length > 2 &&
        (o.children = arguments.length > 3 ? _g.call(arguments, 2) : n),
      "function" == typeof t && null != t.defaultProps)
    )
      for (a in t.defaultProps) void 0 === o[a] && (o[a] = t.defaultProps[a]);
    return Tg(t, o, i, r, null);
  }
  function Tg(t, e, n, i, r) {
    var a = {
      type: t,
      props: e,
      key: n,
      ref: i,
      __k: null,
      __: null,
      __b: 0,
      __e: null,
      __d: void 0,
      __c: null,
      __h: null,
      constructor: void 0,
      __v: null == r ? ++kg : r,
    };
    return null == r && null != wg.vnode && wg.vnode(a), a;
  }
  function jg(t) {
    return t.children;
  }
  function Pg(t, e) {
    (this.props = t), (this.context = e);
  }
  function Bg(t, e) {
    if (null == e) return t.__ ? Bg(t.__, t.__.__k.indexOf(t) + 1) : null;
    for (var n; e < t.__k.length; e++)
      if (null != (n = t.__k[e]) && null != n.__e) return n.__e;
    return "function" == typeof t.type ? Bg(t) : null;
  }
  function Dg(t) {
    var e, n;
    if (null != (t = t.__) && null != t.__c) {
      for (t.__e = t.__c.base = null, e = 0; e < t.__k.length; e++)
        if (null != (n = t.__k[e]) && null != n.__e) {
          t.__e = t.__c.base = n.__e;
          break;
        }
      return Dg(t);
    }
  }
  function Fg(t) {
    ((!t.__d && (t.__d = !0) && Mg.push(t) && !Lg.__r++) ||
      Sg !== wg.debounceRendering) &&
      ((Sg = wg.debounceRendering) || Rg)(Lg);
  }
  function Lg() {
    for (var t; (Lg.__r = Mg.length); )
      (t = Mg.sort(function (t, e) {
        return t.__v.__b - e.__v.__b;
      })),
        (Mg = []),
        t.some(function (t) {
          var e, n, i, r, a, o;
          t.__d &&
            ((a = (r = (e = t).__v).__e),
            (o = e.__P) &&
              ((n = []),
              ((i = Ag({}, r)).__v = r.__v + 1),
              Yg(
                o,
                r,
                i,
                e.__n,
                void 0 !== o.ownerSVGElement,
                null != r.__h ? [a] : null,
                n,
                null == a ? Bg(r) : a,
                r.__h
              ),
              Xg(n, r),
              r.__e != a && Dg(r)));
        });
  }
  function Ig(t, e, n, i, r, a, o, s, c, u) {
    var l,
      h,
      f,
      d,
      g,
      p,
      y,
      v = (i && i.__k) || zg,
      m = v.length;
    for (n.__k = [], l = 0; l < e.length; l++)
      if (
        null !=
        (d = n.__k[l] =
          null == (d = e[l]) || "boolean" == typeof d
            ? null
            : "string" == typeof d ||
              "number" == typeof d ||
              "bigint" == typeof d
            ? Tg(null, d, null, null, d)
            : Array.isArray(d)
            ? Tg(jg, { children: d }, null, null, null)
            : d.__b > 0
            ? Tg(d.type, d.props, d.key, null, d.__v)
            : d)
      ) {
        if (
          ((d.__ = n),
          (d.__b = n.__b + 1),
          null === (f = v[l]) || (f && d.key == f.key && d.type === f.type))
        )
          v[l] = void 0;
        else
          for (h = 0; h < m; h++) {
            if ((f = v[h]) && d.key == f.key && d.type === f.type) {
              v[h] = void 0;
              break;
            }
            f = null;
          }
        Yg(t, d, (f = f || Ng), r, a, o, s, c, u),
          (g = d.__e),
          (h = d.ref) &&
            f.ref != h &&
            (y || (y = []),
            f.ref && y.push(f.ref, null, d),
            y.push(h, d.__c || g, d)),
          null != g
            ? (null == p && (p = g),
              "function" == typeof d.type && d.__k === f.__k
                ? (d.__d = c = Wg(d, c, t))
                : (c = Vg(t, d, f, v, g, c)),
              "function" == typeof n.type && (n.__d = c))
            : c && f.__e == c && c.parentNode != t && (c = Bg(f));
      }
    for (n.__e = p, l = m; l--; )
      null != v[l] &&
        ("function" == typeof n.type &&
          null != v[l].__e &&
          v[l].__e == n.__d &&
          (n.__d = Bg(i, l + 1)),
        Jg(v[l], v[l]));
    if (y) for (l = 0; l < y.length; l++) Zg(y[l], y[++l], y[++l]);
  }
  function Wg(t, e, n) {
    for (var i, r = t.__k, a = 0; r && a < r.length; a++)
      (i = r[a]) &&
        ((i.__ = t),
        (e =
          "function" == typeof i.type
            ? Wg(i, e, n)
            : Vg(n, i, i, r, i.__e, e)));
    return e;
  }
  function Vg(t, e, n, i, r, a) {
    var o, s, c;
    if (void 0 !== e.__d) (o = e.__d), (e.__d = void 0);
    else if (null == n || r != a || null == r.parentNode)
      t: if (null == a || a.parentNode !== t) t.appendChild(r), (o = null);
      else {
        for (s = a, c = 0; (s = s.nextSibling) && c < i.length; c += 2)
          if (s == r) break t;
        t.insertBefore(r, a), (o = a);
      }
    return void 0 !== o ? o : r.nextSibling;
  }
  function $g(t, e, n) {
    "-" === e[0]
      ? t.setProperty(e, n)
      : (t[e] =
          null == n ? "" : "number" != typeof n || Cg.test(e) ? n : n + "px");
  }
  function Hg(t, e, n, i, r) {
    var a;
    t: if ("style" === e)
      if ("string" == typeof n) t.style.cssText = n;
      else {
        if (("string" == typeof i && (t.style.cssText = i = ""), i))
          for (e in i) (n && e in n) || $g(t.style, e, "");
        if (n) for (e in n) (i && n[e] === i[e]) || $g(t.style, e, n[e]);
      }
    else if ("o" === e[0] && "n" === e[1])
      (a = e !== (e = e.replace(/Capture$/, ""))),
        (e = e.toLowerCase() in t ? e.toLowerCase().slice(2) : e.slice(2)),
        t.l || (t.l = {}),
        (t.l[e + a] = n),
        n
          ? i || t.addEventListener(e, a ? Ug : qg, a)
          : t.removeEventListener(e, a ? Ug : qg, a);
    else if ("dangerouslySetInnerHTML" !== e) {
      if (r) e = e.replace(/xlink[H:h]/, "h").replace(/sName$/, "s");
      else if (
        "href" !== e &&
        "list" !== e &&
        "form" !== e &&
        "tabIndex" !== e &&
        "download" !== e &&
        e in t
      )
        try {
          t[e] = null == n ? "" : n;
          break t;
        } catch (t) {}
      "function" == typeof n ||
        (null != n && (!1 !== n || ("a" === e[0] && "r" === e[1]))
          ? t.setAttribute(e, n)
          : t.removeAttribute(e));
    }
  }
  function qg(t) {
    this.l[t.type + !1](wg.event ? wg.event(t) : t);
  }
  function Ug(t) {
    this.l[t.type + !0](wg.event ? wg.event(t) : t);
  }
  function Yg(t, e, n, i, r, a, o, s, c) {
    var u,
      l,
      h,
      f,
      d,
      g,
      p,
      y,
      v,
      m,
      x,
      b = e.type;
    if (void 0 !== e.constructor) return null;
    null != n.__h &&
      ((c = n.__h), (s = e.__e = n.__e), (e.__h = null), (a = [s])),
      (u = wg.__b) && u(e);
    try {
      t: if ("function" == typeof b) {
        if (
          ((y = e.props),
          (v = (u = b.contextType) && i[u.__c]),
          (m = u ? (v ? v.props.value : u.__) : i),
          n.__c
            ? (p = (l = e.__c = n.__c).__ = l.__E)
            : ("prototype" in b && b.prototype.render
                ? (e.__c = l = new b(y, m))
                : ((e.__c = l = new Pg(y, m)),
                  (l.constructor = b),
                  (l.render = Kg)),
              v && v.sub(l),
              (l.props = y),
              l.state || (l.state = {}),
              (l.context = m),
              (l.__n = i),
              (h = l.__d = !0),
              (l.__h = [])),
          null == l.__s && (l.__s = l.state),
          null != b.getDerivedStateFromProps &&
            (l.__s == l.state && (l.__s = Ag({}, l.__s)),
            Ag(l.__s, b.getDerivedStateFromProps(y, l.__s))),
          (f = l.props),
          (d = l.state),
          h)
        )
          null == b.getDerivedStateFromProps &&
            null != l.componentWillMount &&
            l.componentWillMount(),
            null != l.componentDidMount && l.__h.push(l.componentDidMount);
        else {
          if (
            (null == b.getDerivedStateFromProps &&
              y !== f &&
              null != l.componentWillReceiveProps &&
              l.componentWillReceiveProps(y, m),
            (!l.__e &&
              null != l.shouldComponentUpdate &&
              !1 === l.shouldComponentUpdate(y, l.__s, m)) ||
              e.__v === n.__v)
          ) {
            (l.props = y),
              (l.state = l.__s),
              e.__v !== n.__v && (l.__d = !1),
              (l.__v = e),
              (e.__e = n.__e),
              (e.__k = n.__k),
              e.__k.forEach(function (t) {
                t && (t.__ = e);
              }),
              l.__h.length && o.push(l);
            break t;
          }
          null != l.componentWillUpdate && l.componentWillUpdate(y, l.__s, m),
            null != l.componentDidUpdate &&
              l.__h.push(function () {
                l.componentDidUpdate(f, d, g);
              });
        }
        (l.context = m),
          (l.props = y),
          (l.state = l.__s),
          (u = wg.__r) && u(e),
          (l.__d = !1),
          (l.__v = e),
          (l.__P = t),
          (u = l.render(l.props, l.state, l.context)),
          (l.state = l.__s),
          null != l.getChildContext && (i = Ag(Ag({}, i), l.getChildContext())),
          h ||
            null == l.getSnapshotBeforeUpdate ||
            (g = l.getSnapshotBeforeUpdate(f, d)),
          (x =
            null != u && u.type === jg && null == u.key ? u.props.children : u),
          Ig(t, Array.isArray(x) ? x : [x], e, n, i, r, a, o, s, c),
          (l.base = e.__e),
          (e.__h = null),
          l.__h.length && o.push(l),
          p && (l.__E = l.__ = null),
          (l.__e = !1);
      } else null == a && e.__v === n.__v ? ((e.__k = n.__k), (e.__e = n.__e)) : (e.__e = Gg(n.__e, e, n, i, r, a, o, c));
      (u = wg.diffed) && u(e);
    } catch (t) {
      (e.__v = null),
        (c || null != a) &&
          ((e.__e = s), (e.__h = !!c), (a[a.indexOf(s)] = null)),
        wg.__e(t, e, n);
    }
  }
  function Xg(t, e) {
    wg.__c && wg.__c(e, t),
      t.some(function (e) {
        try {
          (t = e.__h),
            (e.__h = []),
            t.some(function (t) {
              t.call(e);
            });
        } catch (t) {
          wg.__e(t, e.__v);
        }
      });
  }
  function Gg(t, e, n, i, r, a, o, s) {
    var c,
      u,
      l,
      h = n.props,
      f = e.props,
      d = e.type,
      g = 0;
    if (("svg" === d && (r = !0), null != a))
      for (; g < a.length; g++)
        if (
          (c = a[g]) &&
          "setAttribute" in c == !!d &&
          (d ? c.localName === d : 3 === c.nodeType)
        ) {
          (t = c), (a[g] = null);
          break;
        }
    if (null == t) {
      if (null === d) return document.createTextNode(f);
      (t = r
        ? document.createElementNS("http://www.w3.org/2000/svg", d)
        : document.createElement(d, f.is && f)),
        (a = null),
        (s = !1);
    }
    if (null === d) h === f || (s && t.data === f) || (t.data = f);
    else {
      if (
        ((a = a && _g.call(t.childNodes)),
        (u = (h = n.props || Ng).dangerouslySetInnerHTML),
        (l = f.dangerouslySetInnerHTML),
        !s)
      ) {
        if (null != a)
          for (h = {}, g = 0; g < t.attributes.length; g++)
            h[t.attributes[g].name] = t.attributes[g].value;
        (l || u) &&
          ((l && ((u && l.__html == u.__html) || l.__html === t.innerHTML)) ||
            (t.innerHTML = (l && l.__html) || ""));
      }
      if (
        ((function (t, e, n, i, r) {
          var a;
          for (a in n)
            "children" === a ||
              "key" === a ||
              a in e ||
              Hg(t, a, null, n[a], i);
          for (a in e)
            (r && "function" != typeof e[a]) ||
              "children" === a ||
              "key" === a ||
              "value" === a ||
              "checked" === a ||
              n[a] === e[a] ||
              Hg(t, a, e[a], n[a], i);
        })(t, f, h, r, s),
        l)
      )
        e.__k = [];
      else if (
        ((g = e.props.children),
        Ig(
          t,
          Array.isArray(g) ? g : [g],
          e,
          n,
          i,
          r && "foreignObject" !== d,
          a,
          o,
          a ? a[0] : n.__k && Bg(n, 0),
          s
        ),
        null != a)
      )
        for (g = a.length; g--; ) null != a[g] && Eg(a[g]);
      s ||
        ("value" in f &&
          void 0 !== (g = f.value) &&
          (g !== h.value || g !== t.value || ("progress" === d && !g)) &&
          Hg(t, "value", g, h.value, !1),
        "checked" in f &&
          void 0 !== (g = f.checked) &&
          g !== t.checked &&
          Hg(t, "checked", g, h.checked, !1));
    }
    return t;
  }
  function Zg(t, e, n) {
    try {
      "function" == typeof t ? t(e) : (t.current = e);
    } catch (t) {
      wg.__e(t, n);
    }
  }
  function Jg(t, e, n) {
    var i, r;
    if (
      (wg.unmount && wg.unmount(t),
      (i = t.ref) && ((i.current && i.current !== t.__e) || Zg(i, null, e)),
      null != (i = t.__c))
    ) {
      if (i.componentWillUnmount)
        try {
          i.componentWillUnmount();
        } catch (t) {
          wg.__e(t, e);
        }
      i.base = i.__P = null;
    }
    if ((i = t.__k))
      for (r = 0; r < i.length; r++)
        i[r] && Jg(i[r], e, "function" != typeof t.type);
    n || null == t.__e || Eg(t.__e), (t.__e = t.__d = void 0);
  }
  function Kg(t, e, n) {
    return this.constructor(t, n);
  }
  function Qg(t, e, n) {
    var i, r, a;
    wg.__ && wg.__(t, e),
      (r = (i = "function" == typeof n) ? null : (n && n.__k) || e.__k),
      (a = []),
      Yg(
        e,
        (t = ((!i && n) || e).__k = Og(jg, null, [t])),
        r || Ng,
        Ng,
        void 0 !== e.ownerSVGElement,
        !i && n ? [n] : r ? null : e.firstChild ? _g.call(e.childNodes) : null,
        a,
        !i && n ? n : r ? r.__e : e.firstChild,
        i
      ),
      Xg(a, t);
  }
  function tp() {
    var t,
      e,
      n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      i = n.createElement,
      r = void 0 === i ? document.createElement.bind(document) : i,
      a = Bd(),
      o = { transform: void 0 },
      s = Dd();
    return (
      (s.element = function () {
        return t;
      }),
      (s.root = function () {
        return t;
      }),
      (s.settings = function (t) {
        return (
          t &&
            Object.keys(o).forEach(function (e) {
              void 0 !== t[e] && (o[e] = t[e]);
            }),
          o
        );
      }),
      (s.appendTo = function (e) {
        return (
          t ||
            (((t = r("div")).style.position = "absolute"),
            (t.style["-webkit-font-smoothing"] = "antialiased"),
            (t.style["-moz-osx-font-smoothing"] = "antialiased"),
            (t.style.pointerEvents = "none")),
          e.appendChild(t),
          t
        );
      }),
      (s.render = function (n) {
        if (!t) return !1;
        var i,
          r = "function" == typeof o.transform && o.transform();
        if (r) {
          var s = r.a,
            c = r.b,
            u = r.c,
            l = r.d,
            h = r.e,
            f = r.f;
          return (
            (t.style.transform = "matrix("
              .concat(s, ", ")
              .concat(c, ", ")
              .concat(u, ", ")
              .concat(l, ", ")
              .concat(h, ", ")
              .concat(f, ")")),
            !0
          );
        }
        return (
          (t.style.transform = ""),
          (t.style.left = "".concat(a.computedPhysical.x, "px")),
          (t.style.top = "".concat(a.computedPhysical.y, "px")),
          (t.style.width = "".concat(a.computedPhysical.width, "px")),
          (t.style.height = "".concat(a.computedPhysical.height, "px")),
          (i = Array.isArray(n) ? Og("div", null, n) : n),
          (e = Qg(i, t, e)),
          !0
        );
      }),
      (s.renderArgs = [Og]),
      (s.clear = function () {
        if (t) {
          for (var n = t.firstChild; n; ) t.removeChild(n), (n = t.firstChild);
          e = null;
        }
        return s;
      }),
      (s.destroy = function () {
        t && t.parentElement && t.parentElement.removeChild(t),
          (t = null),
          (e = null);
      }),
      (s.size = function (t) {
        return t && (a = Bd(t)), a;
      }),
      s
    );
  }
  (_g = zg.slice),
    (wg = {
      __e: function (t, e) {
        for (var n, i, r; (e = e.__); )
          if ((n = e.__c) && !n.__)
            try {
              if (
                ((i = n.constructor) &&
                  null != i.getDerivedStateFromError &&
                  (n.setState(i.getDerivedStateFromError(t)), (r = n.__d)),
                null != n.componentDidCatch &&
                  (n.componentDidCatch(t), (r = n.__d)),
                r)
              )
                return (n.__E = n);
            } catch (e) {
              t = e;
            }
        throw t;
      },
    }),
    (kg = 0),
    (Pg.prototype.setState = function (t, e) {
      var n;
      (n =
        null != this.__s && this.__s !== this.state
          ? this.__s
          : (this.__s = Ag({}, this.state))),
        "function" == typeof t && (t = t(Ag({}, n), this.props)),
        t && Ag(n, t),
        null != t && this.__v && (e && this.__h.push(e), Fg(this));
    }),
    (Pg.prototype.forceUpdate = function (t) {
      this.__v && ((this.__e = !0), t && this.__h.push(t), Fg(this));
    }),
    (Pg.prototype.render = jg),
    (Mg = []),
    (Rg =
      "function" == typeof Promise
        ? Promise.prototype.then.bind(Promise.resolve())
        : setTimeout),
    (Lg.__r = 0);
  var ep = [
      function (t) {
        t.renderer("svg", bg);
      },
      function (t) {
        t.renderer("canvas", Gd);
      },
      function (t) {
        t.renderer("dom", tp);
      },
    ],
    np = { OFF: 0, ERROR: 1, WARN: 2, INFO: 3, DEBUG: 4 },
    ip = function () {
      var t,
        e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        n = e.level,
        i = void 0 === n ? np.OFF : n,
        r = e.pipe,
        a = void 0 === r ? console : r,
        o = i,
        s =
          (E((t = {}), np.OFF, function () {}),
          E(t, np.ERROR, function () {
            return a.error.apply(a, arguments);
          }),
          E(t, np.WARN, function () {
            return a.warn.apply(a, arguments);
          }),
          E(t, np.INFO, function () {
            return a.info.apply(a, arguments);
          }),
          E(t, np.DEBUG, function () {
            return a.log.apply(a, arguments);
          }),
          t),
        c = function (t) {
          if (t && !(o < t)) {
            for (
              var e = arguments.length, n = new Array(e > 1 ? e - 1 : 0), i = 1;
              i < e;
              i++
            )
              n[i - 1] = arguments[i];
            (s[t] || s[np.DEBUG]).apply(void 0, n);
          }
        };
      return {
        log: c,
        error: function () {
          for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
            e[n] = arguments[n];
          return c.apply(void 0, [np.ERROR].concat(e));
        },
        warn: function () {
          for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
            e[n] = arguments[n];
          return c.apply(void 0, [np.WARN].concat(e));
        },
        info: function () {
          for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
            e[n] = arguments[n];
          return c.apply(void 0, [np.INFO].concat(e));
        },
        debug: function () {
          for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++)
            e[n] = arguments[n];
          return c.apply(void 0, [np.DEBUG].concat(e));
        },
        level: function (t) {
          return "number" == typeof t && (o = t), o;
        },
        LOG_LEVEL: np,
      };
    };
  function rp(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      n = arguments.length > 2 ? arguments[2] : void 0;
    t(n, e);
  }
  var ap = (function t() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
      n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
      i = ip(e.logger),
      r = {
        component: oe(n.component, "component", i),
        data: oe(n.data, "data", i),
        formatter: oe(n.formatter, "formatter", i),
        interaction: oe(n.interaction, "interaction", i),
        renderer: Ks(n.renderer),
        scale: oe(n.scale, "scale", i),
        symbol: oe(n.symbol, "symbol", i),
        logger: i,
      };
    function a() {
      var n =
          arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        i = {
          palettes: e.palettes.concat(n.palettes || []),
          style: k({}, e.style, n.style),
          logger: n.logger || e.logger,
          renderer: n.renderer || e.renderer,
        };
      return t(i, r);
    }
    return (
      e.renderer && e.renderer.prio && r.renderer.default(e.renderer.prio[0]),
      (a.use = function (t) {
        var e =
          arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
        return rp(t, e, r);
      }),
      (a.chart = function (t) {
        return Js(t, {
          registries: r,
          logger: i,
          style: e.style,
          palettes: e.palettes,
        });
      }),
      (a.config = function () {
        return e;
      }),
      Object.keys(r).forEach(function (t) {
        a[t] = r[t];
      }),
      (a.version = M),
      a
    );
  })(
    {
      renderer: { prio: ["svg", "canvas"] },
      logger: { level: 0 },
      style: {
        "$font-family": "'Source Sans Pro', Arial, sans-serif",
        "$font-size": "12px",
        "$line-height": "16px",
        "$font-size--l": "16px",
        "$gray-100": "#ffffff",
        "$gray-98": "#f9f9f9",
        "$gray-95": "#f2f2f2",
        "$gray-90": "#e6e6e6",
        "$gray-35": "#595959",
        "$gray-30": "#4d4d4d",
        "$gray-25": "#404040",
        "$gray-20": "#333333",
        "$border-95": "rgba(255, 255, 255, 0.05)",
        "$border-90": "rgba(255, 255, 255, 0.1)",
        "$border-80": "rgba(255, 255, 255, 0.2)",
        "$border-20": "rgba(0, 0, 0, 0.2)",
        "$border-10": "rgba(0, 0, 0, 0.1)",
        "$border-5": "rgba(0, 0, 0, 0.05)",
        "$primary-blue": "#3F8AB3",
        "$primary-green": "#6CB33F",
        "$primary-red": "#DC423F",
        "$primary-orange": "#EF960F",
        "$spacing--s": 4,
        $spacing: 8,
        "$spacing--l": 12,
        "$font-color": "$gray-35",
        "$font-color--inverted": "$gray-90",
        "$guide-color": "$gray-90",
        "$guide-color--inverted": "$gray-35",
        $border: "$border-80",
        "$border--inverted": "$border-10",
        $shape: { fill: "$primary-blue", strokeWidth: 1, stroke: "$border" },
        "$shape-outline": { stroke: "$primary-blue", strokeWidth: 2 },
        "$shape-guide": { stroke: "$guide-color", strokeWidth: 1 },
        "$shape-guide--inverted": {
          "@extend": "$shape-guide",
          stroke: "$guide-color--inverted",
        },
        $label: {
          fontSize: "$font-size",
          fontFamily: "$font-family",
          fill: "$font-color",
        },
        "$label--inverted": {
          $extend: "$label",
          fill: "$font-color--inverted",
        },
        "$label-overlay": {
          fontSize: "$font-size--l",
          fontFamily: "$font-family",
          fill: "$gray-100",
          color: "$font-color",
          stroke: "$guide-color--inverted",
          strokeWidth: 1,
          borderRadius: 4,
        },
        $title: { "@extend": "$label", fontSize: "$font-size--l" },
        "$guide-line": { strokeWidth: 1, stroke: "$guide-color" },
        "$guide-line--minor": { strokeWidth: 1, stroke: "$gray-95" },
        "$padding--s": {
          left: "$spacing--s",
          right: "$spacing--s",
          top: "$spacing--s",
          bottom: "$spacing--s",
        },
        $padding: {
          left: "$spacing",
          right: "$spacing",
          top: "$spacing",
          bottom: "$spacing",
        },
        "$selection-area-target": {
          fill: "$primary-green",
          strokeWidth: 0,
          opacity: 0.2,
        },
      },
      palettes: [
        {
          key: "categorical",
          colors: [
            [
              "#a54343",
              "#d76c6c",
              "#ec983d",
              "#ecc43d",
              "#f9ec86",
              "#cbe989",
              "#70ba6e",
              "#578b60",
              "#79d69f",
              "#26a0a7",
              "#138185",
              "#65d3da",
            ],
          ],
        },
        {
          key: "diverging",
          colors: [
            [
              "#3d52a1",
              "#3a89c9",
              "#77b7e5",
              "#b4ddf7",
              "#e6f5fe",
              "#ffe3aa",
              "#f9bd7e",
              "#ed875e",
              "#d24d3e",
              "#ae1c3e",
            ],
          ],
        },
        {
          key: "sequential",
          colors: [["rgb(180,221,212)", "rgb(34, 83, 90)"]],
        },
      ],
    },
    {
      component: Qs,
      data: gc,
      formatter: hi,
      interaction: pc,
      renderer: Ks(),
      scale: za,
      symbol: Io,
    }
  );
  return pf.forEach(ap.use), ep.forEach(ap.use), [].forEach(ap.use), ap;
});
//# sourceMappingURL=/sm/5ffa326a027cb9fe8999716728a8dbe603a101b3b063c2097a80c94fcf609642.map
